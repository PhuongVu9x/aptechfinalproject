use TestJava
go
create proc Sp_Airport_ChangeStatus
@_ap_id int,
@_ap_status bit,
@_ap_updated_date datetime
as
begin
	update Airport
	set _ap_status = @_ap_status,
		_ap_updated_date = @_ap_updated_date
	where _ap_id = @_ap_id

	select * from dbo.Fun_Get_Airports()
	where _ap_id = @_ap_id
end