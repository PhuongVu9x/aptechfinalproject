use TestJava
go
alter proc Sp_Admin_ChangePassword
@ad_id int,
@ad_password varchar(1000)
as
begin
	update [Admin]
	set _ad_password = @ad_password
	where _ad_id = @ad_id

	select * from dbo.Fun_Get_Admins()
	where _ad_id = @ad_id and _ad_password = @ad_password
end