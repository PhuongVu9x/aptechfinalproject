use TestJava
create function Fun_Get_Positions() returns table 
as
	return (
		select * from Position
	)

go

create proc Sp_Position_Insert
@_pos_name nvarchar(100),
@_pos_icon varchar(1000),
@_pos_created_date datetime
as
begin
	Declare @tmp TABLE(
		_pos_id int
	)
	insert into Position(_pos_name,_pos_icon,_pos_created_date,_pos_updated_date,_pos_status)
	OUTPUT  inserted._pos_id INTO @tmp
	values (@_pos_name,@_pos_icon,@_pos_created_date,@_pos_created_date,1)

	declare @Id int = (select top 1 _pos_id from @tmp)
	select top 1 * from dbo.Fun_Get_Positions() where _pos_id = @Id
end

go
alter proc Sp_Position_Gets
@_pos_name nvarchar(500) = null
as
begin
	select * from dbo.Fun_Get_Positions()
	where 
		(dbo.IsNullOrEmpty(@_pos_name) = 1 
		or dbo.StringContain(_pos_name,@_pos_name) = 1
		or _pos_name like @_pos_name)
	order by _pos_updated_date desc
end
go
create proc Sp_Position_GetsActive
as
begin
	select * from dbo.Fun_Get_Positions()
	where _pos_status = 1
end
GO
create proc Sp_Position_UpdateIcon
@_pos_id int,
@_pos_icon varchar(1000)
as
begin
	update Position
	set _pos_icon = @_pos_icon
	where _pos_id = @_pos_id

	select * from dbo.Fun_Get_Positions()
	where _pos_id = @_pos_id
end

go
alter proc Sp_Position_Update
@_pos_id int,
@_pos_name varchar(1000),
@_pos_icon varchar(1000),
@_pos_updated_date datetime
as
begin
	update Position
	set _pos_name = @_pos_name,_pos_icon = @_pos_icon,
		_pos_updated_date = @_pos_updated_date
	where _pos_id = @_pos_id

	select * from dbo.Fun_Get_Positions()
	where _pos_id = @_pos_id
end

go
alter proc Sp_Position_ChangeStatus
@_pos_id int,
@_pos_status bit,
@_pos_updated_date datetime
as
begin
	update Position
	set _pos_status = @_pos_status,
		_pos_updated_date = @_pos_updated_date
	where _pos_id = @_pos_id

	select * from dbo.Fun_Get_Positions()
	where _pos_id = @_pos_id
end