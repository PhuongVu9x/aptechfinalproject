use TestJava
go
alter proc Sp_Admin_Update
@ad_id int,
@ad_first_name nvarchar(500),
@ad_last_name nvarchar(500),
@ad_full_name nvarchar(1000),
@ad_email varchar(500),
@ad_phone varchar(50),
@ad_avatar varchar(50),
@ad_title varchar(50),
@ad_position_id int ,
@ad_address nvarchar(1000),
@ad_updated_date datetime
as
begin
	update [Admin]
	set _ad_first_name = @ad_first_name, _ad_last_name = @ad_last_name,
		_ad_full_name = @ad_full_name, _ad_email = @ad_email,
		_ad_phone = @ad_phone, _ad_title = @ad_title,
		 _ad_position_id = @ad_position_id,_ad_address = @ad_address,
		 _ad_updated_date = @ad_updated_date
	where _ad_id = @ad_id

	select * from dbo.Fun_Get_Admins()
	where _ad_id = @ad_id
end