declare @index int = 0

while @index <= 10
	begin
		declare @cusId int = ((SELECT floor(rand() * (20 - 1)) + 1));
		Declare @tmp TABLE(
			id int
		);
		insert into Bill(_bil_cus_id) OUTPUT  inserted._bil_id INTO @tmp values (@cusId);
		declare @totalPas int = ((SELECT floor(rand() * (6 - 1)) + 1));
		declare @payment decimal = 0;

		declare @billId int = (select top 1 id from @tmp);
		
		declare @flightId int = ((SELECT floor(rand() * (29 - 15)) + 15));
		declare @defaultPayment decimal = ((SELECT floor(rand() * (5000000 - 500000)) + 500000));
		declare @countryId int = ((SELECT floor(rand() * (100 - 2)) + 2));
		declare @nationality nvarchar(max) = (select _ct_name from Country where _ct_id = @countryId)
		while @totalPas > 0
			begin
				declare @pasid int = ((SELECT floor(rand() * (3 - 1)) + 1));
				declare @percentPayment int = (select _pas_percent from Passenger where _pas_id = @pasid)
				declare @pasPayment decimal = @defaultPayment * @percentPayment / 100;
				declare @firstnamelengh int = ((SELECT floor(rand() * (5 - 2)) + 2));
				declare @firstname varchar(10) = (select dbo.GenerateToken(crypt_gen_random(@firstnamelengh)))
				declare @lastnamelengh int = ((SELECT floor(rand() * (10 - 2)) + 2));
				declare @lastname varchar(10) = (select dbo.GenerateToken(crypt_gen_random(@lastnamelengh)))
				declare @fullname varchar(20) = @firstname + ' ' + @lastname
				declare @emaillengh int = ((SELECT floor(rand() * (20 - 10)) + 10));
				declare @email varchar(30) = (select dbo.GenerateToken(crypt_gen_random(@emaillengh))) + '@gmail.com'
				declare @age int = ((SELECT floor(rand() * (40 - 18)) + 18));
				declare @dob datetime = getdate() - 365 * @age
				declare @passport varchar(10) = (select substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ',
		             (abs(checksum(newid())) % 18)+1, 1)) + (SELECT FORMAT((SELECT floor(rand() * (100000000 - 10000000)) + 10000000), '000000000'));
				declare @passportexpried datetime = getdate() + 365 * 5
				declare @returnid int = 1;
				declare @titleid int = ((SELECT floor(rand() * (4 - 1)) + 1));
				declare @title varchar(10)
				if(@titleid = 1)
					set @title = 'Mr'
				if(@titleid = 2)
					set @title = 'Mrs'
				if(@titleid = 3)
					set @title = 'Ms'
				declare @seat varchar(10) = (SELECT CONVERT(varchar(10),(SELECT floor(rand() * (20 - 1)) + 1))) + (select substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ',
		             (abs(checksum(newid())) % 18)+1, 1))
				declare @gate int = (SELECT floor(rand() * (12 - 1)) + 1);
				
				insert into Ticket(_tk_bill_id,_tk_flight_id,_tk_payment,_tk_first_name,_tk_last_name,_tk_full_name,
									_tk_dob,_tk_nationality,_tk_passport,_tk_country,_tk_passport_expried,_tk_return_type,
									_tk_title,_tk_seat,_tk_gate,_tk_pas_id)
				values(@billId,@flightId,@pasPayment,@firstname,@lastname,@fullname,
						@dob,@nationality,@passport,@nationality,@passportexpried,@returnid,
						@title,@seat,@gate,@pasid)

				set @payment = @payment + @pasPayment
				set @totalPas = @totalPas - 1
			end
		update Bill
		set _bil_payment = @payment where _bil_id = @billId

		delete from @tmp
		set @index = @index +1
	end
	


