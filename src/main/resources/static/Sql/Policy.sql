use TestJava
create function Fun_Get_Policies() returns table 
as
	return (
		select * from [Policy]
	)

go
create proc Sp_Policy_Insert
@_po_name nvarchar(100),
@_po_icon varchar(1000),
@_po_created_date datetime
as
begin
	Declare @tmp TABLE(
		_po_id int
	)
	insert into [Policy](_po_name,_po_icon,_po_created_date,_po_updated_date,_po_status)
	OUTPUT  inserted._po_id INTO @tmp
	values (@_po_name,@_po_icon,@_po_created_date,@_po_created_date,1)

	declare @Id int = (select top 1 _po_id from @tmp)
	select top 1 * from dbo.Fun_Get_Policies() where _po_id = @Id
end

go
alter proc Sp_Policy_Gets
@_po_name nvarchar(500) = null
as
begin
	select * from dbo.Fun_Get_Policies()
	where 
		(dbo.IsNullOrEmpty(@_po_name) = 1 
		or dob.StringContain(_po_name,@_po_name) = 1
		or _po_name like @_po_name)
	order by _po_updated_date desc
end

GO
create proc Sp_Policy_UpdateIcon
@_po_id int,
@_po_icon varchar(1000)
as
begin
	update [Policy]
	set _po_icon = @_po_icon
	where _po_id = @_po_id

	select * from dbo.Fun_Get_Policies()
	where _po_id = @_po_id
end

go
create proc Sp_Policy_Update
@_po_id int,
@_po_name varchar(1000),
@_po_updated_date datetime
as
begin
	update [Policy]
	set _po_name = @_po_name,
		_po_updated_date = @_po_updated_date
	where _po_id = @_po_id

	select * from dbo.Fun_Get_Policies()
	where _po_id = @_po_id
end

go
create proc Sp_Policy_ChangeStatus
@_po_id int,
@_po_status bit
as
begin
	update [Policy]
	set _po_status = @_po_status
	where _po_id = @_po_id

	select * from dbo.Fun_Get_Policies()
	where _po_id = @_po_id
end