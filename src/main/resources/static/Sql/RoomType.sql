
use TestJava
create function Fun_Get_RoomTypes() returns table 
as
	return (
		select * from RoomType
	)
	select * from roomtype
go
create proc Sp_RoomType_Insert
@_rt_name nvarchar(100),
@_rt_created_date datetime
as
begin
	Declare @tmp TABLE(
		_rt_id int
	)
	insert into RoomType(_rt_name,_rt_created_date,_rt_updated_date,_rt_status)
	OUTPUT  inserted._rt_id INTO @tmp
	values (@_rt_name,@_rt_created_date,@_rt_created_date,1)

	declare @Id int = (select top 1 _rt_id from @tmp)
	select top 1 * from dbo.Fun_Get_RoomTypes() where _rt_id = @Id
end

go
alter proc Sp_RoomType_Gets
@_rt_name nvarchar(500) = null
as
begin
	select * from dbo.Fun_Get_RoomTypes()
	where 
		(dbo.IsNullOrEmpty(@_rt_name) = 1 
		or dob.StringContain(_rt_name,@_rt_name) = 1
		or _rt_name like @_rt_name)
	order by _rt_updated_date desc
end

go
create proc Sp_RoomType_Update
@_rt_id int,
@_rt_name varchar(1000),
@_rt_updated_date datetime
as
begin
	update RoomType
	set _rt_name = @_rt_name,
		_rt_updated_date = @_rt_updated_date
	where _rt_id = @_rt_id

	select * from dbo.Fun_Get_RoomTypes()
	where _rt_id = @_rt_id
end

go
create proc Sp_RoomType_ChangeStatus
@_rt_id int,
@_rt_status bit
as
begin
	update RoomType
	set _rt_status = @_rt_status
	where _rt_id = @_rt_id

	select * from dbo.Fun_Get_RoomTypes()
	where _rt_id = @_rt_id
end
