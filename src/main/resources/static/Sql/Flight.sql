use TestJava
alter table flight
add _fl_ec_price float constraint default_fl_ec_price default 0
alter table flight
add _fl_sc_price float constraint default_fl_sc_price default 0
alter table flight
add _fl_bs_price float constraint default_fl_bs_price default 0
alter table flight
add _fl_fc_price float constraint default_fl_fc_price default 0
update flight 
set _fl_ec_price = 500000,_fl_sc_price = 700000,
_fl_bs_price = 1200000,_fl_fc_price = 1700000
select * from dbo.Fun_Get_Flights()
alter function Fun_Get_Flights() returns table 
as
	return (
		select _fl_id,_fl_from_id,c1._city_name as _fl_from_name,c1._ap_abbreviation as _fl_from_abbreviation, 
				_fl_to_id,c2._city_name as _fl_to_name,c2._ap_abbreviation as _fl_to_abbreviation,
				_fl_transit_id,ISNULL(c3._city_name,'') as _fl_trainsit_name,
				c1._ap_name as _fl_ap_from_name,c2._ap_name as _fl_ap_to_name,
				_fl_estimate_take_off,_fl_estimate_arrival,
				_fl_take_off,_fl_arrival,_fl_late_time,
				_fl_ec_price,_fl_sc_price,_fl_bs_price,_fl_fc_price,
				_car_id,_car_name,_car_icon,_car_cabin,_car_checked,
				_pl_id,_pl_code,_pt_name,_pt_total_seats,
				_pt_ec_quantity,_pt_sc_quantity,_pt_bs_quantity,_pt_fc_quantity,
				_fl_created_date,_fl_updated_date,_fl_status
		from [Flight] f
		join dbo.Fun_Get_Airports() c1 on (f._fl_from_id = c1._ap_id)
		join dbo.Fun_Get_Airports() c2 on (f._fl_to_id = c2._ap_id)
		left join dbo.Fun_Get_Airports() c3 on (f._fl_transit_id = c3._ap_id)
		join Plane p on (f._fl_plane_id = p._pl_id)
		join PlaneType pt on(p._pl_pt_id = pt._pt_id)
		join Carrier c on(p._pl_carrier_id = c._car_id)
	)
go 
select * from passenger
select * from carrier
alter proc Sp_Flight_Insert
@_fl_plane_id int,
@_fl_from_id int,
@_fl_to_id int,
@_fl_ec_price float,
@_fl_sc_price float,
@_fl_bs_price float,
@_fl_fc_price float,
@_fl_take_off datetime,
@_fl_arrival datetime,
@_fl_created_date datetime
as
begin
	Declare @tmp TABLE(
		_fl_id int
	)
	insert into [Flight](_fl_plane_id,_fl_from_id,_fl_to_id,
						_fl_ec_price,_fl_sc_price,_fl_bs_price,_fl_fc_price,
						_fl_estimate_take_off,_fl_estimate_arrival,
						_fl_take_off,_fl_arrival,
						_fl_created_date,_fl_updated_date,_fl_status)
	OUTPUT  inserted._fl_id INTO @tmp
	values (@_fl_plane_id,@_fl_from_id,@_fl_to_id,
			@_fl_ec_price,@_fl_sc_price,@_fl_bs_price,@_fl_fc_price,
			@_fl_take_off,@_fl_arrival,
			@_fl_take_off,@_fl_arrival,
			@_fl_created_date,@_fl_created_date,1)

	declare @Id int = (select top 1 _fl_id from @tmp)
	select top 1 * from dbo.Fun_Get_Flights() where _fl_id = @Id
end
select * from flight
go
exec Sp_Flight_Gets 0,0,0,'null'
alter proc Sp_Flight_Gets
@_fl_from_id int = 0,
@_fl_to_id int = 0,
@_car_id int = 0,
@_fl_take_off datetime = null,
@_tc_id int = 0,
@_passenger_quan int = 0
as
begin
	select * from dbo.Fun_Get_Flights()
    where [dbo].Get_Sold_Seat_By_TicketClass(_fl_id, @_tc_id, _pt_ec_quantity, _pt_sc_quantity, _pt_bs_quantity, _pt_fc_quantity) >= @_passenger_quan
    and	(@_fl_to_id = 0 or _fl_to_id = @_fl_to_id)
	and	(@_car_id = 0 or _car_id = @_car_id)
	and	(@_fl_take_off is null or _fl_take_off BETWEEN dbo.DefaultDatetime(@_fl_take_off) AND dbo.DefaultDatetime(@_fl_take_off + 1) )
	order by _fl_updated_date desc
end
declare @test datetime = '2023-05-03 18:32:00.000'
print 

alter function DefaultDatetime(@inputDate datetime) returns datetime as
begin
	declare @year varchar(5) = Year(@inputDate);
	declare @month varchar(5) = Month(@inputDate);
	declare @day varchar(5) = Day(@inputDate);
	declare @stringDate varchar(50) = @year + '-' + @month + '-' + @day + ' 00:00:00'
	declare @date datetime = convert(datetime, @stringDate, 102);
	return @date;
end

go
exec Sp_Flight_Info 20
alter proc  Sp_Flight_Info
@_fl_id int
as
begin
	select * from dbo.Fun_Get_Flights()
	where _fl_id = @_fl_id
end
select * from Flight
delete from Flight where _fl_id in (23,28)
delete from Ticket where _tk_flight_id in (23,28)
go
alter proc Sp_Flight_Update
@_fl_id int,
@_fl_plane_id int,
@_fl_from_id int,
@_fl_to_id int,
@_fl_ec_price float,
@_fl_sc_price float,
@_fl_bs_price float,
@_fl_fc_price float,
@_fl_take_off datetime,
@_fl_arrival datetime,
@_fl_updated_date datetime
as
begin
	update [Flight]
	set _fl_plane_id = @_fl_plane_id,_fl_from_id = @_fl_from_id,_fl_to_id = @_fl_to_id,
		_fl_ec_price = @_fl_ec_price,_fl_sc_price = @_fl_sc_price,
		_fl_bs_price = @_fl_bs_price,_fl_fc_price = @_fl_fc_price,
		_fl_take_off = @_fl_take_off,_fl_arrival = @_fl_arrival,
		_fl_updated_date = @_fl_updated_date
	where _fl_id = @_fl_id

	select * from dbo.Fun_Get_Flights()
	where _fl_id = @_fl_id
end

go
alter proc Sp_Flight_ChangeStatus
@_fl_id int,
@_fl_status bit,
@_fl_updated_date datetime
as
begin
	update [Flight]
	set _fl_status = @_fl_status,_fl_updated_date = @_fl_updated_date
	where _fl_id = @_fl_id

	select * from dbo.Fun_Get_Flights()
	where _fl_id = @_fl_id
end