use TestJava
go
create proc Sp_Carrier_Update
@_car_id int,
@_car_name varchar(1000),
@_car_icon varchar(1000),
@_car_city_id int,
@_car_updated_date datetime
as
begin
	update Carrier
	set _car_name = @_car_name,_car_icon = @_car_icon,
		_car_city_id = @_car_city_id,_car_updated_date = @_car_updated_date
	where _car_id = @_car_id

	select * from dbo.Fun_Get_Carriers()
	where _car_id = @_car_id
end
