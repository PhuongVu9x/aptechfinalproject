use TestJava
create function Fun_Get_Permisions() returns table 
as
	return (
		select * from Permision
	)

go
create proc Sp_Permision_Insert
@_per_name nvarchar(100),
@_per_icon varchar(1000),
@_per_created_date datetime
as
begin
	Declare @tmp TABLE(
		_per_id int
	)
	insert into Permision(_per_name,_per_icon,_per_created_date,_per_updated_date,_per_status)
	OUTPUT  inserted._per_id INTO @tmp
	values (@_per_name,@_per_icon,@_per_created_date,@_per_created_date,1)

	declare @Id int = (select top 1 _per_id from @tmp)
	select top 1 * from dbo.Fun_Get_Permisions() where _per_id = @Id
end

go
alter proc Sp_Permision_Gets
@_per_name nvarchar(500) = null
as
begin
	select * from dbo.Fun_Get_Permisions()
	where 
		(dbo.IsNullOrEmpty(@_per_name) = 1 
		or dbo.StringContain(_per_name,@_per_name) = 1
		or _per_name like @_per_name)
	order by _per_updated_date desc
end

go
create proc Sp_Permision_GetsActive
as
begin
	select * from dbo.Fun_Get_Permisions()
	where _per_status = 1
end

GO
create proc Sp_Permision_UpdateIcon
@_per_id int,
@_per_icon varchar(1000)
as
begin
	update Permision
	set _per_icon = @_per_icon
	where _per_id = @_per_id

	select * from dbo.Fun_Get_Permisions()
	where _per_id = @_per_id
end

go
alter proc Sp_Permision_Update
@_per_id int,
@_per_name varchar(1000),
@_per_icon varchar(1000),
@_per_updated_date datetime
as
begin
	update Permision
	set _per_name = @_per_name,
		_per_icon = @_per_icon,
		_per_updated_date = @_per_updated_date
	where _per_id = @_per_id

	select * from dbo.Fun_Get_Permisions()
	where _per_id = @_per_id
end

go
alter proc Sp_Permision_ChangeStatus
@_per_id int,
@_per_status bit,
@_per_updated_date datetime
as
begin
	update Permision
	set _per_status = @_per_status,
		_per_updated_date = @_per_updated_date
	where _per_id = @_per_id

	select * from dbo.Fun_Get_Permisions()
	where _per_id = @_per_id
end