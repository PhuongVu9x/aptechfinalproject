use TestJava
go
alter proc Sp_Category_ChangeStatus
@_cate_id int,
@_cate_status bit,
@_cate_updated_date datetime
as
begin
	update Category
	set _cate_status = @_cate_status,
		_cate_updated_date = @_cate_updated_date
	where _cate_id = @_cate_id

	select * from dbo.Fun_Get_Categories()
	where _cate_id = @_cate_id
end