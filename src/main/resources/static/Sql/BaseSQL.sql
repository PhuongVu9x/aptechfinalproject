
alter function GenerateToken(@randomBytes varbinary(max))
	returns varchar(max) as begin
	
	declare @allowedChars char(64);
	set @allowedChars = 'abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ';
	
	declare @oneByte tinyint;
	declare @oneChar char(1);
	declare @index int;
	declare @token varchar(max);
	
	set @index = 0;
	set @token = '';
	
	while @index < datalength(@randomBytes)
	begin
	    -- Get next byte, use it to index into @allowedChars, and append to @token.
	    -- Note: substring is 1-based.
	    set @index = @index + 1;
	    select @oneByte = convert(tinyint, substring(@randomBytes, @index, 1));
	    select @oneChar = substring(@allowedChars, 1 + (@oneByte % 32), 1); -- 32 is the number of @allowedChars
	    select @token = @token + @oneChar;
	end
	
	return @token;
end
-- Demo
select dbo.GenerateToken(crypt_gen_random(5))
-- END
create table Passenger(
	_pas_id int identity(1,1) constraint pk_pas_id primary key,
	_pas_title varchar(15) constraint default_pas_title default '',
	_pas_percent int constraint default_pas_percent default 20,
	_pas_created_date datetime constraint default_pas_created_date default getdate(),
	_pas_updated_date datetime constraint default_pas_updated_date default getdate(),
	_pas_status bit constraint default_pas_status default 1
)

create table BookingDetail(
	_db_id int identity(1,1) constraint pk_db_id primary key,
	_db_bil_id int constraint fk_db_bo_id references Booking(_bo_id),
	_db_ro_id int constraint fk_db_ro_id references Room(_ro_id),
	_db_payment decimal constraint default_bd_payment default 0,
	_db_created_date datetime constraint default_db_created_date default getdate(),
	_db_updated_date datetime constraint default_db_updated_date default getdate(),
	_db_status bit constraint default_db_status default 1
)

create function GeneratePhone(@randomBytes varbinary(max))
	returns varchar(max) as begin
	
	declare @allowedChars char(64);
	set @allowedChars = '1234567890';
	
	declare @oneByte tinyint;
	declare @oneChar char(1);
	declare @index int;
	declare @token varchar(max);
	
	set @index = 0;
	set @token = '';
	
	while @index < datalength(@randomBytes)
	begin
	    -- Get next byte, use it to index into @allowedChars, and append to @token.
	    -- Note: substring is 1-based.
	    set @index = @index + 1;
	    select @oneByte = convert(tinyint, substring(@randomBytes, @index, 1));
	    select @oneChar = substring(@allowedChars, 1 + (@oneByte % 32), 1); -- 32 is the number of @allowedChars
	    select @token = @token + @oneChar;
	end
	
	return @token;
end
-- Demo
select dbo.GeneratePhone(crypt_gen_random(10))
-- END