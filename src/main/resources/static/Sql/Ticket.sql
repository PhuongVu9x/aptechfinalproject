
create function Fun_GetTickets() returns table 
as
	return (
		select * from Ticket
	)

go
create proc Sp_Ticket_Insert
@_tk_bill_id int,
@_tk_flight_id int,
@_tk_payment decimal,
@_tk_first_name nvarchar(100),
@_tk_last_name nvarchar(100),
@_tk_full_name nvarchar(200),
@_tk_dob datetime,
@_tk_nationality nvarchar(400),
@_tk_passport nvarchar(400) ,
@_tk_country nvarchar(200) ,
@_tk_passport_expried datetime ,
@_tk_title varchar(10) ,
@_tk_return_type int ,
@_tk_created_date datetime
as
begin
	Declare @tmp TABLE(
		_tk_id int
	)
	insert into Ticket(_tk_bill_id,_tk_flight_id,_tk_payment,_tk_first_name,_tk_last_name,
						_tk_full_name,_tk_dob,_tk_nationality,_tk_passport,_tk_country,
						_tk_passport_expried,_tk_title,_tk_return_type,
						_tk_created_date,_tk_updated_date,_tk_status)
	OUTPUT  inserted._tk_id INTO @tmp
	values (@_tk_bill_id,@_tk_flight_id,@_tk_payment,@_tk_first_name,@_tk_last_name,
			@_tk_full_name,@_tk_dob,@_tk_nationality,@_tk_passport,@_tk_country,
			@_tk_passport_expried,@_tk_title,@_tk_return_type,
			@_tk_created_date,@_tk_created_date,1)

	declare @Id int = (select top 1 _tk_id from @tmp)
	select top 1 * from dbo.Fun_GetTickets() where _tk_id = @Id
end

go
create proc Sp_Ticket_ChangeStatus
@_tk_id int,
@_tk_status bit
as
begin
	Update Ticket
	set _tk_status = @_tk_status
	where _tk_id = @_tk_id

	select top 1 * from dbo.Fun_GetTickets() where _tk_id = @_tk_id
end


go
alter proc Sp_Ticket_Gets
@_tk_bill_id int = 0
as
begin
	select * 
	from dbo.Fun_GetTickets()
	where (@_tk_bill_id = 0 or _tk_bill_id = @_tk_bill_id)
	order by _tk_updated_date desc
end