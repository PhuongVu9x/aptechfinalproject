use TestJava
go
alter proc Sp_Admin_Login
@ad_email varchar(500),
@ad_password varchar(500),
@ad_token varchar(max)
as
begin
	update [Admin]
	set _ad_token = @ad_token
	where _ad_email like @ad_email and _ad_password like @ad_password

	select * from dbo.Fun_Get_Admins()
	where _ad_email like @ad_email 
	and _ad_password like @ad_password
end
