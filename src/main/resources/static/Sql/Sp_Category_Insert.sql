use TestJava
go
create proc Sp_Category_Insert
@_cate_name nvarchar(100),
@_cate_icon varchar(1000),
@_cate_created_date datetime
as
begin
	Declare @tmp TABLE(
		_cate_id int
	)
	insert into Category(_cate_name,_cate_icon,_cate_created_date,_cate_updated_date,_cate_status)
	OUTPUT  inserted._cate_id INTO @tmp
	values (@_cate_name,@_cate_icon,@_cate_created_date,@_cate_created_date,1)

	declare @Id int = (select top 1 _cate_id from @tmp)
	select top 1 * from dbo.Fun_Get_Categories() where _cate_id = @Id
end
