use TestJava
go
alter proc Sp_Admin_Logout
@ad_id int
as
begin
	update [Admin] 
	set _ad_token = ''
	where _ad_id = @ad_id

	select * from dbo.Fun_Get_Admins()
	where _ad_id = @ad_id
end