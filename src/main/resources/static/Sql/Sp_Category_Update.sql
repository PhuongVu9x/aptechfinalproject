use TestJava
go
create proc Sp_Category_Update
@_cate_id int,
@_cate_name varchar(1000),
@_cate_icon varchar(1000),
@_cate_updated_date datetime
as
begin
	update Category
	set _cate_name = @_cate_name,
		_cate_icon = @_cate_icon,
		_cate_updated_date = @_cate_updated_date
	where _cate_id = @_cate_id

	select * from dbo.Fun_Get_Categories()
	where _cate_id = @_cate_id
end