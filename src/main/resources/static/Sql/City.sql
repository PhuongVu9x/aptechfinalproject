use TestJava
alter table City
add _city_ct_id int constraint fk_city_ct_id references Country(_ct_id)
alter function Fun_Get_Cities() returns table 
as
	return (
		select * 
		from City c,Country ct
		where c._city_ct_id = _ct_id
	)

go
alter proc Sp_City_Insert
@_city_name nvarchar(100),
@_city_ct_id int,
@_city_created_date datetime
as
begin
	Declare @tmp TABLE(
		_city_id int
	)
	insert into City(_city_name,_city_ct_id,_city_created_date,_city_updated_date,_city_status)
	OUTPUT  inserted._city_id INTO @tmp
	values (@_city_name,@_city_ct_id,@_city_created_date,@_city_created_date,1)

	declare @Id int = (select top 1 _city_id from @tmp)
	select top 1 * from dbo.Fun_Get_Cities() where _city_id = @Id
end

go
alter proc Sp_City_Gets
@_city_name nvarchar(500) = null,
@_ct_name nvarchar(500) = null
as
begin
	select * from dbo.Fun_Get_Cities()
	where 
		(dbo.IsNullOrEmpty(@_city_name) = 1 
		or dbo.StringContain(_city_name,@_city_name) = 1
		or _city_name like @_city_name)
	and (dbo.IsNullOrEmpty(@_ct_name) = 1 
		or dbo.StringContain(_ct_name,@_ct_name) = 1
		or _ct_name like @_ct_name)
	order by _city_updated_date desc
end

GO
create proc Sp_City_UpdateIcon
@_city_id int,
@_city_icon varchar(1000)
as
begin
	update City
	set _city_icon = @_city_icon
	where _city_id = @_city_id

	select * from dbo.Fun_Get_Cities()
	where _city_id = @_city_id
end

go
alter proc Sp_City_Update
@_city_id int,
@_city_ct_id int,
@_city_name varchar(1000),
@_city_updated_date datetime
as
begin
	update City
	set _city_name = @_city_name, _city_ct_id = @_city_ct_id,
		_city_updated_date = @_city_updated_date
	where _city_id = @_city_id

	select * from dbo.Fun_Get_Cities()
	where _city_id = @_city_id
end

go
alter proc Sp_City_ChangeStatus
@_city_id int,
@_city_status bit,
@_city_updated_date datetime
as
begin
	update City
	set _city_status = @_city_status,
		_city_updated_date = @_city_updated_date
	where _city_id = @_city_id

	select * from dbo.Fun_Get_Cities()
	where _city_id = @_city_id
end

