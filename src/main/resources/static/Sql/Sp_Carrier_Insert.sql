use TestJava
go
alter proc Sp_Carrier_Insert
@_car_name nvarchar(100),
@_car_icon varchar(1000),
@_car_city_id int,
@_car_created_date datetime
as
begin
	Declare @tmp TABLE(
		_car_id int
	)
	insert into Carrier(_car_name,_car_icon,_car_city_id,_car_created_date,_car_updated_date,_car_status)
	OUTPUT  inserted._car_id INTO @tmp
	values (@_car_name,@_car_icon,@_car_city_id,@_car_created_date,@_car_created_date,1)

	declare @Id int = (select top 1 _car_id from @tmp)
	select top 1 * from dbo.Fun_Get_Carriers() where _car_id = @Id
end
