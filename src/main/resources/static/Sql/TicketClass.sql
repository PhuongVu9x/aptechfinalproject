use TestJava
create function Fun_Get_TicketClass() returns table 
as
	return (
		select * from TicketClass
	)

go
create proc Sp_TicketClass_Insert
@_tc_name nvarchar(100),
@_tc_icon varchar(1000),
@_tc_created_date datetime
as
begin
	Declare @tmp TABLE(
		_tc_id int
	)
	insert into TicketClass(_tc_name,_tc_icon,_tc_created_date,_tc_updated_date,_tc_status)
	OUTPUT  inserted._tc_id INTO @tmp
	values (@_tc_name,@_tc_icon,@_tc_created_date,@_tc_created_date,1)

	declare @Id int = (select top 1 _tc_id from @tmp)
	select top 1 * from dbo.Fun_Get_TicketClass() where _tc_id = @Id
end

go
alter proc Sp_TicketClass_Gets
@_tc_name nvarchar(500) = null
as
begin
	select * from dbo.Fun_Get_TicketClasss()
	where 
		(dbo.IsNullOrEmpty(@_tc_name) = 1 
		or dbo.StringContain(_tc_name,@_tc_name) = 1
		or _tc_name like @_tc_name)
	order by _tc_updated_date desc
end
select * from ticket
GO
create proc Sp_TicketClass_UpdateIcon
@_tc_id int,
@_tc_icon varchar(1000)
as
begin
	update TicketClass
	set _tc_icon = @_tc_icon
	where _tc_id = @_tc_id

	select * from dbo.Fun_Get_TicketClass()
	where _tc_id = @_tc_id
end

go
alter proc Sp_TicketClass_Update
@_tc_id int,
@_tc_name varchar(1000),
@_tc_icon varchar(1000),
@_tc_updated_date datetime
as
begin
	update TicketClass
	set _tc_name = @_tc_name,_tc_icon = @_tc_icon,
		_tc_updated_date = @_tc_updated_date
	where _tc_id = @_tc_id

	select * from dbo.Fun_Get_TicketClass()
	where _tc_id = @_tc_id
end

go
alter proc Sp_TicketClass_ChangeStatus
@_tc_id int,
@_tc_status bit,
@_tc_updated_date datetime
as
begin
	update TicketClass
	set _tc_status = @_tc_status,
		_tc_updated_date = @_tc_updated_date
	where _tc_id = @_tc_id

	select * from dbo.Fun_Get_TicketClass()
	where _tc_id = @_tc_id
end