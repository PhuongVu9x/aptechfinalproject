use TestJava
go
alter proc Sp_Airport_Update
@_ap_id int,
@_ap_name varchar(1000),
@_ap_city_id int,
@_ap_updated_date datetime
as
begin
	update Airport
	set _ap_name = @_ap_name,_ap_city_id = @_ap_city_id,
		_ap_updated_date = @_ap_updated_date
	where _ap_id = @_ap_id

	select * from dbo.Fun_Get_Airports()
	where _ap_id = @_ap_id
end