use TestJava
go
create proc Sp_Carrier_ChangeStatus
@_car_id int,
@_car_status bit,
@_car_updated_date datetime
as
begin
	update Carrier
	set _car_status = @_car_status,
		_car_updated_date = @_car_updated_date
	where _car_id = @_car_id

	select * from dbo.Fun_Get_Carriers()
	where _car_id = @_car_id
end