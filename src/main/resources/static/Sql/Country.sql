use TestJava
create function Fun_Get_Countries() returns table 
as
	return (
		select * from Country
	)

go
create proc Sp_Country_Insert
@_ct_name nvarchar(100),
@_ct_created_date datetime
as
begin
	Declare @tmp TABLE(
		_ct_id int
	)
	insert into Country(_ct_name,_ct_created_date,_ct_updated_date,_ct_status)
	OUTPUT  inserted._ct_id INTO @tmp
	values (@_ct_name,@_ct_created_date,@_ct_created_date,1)

	declare @Id int = (select top 1 _ct_id from @tmp)
	select top 1 * from dbo.Fun_Get_Countries() where _ct_id = @Id
end

go
alter proc Sp_Country_Gets
@_ct_name nvarchar(500) = null
as
begin
	select * from dbo.Fun_Get_Countries()
	where 
		(dbo.IsNullOrEmpty(@_ct_name) = 1 
		or dbo.StringContain(_ct_name,@_ct_name) = 1
		or _ct_name like @_ct_name)
	order by _ct_updated_date desc
end

go
alter proc Sp_Country_Update
@_ct_id int,
@_ct_name nvarchar(1000),
@_ct_updated_date datetime
as
begin
	update Country
	set _ct_name = @_ct_name,
		_ct_updated_date = @_ct_updated_date
	where _ct_id = @_ct_id

	select * from dbo.Fun_Get_Countries()
	where _ct_id = @_ct_id
end

go
alter proc Sp_Country_ChangeStatus
@_ct_id int,
@_ct_status bit,
@_ct_updated_date datetime
as
begin
	update Country
	set _ct_status = @_ct_status,
		_ct_updated_date = @_ct_updated_date
	where _ct_id = @_ct_id

	select * from dbo.Fun_Get_Countries()
	where _ct_id = @_ct_id
end


exec Sp_Plane_Gets