USE [TestJava]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Admin_UpdateAvatar]    Script Date: 8/29/2023 6:23:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_Category_UpdateIcon]
@_cate_id int,
@_cate_icon varchar(1000)
as
begin
	update Category
	set _cate_icon = @_cate_icon
	where _cate_id = @_cate_id

	select * from dbo.Fun_Get_Categorys()
	where _cate_id = @_cate_id
end