
declare @index int = 0

while @index <= 20
	begin
		declare @cusId int = ((SELECT floor(rand() * (20 - 1)) + 1));
		Declare @tmp TABLE(
			id int
		);

		declare @rand int = (select floor(rand() * 2 + 1))
		declare @year varchar(5) = (select convert(varchar(5),(SELECT floor(rand() * (2023 - 2019)) + 2021)));
		declare @month varchar(5) = (select convert(varchar(5), (SELECT floor(rand() * (12 - 1)) + 1)));
		declare @day varchar(5) = (select convert(varchar(5), (SELECT floor(rand() * (28 - 1)) + 1)));
		declare @hour varchar(5) = (select convert(varchar(5), (SELECT floor(rand() * (24 - 6)) + 6)));
		declare @min varchar(5) = (select convert(varchar(5), (SELECT floor(rand() * (60 - 1)) + 1)));
		declare @days int = (SELECT floor(rand() * (7 - 1)) + 1);
		declare @checkin datetime = (select convert(datetime, @year +'-'+@month +'-' + @day + ' ' +'14:00'));
		declare @checkout datetime = @checkin + @days
		declare @hoId int = (SELECT floor(rand() * (6 - 1)) + 1)
		insert into booking(_bo_cus_id,_bo_check_in,_bo_check_out) OUTPUT  inserted._bo_id INTO @tmp values (@cusId,@checkin,@checkin);
		declare @bookingId int = (select top 1 id from @tmp);
		declare @totalPayment decimal = 0;
		if(@hoId = 2)
			begin
				set @hoId = 7;
			end
		if(@hoId = 5)
			begin
				set @hoId = 11;
			end
		declare @singleRoomCount int = (SELECT floor(rand() * (4 - 1)) + 1)
		while @singleRoomCount > 0
			begin
				declare @roomTypeIdSignle int = ((SELECT floor(rand() * (3 - 1)) + 1));
				declare @roomId int  = ((SELECT floor(rand() * (162 - 1)) + 1));
				while @roomId not in (select _ro_id
										from Room r
										join Hotel h on(r._ro_ho_id = h._ho_id)
										where h._ho_id = @hoId and r._ro_room_type_id = @roomTypeIdSignle)
					begin
						set @roomId  = ((SELECT floor(rand() * (162 - 1)) + 1));
					end
				declare @roomPrice decimal = (select _ro_price from Room where _ro_id = @roomId)
				set @totalPayment = @totalPayment +@roomPrice;
				insert into BookingDetail(_db_bo_id,_db_ro_id,_db_payment)
				values (@bookingId,@roomId,@roomPrice)
				set @singleRoomCount = @singleRoomCount -1
			end
		update booking
		set _bo_payment = @totalPayment where _bo_id = @bookingId
		delete from @tmp
	
		set @index = @index +1
	end


