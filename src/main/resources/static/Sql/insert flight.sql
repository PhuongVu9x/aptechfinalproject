
declare @index int = 0
while @index < 15
	begin
		declare @from_id int = (SELECT floor(rand() * (300 - 201)) + 201);
		declare @to_id int = (SELECT floor(rand() * (300 - 1)) + 1);
		declare @plane_id int = (SELECT floor(rand() * (100 - 1)) + 1);
		declare @latetime int = 0
		
        declare @rand int = (select floor(rand() * 2 + 1))
		declare @year varchar(5) = (select convert(varchar(5),(SELECT floor(rand() * (2023 - 2019)) + 2021)));
		declare @month varchar(5) = (select convert(varchar(5), (SELECT floor(rand() * (12 - 1)) + 1)));
		declare @day varchar(5) = (select convert(varchar(5), (SELECT floor(rand() * (28 - 1)) + 1)));
		declare @hour varchar(5) = (select convert(varchar(5), (SELECT floor(rand() * (24 - 6)) + 6)));
		declare @min varchar(5) = (select convert(varchar(5), (SELECT floor(rand() * (60 - 1)) + 1)));
		declare @arrivalTime int = (SELECT floor(rand() * (1200 - 120)) + 120);
		declare @estimatetakeoff datetime = (select convert(datetime, @year +'-'+@month +'-' + @day + ' ' + @hour +':' +@min));
		declare @estimatearrival datetime = @estimatetakeoff + 0.00069444 * @arrivalTime
		declare @takeoff datetime = @estimatetakeoff
		declare @arrival datetime = @estimatearrival
		if(@rand = 1)
			begin
				set @latetime = (select floor(rand() * 60 + 1))
			end
		declare @code varchar(10) = (select left(NEWID(),5))

		insert into Flight(_fl_from_id,_fl_to_id,_fl_plane_id,_fl_estimate_take_off,_fl_estimate_arrival,
							_fl_take_off,_fl_arrival,_fl_late_time,_fl_created_date,_fl_updated_date,_fl_code)
		values (@from_id,@to_id,@plane_id,@estimatetakeoff,@estimatearrival,
				@takeoff,@arrival,@latetime,@takeoff,@arrival,@code)
		set @index = @index+1
	end

	