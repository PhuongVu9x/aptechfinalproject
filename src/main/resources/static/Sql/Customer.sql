use TestJava
go
create function Fun_Get_Customers() returns table
as
	return (
		select * from Customer 
	)
go
create proc Sp_Customer_Insert
@_cus_email varchar(max),
@_cus_password varchar(max),
@_cus_created_date datetime
as
begin
	if(not exists (select _cus_email from Customer where _cus_email like @_cus_email))
		begin
			Declare @tmp TABLE(
				_cus_id int
			)
			insert into Customer(_cus_email,_cus_password,
								_cus_created_date,_cus_updated_date,_cus_status)
			OUTPUT  inserted._cus_id INTO @tmp
			values (@_cus_email,@_cus_password,
					@_cus_created_date,@_cus_created_date,1)

			declare @Id int = (select top 1 _cus_id from @tmp)
			select top 1 * from dbo.Fun_Get_Customers() where _cus_id = @Id
		end
end
go
create proc Sp_Customer_Login
@_cus_email varchar(500),
@_cus_password varchar(500),
@_cus_token varchar(max)
as
begin
	if(exists (select _cus_id from Customer where _cus_email like @_cus_email and _cus_password like @_cus_password))
		begin
			update Customer
			set _cus_token = @_cus_token
			where _cus_email like @_cus_email and _cus_password like @_cus_password

			select * from dbo.Fun_Get_Customers()
			where _cus_email like @_cus_email 
			and _cus_password like @_cus_password
		end
end
go
create proc Sp_Customer_Logout
@_cus_id int
as
begin
	update Customer
	set _cus_token = ''
	where _cus_id = @_cus_id 

	select * from dbo.Fun_Get_Customers()
	where _cus_id = @_cus_id 
end

go
alter proc Sp_Customer_Gets
@_cus_first_name nvarchar(500) = null,
@_cus_last_name nvarchar(500) = null,
@_cus_full_name nvarchar(1000) = null,
@_cus_email varchar(500) = null,
@_cus_dob datetime = null,
@_cus_phone varchar(50) = null
as
begin
	select * from dbo.Fun_Get_Customers()
	where 
		(dbo.IsNullOrEmpty(@_cus_first_name) = 1 
		or dbo.StringContain(_cus_first_name,@_cus_first_name) = 1
		or _cus_first_name like @_cus_first_name)
	or (dbo.IsNullOrEmpty(@_cus_last_name) = 1 
		or dbo.StringContain(_cus_last_name,@_cus_last_name) = 1
		or _cus_last_name like @_cus_last_name)
	or (dbo.IsNullOrEmpty(@_cus_full_name) = 1 
		or dbo.StringContain(_cus_full_name,@_cus_full_name) = 1  
		or _cus_full_name like @_cus_full_name)
	or (dbo.IsNullOrEmpty(@_cus_email) = 1 
		or _cus_email like @_cus_email)
	or (@_cus_dob is null 
		or _cus_dob like @_cus_dob)
	or (dbo.IsNullOrEmpty(@_cus_phone) = 1 
		or dbo.StringContain(_cus_phone,@_cus_phone) = 1  
		or _cus_phone like @_cus_phone)
	order by _cus_updated_date desc
end

go
create proc Sp_Customer_Info
@_cus_id int
as
begin
	select * from dbo.Fun_Get_Customers()
	where _cus_id = @_cus_id
end

go
create proc Sp_Customer_UpdateAvatar
@_cus_id int,
@_cus_avatar varchar(max)
as
begin
	Update Customer
	set _cus_avatar = @_cus_avatar
	where _cus_id = @_cus_id

	select * from dbo.Fun_Get_Customers()
	where _cus_id = @_cus_id
end
go
create proc Sp_Customer_Update
@_cus_id int,
@_cus_first_name nvarchar(500),
@_cus_last_name nvarchar(500),
@_cus_full_name nvarchar(1000),
@_cus_email varchar(500),
@_cus_dob datetime,
@_cus_phone varchar(50),
@_cus_updated_date datetime
as
begin
	update Customer
	set _cus_first_name = @_cus_first_name, _cus_last_name = @_cus_last_name,
		_cus_full_name = @_cus_full_name, _cus_email = @_cus_email,
		_cus_dob = @_cus_dob, _cus_phone = @_cus_phone, _cus_updated_date = @_cus_updated_date
	where _cus_id = @_cus_id

	select * from dbo.Fun_Get_Customers()
	where _cus_id = @_cus_id
end
go
create proc Sp_Customer_ChangeStatus
@_cus_id int,
@_cus_status bit
as
begin
	Update Customer
	set _cus_status = @_cus_status
	where _cus_id = @_cus_id

	select * from dbo.Fun_Get_Customers()
	where _cus_id = @_cus_id
end
go
create proc Sp_Customer_ChangePassword
@_cus_id int,
@_cus_email varchar(max),
@_cus_password varchar(max),
@_cus_new_password varchar(max)
as
begin
	if(exists (select _cus_email from Customer where _cus_email like @_cus_email))
		begin
			Update Customer
			set _cus_password = @_cus_new_password
			where _cus_email like @_cus_email and _cus_password = @_cus_password

			select * from dbo.Fun_Get_Customers()
			where _cus_email like @_cus_email and _cus_password = @_cus_new_password
		end
end