use TestJava
create function Fun_Get_AdminPermisions() returns table 
as
	return (
		select * from AdminPermision ap, Permision p
		where ap._ap_per_id = p._per_id
	)

go
create proc Sp_AdminPermision_Insert
@_ap_ad_id int,
@_ap_per_id int,
@_ap_created_date datetime
as
begin
	Declare @tmp TABLE(
		_ap_id int
	)
	insert into AdminPermision(_ap_ad_id,_ap_per_id,_ap_created_date,_ap_updated_date,_ap_status)
	OUTPUT  inserted._ap_id INTO @tmp
	values (@_ap_ad_id,@_ap_per_id,@_ap_created_date,@_ap_created_date,1)

	declare @Id int = (select top 1 _ap_id from @tmp)
	select top 1 * from dbo.Fun_Get_AdminPermisions() where _ap_id = @Id
end

go
alter proc Sp_AdminPermision_Gets
@_per_name nvarchar(500) = null
as
begin
	select * from dbo.Fun_Get_AdminPermisions()
	where 
		(dbo.IsNullOrEmpty(@_per_name) = 1 
		or dob.StringContain(_per_name,@_per_name) = 1
		or _per_name like @_per_name)
	order by _per_updated_date desc
end

go
create proc Sp_AdminPermision_Update
@_ap_id int,
@_ap_ad_id int,
@_ap_per_id int,
@_ap_updated_date datetime
as
begin
	update AdminPermision
	set _ap_ad_id = @_ap_ad_id, _ap_per_id = @_ap_per_id,
		_ap_updated_date = @_ap_updated_date
	where _ap_id = @_ap_id

	select * from dbo.Fun_Get_AdminPermisions()
	where _ap_id = @_ap_id
end

go
create proc Sp_AdminPermision_ChangeStatus
@_ap_id int,
@_ap_status bit
as
begin
	update AdminPermision
	set _ap_status = @_ap_status
	where _ap_id = @_ap_id

	select * from dbo.Fun_Get_AdminPermisions()
	where _ap_id = @_ap_id
end