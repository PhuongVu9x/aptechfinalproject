USE [TestJava]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Admin_UpdateAvatar]    Script Date: 8/29/2023 6:23:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_Carrier_UpdateIcon]
@_car_id int,
@_car_icon varchar(1000)
as
begin
	update Carrier
	set _car_icon = @_car_icon
	where _car_id = @_car_id

	select * from dbo.Fun_Get_Carriers()
	where _car_id = @_car_id
end