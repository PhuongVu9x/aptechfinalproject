

declare @index int = 0

while @index <= 5
	begin
		declare @hoId int = ((SELECT floor(rand() * (19 - 1)) + 1));
		declare @hoStar int = (select _ho_stars from Hotel where _ho_id = @hoId)
		declare @baseSignle int = (select _ho_signle_rooms from Hotel where _ho_id = @hoId)
		declare @currentSignle int = ((SELECT floor(rand() * (@baseSignle - 1)) + 1));
		declare @roomSinglePrice decimal = ((SELECT floor(rand() * (500000 - 300000)) + 300000));
		declare @furniture varchar(max) = '[1,3,4,5,6]'
		declare @policy varchar(max) = '[1,3,4,5,6]'

		while @currentSignle > 0
			begin
				declare @roomTypeIdSignle int = ((SELECT floor(rand() * (3 - 1)) + 1));
				declare @roomSinglePriceCurrent decimal = @roomSinglePrice * @hoStar * 10 / 100
				if(@roomTypeIdSignle = 2)
					begin
						set @roomSinglePriceCurrent = @roomSinglePriceCurrent * 20 / 100
					end
				declare @roomNumberSingle varchar(10) = (select substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ',
		             (abs(checksum(newid())) % 18)+1, 1)) + (SELECT FORMAT((SELECT floor(rand() * (1000 - 100)) + 100), '000'));
				insert into Room(_ro_ho_id,_ro_room_type_id,_ro_price,_ro_furnitures,_ro_policies,_ro_number) 
				values (@hoId,@roomTypeIdSignle,@roomSinglePriceCurrent,@furniture,@policy,@roomNumberSingle)
				set @currentSignle = @currentSignle -1
			end

		declare @baseDouble int = (select _ho_double_rooms from Hotel where _ho_id = @hoId)
		declare @currentDouble int = ((SELECT floor(rand() * (@baseDouble - 1)) + 1));
		declare @roomDoublePrice decimal = ((SELECT floor(rand() * (800000 - 500000)) + 500000));
		while @currentDouble > 0
			begin
				declare @roomTypeIdDouble int = ((SELECT floor(rand() * (5 - 3)) + 3));
				declare @roomDoublePriceCurrent decimal = @roomSinglePrice * @hoStar * 10 / 100
				if(@roomTypeIdDouble = 3)
					begin
						set @roomDoublePriceCurrent = @roomSinglePriceCurrent * 20 / 100
					end
				declare @roomNumberDouble varchar(10) = (select substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ',
		             (abs(checksum(newid())) % 18)+1, 1)) + (SELECT FORMAT((SELECT floor(rand() * (1000 - 100)) + 100), '000'));
				insert into Room(_ro_ho_id,_ro_room_type_id,_ro_price,_ro_furnitures,_ro_policies,_ro_number) 
				values (@hoId,@roomTypeIdDouble,@roomDoublePriceCurrent,@furniture,@policy,@roomNumberDouble)
				set @currentDouble = @currentDouble -1
			end

		declare @basetriple int = (select _ho_triple_rooms from Hotel where _ho_id = @hoId)
		declare @currentTriple int = ((SELECT floor(rand() * (@basetriple - 1)) + 1));
		declare @roomTriplePrice decimal = ((SELECT floor(rand() * (1200000 - 800000)) + 800000));

		while @currentTriple > 0
			begin
				declare @roomTypeIdTriple int = ((SELECT floor(rand() * (7 - 5)) + 5));
				declare @roomTriplePriceCurrent decimal = @roomSinglePrice * @hoStar * 10 / 100
				if(@roomTypeIdTriple = 2)
					begin
						set @roomTriplePriceCurrent = @roomSinglePriceCurrent * 20 / 100
					end
				declare @roomNumberTriple varchar(10) = (select substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ',
		             (abs(checksum(newid())) % 18)+1, 1)) + (SELECT FORMAT((SELECT floor(rand() * (1000 - 100)) + 100), '000'));
				insert into Room(_ro_ho_id,_ro_room_type_id,_ro_price,_ro_furnitures,_ro_policies,_ro_number) 
				values (@hoId,@roomTypeIdTriple,@roomTriplePriceCurrent,@furniture,@policy,@roomNumberTriple)
				set @currentTriple = @currentTriple -1
			end

		set @index = @index +1
	end
	