use TestJava
go
alter proc Sp_Airport_Gets
@_ap_name nvarchar(500) = null,
@_city_name nvarchar(500) = null
as
begin
	select * from dbo.Fun_Get_Airports()
	where 
		(dbo.IsNullOrEmpty(@_ap_name) = 1 
		or dbo.StringContain(_ap_name,@_ap_name) = 1
		or _ap_name like @_ap_name)
	or (dbo.IsNullOrEmpty(@_city_name) = 1 
		or dbo.StringContain(_city_name,@_city_name) = 1
		or _city_name like @_city_name)
	order by _ap_updated_date desc
end

exec Sp_Airport_Gets