use TestJava
go
alter proc Sp_Category_Gets
@_cate_name nvarchar(500) = null
as
begin
	select * from dbo.Fun_Get_Categories()
	where 
		(dbo.IsNullOrEmpty(@_cate_name) = 1 
		or dbo.StringContain(_cate_name,@_cate_name) = 1
		or _cate_name like @_cate_name)
	order by _cate_updated_date desc
end