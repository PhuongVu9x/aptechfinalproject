alter function Fun_Get_Airports() returns table
as 
	return(
		select * from Airport a, City c
		where a._ap_city_id = c._city_id
	)