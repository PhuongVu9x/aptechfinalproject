declare @index int = 0
declare @password varchar(max) = 'e10adc3949ba59abbe56e057f20f883e'
while @index <= 20
	begin
		declare @firstnamelengh int = ((SELECT floor(rand() * (5 - 2)) + 2));
		declare @firstname varchar(10) = (select dbo.GenerateToken(crypt_gen_random(@firstnamelengh)))
		declare @lastnamelengh int = ((SELECT floor(rand() * (10 - 2)) + 2));
		declare @lastname varchar(10) = (select dbo.GenerateToken(crypt_gen_random(@lastnamelengh)))
		declare @fullname varchar(20) = @firstname + ' ' + @lastname
		declare @emaillengh int = ((SELECT floor(rand() * (20 - 10)) + 10));
		declare @email varchar(30) = (select dbo.GenerateToken(crypt_gen_random(@emaillengh))) + '@gmail.com'
		declare @age int = ((SELECT floor(rand() * (40 - 18)) + 18));
		declare @dob datetime = getdate() - 365 * @age
		declare @phone varchar(15) = (SELECT FORMAT((SELECT floor(rand() * (10000000000 - 1000000000)) + 1000000000), '000 000 0000'));
		insert into Customer(_cus_first_name,_cus_last_name,_cus_full_name,
								_cus_email,_cus_dob,_cus_password,_cus_phone)
		values(@firstname,@lastname,@fullname,
				@email,@dob,@password,@phone)
		set @index = @index +1
	end



