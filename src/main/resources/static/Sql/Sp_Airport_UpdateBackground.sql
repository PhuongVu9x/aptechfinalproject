use TestJava
go
create proc Sp_Airport_UpdateBackground
@_ap_id int,
@_ap_backgound varchar(1000)
as
begin
	update Airport
	set _ap_backgound = @_ap_backgound
	where _ap_id = @_ap_id

	select * from dbo.Fun_Get_Airports()
	where _ap_id = @_ap_id
end