USE [master]
GO
/****** Object:  Database [TestJava]    Script Date: 8/27/2023 8:40:26 AM ******/
CREATE DATABASE [TestJava]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'TestJava_Data', FILENAME = N'c:\dzsqls\TestJava.mdf' , SIZE = 8192KB , MAXSIZE = 30720KB , FILEGROWTH = 22528KB )
 LOG ON 
( NAME = N'TestJava_Logs', FILENAME = N'c:\dzsqls\TestJava.ldf' , SIZE = 8192KB , MAXSIZE = 30720KB , FILEGROWTH = 22528KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [TestJava] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TestJava].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TestJava] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TestJava] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TestJava] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TestJava] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TestJava] SET ARITHABORT OFF 
GO
ALTER DATABASE [TestJava] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [TestJava] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TestJava] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TestJava] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TestJava] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TestJava] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TestJava] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TestJava] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TestJava] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TestJava] SET  ENABLE_BROKER 
GO
ALTER DATABASE [TestJava] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TestJava] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TestJava] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TestJava] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TestJava] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TestJava] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [TestJava] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TestJava] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [TestJava] SET  MULTI_USER 
GO
ALTER DATABASE [TestJava] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TestJava] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TestJava] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TestJava] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [TestJava] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [TestJava] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [TestJava] SET QUERY_STORE = ON
GO
ALTER DATABASE [TestJava] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [TestJava]
GO
/****** Object:  User [phongvan1412_SQLLogin_1]    Script Date: 8/27/2023 8:40:29 AM ******/
CREATE USER [phongvan1412_SQLLogin_1] FOR LOGIN [phongvan1412_SQLLogin_1] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [phongvan1412_SQLLogin_1]
GO
/****** Object:  Schema [phongvan1412_SQLLogin_1]    Script Date: 8/27/2023 8:40:29 AM ******/
CREATE SCHEMA [phongvan1412_SQLLogin_1]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 8/27/2023 8:40:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[_ad_id] [int] IDENTITY(1,1) NOT NULL,
	[_ad_first_name] [nvarchar](200) NULL,
	[_ad_last_name] [nvarchar](200) NULL,
	[_ad_full_name] [nvarchar](400) NULL,
	[_ad_email] [varchar](500) NULL,
	[_ad_token] [varchar](500) NULL,
	[_ad_dob] [datetime] NULL,
	[_ad_password] [varchar](max) NULL,
	[_ad_phone] [varchar](50) NULL,
	[_ad_avatar] [varchar](max) NULL,
	[_ad_position_id] [int] NULL,
	[_ad_created_date] [datetime] NULL,
	[_ad_updated_date] [datetime] NULL,
	[_ad_status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[_ad_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AdminPermision]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdminPermision](
	[_ap_id] [int] IDENTITY(1,1) NOT NULL,
	[_ap_ad_id] [int] NULL,
	[_ap_per_id] [int] NULL,
	[_ap_created_date] [datetime] NULL,
	[_ap_updated_date] [datetime] NULL,
	[_ap_status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[_ap_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AirPort]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AirPort](
	[_ap_id] [int] IDENTITY(1,1) NOT NULL,
	[_ap_name] [nvarchar](500) NULL,
	[_ap_icon] [varchar](max) NULL,
	[_ap_backgound] [varchar](max) NULL,
	[_ap_city_id] [int] NULL,
	[_ap_created_date] [datetime] NULL,
	[_ap_updated_date] [datetime] NULL,
	[_ap_status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[_ap_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bill]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bill](
	[_bil_id] [int] IDENTITY(1,1) NOT NULL,
	[_bil_cus_id] [int] NULL,
	[_bil_payment] [decimal](18, 0) NULL,
	[_bil_created_date] [datetime] NULL,
	[_bil_updated_date] [datetime] NULL,
	[_bil_status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[_bil_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Booking]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Booking](
	[_bo_id] [int] IDENTITY(1,1) NOT NULL,
	[_bo_cus_id] [int] NULL,
	[_bo_check_in] [datetime] NULL,
	[_bo_check_out] [datetime] NULL,
	[_bo_deposit] [decimal](18, 0) NULL,
	[_bo_payment] [decimal](18, 0) NULL,
	[_bo_payment_type] [nvarchar](max) NULL,
	[_bo_created_date] [datetime] NULL,
	[_bo_updated_date] [datetime] NULL,
	[_bo_status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[_bo_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Carrier]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Carrier](
	[_car_id] [int] IDENTITY(1,1) NOT NULL,
	[_car_name] [nvarchar](500) NULL,
	[_car_icon] [varchar](max) NULL,
	[_car_created_date] [datetime] NULL,
	[_car_updated_date] [datetime] NULL,
	[_car_status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[_car_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[_cate_id] [int] IDENTITY(1,1) NOT NULL,
	[_cate_name] [nvarchar](200) NULL,
	[_cate_icon] [varchar](max) NULL,
	[_cate_created_date] [datetime] NULL,
	[_cate_updated_date] [datetime] NULL,
	[_cate_status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[_cate_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[City]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[City](
	[_city_id] [int] IDENTITY(1,1) NOT NULL,
	[_city_name] [nvarchar](500) NULL,
	[_city_icon] [varchar](max) NULL,
	[_city_created_date] [datetime] NULL,
	[_city_updated_date] [datetime] NULL,
	[_city_status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[_city_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[_ct_id] [int] IDENTITY(1,1) NOT NULL,
	[_ct_name] [nvarchar](200) NULL,
	[_ct_created_date] [datetime] NULL,
	[_ct_updated_date] [datetime] NULL,
	[_ct_status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[_ct_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[_cus_id] [int] IDENTITY(1,1) NOT NULL,
	[_cus_first_name] [nvarchar](200) NULL,
	[_cus_last_name] [nvarchar](200) NULL,
	[_cus_full_name] [nvarchar](400) NULL,
	[_cus_email] [varchar](500) NULL,
	[_cus_dob] [datetime] NULL,
	[_cus_password] [varchar](max) NULL,
	[_cus_phone] [varchar](50) NULL,
	[_cus_avatar] [varchar](max) NULL,
	[_cus_created_date] [datetime] NULL,
	[_cus_updated_date] [datetime] NULL,
	[_cus_status] [bit] NULL,
	[_cus_token] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[_cus_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Flight]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Flight](
	[_fl_id] [int] IDENTITY(1,1) NOT NULL,
	[_fl_from_id] [int] NULL,
	[_fl_to_id] [int] NULL,
	[_fl_transit_id] [int] NULL,
	[_fl_plane_id] [int] NULL,
	[_fl_estimate_take_off] [datetime] NULL,
	[_fl_estimate_arrival] [datetime] NULL,
	[_fl_take_off] [datetime] NULL,
	[_fl_arrival] [datetime] NULL,
	[_fl_late_time] [int] NULL,
	[_fl_created_date] [datetime] NULL,
	[_fl_updated_date] [datetime] NULL,
	[_fl_status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[_fl_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Furniture]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Furniture](
	[_fur_id] [int] IDENTITY(1,1) NOT NULL,
	[_fur_name] [nvarchar](1000) NULL,
	[_fur_icon] [varchar](max) NULL,
	[_fur_created_date] [datetime] NULL,
	[_fur_updated_date] [datetime] NULL,
	[_fur_status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[_fur_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Hotel]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel](
	[_ho_id] [int] IDENTITY(1,1) NOT NULL,
	[_ho_name] [nvarchar](200) NULL,
	[_ho_city_id] [int] NULL,
	[_ho_rooms] [int] NULL,
	[_ho_signle_rooms] [int] NULL,
	[_ho_double_rooms] [int] NULL,
	[_ho_triple_rooms] [int] NULL,
	[_ho_stars] [int] NULL,
	[_ho_address] [nvarchar](1000) NULL,
	[_ho_from_price] [decimal](18, 0) NULL,
	[_ho_to_price] [decimal](18, 0) NULL,
	[_ho_created_date] [datetime] NULL,
	[_ho_updated_date] [datetime] NULL,
	[_ho_status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[_ho_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Permision]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permision](
	[_per_id] [int] IDENTITY(1,1) NOT NULL,
	[_per_name] [nvarchar](200) NULL,
	[_per_icon] [varchar](max) NULL,
	[_per_created_date] [datetime] NULL,
	[_per_updated_date] [datetime] NULL,
	[_per_status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[_per_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Plane]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Plane](
	[_pl_id] [int] IDENTITY(1,1) NOT NULL,
	[_pl_code] [varchar](10) NULL,
	[_pl_carrier_id] [int] NULL,
	[_pl_created_date] [datetime] NULL,
	[_pl_updated_date] [datetime] NULL,
	[_pl_status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[_pl_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Policy]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Policy](
	[_po_id] [int] IDENTITY(1,1) NOT NULL,
	[_po_name] [nvarchar](1000) NULL,
	[_po_description] [nvarchar](1000) NULL,
	[_po_icon] [varchar](max) NULL,
	[_po_created_date] [datetime] NULL,
	[_po_updated_date] [datetime] NULL,
	[_po_status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[_po_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Position]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Position](
	[_pos_id] [int] IDENTITY(1,1) NOT NULL,
	[_pos_name] [nvarchar](200) NULL,
	[_pos_icon] [varchar](max) NULL,
	[_pos_created_date] [datetime] NULL,
	[_pos_updated_date] [datetime] NULL,
	[_pos_status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[_pos_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReturnType]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReturnType](
	[_ret_id] [int] IDENTITY(1,1) NOT NULL,
	[_ret_name] [nvarchar](200) NULL,
	[_ret_created_date] [datetime] NULL,
	[_ret_updated_date] [datetime] NULL,
	[_ret_status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[_ret_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Room]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Room](
	[_ro_id] [int] IDENTITY(1,1) NOT NULL,
	[_ro_ho_id] [int] NULL,
	[_ro_room_type_id] [int] NULL,
	[_ro_price] [decimal](18, 0) NULL,
	[_ro_furnitures] [varchar](50) NULL,
	[_ro_policies] [varchar](50) NULL,
	[_ro_icon] [varchar](max) NULL,
	[_ro_images] [ntext] NULL,
	[_ro_max_customer] [int] NULL,
	[_ro_created_date] [datetime] NULL,
	[_ro_udpated_date] [datetime] NULL,
	[_ro_status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[_ro_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoomType]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoomType](
	[_rt_id] [int] IDENTITY(1,1) NOT NULL,
	[_rt_name] [nvarchar](200) NULL,
	[_rt_created_date] [datetime] NULL,
	[_rt_updated_date] [datetime] NULL,
	[_rt_status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[_rt_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ticket]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ticket](
	[_tk_id] [int] IDENTITY(1,1) NOT NULL,
	[_tk_bill_id] [int] NULL,
	[_tk_flight_id] [int] NULL,
	[_tk_payment] [decimal](18, 0) NULL,
	[_tk_first_name] [nvarchar](200) NULL,
	[_tk_last_name] [nvarchar](200) NULL,
	[_tk_full_name] [nvarchar](400) NULL,
	[_tk_dob] [datetime] NULL,
	[_tk_nationality] [nvarchar](400) NULL,
	[_tk_passport] [nvarchar](400) NULL,
	[_tk_country] [nvarchar](200) NULL,
	[_tk_passport_expried] [datetime] NULL,
	[_tk_return_type] [int] NULL,
	[_tk_created_date] [datetime] NULL,
	[_tk_updated_date] [datetime] NULL,
	[_tk_status] [int] NULL,
	[_tk_title] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[_tk_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TicketClass]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketClass](
	[_tc_id] [int] IDENTITY(1,1) NOT NULL,
	[_tc_name] [nvarchar](500) NULL,
	[_tc_icon] [varchar](max) NULL,
	[_tc_created_date] [datetime] NULL,
	[_tc_updated_date] [datetime] NULL,
	[_tc_status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[_tc_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Admin] ADD  DEFAULT ('') FOR [_ad_first_name]
GO
ALTER TABLE [dbo].[Admin] ADD  DEFAULT ('') FOR [_ad_last_name]
GO
ALTER TABLE [dbo].[Admin] ADD  DEFAULT ('') FOR [_ad_full_name]
GO
ALTER TABLE [dbo].[Admin] ADD  DEFAULT ('') FOR [_ad_email]
GO
ALTER TABLE [dbo].[Admin] ADD  DEFAULT ('') FOR [_ad_token]
GO
ALTER TABLE [dbo].[Admin] ADD  DEFAULT (getdate()-(6600)) FOR [_ad_dob]
GO
ALTER TABLE [dbo].[Admin] ADD  DEFAULT ('') FOR [_ad_password]
GO
ALTER TABLE [dbo].[Admin] ADD  DEFAULT ('') FOR [_ad_phone]
GO
ALTER TABLE [dbo].[Admin] ADD  DEFAULT ('') FOR [_ad_avatar]
GO
ALTER TABLE [dbo].[Admin] ADD  DEFAULT (getdate()) FOR [_ad_created_date]
GO
ALTER TABLE [dbo].[Admin] ADD  DEFAULT (getdate()) FOR [_ad_updated_date]
GO
ALTER TABLE [dbo].[Admin] ADD  DEFAULT ((1)) FOR [_ad_status]
GO
ALTER TABLE [dbo].[AdminPermision] ADD  DEFAULT (getdate()) FOR [_ap_created_date]
GO
ALTER TABLE [dbo].[AdminPermision] ADD  DEFAULT (getdate()) FOR [_ap_updated_date]
GO
ALTER TABLE [dbo].[AdminPermision] ADD  DEFAULT ((1)) FOR [_ap_status]
GO
ALTER TABLE [dbo].[AirPort] ADD  DEFAULT ('') FOR [_ap_name]
GO
ALTER TABLE [dbo].[AirPort] ADD  DEFAULT ('') FOR [_ap_icon]
GO
ALTER TABLE [dbo].[AirPort] ADD  DEFAULT ('') FOR [_ap_backgound]
GO
ALTER TABLE [dbo].[AirPort] ADD  DEFAULT (getdate()) FOR [_ap_created_date]
GO
ALTER TABLE [dbo].[AirPort] ADD  DEFAULT (getdate()) FOR [_ap_updated_date]
GO
ALTER TABLE [dbo].[AirPort] ADD  DEFAULT ((1)) FOR [_ap_status]
GO
ALTER TABLE [dbo].[Bill] ADD  DEFAULT ((0)) FOR [_bil_payment]
GO
ALTER TABLE [dbo].[Bill] ADD  DEFAULT (getdate()) FOR [_bil_created_date]
GO
ALTER TABLE [dbo].[Bill] ADD  DEFAULT (getdate()) FOR [_bil_updated_date]
GO
ALTER TABLE [dbo].[Bill] ADD  DEFAULT ((1)) FOR [_bil_status]
GO
ALTER TABLE [dbo].[Booking] ADD  DEFAULT (getdate()) FOR [_bo_check_in]
GO
ALTER TABLE [dbo].[Booking] ADD  DEFAULT (getdate()) FOR [_bo_check_out]
GO
ALTER TABLE [dbo].[Booking] ADD  DEFAULT ((0)) FOR [_bo_deposit]
GO
ALTER TABLE [dbo].[Booking] ADD  DEFAULT ((0)) FOR [_bo_payment]
GO
ALTER TABLE [dbo].[Booking] ADD  DEFAULT ('') FOR [_bo_payment_type]
GO
ALTER TABLE [dbo].[Booking] ADD  DEFAULT (getdate()) FOR [_bo_created_date]
GO
ALTER TABLE [dbo].[Booking] ADD  DEFAULT (getdate()) FOR [_bo_updated_date]
GO
ALTER TABLE [dbo].[Booking] ADD  DEFAULT ((1)) FOR [_bo_status]
GO
ALTER TABLE [dbo].[Carrier] ADD  DEFAULT ('') FOR [_car_name]
GO
ALTER TABLE [dbo].[Carrier] ADD  DEFAULT ('') FOR [_car_icon]
GO
ALTER TABLE [dbo].[Carrier] ADD  DEFAULT (getdate()) FOR [_car_created_date]
GO
ALTER TABLE [dbo].[Carrier] ADD  DEFAULT (getdate()) FOR [_car_updated_date]
GO
ALTER TABLE [dbo].[Carrier] ADD  DEFAULT ((1)) FOR [_car_status]
GO
ALTER TABLE [dbo].[Category] ADD  DEFAULT ('') FOR [_cate_name]
GO
ALTER TABLE [dbo].[Category] ADD  DEFAULT ('') FOR [_cate_icon]
GO
ALTER TABLE [dbo].[Category] ADD  DEFAULT (getdate()) FOR [_cate_created_date]
GO
ALTER TABLE [dbo].[Category] ADD  DEFAULT (getdate()) FOR [_cate_updated_date]
GO
ALTER TABLE [dbo].[Category] ADD  DEFAULT ((1)) FOR [_cate_status]
GO
ALTER TABLE [dbo].[City] ADD  DEFAULT ('') FOR [_city_name]
GO
ALTER TABLE [dbo].[City] ADD  DEFAULT ('') FOR [_city_icon]
GO
ALTER TABLE [dbo].[City] ADD  DEFAULT (getdate()) FOR [_city_created_date]
GO
ALTER TABLE [dbo].[City] ADD  DEFAULT (getdate()) FOR [_city_updated_date]
GO
ALTER TABLE [dbo].[City] ADD  DEFAULT ((1)) FOR [_city_status]
GO
ALTER TABLE [dbo].[Country] ADD  DEFAULT ('') FOR [_ct_name]
GO
ALTER TABLE [dbo].[Country] ADD  DEFAULT (getdate()) FOR [_ct_created_date]
GO
ALTER TABLE [dbo].[Country] ADD  DEFAULT (getdate()) FOR [_ct_updated_date]
GO
ALTER TABLE [dbo].[Country] ADD  DEFAULT ((1)) FOR [_ct_status]
GO
ALTER TABLE [dbo].[Customer] ADD  DEFAULT ('') FOR [_cus_first_name]
GO
ALTER TABLE [dbo].[Customer] ADD  DEFAULT ('') FOR [_cus_last_name]
GO
ALTER TABLE [dbo].[Customer] ADD  DEFAULT ('') FOR [_cus_full_name]
GO
ALTER TABLE [dbo].[Customer] ADD  DEFAULT ('') FOR [_cus_email]
GO
ALTER TABLE [dbo].[Customer] ADD  DEFAULT (getdate()-(6600)) FOR [_cus_dob]
GO
ALTER TABLE [dbo].[Customer] ADD  DEFAULT ('') FOR [_cus_password]
GO
ALTER TABLE [dbo].[Customer] ADD  DEFAULT ('') FOR [_cus_phone]
GO
ALTER TABLE [dbo].[Customer] ADD  DEFAULT ('') FOR [_cus_avatar]
GO
ALTER TABLE [dbo].[Customer] ADD  DEFAULT (getdate()) FOR [_cus_created_date]
GO
ALTER TABLE [dbo].[Customer] ADD  DEFAULT (getdate()) FOR [_cus_updated_date]
GO
ALTER TABLE [dbo].[Customer] ADD  DEFAULT ((1)) FOR [_cus_status]
GO
ALTER TABLE [dbo].[Customer] ADD  DEFAULT ('') FOR [_cus_token]
GO
ALTER TABLE [dbo].[Flight] ADD  DEFAULT ((0)) FOR [_fl_transit_id]
GO
ALTER TABLE [dbo].[Flight] ADD  DEFAULT (getdate()) FOR [_fl_estimate_take_off]
GO
ALTER TABLE [dbo].[Flight] ADD  DEFAULT (getdate()) FOR [_fl_estimate_arrival]
GO
ALTER TABLE [dbo].[Flight] ADD  DEFAULT (getdate()) FOR [_fl_take_off]
GO
ALTER TABLE [dbo].[Flight] ADD  DEFAULT (getdate()) FOR [_fl_arrival]
GO
ALTER TABLE [dbo].[Flight] ADD  DEFAULT ((0)) FOR [_fl_late_time]
GO
ALTER TABLE [dbo].[Flight] ADD  DEFAULT (getdate()) FOR [_fl_created_date]
GO
ALTER TABLE [dbo].[Flight] ADD  DEFAULT (getdate()) FOR [_fl_updated_date]
GO
ALTER TABLE [dbo].[Flight] ADD  DEFAULT ((1)) FOR [_fl_status]
GO
ALTER TABLE [dbo].[Furniture] ADD  DEFAULT ('') FOR [_fur_name]
GO
ALTER TABLE [dbo].[Furniture] ADD  DEFAULT ('') FOR [_fur_icon]
GO
ALTER TABLE [dbo].[Furniture] ADD  DEFAULT (getdate()) FOR [_fur_created_date]
GO
ALTER TABLE [dbo].[Furniture] ADD  DEFAULT (getdate()) FOR [_fur_updated_date]
GO
ALTER TABLE [dbo].[Furniture] ADD  DEFAULT ((1)) FOR [_fur_status]
GO
ALTER TABLE [dbo].[Hotel] ADD  DEFAULT ('') FOR [_ho_name]
GO
ALTER TABLE [dbo].[Hotel] ADD  DEFAULT ((0)) FOR [_ho_rooms]
GO
ALTER TABLE [dbo].[Hotel] ADD  DEFAULT ((0)) FOR [_ho_signle_rooms]
GO
ALTER TABLE [dbo].[Hotel] ADD  DEFAULT ((0)) FOR [_ho_double_rooms]
GO
ALTER TABLE [dbo].[Hotel] ADD  DEFAULT ((0)) FOR [_ho_triple_rooms]
GO
ALTER TABLE [dbo].[Hotel] ADD  DEFAULT ((0)) FOR [_ho_stars]
GO
ALTER TABLE [dbo].[Hotel] ADD  DEFAULT ('') FOR [_ho_address]
GO
ALTER TABLE [dbo].[Hotel] ADD  DEFAULT ((0)) FOR [_ho_from_price]
GO
ALTER TABLE [dbo].[Hotel] ADD  DEFAULT ((0)) FOR [_ho_to_price]
GO
ALTER TABLE [dbo].[Hotel] ADD  DEFAULT (getdate()) FOR [_ho_created_date]
GO
ALTER TABLE [dbo].[Hotel] ADD  DEFAULT (getdate()) FOR [_ho_updated_date]
GO
ALTER TABLE [dbo].[Hotel] ADD  CONSTRAINT [default_ho_status]  DEFAULT ((1)) FOR [_ho_status]
GO
ALTER TABLE [dbo].[Permision] ADD  DEFAULT ('') FOR [_per_name]
GO
ALTER TABLE [dbo].[Permision] ADD  DEFAULT ('') FOR [_per_icon]
GO
ALTER TABLE [dbo].[Permision] ADD  DEFAULT (getdate()) FOR [_per_created_date]
GO
ALTER TABLE [dbo].[Permision] ADD  DEFAULT (getdate()) FOR [_per_updated_date]
GO
ALTER TABLE [dbo].[Permision] ADD  DEFAULT ((1)) FOR [_per_status]
GO
ALTER TABLE [dbo].[Plane] ADD  DEFAULT ('') FOR [_pl_code]
GO
ALTER TABLE [dbo].[Plane] ADD  DEFAULT ((0)) FOR [_pl_seats]
GO
ALTER TABLE [dbo].[Plane] ADD  DEFAULT ((0)) FOR [_pl_economy_seats]
GO
ALTER TABLE [dbo].[Plane] ADD  DEFAULT ((0)) FOR [_pl_special_economy_seats]
GO
ALTER TABLE [dbo].[Plane] ADD  DEFAULT ((0)) FOR [_pl_business_seats]
GO
ALTER TABLE [dbo].[Plane] ADD  DEFAULT ((0)) FOR [_pl_first_class_seats]
GO
ALTER TABLE [dbo].[Plane] ADD  DEFAULT (getdate()) FOR [_pl_created_date]
GO
ALTER TABLE [dbo].[Plane] ADD  DEFAULT (getdate()) FOR [_pl_updated_date]
GO
ALTER TABLE [dbo].[Plane] ADD  DEFAULT ((1)) FOR [_pl_status]
GO
ALTER TABLE [dbo].[Policy] ADD  DEFAULT ('') FOR [_po_name]
GO
ALTER TABLE [dbo].[Policy] ADD  DEFAULT ('') FOR [_po_description]
GO
ALTER TABLE [dbo].[Policy] ADD  DEFAULT ('') FOR [_po_icon]
GO
ALTER TABLE [dbo].[Policy] ADD  DEFAULT (getdate()) FOR [_po_created_date]
GO
ALTER TABLE [dbo].[Policy] ADD  DEFAULT (getdate()) FOR [_po_updated_date]
GO
ALTER TABLE [dbo].[Policy] ADD  DEFAULT ((1)) FOR [_po_status]
GO
ALTER TABLE [dbo].[Position] ADD  DEFAULT ('') FOR [_pos_name]
GO
ALTER TABLE [dbo].[Position] ADD  DEFAULT ('') FOR [_pos_icon]
GO
ALTER TABLE [dbo].[Position] ADD  DEFAULT (getdate()) FOR [_pos_created_date]
GO
ALTER TABLE [dbo].[Position] ADD  DEFAULT (getdate()) FOR [_pos_updated_date]
GO
ALTER TABLE [dbo].[Position] ADD  DEFAULT ((1)) FOR [_pos_status]
GO
ALTER TABLE [dbo].[ReturnType] ADD  DEFAULT ('') FOR [_ret_name]
GO
ALTER TABLE [dbo].[ReturnType] ADD  DEFAULT (getdate()) FOR [_ret_created_date]
GO
ALTER TABLE [dbo].[ReturnType] ADD  DEFAULT (getdate()) FOR [_ret_updated_date]
GO
ALTER TABLE [dbo].[ReturnType] ADD  DEFAULT ((1)) FOR [_ret_status]
GO
ALTER TABLE [dbo].[Room] ADD  DEFAULT ((0)) FOR [_ro_price]
GO
ALTER TABLE [dbo].[Room] ADD  DEFAULT ('') FOR [_ro_furnitures]
GO
ALTER TABLE [dbo].[Room] ADD  DEFAULT ('') FOR [_ro_policies]
GO
ALTER TABLE [dbo].[Room] ADD  DEFAULT ('') FOR [_ro_icon]
GO
ALTER TABLE [dbo].[Room] ADD  DEFAULT ('') FOR [_ro_images]
GO
ALTER TABLE [dbo].[Room] ADD  DEFAULT ((0)) FOR [_ro_max_customer]
GO
ALTER TABLE [dbo].[Room] ADD  DEFAULT (getdate()) FOR [_ro_created_date]
GO
ALTER TABLE [dbo].[Room] ADD  DEFAULT (getdate()) FOR [_ro_udpated_date]
GO
ALTER TABLE [dbo].[Room] ADD  DEFAULT ((1)) FOR [_ro_status]
GO
ALTER TABLE [dbo].[RoomType] ADD  DEFAULT ('') FOR [_rt_name]
GO
ALTER TABLE [dbo].[RoomType] ADD  DEFAULT (getdate()) FOR [_rt_created_date]
GO
ALTER TABLE [dbo].[RoomType] ADD  DEFAULT (getdate()) FOR [_rt_updated_date]
GO
ALTER TABLE [dbo].[RoomType] ADD  DEFAULT ((1)) FOR [_rt_status]
GO
ALTER TABLE [dbo].[Ticket] ADD  DEFAULT ((0)) FOR [_tk_payment]
GO
ALTER TABLE [dbo].[Ticket] ADD  DEFAULT ('') FOR [_tk_first_name]
GO
ALTER TABLE [dbo].[Ticket] ADD  DEFAULT ('') FOR [_tk_last_name]
GO
ALTER TABLE [dbo].[Ticket] ADD  DEFAULT ('') FOR [_tk_full_name]
GO
ALTER TABLE [dbo].[Ticket] ADD  DEFAULT (getdate()-(6600)) FOR [_tk_dob]
GO
ALTER TABLE [dbo].[Ticket] ADD  DEFAULT ('') FOR [_tk_nationality]
GO
ALTER TABLE [dbo].[Ticket] ADD  DEFAULT ('') FOR [_tk_passport]
GO
ALTER TABLE [dbo].[Ticket] ADD  DEFAULT ('') FOR [_tk_country]
GO
ALTER TABLE [dbo].[Ticket] ADD  DEFAULT (getdate()) FOR [_tk_passport_expried]
GO
ALTER TABLE [dbo].[Ticket] ADD  DEFAULT (getdate()) FOR [_tk_created_date]
GO
ALTER TABLE [dbo].[Ticket] ADD  DEFAULT (getdate()) FOR [_tk_updated_date]
GO
ALTER TABLE [dbo].[Ticket] ADD  DEFAULT ((1)) FOR [_tk_status]
GO
ALTER TABLE [dbo].[Ticket] ADD  DEFAULT ('') FOR [_tk_title]
GO
ALTER TABLE [dbo].[TicketClass] ADD  DEFAULT ('') FOR [_tc_name]
GO
ALTER TABLE [dbo].[TicketClass] ADD  DEFAULT ('') FOR [_tc_icon]
GO
ALTER TABLE [dbo].[TicketClass] ADD  DEFAULT (getdate()) FOR [_tc_created_date]
GO
ALTER TABLE [dbo].[TicketClass] ADD  DEFAULT (getdate()) FOR [_tc_updated_date]
GO
ALTER TABLE [dbo].[TicketClass] ADD  DEFAULT ((1)) FOR [_tc_status]
GO
ALTER TABLE [dbo].[Admin]  WITH CHECK ADD FOREIGN KEY([_ad_position_id])
REFERENCES [dbo].[Position] ([_pos_id])
GO
ALTER TABLE [dbo].[AdminPermision]  WITH CHECK ADD FOREIGN KEY([_ap_ad_id])
REFERENCES [dbo].[Admin] ([_ad_id])
GO
ALTER TABLE [dbo].[AdminPermision]  WITH CHECK ADD FOREIGN KEY([_ap_per_id])
REFERENCES [dbo].[Permision] ([_per_id])
GO
ALTER TABLE [dbo].[AirPort]  WITH CHECK ADD FOREIGN KEY([_ap_city_id])
REFERENCES [dbo].[City] ([_city_id])
GO
ALTER TABLE [dbo].[Bill]  WITH CHECK ADD FOREIGN KEY([_bil_cus_id])
REFERENCES [dbo].[Customer] ([_cus_id])
GO
ALTER TABLE [dbo].[Booking]  WITH CHECK ADD FOREIGN KEY([_bo_cus_id])
REFERENCES [dbo].[Customer] ([_cus_id])
GO
ALTER TABLE [dbo].[Flight]  WITH CHECK ADD FOREIGN KEY([_fl_from_id])
REFERENCES [dbo].[City] ([_city_id])
GO
ALTER TABLE [dbo].[Flight]  WITH CHECK ADD FOREIGN KEY([_fl_plane_id])
REFERENCES [dbo].[Plane] ([_pl_id])
GO
ALTER TABLE [dbo].[Flight]  WITH CHECK ADD FOREIGN KEY([_fl_to_id])
REFERENCES [dbo].[City] ([_city_id])
GO
ALTER TABLE [dbo].[Hotel]  WITH CHECK ADD FOREIGN KEY([_ho_city_id])
REFERENCES [dbo].[City] ([_city_id])
GO
ALTER TABLE [dbo].[Plane]  WITH CHECK ADD FOREIGN KEY([_pl_carrier_id])
REFERENCES [dbo].[Carrier] ([_car_id])
GO
ALTER TABLE [dbo].[Room]  WITH CHECK ADD FOREIGN KEY([_ro_ho_id])
REFERENCES [dbo].[Hotel] ([_ho_id])
GO
ALTER TABLE [dbo].[Room]  WITH CHECK ADD FOREIGN KEY([_ro_room_type_id])
REFERENCES [dbo].[RoomType] ([_rt_id])
GO
ALTER TABLE [dbo].[Ticket]  WITH CHECK ADD FOREIGN KEY([_tk_bill_id])
REFERENCES [dbo].[Bill] ([_bil_id])
GO
ALTER TABLE [dbo].[Ticket]  WITH CHECK ADD FOREIGN KEY([_tk_flight_id])
REFERENCES [dbo].[Flight] ([_fl_id])
GO
ALTER TABLE [dbo].[Ticket]  WITH CHECK ADD FOREIGN KEY([_tk_return_type])
REFERENCES [dbo].[ReturnType] ([_ret_id])
GO
/****** Object:  StoredProcedure [dbo].[Sp_Admin_GetByToken]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_Admin_GetByToken]
@token varchar(1000)
as
begin
	select * from [Admin] 
	where _token like @token 
end
GO
/****** Object:  StoredProcedure [dbo].[Sp_Admin_Login]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_Admin_Login]
@email varchar(500),
@password varchar(500),
@token varchar(1000)
as
begin
    set nocount on
	update [Admin]
	set _token = @token
	where _email like @email and _password like @password

	select * from [Admin] 
	where _email like @email and _password like @password
	set nocount off
end
GO
/****** Object:  StoredProcedure [dbo].[Sp_CheckAcceptedCodeUser]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CheckAcceptedCodeUser]
@UserId int,
@CodeAccept varchar(500)
as 
begin 
    Declare @UserTmp TABLE(
        id int
	)
    set nocount on
    declare @Id int = (select top 1 id from @UserTmp)
	select top 1 * from [user] where id = @UserId and codeaccept = @CodeAccept
end
GO
/****** Object:  StoredProcedure [dbo].[Sp_InsertAcceptedCodeUser]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Sp_InsertAcceptedCodeUser]
@UserId int,
@CodeAccept varchar(500)
as 
begin 
    Declare @UserTmp TABLE(
        id int
	)
    set nocount on
    update [user] set codeaccept = @CodeAccept where id=@UserId

    declare @Id int = (select top 1 id from @UserTmp)
	select top 1 * from [user] where id = @UserId
end
GO
/****** Object:  StoredProcedure [dbo].[Sp_InsertUser]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Sp_InsertUser]
    @Phone varchar(15),
    @Pass varchar(255),
    @CodeGen varchar(500)
as
begin
    Declare @UserTmp TABLE(
        id int
	)
    SET NOCOUNT ON
    insert into [user]([phone],[pass],[codegen])
    OUTPUT  inserted.id INTO @UserTmp
    values(@Phone, @Pass, @CodeGen)

    declare @Id int = (select top 1 id from @UserTmp)
	select top 1 * from [user] where id = @Id
end
GO
/****** Object:  StoredProcedure [dbo].[Sp_UpdateAcceptedCodeUser]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_UpdateAcceptedCodeUser]
@UserId int
as 
begin 
    Declare @UserTmp TABLE(
        id int
	)
    set nocount on
    update [user] set codeaccept = '' where id=@UserId

    declare @Id int = (select top 1 id from @UserTmp)
	select top 1 * from [user] where id = @UserId
end
GO
/****** Object:  StoredProcedure [dbo].[Sp_UpdateUser]    Script Date: 8/27/2023 8:40:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Sp_UpdateUser]
@UserId int
as 
begin 
    Declare @UserTmp TABLE(
        id int
	)
    set nocount on
    update [user] set codegen = '' where id=@UserId

    declare @Id int = (select top 1 id from @UserTmp)
	select top 1 * from [user] where id = @Id
end
GO
create function IsNullOrEmpty(@text nvarchar(max)) returns bit
as
begin
	declare @check bit = 0;
	if(@text is null or @text like '')
		begin
			set @check = 1;
		end
	return @check
end
go
create function StringContain(@baseText nvarchar(max),@text nvarchar(max)) returns bit
as
begin
	declare @check bit = 0;
	if(@baseText like '%' + @text + '%')
		begin
			set @check = 1;
		end
	return @check
end

create table PlaneType(
	_pt_id int identity(1,1) constraint pk_pt_id primary key,
	_pt_name nvarchar(500) constraint default_pt_name default '',
	_pt_total_seats int constraint default_pt_total_seats default 0,
	_pt_ec_quantity int constraint default_pt_ec_quantity default 0,
	_pt_sc_quantity int constraint default_pt_sc_quantity default 0,
	_pt_bs_quantity int constraint default_pt_bs_quantity default 0,
	_pt_fc_quantity int constraint default_pt_fc_quantity default 0,
	_pt_created_date datetime constraint default_pt_created_date default getdate(),
	_pt_updated_date datetime constraint default_pt_updated_date default getdate(),
	_pt_status bit constraint default_pt_status default 1
)

insert into PlaneType(_pt_name,_pt_total_seats,_pt_ec_quantity,_pt_sc_quantity,_pt_bs_quantity,_pt_fc_quantity)
values ('Airbus 320',90 + 60 + 24 + 2,90 , 60 , 24 , 2)

create table [session] (
	_ses_id int identity(1,1) constraint pk_ses_id primary key,
	_ses_email varchar(500) constraint default_ses_email default '',
	_ses_token varchar(max) constraint default_ses_token default '',
	_ses_account ntext constraint default_ses_account default '',
	_ses_cart ntext constraint default_ses_cart default '',
	_ses_search ntext constraint default_ses_search default '',
	_ses_created_date datetime constraint default_ses_created_date default getdate(),
	_ses_updated_date datetime constraint default_ses_updated_date default getdate()
)

USE [master]
GO
ALTER DATABASE [TestJava] SET  READ_WRITE 
GO
