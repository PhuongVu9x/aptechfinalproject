create function Fun_GetBills() returns table 
as
	return (
		select * 
		from Bill b,Customer c 
		where b._bil_cus_id = c._cus_id
	)

go
create proc Sp_Bill_Insert
@_bil_cus_id int,
@_bil_payment decimal,
@_bil_created_date datetime
as
begin
	Declare @tmp TABLE(
		_bil_id int
	)
	insert into Bill(_bil_cus_id,_bil_payment,_bil_created_date,_bil_updated_date,_bil_status)
	OUTPUT  inserted._bil_id INTO @tmp
	values (@_bil_cus_id,@_bil_payment,@_bil_created_date,@_bil_created_date,1)

	declare @Id int = (select top 1 _bil_id from @tmp)
	select top 1 * from dbo.Fun_GetBills() where _bil_id = @Id
end

go
alter proc Sp_Bill_Gets
@_cus_full_name nvarchar(500) = null,
@_bil_created_date datetime 
as
begin
	select * from dbo.Fun_GetBills()
	where
		(dbo.IsNullOrEmpty(@_cus_full_name) = 1 
		or dob.StringContain(_cus_full_name,@_cus_full_name) = 1
		or _cus_full_name like @_cus_full_name)
	and (@_bil_created_date is null or _bil_created_date > = @_bil_created_date)
	order by _bil_updated_date desc
end

go
create proc  Sp_Bill_Info
@_bil_id int
as
begin
	select * from dbo.Fun_GetBills()
	where _bil_id = @_bil_id
end

go
create proc Sp_Bill_ChangeStatus
@_bil_id int,
@_bil_status bit
as
begin
	update Bill
	set _bil_status = @_bil_status
	where _bil_id = @_bil_id

	select * from dbo.Fun_Get_Bills()
	where _bil_id = @_bil_id
end

select * from member