
insert into roomType(_rt_name,_rt_max_customer) values
('Single',1),('Delux Single',1),
('Double',2),('Delux Double',2),
('Triple',3),('Delux Triple',3)

select * from furniture
insert into furniture(_fur_name) values
('Private bathroom'),('Personal hygiene kit'),
('Standing shower faucet'),('Hot water'),
('Free bottled water'),('Coffee/tea maker'),
('Mini Bar'),('Tv'),
('Hairdryer'),('Air-conditioner')

select * from policy
insert into policy(_po_name,_po_description) values
('1','No refunds'),('2','Free Wifi'),
('3','No smoking'),('4','Free cancellation'),
('5','Can reschedule'),('6','No refunds'),
('7','No reschedule')