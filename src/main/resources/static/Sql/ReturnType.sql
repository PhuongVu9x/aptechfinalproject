use TestJava
alter function Fun_Get_ReturnType() returns table 
as
	return (
		select * from ReturnType
	)

go
alter proc Sp_ReturnType_Insert
@_ret_name nvarchar(100),
@_ret_percent int,
@_ret_created_date datetime
as
begin
	Declare @tmp TABLE(
		_ret_id int
	)
	insert into ReturnType(_ret_name,_ret_percent,_ret_created_date,
				_ret_updated_date,_ret_status)
	OUTPUT  inserted._ret_id INTO @tmp
	values (@_ret_name,@_ret_percent,@_ret_created_date,@_ret_created_date,1)

	declare @Id int = (select top 1 _ret_id from @tmp)
	select top 1 * from dbo.Fun_Get_ReturnType() where _ret_id = @Id
end

go
alter proc Sp_ReturnType_Gets
@_ret_name nvarchar(500) = null
as
begin
	select * from dbo.Fun_Get_ReturnType()
	where 
		(dbo.IsNullOrEmpty(@_ret_name) = 1 
		or dbo.StringContain(_ret_name,@_ret_name) = 1
		or _ret_name like @_ret_name)
	order by _ret_updated_date desc
end
go
alter proc Sp_ReturnType_Update
@_ret_id int,
@_ret_name varchar(1000),
@_ret_percent int,
@_ret_updated_date datetime
as
begin
	update ReturnType
	set _ret_name = @_ret_name,_ret_percent = @_ret_percent,
		_ret_updated_date = @_ret_updated_date
	where _ret_id = @_ret_id

	select * from dbo.Fun_Get_ReturnType()
	where _ret_id = @_ret_id
end

go
alter proc Sp_ReturnType_ChangeStatus
@_ret_id int,
@_ret_status bit,
@_ret_updated_date datetime
as
begin
	update ReturnType
	set _ret_status = @_ret_status,
		_ret_updated_date = @_ret_updated_date
	where _ret_id = @_ret_id

	select * from dbo.Fun_Get_ReturnType()
	where _ret_id = @_ret_id
end