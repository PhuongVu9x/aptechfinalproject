use TestJava
go
create proc  Sp_Airport_Info
@_ap_id int
as
begin
	select * from dbo.Fun_Get_Airports()
	where _ap_id = @_ap_id
end