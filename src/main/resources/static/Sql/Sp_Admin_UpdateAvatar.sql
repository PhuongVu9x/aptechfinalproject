use TestJava
go
alter proc Sp_Admin_UpdateAvatar
@ad_id int,
@ad_avatar varchar(1000),
@ad_updated_date datetime
as
begin
	update [Admin]
	set _ad_avatar = @ad_avatar,
		 _ad_updated_date = @ad_updated_date
	where _ad_id = @ad_id

	select * from dbo.Fun_Get_Admins()
	where _ad_id = @ad_id
end