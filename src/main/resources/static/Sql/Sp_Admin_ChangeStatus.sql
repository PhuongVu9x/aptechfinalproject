use TestJava
go
alter proc Sp_Admin_ChangeStatus
@ad_id int,
@ad_status bit,
@ad_updated_date datetime
as
begin
	update [Admin] 
	set _ad_status = @ad_status,_ad_updated_date = @ad_updated_date
	where _ad_id = @ad_id

	select * from dbo.Fun_Get_Admins()
	where _ad_id = @ad_id and _ad_status = @ad_status
end

update [Admin]
set _ad_avatar = 'http://localhost:8080/images/default_avatar.jpg'