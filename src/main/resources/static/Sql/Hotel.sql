use TestJava
alter function Fun_Get_Hotels() returns table 
as
	return (
		select *
		from [Hotel] h, City c , Country ct 
		where h._ho_city_id = c._city_id
		and c._city_ct_id = ct._ct_id
	)
go

alter proc Sp_Hotel_Insert
@_ho_name nvarchar(100),
@_ho_city_id int,
@_ho_rooms int,
@_ho_single_rooms int,
@_ho_double_rooms int,
@_ho_triple_rooms int,
@_ho_stars int,
@_ho_address nvarchar(max),
@_ho_from_price decimal,
@_ho_to_price decimal,
@_ho_icon nvarchar(max),
@_ho_background nvarchar(max),
@_ho_created_date datetime
as
begin
	Declare @tmp TABLE(
		_ho_id int
	)
	insert into [Hotel](_ho_name,_ho_city_id,_ho_rooms,_ho_single_rooms,
						_ho_double_rooms,_ho_triple_rooms,_ho_stars,
						_ho_address,_ho_from_price,_ho_to_price,
						_ho_icon,_ho_background,
						_ho_updated_date,_ho_status)
	OUTPUT  inserted._ho_id INTO @tmp
	values (@_ho_name,@_ho_city_id,@_ho_rooms,@_ho_single_rooms,
			@_ho_double_rooms,@_ho_triple_rooms,@_ho_stars,
			@_ho_address,@_ho_from_price,@_ho_to_price,
			@_ho_icon,@_ho_background,
			@_ho_created_date,1)

	declare @Id int = (select top 1 _ho_id from @tmp)
	select top 1 * from dbo.Fun_Get_Hotels() where _ho_id = @Id
end

go
alter proc Sp_Hotel_Gets
@_ho_name nvarchar(100) = null,
@_ho_city_id varchar(1000) = 0,
@_ho_rooms int = 0,
@_ho_single_rooms int = 0,
@_ho_double_rooms int = 0,
@_ho_triple_rooms int = 0,
@_ho_stars int = 0,
@_ho_address nvarchar(max) = null,
@_ho_from_price decimal = 0,
@_ho_to_price decimal = 0
as
begin
	select * from dbo.Fun_Get_Hotels()
	where 
		(dbo.IsNullOrEmpty(@_ho_name) = 1 
		or dbo.StringContain(_ho_name,@_ho_name) = 1
		or _ho_name like @_ho_name)
	and	(@_ho_city_id = 0 or _ho_city_id = @_ho_city_id)
	and	(@_ho_rooms = 0 or _ho_rooms >= @_ho_rooms)
	and	(@_ho_single_rooms = 0 or _ho_single_rooms >= @_ho_single_rooms)
	and	(@_ho_double_rooms = 0 or _ho_double_rooms >= @_ho_double_rooms)
	and	(@_ho_triple_rooms = 0 or _ho_triple_rooms >= @_ho_triple_rooms)
	and	(@_ho_stars = 0 or _ho_stars >= @_ho_stars)
	and (dbo.IsNullOrEmpty(@_ho_address) = 1 
		or dbo.StringContain(_ho_address,@_ho_address) = 1
		or _ho_address like @_ho_address)
	and	(@_ho_from_price = 0 or _ho_from_price >= @_ho_from_price)
	and	(@_ho_to_price = 0 or _ho_to_price >= @_ho_to_price)
	order by _ho_updated_date desc
end
select * from hotel
exec Sp_Hotel_Gets
go
create proc  Sp_Hotel_Info
@_ho_id int
as
begin
	select * from dbo.Fun_Get_Hotels()
	where _ho_id = @_ho_id
end

go
create proc Sp_Hotel_Update
@_ho_id int,
@_ho_name nvarchar(100),
@_ho_city_id varchar(1000),
@_ho_rooms int,
@_ho_single_rooms int,
@_ho_double_rooms int,
@_ho_triple_rooms int,
@_ho_stars int,
@_ho_address nvarchar(max),
@_ho_from_price decimal,
@_ho_to_price decimal,
@_ho_updated_date datetime
as
begin
	update [Hotel]
	set _ho_name = @_ho_name,_ho_city_id = @_ho_city_id,
		_ho_rooms = @_ho_rooms,_ho_signle_rooms = @_ho_single_rooms,
		_ho_double_rooms = @_ho_double_rooms,
		_ho_triple_rooms = @_ho_triple_rooms,_ho_stars = @_ho_stars,
		_ho_address = @_ho_address,_ho_from_price = @_ho_from_price,
		_ho_to_price = @_ho_to_price,_ho_updated_date = @_ho_updated_date
	where _ho_id = @_ho_id

	select * from dbo.Fun_Get_Hotels()
	where _ho_id = @_ho_id
end
GO
create proc Sp_Hotel_UpdateIcon
@_ho_id int,
@_ho_icon varchar(1000)
as
begin
	update Hotel
	set _ho_icon = @_ho_icon
	where _ho_id = @_ho_id

	select * from dbo.Fun_Get_Hotels()
	where _ho_id = @_ho_id
end

GO
create proc Sp_Hotel_UpdateBackground
@_ho_id int,
@_ho_background varchar(1000)
as
begin
	update Hotel
	set _ho_background = @_ho_background
	where _ho_id = @_ho_id

	select * from dbo.Fun_Get_Hotels()
	where _ho_id = @_ho_id
end
go
create proc Sp_Hotel_ChangeStatus
@_ho_id int,
@_ho_status bit
as
begin
	update [Hotel]
	set _ho_status = @_ho_status
	where _ho_id = @_ho_id

	select * from dbo.Fun_Get_Hotels()
	where _ho_id = @_ho_id
end