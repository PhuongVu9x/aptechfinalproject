USE [TestJava]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Admin_UpdateAvatar]    Script Date: 8/29/2023 6:23:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_Ariport_UpdateIcon]
@_ap_id int,
@_ap_icon varchar(1000)
as
begin
	update AirPort
	set _ap_icon = @_ap_icon
	where _ap_id = @_ap_id

	select * from dbo.Fun_Get_AirPorts()
	where _ap_id = @_ap_id
end