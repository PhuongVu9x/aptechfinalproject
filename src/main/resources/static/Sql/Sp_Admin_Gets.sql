use TestJava
go
select * from [Admin]

alter proc Sp_Admin_Gets
@ad_first_name nvarchar(500) = null,
@ad_last_name nvarchar(500) = null,
@ad_full_name nvarchar(1000) = null,
@ad_email varchar(500) = null,
@ad_dob datetime = null,
@ad_phone varchar(50) = null,
@pos_name nvarchar(100)  = null
as
begin
	select * from dbo.Fun_Get_Admins()
	where 
		(dbo.IsNullOrEmpty(@ad_first_name) = 1 
		or dbo.StringContain(_ad_first_name,@ad_first_name) = 1
		or _ad_first_name like @ad_first_name)
	or (dbo.IsNullOrEmpty(@ad_last_name) = 1 
		or dbo.StringContain(_ad_last_name,@ad_last_name) = 1
		or _ad_last_name like @ad_last_name)
	or (dbo.IsNullOrEmpty(@ad_full_name) = 1 
		or dbo.StringContain(_ad_full_name,@ad_full_name) = 1  
		or _ad_full_name like @ad_full_name)
	or (dbo.IsNullOrEmpty(@ad_email) = 1 
		or _ad_email like @ad_email)
	or (@ad_dob is null 
		or _ad_dob like @ad_dob)
	or (dbo.IsNullOrEmpty(@ad_phone) = 1 
		or dbo.StringContain(_ad_phone,@ad_phone) = 1  
		or _ad_phone like @ad_phone)
	or (dbo.IsNullOrEmpty(@ad_phone) = 1 
		or dbo.StringContain(_pos_name,@pos_name) = 1  
		or _pos_name like @pos_name)
end