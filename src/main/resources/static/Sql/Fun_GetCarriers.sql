alter function Fun_Get_Carriers() returns table
as
	return (
		select * 
		from Carrier c,City ci
		where c._car_city_id = ci._city_id
	)