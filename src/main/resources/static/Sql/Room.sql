use TestJava
create function Fun_Get_Rooms() returns table 
as
	return (
		select *
		from Room r, Room h,RoomType rt
		where r._ro_ho_id = h._ho_id and r._ro_room_type_id = rt._rt_id
	)
go
alter proc Sp_Room_Insert
@_ro_code nvarchar(100),
@_ro_ho_id int,
@_ro_room_type_id int,
@_ro_price decimal,
@_ro_furnitures nvarchar(max),
@_ro_policies nvarchar(max),
@_ro_icon nvarchar(max),
@_ro_images nvarchar(max),
@_ro_created_date datetime
as
begin
	Declare @tmp TABLE(
		_ro_id int
	)
	insert into Room(_ro_code,_ro_ho_id,_ro_room_type_id,
					_ro_price,_ro_furnitures,_ro_policies,
					_ro_icon,_ro_images,_ro_created_date,_ro_updated_date,_ro_status)
	OUTPUT  inserted._ro_id INTO @tmp
	values (@_ro_code,@_ro_ho_id,@_ro_room_type_id,
			@_ro_price,@_ro_furnitures,@_ro_policies,
			@_ro_icon,@_ro_images,@_ro_created_date,@_ro_created_date,1)

	declare @Id int = (select top 1 _ro_id from @tmp)
	select top 1 * from dbo.Fun_Get_Rooms() where _ro_id = @Id
end

go
alter proc Sp_Room_Gets
@_ro_code nvarchar(100) = null,
@_ro_ho_id int = 0,
@_ro_room_type_id int = 0,
@_ro_price decimal = 0,
@_ro_furnitures nvarchar(max) = null,
@_ro_policies nvarchar(max) = null
as
begin
	select * from dbo.Fun_Get_Rooms()
	where 
		(dbo.IsNullOrEmpty(@_ro_code) = 1 
		or dob.StringContain(_ro_code,@_ro_code) = 1
		or _ro_code like @_ro_code)
	and	(@_ro_ho_id = 0 or _ro_ho_id = @_ro_ho_id)
	and	(@_ro_room_type_id = 0 or _ro_room_type_id >= @_ro_room_type_id)
	and	(@_ro_price = 0 or _ro_price >= @_ro_price)
	and (dbo.IsNullOrEmpty(@_ro_furnitures) = 1 
		or dob.StringContain(_ro_furnitures,@_ro_furnitures) = 1
		or _ro_furnitures like @_ro_furnitures)
	and (dbo.IsNullOrEmpty(@_ro_policies) = 1 
		or dob.StringContain(_ro_policies,@_ro_policies) = 1
		or _ro_policies like @_ro_policies)
	order by _ro_updated_date desc
end

go
create proc  Sp_Room_Info
@_ro_id int
as
begin
	select * from dbo.Fun_Get_Rooms()
	where _ro_id = @_ro_id
end

go
create proc Sp_Room_Update
@_ro_id int,
@_ro_code nvarchar(100),
@_ro_ho_id int,
@_ro_room_type_id int,
@_ro_price decimal,
@_ro_furnitures nvarchar(max),
@_ro_policies nvarchar(max),
@_ro_updated_date datetime
as
begin
	update Room
	set _ro_code = @_ro_code,_ro_ho_id = @_ro_ho_id,
		_ro_room_type_id = @_ro_room_type_id,_ro_price = @_ro_price,
		_ro_furnitures = _ro_furnitures,_ro_policies = @_ro_policies,
		_ro_updated_date = @_ro_updated_date
	where _ro_id = @_ro_id

	select * from dbo.Fun_Get_Rooms()
	where _ro_id = @_ro_id
end

GO
create proc Sp_Room_UpdateIcon
@_ro_id int,
@_ro_icon varchar(1000)
as
begin
	update Room
	set _ro_icon = @_ro_icon
	where _ro_id = @_ro_id

	select * from dbo.Fun_Get_Rooms()
	where _ro_id = @_ro_id
end

GO
create proc Sp_Room_UpdateBackground
@_ro_id int,
@_ro_images varchar(1000)
as
begin
	update Room
	set _ro_images = @_ro_images
	where _ro_id = @_ro_id

	select * from dbo.Fun_Get_Rooms()
	where _ro_id = @_ro_id
end
go
create proc Sp_Room_ChangeStatus
@_ro_id int,
@_ro_status bit
as
begin
	update Room
	set _ro_status = @_ro_status
	where _ro_id = @_ro_id

	select * from dbo.Fun_Get_Rooms()
	where _ro_id = @_ro_id
end