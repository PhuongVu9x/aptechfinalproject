select * from hotel
declare @index int = 0
while @index < 20
	begin
		declare @namelengh int = ((SELECT floor(rand() * (5 - 2)) + 2));
		declare @name varchar(10) = (select dbo.GenerateToken(crypt_gen_random(@namelengh)))
		declare @cityId int = ((SELECT floor(rand() * (99 - 2)) + 2));
		declare @signle int = (SELECT floor(rand() * (100 - 1)) + 1);
		declare @double int = (SELECT floor(rand() * (50 - 1)) + 1);
		declare @triple int = (SELECT floor(rand() * (20 - 1)) + 1);
		declare @rooms int = @signle + @double + @triple
		declare @star int = (SELECT floor(rand() * (6 - 1)) + 1);
		declare @addresslengh int = ((SELECT floor(rand() * (30 - 20)) + 20));
		declare @address varchar(10) = (select dbo.GenerateToken(crypt_gen_random(@addresslengh)))
		declare @fromprice decimal = (SELECT floor(rand() * (1000000 - 500000)) + 500000);
		declare @toprice decimal = (SELECT floor(rand() * (5000000 - 1000000)) + 1000000);
		
		insert into Hotel(_ho_name,_ho_city_id,_ho_signle_rooms,_ho_double_rooms,_ho_triple_rooms,_ho_rooms,
							_ho_stars,_ho_address,_ho_from_price,_ho_to_price)
		values (@name,@cityId,@signle,@double,@triple,@rooms,
				@star,@address,@fromprice,@toprice)

		set @index = @index+1
	end

	select * from hotel