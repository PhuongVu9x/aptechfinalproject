use TestJava

alter function Fun_Get_Planes() returns table 
as
	return (
		select *
		from [Plane] p, Carrier c, PlaneType pt
		where p._pl_carrier_id = c._car_id
		and p._pl_pt_id = pt._pt_id
	)
go
alter table Plane
add _pl_pt_id int constraint fk_pl_type_id references PlaneType(_pt_id)

alter proc Sp_Plane_Insert
@_pl_code nvarchar(100),
@_pl_carrier_id varchar(1000),
@_pl_seats int,
@_pl_economy_seats int,
@_pl_special_economy_seats int,
@_pl_business_seats int,
@_pl_first_class_seats int,
@_pl_created_date datetime
as
begin
	Declare @tmp TABLE(
		_pl_id int
	)
	insert into [Plane](_pl_code,_pl_carrier_id,_pl_seats,_pl_economy_seats,
						_pl_special_economy_seats,_pl_business_seats,_pl_first_class_seats,
						_pl_created_date,_pl_updated_date,_pl_status)
	OUTPUT  inserted._pl_id INTO @tmp
	values (@_pl_code,@_pl_carrier_id,@_pl_seats,@_pl_economy_seats,
			@_pl_special_economy_seats,@_pl_business_seats,@_pl_first_class_seats,
			@_pl_created_date,@_pl_created_date,1)

	declare @Id int = (select top 1 _pl_id from @tmp)
	select top 1 * from dbo.Fun_Get_Planes() where _pl_id = @Id
end
exec Sp_Plane_Gets
go
alter proc Sp_Plane_Gets
@_pl_code nvarchar(100) = null,
@_pl_carrier_id int = 0,
@_pt_es_quantity int = 0,
@_pt_sc_quantity int = 0,
@_pt_bs_quantity int = 0,
@_pt_fc_quantity int = 0
as
begin
	select * from dbo.Fun_Get_Planes()
	where 
		(dbo.IsNullOrEmpty(@_pl_code) = 1 
		or dbo.StringContain(_pl_code,@_pl_code) = 1
		or _pl_code like @_pl_code)
	and	(@_pl_carrier_id = 0 or _pl_carrier_id = @_pl_carrier_id)
	and	(@_pt_es_quantity = 0 or _pt_ec_quantity >= @_pt_es_quantity)
	and	(@_pt_sc_quantity  = 0 or _pt_sc_quantity  >= @_pt_sc_quantity )
	and	(@_pt_bs_quantity  = 0 or _pt_bs_quantity  >= @_pt_bs_quantity )
	and	(@_pt_fc_quantity  = 0 or _pt_fc_quantity  >= @_pt_fc_quantity )
	order by _pl_updated_date desc
end

go
create proc  Sp_Plane_Info
@_pl_id int
as
begin
	select * from dbo.Fun_Get_Planes()
	where _pl_id = @_pl_id
end

go
alter proc Sp_Plane_Update
@_pl_id int,
@_pl_code nvarchar(100),
@_pl_carrier_id varchar(1000),
@_pl_seats int,
@_pl_economy_seats int,
@_pl_special_economy_seats int,
@_pl_business_seats int,
@_pl_first_class_seats int,
@_pl_updated_date datetime
as
begin
	update [Plane]
	set _pl_code = @_pl_code,_pl_carrier_id = @_pl_carrier_id,
		_pl_seats = @_pl_seats,_pl_economy_seats = @_pl_economy_seats,
		_pl_special_economy_seats = @_pl_special_economy_seats,
		_pl_business_seats = @_pl_business_seats,
		_pl_first_class_seats = @_pl_first_class_seats,
		_pl_updated_date = @_pl_updated_date
	where _pl_id = @_pl_id

	select * from dbo.Fun_Get_Planes()
	where _pl_id = @_pl_id
end

go
alter proc Sp_Plane_ChangeStatus
@_pl_id int,
@_pl_status bit,
@_pl_updated_date datetime
as
begin
	update [Plane]
	set _pl_status = @_pl_status,
		_pl_updated_date = @_pl_updated_date
	where _pl_id = @_pl_id

	select * from dbo.Fun_Get_Planes()
	where _pl_id = @_pl_id
end