use TestJava
go
alter proc Sp_Airport_Insert
@_ap_name nvarchar(100),
@_ap_icon varchar(1000),
@_ap_backgound varchar(1000),
@_ap_city_id int,
@_ap_created_date datetime
as
begin
	Declare @tmp TABLE(
		_ap_id int
	)
	insert into AirPort(_ap_name,_ap_icon,_ap_backgound,_ap_city_id,_ap_created_date,_ap_updated_date,_ap_status)
	OUTPUT  inserted._ap_id INTO @tmp
	values (@_ap_name,@_ap_icon,@_ap_backgound,@_ap_city_id,@_ap_created_date,@_ap_created_date,1)

	declare @Id int = (select top 1 _ap_id from @tmp)
	select top 1 * from dbo.Fun_Get_Airports() where _ap_id = @Id
end
select * from dbo.Fun_Get_Airports()