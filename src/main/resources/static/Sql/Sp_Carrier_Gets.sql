use TestJava
go
alter proc Sp_Carrier_Gets
@_car_name nvarchar(500) = null
as
begin
	select * from dbo.Fun_Get_Carriers()
	where 
		(dbo.IsNullOrEmpty(@_car_name) = 1 
		or dbo.StringContain(_car_name,@_car_name) = 1
		or _car_name like @_car_name)
	
	order by _car_updated_date desc
end

alter proc Sp_Carrier_GetsAllByPlane
as
begin
	select distinct c._car_id,c._car_name,c._car_status from dbo.Fun_Get_Carriers() c
	join Plane p on(c._car_id = p._pl_carrier_id)
	group by c._car_id,c._car_name,c._car_status
	order by c._car_name
end
