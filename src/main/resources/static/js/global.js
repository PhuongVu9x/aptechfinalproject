const errorTempalte = $("<span style='color: red;'></span>")
const dateFormat = "YYYY-MM-DD HH:mm";
const dayTimestamp = 60 * 60 * 24 * 1000;
const backgroundColor = ['rgb(255,0,0)', 'rgb(255,165,0)', 'rgb(255,255,0)', 'rgb(0,250,154)',
	'rgb(0,255,255)', 'rgb(30,144,255)', 'rgb(138,43,226)', 'rgb(238,130,238)',
	'rgb(255,250,205)', 'rgb(112,128,144)', 'rgb(240,255,255)', 'rgb(245,245,245)'];
const CheckPermision = () => {
	const $leftNav = $(".sidebar-nav");
	const currentAdmin = getLocal("admin");
	const $welcomeMes = $("#welcome_mes");
	const $menu_name = $("#menu_name");
	const $menu_icon = $("#menu_icon");
	if ($welcomeMes[0])
		$welcomeMes[0].innerHTML = `Hi ` + currentAdmin._ad_full_name + ` . Welcome To Admin Management Tools.`;
	if ($menu_name[0])
		$menu_name[0].innerHTML = currentAdmin._ad_full_name;
	if ($menu_icon[0])
		$menu_icon.attr('src', currentAdmin._ad_avatar);
	for (var ele of $leftNav) {
		const elePermision = $(ele).data("permision");
		const adminPermision = currentAdmin._per_id;
		if ((adminPermision == 1 && elePermision == "super_admin") ||
			(adminPermision == 2 && elePermision == "flight_admin") ||
			(adminPermision == 3 && elePermision == "hotel_admin")
		) {
			ele.hidden = false;
		}
	}
}

const ErrorId = ($container) => {
	return $container[0].id + "-error";
}

const AddError = ($container, mes) => {
	const container = $container[0];
	const parent = container.parentElement;
	const lastChild = parent.lastElementChild
	const errorTempalte = $("<span style='color: red;'></span>")
	errorTempalte[0].innerHTML = mes;
	errorTempalte[0].id = ErrorId($container);
	container.classList.add("error-input");

	if (lastChild.id != ErrorId($container)) {
		parent.appendChild(errorTempalte[0]);
	} else {
		lastChild.innerHTML = mes;
	}

}

const RemoveError = ($container) => {
	const container = $container[0];
	const parent = $container[0].parentElement;
	const lastChild = parent.lastElementChild
	container.classList.remove("error-input");
	if (lastChild.id == ErrorId($container)) {
		parent.removeChild(lastChild);
	}
}

const AddSpiner = ($container) => {
	const loading = $("<div class='d-flex align-items-center ms-2'> " +
		"<div class='spinner-border spinner-border-sm'' role='status'' aria-hidden='true''></div>" +
		"</div>");
	$container.appendChild(loading[0]);

	for (let className of $container.classList) {
		if (className == 'btn') {
			$container.disabled = true;
			break;
		}
	}
}

const BlockUI = () => {
	$("#blockUi")[0].removeAttribute("hidden", false)
}

const UnBlockUI = () => {
	$("#blockUi")[0].setAttribute("hidden", true)
}

const RemoveSpiner = ($container) => {
	const container = $container[0];
	$container.removeChild($container.lastElementChild);
	for (let className of $container.classList) {
		if (className == 'btn') {
			$container.disabled = false;
			break;
		}
	}
}

const setLocal = (title, data) => {
	localStorage.setItem(title, JSON.stringify(data));
}

const getLocal = (title) => {
	return JSON.parse(JSON.parse(localStorage.getItem(title)))
}

const removeLocal = (title) => {
	localStorage.removeItem(title)
}

const setupSelect = ($select, value) => {
	for (var ele of $select[0].options) {
		if (ele.value == value) {

			ele.selected = true;
			break;
		}
	}
}

const getPosition = (id) => {
	for (var item of getLocal("position").data) {
		if (item._pos_id == id) {
			return item;
		}
	}
}
const getPermision = (id) => {
	for (var item of getLocal("permision").data) {
		if (item._per_id == id) {
			return item;
		}
	}
}

const checkNumber = ($ele, minMes, maxMes) => {
	const ele = $ele.val();
	const min = parseInt($ele[0].min);
	const max = parseInt($ele[0].max);
	let check = false;
	if (ele > max) {
		$ele.val(max);
		if (maxMes)
			AddError($ele, maxMes);
	}
	else if (ele <= min) {
		$ele.val(min);
		if (minMes)
			AddError($ele, minMes);
	}
	else {
		RemoveError($ele, minMes);
		check = true;
	}
	return check;
}

const getDate = (date) => {
	return moment(date);
}

const toVnd = (money) => {
	return Number(money.toFixed(0)).toLocaleString() + " VND";
	//return money.toLocaleString('vi-VN').fixed(0) + " VND";
}
const onLogOut = () => {
	const admin = getLocal("admin");
	BlockUI();
	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: "/api/admin/logout",
		data: JSON.stringify(admin),
		dataType: 'json',
		timeout: 100000,
		success: function(data) {
			UnBlockUI();
			removeLocal("admin");
			location.href = "http://localhost:8080/admin/login";
		},
		error: function(e) {
			UnBlockUI();
			removeLocal("admin");
			location.href = "http://localhost:8080/admin/login";
		}
	});
}

const $tabControl = $("#ex-with-icons");
const $tabPane = $(".tab-pane");
const onSelectTab = (event) => {
	const controls = event.getAttribute('aria-controls');
	setLocal("lastTabControl", JSON.stringify(controls));
}
const SetupDefault = () => {
	const lastTabControl = getLocal("lastTabControl");
	for (var index = 0; index < $tabControl[0].children.length; index++) {
		const a = $tabControl[0].children[index].firstElementChild;
		const tabPane = $tabPane[index];
		const control = a.getAttribute('aria-controls');
		var selected = false;
		if (lastTabControl == control) {
			console.log()
			selected = true;
			$(a).addClass('active');
			$(tabPane).addClass('active');
			$(tabPane).addClass('show');
		} else {
			$(a).removeClass('active');
			$(tabPane).removeClass('active');
			$(tabPane).removeClass('show');
		}
		a.setAttribute('aria-selected', selected);
	}
}

const successToast = (mes) =>{
	var index = getLocal('toastIndex');
		var toastTemplate = `
				<div style="background-color:green;" class="toast liveToast${index}" role="alert" aria-live="assertive" aria-atomic="true">
					<div class="toast-body">
						<span style='color:white'>${mes}</span>
						<button type="button" class="btn-close float-end" data-bs-dismiss="toast"
							aria-label="Close"></button>
					</div>
				</div>`
		$('.toast-container')[0].innerHTML += toastTemplate;
		var toastLiveExample = $(`.liveToast${index}`);
		var toast = new bootstrap.Toast(toastLiveExample);
		toast.show()
		index++;
		setLocal('toastIndex',JSON.stringify(index))
}

const failToast = (mes) =>{
	var index = getLocal('toastIndex');
		var toastTemplate = `
				<div style="background-color:red;" class="toast liveToast${index}" role="alert" aria-live="assertive" aria-atomic="true">
					<div class="toast-body">
						<span style='color:white'>${mes}</span>
						<button type="button" class="btn-close float-end" data-bs-dismiss="toast"
							aria-label="Close"></button>
					</div>
				</div>`
		$('.toast-container')[0].innerHTML += toastTemplate;
		var toastLiveExample = $(`.liveToast${index}`);
		var toast = new bootstrap.Toast(toastLiveExample);
		toast.show()
		index++;
		setLocal('toastIndex',JSON.stringify(index))
}

const reloadPage = () =>{
	location.reload();
}
