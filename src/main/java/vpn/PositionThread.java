package vpn;

import java.sql.ResultSet;
import java.util.LinkedList;

import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.PositionDTO;

public class PositionThread extends Thread {
	public static LinkedList<PositionDTO> positions;

	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		try {
			ResultSet rsPosition = DBConnection.CallProc("Sp_Position_Gets");
			positions =  Mapper.MapTo(new PositionDTO(), rsPosition);
			System.out.println(AppThread.CountDown + ": Position All Done.");
			AppThread.CountDown--;
			if(AppThread.CountDown < 0)
				AppThread.CountDown = 0;
			this.interrupt();
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
