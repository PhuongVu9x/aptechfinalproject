package vpn;

import java.sql.ResultSet;
import java.util.LinkedList;

import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.PlaneTypeDTO;
import vpn.model.PositionDTO;

public class PlaneTypeThread extends Thread{
	public static LinkedList<PlaneTypeDTO> planeTypes;

	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		try {
			var rsPlaneType = DBConnection.CallProc("Sp_PlaneType_Gets");
			planeTypes = Mapper.MapTo(new PlaneTypeDTO(), rsPlaneType);
			System.out.println(AppThread.CountDown + ": Plane Type All Done.");
			AppThread.CountDown--;
			if(AppThread.CountDown < 0)
				AppThread.CountDown = 0;
			this.interrupt();
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
