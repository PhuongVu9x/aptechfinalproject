package vpn;

import java.util.HashMap;
import java.util.LinkedList;

import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.TicketClassDTO;
import vpn.model.TicketDTO;

public class TicketThread extends Thread{
	public static LinkedList<TicketDTO> tickets;
	public static HashMap<String,LinkedList<TicketDTO>> ticketsByUser;
	public void run() {
		SetupDefault();
	}
	
	public void SetupDefault() {
		try {
			var rs = DBConnection.CallProc("Sp_Ticket_Gets");
			tickets = Mapper.MapTo(new TicketDTO(), rs);
			System.out.println(AppThread.CountDown + ": Ticket All Done.");
			AppThread.CountDown--;
			if(AppThread.CountDown < 0) 
				AppThread.CountDown = 0;
			this.interrupt();
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
