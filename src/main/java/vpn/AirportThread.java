package vpn;

import java.util.LinkedList;
import vpn.model.AirportDTO;
import vpn.model.DBConnection;
import vpn.model.Mapper;

public class AirportThread extends Thread {
	public static LinkedList<AirportDTO> airports;

	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		try {
			var rsAirport = DBConnection.CallProc("Sp_Airport_Gets");
			airports = Mapper.MapTo(new AirportDTO(), rsAirport);
			System.out.println(AppThread.CountDown + ": Airport All Done.");
			AppThread.CountDown--;
			if (AppThread.CountDown < 0)
				AppThread.CountDown = 0;
			this.interrupt();
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
