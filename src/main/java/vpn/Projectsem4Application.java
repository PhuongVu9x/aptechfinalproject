package vpn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class Projectsem4Application {

	public static void main(String[] args) throws ClassNotFoundException {
		SpringApplication.run(Projectsem4Application.class, args);
		
		try {
			var adminThread = new AdminThread();
			adminThread.start();
			
			var airportThread = new AirportThread();
			airportThread.start();
			
			var billThread = new BillThread();
			billThread.start();
			
			var bookingThread = new BookingThread();
			bookingThread.start();
			
			var carrierThread = new CarrierThread();
			carrierThread.start();
			
			var categoryThread = new CategoryThread();
			categoryThread.start();

			var cityThread = new CityThread();
			cityThread.start();
			
			var countryThread = new CountryThread();
			countryThread.start();
			
			var customerThread = new CustomerThread();
			customerThread.start();

			var flightThread = new FlightThread();
			flightThread.start();

			var furnitureThread = new FurnitureThread();
			furnitureThread.start();
			
			var hotelThread = new HotelThread();
			hotelThread.start();
			
			var permisionThread = new PermisionThread();
			permisionThread.start();

			var planeThread = new PlaneThread();
			planeThread.start();

			var planeTypeThread = new PlaneTypeThread();
			planeTypeThread.start();
			
			var policyThread = new PolicyThread();
			policyThread.start();
			
			var positionThread = new PositionThread();
			positionThread.start();
			
			var returnTypeThread = new ReturnTypeThread();
			returnTypeThread.start();
			
			var roomThread = new RoomThread();
			roomThread.start();
			
			var roomTypeThread = new RoomTypeThread();
			roomTypeThread.start();
			
			var serviceThread = new ServiceThread();
			serviceThread.start();
			
			var ticketClassThread = new TicketClassThread();
			ticketClassThread.start();
			
			var ticketThread = new TicketThread();
			ticketThread.start();
		} catch (IllegalArgumentException  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
