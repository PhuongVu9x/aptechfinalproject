package vpn;

import java.util.LinkedList;

import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.RoomTypeDTO;

public class RoomTypeThread extends Thread{
	public static LinkedList<RoomTypeDTO> roomTypes;
	
	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		try {
			var rs = DBConnection.CallProc("Sp_RoomType_Gets");
			roomTypes = Mapper.MapTo(new RoomTypeDTO(), rs);
			System.out.println(AppThread.CountDown + ": Room Type All Done.");
			AppThread.CountDown--;
			if (AppThread.CountDown < 0)
				AppThread.CountDown = 0;
			this.interrupt(); 
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
