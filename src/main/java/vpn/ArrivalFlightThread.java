package vpn;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import vpn.model.DBConnection;
import vpn.model.FlightDTO;
import vpn.model.Mapper;

public class ArrivalFlightThread extends Thread{
	public static LinkedList<FlightDTO> flights = new LinkedList<FlightDTO>();

	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		try {
			for (var flight : flights) {
				var params = new LinkedList<String>();
				params.add(flight._fl_id + "");
				params.add("2");
				LocalDateTime updatedDate = LocalDateTime.now();  
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
				params.add(dtf.format(updatedDate));
				var rs = DBConnection.CallProc("Sp_Flight_ChangeStatus", params);
			}

		} catch (ClassNotFoundException | IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
