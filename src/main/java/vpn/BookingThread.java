package vpn;

import java.util.LinkedList;

import vpn.model.BookingDTO;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.RoomTypeDTO;

public class BookingThread extends Thread{
	public static LinkedList<BookingDTO> bookings;
	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		try {
			var rs = DBConnection.CallProc("Sp_Booking_Gets");
			bookings = Mapper.MapTo(new BookingDTO(), rs);
			System.out.println(AppThread.CountDown + ": Booking All Done.");
			AppThread.CountDown--;
			if (AppThread.CountDown < 0)
				AppThread.CountDown = 0;
			this.interrupt(); 
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
