package vpn;

import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.concurrent.Executor;

import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import vpn.model.AdminDTO;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.PermisionDTO;
@EnableAsync
public class AdminThread extends Thread{
	public static LinkedList<AdminDTO> admins;
	
	public void run() {
		SetupDefault();
	}
	
	public void SetupDefault() {
		try {
			var rs = DBConnection.CallProc("Sp_Admin_Gets");
			admins = Mapper.MapTo(new AdminDTO(), rs);
			System.out.println(AppThread.CountDown + ": Admin All Done.");
			
			AppThread.CountDown--;
			if(AppThread.CountDown < 0) {
				AppThread.CountDown = 0;
			}
				
			this.stop();
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
