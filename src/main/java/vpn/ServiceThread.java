package vpn;

import java.util.LinkedList;

import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.ServiceDTO;

public class ServiceThread extends Thread{
public static LinkedList<ServiceDTO> services;
	
	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		try {
			var rs = DBConnection.CallProc("Sp_Service_Gets");
			services = Mapper.MapTo(new ServiceDTO(), rs);
			System.out.println(AppThread.CountDown + ": Service All Done.");
			AppThread.CountDown--;
			if (AppThread.CountDown < 0)
				AppThread.CountDown = 0;
			this.interrupt();
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
