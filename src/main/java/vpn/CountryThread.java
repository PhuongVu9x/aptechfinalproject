package vpn;

import java.sql.ResultSet;
import java.util.LinkedList;

import vpn.model.CountryDTO;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.PositionDTO;

public class CountryThread extends Thread{
	public static LinkedList<CountryDTO> countries;
	
	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		try {
			var rsCountry = DBConnection.CallProc("Sp_Country_Gets");
			countries = Mapper.MapTo(new CountryDTO(), rsCountry);
			System.out.println(AppThread.CountDown + ": Country All Done.");
			AppThread.CountDown--;
			if (AppThread.CountDown < 0)
				AppThread.CountDown = 0;
			this.interrupt();
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
