package vpn;

import java.util.LinkedList;

import vpn.model.AirportDTO;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.PolicyDTO;
import vpn.model.PositionDTO;

public class PolicyThread extends Thread{
	public static LinkedList<PolicyDTO> policies;
	
	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		try {
			var rs = DBConnection.CallProc("Sp_Policy_Gets");
			policies = Mapper.MapTo(new PolicyDTO(), rs);
			System.out.println(AppThread.CountDown + ": Policy All Done.");
			AppThread.CountDown--;
			if (AppThread.CountDown < 0)
				AppThread.CountDown = 0;
			this.interrupt(); 
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
