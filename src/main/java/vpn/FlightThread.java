package vpn;

import java.sql.ResultSet;
import java.util.LinkedList;

import vpn.model.DBConnection;
import vpn.model.FlightDTO;
import vpn.model.Mapper;
import vpn.model.PositionDTO;

public class FlightThread extends Thread{
	public static LinkedList<FlightDTO> flights;
	public static LinkedList<FlightDTO> todayFlights;

	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		try {
			var rsFlight = DBConnection.CallProc("Sp_Flight_Gets");
			flights = Mapper.MapTo(new FlightDTO(), rsFlight);
			if(flights == null )
				flights = new LinkedList<FlightDTO>();
			var rsTodayFlight = DBConnection.CallProc("Sp_Flight_TodayGets");
			todayFlights = Mapper.MapTo(new FlightDTO(), rsTodayFlight);
			
			
			System.out.println(AppThread.CountDown + ": Flight All Done.");
			AppThread.CountDown--;
			if(AppThread.CountDown < 0) 
				AppThread.CountDown = 0;
			this.interrupt();
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private LinkedList<FlightDTO> FillJson(LinkedList<FlightDTO> data){
		var dataReturn = new LinkedList<FlightDTO>();
		for (FlightDTO flightDTO : data) {
			flightDTO._fl_estimate_take_off = AppThread.AddHours(flightDTO._fl_estimate_take_off,7);
			flightDTO._fl_estimate_arrival = AppThread.AddHours(flightDTO._fl_estimate_arrival,7);
			flightDTO._fl_take_off = AppThread.AddHours(flightDTO._fl_take_off,7);
			flightDTO._fl_arrival = AppThread.AddHours(flightDTO._fl_arrival,7);
			flightDTO._fl_created_date = AppThread.AddHours(flightDTO._fl_created_date,7);
			flightDTO._fl_updated_date = AppThread.AddHours(flightDTO._fl_updated_date,7);
			dataReturn.add(flightDTO);
		}
		
		return dataReturn;
	}
}
