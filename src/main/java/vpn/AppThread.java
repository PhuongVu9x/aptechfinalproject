package vpn;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.Locale;

import org.springframework.web.multipart.MultipartFile;

public class AppThread extends Thread{
	public static int CountDown = 23;
	
	public void run() {
		try {
			if(CountDown <= 0) {
				System.out.println(CountDown + ": All Set");
				this.stop();
			}
			this.join(5000);
			if(this.isAlive())
				this.run();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Timestamp AddHours(Timestamp old,int hours) {
		return new Timestamp(old.getTime() + (1000 * 60 * 60 * hours));
	}
	
	public static String ToVnTime(Timestamp timestamp) {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(AddHours(timestamp,0));
	}
	
	public static Timestamp ToVnTimestamp(Timestamp timestamp) {
		return AddHours(timestamp,7);
	}
	
	public static String ToVND(double _payment) {
		Locale locale = new Locale("vi", "VN");
		Currency currency = Currency.getInstance("VND");
		DecimalFormatSymbols df = DecimalFormatSymbols.getInstance(locale);
		df.setCurrency(currency);
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
		numberFormat.setCurrency(currency);
		return numberFormat.format(_payment).substring(0, numberFormat.format(_payment).length() - 1) + " VND";
	}
	
	public static void UpdaloadFile(MultipartFile multipartFile, String serverPath) {
		try {
			byte[] bytes = multipartFile.getBytes();

			File serverFile = new File(serverPath + File.separator + multipartFile.getOriginalFilename());
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
			stream.write(bytes);
			stream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
