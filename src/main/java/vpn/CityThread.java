package vpn;

import java.sql.ResultSet;
import java.util.LinkedList;

import vpn.model.CityDTO;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.PositionDTO;

public class CityThread extends Thread{
	public static LinkedList<CityDTO> cities;
	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		try {
			var rsCity = DBConnection.CallProc("Sp_City_Gets");
			cities = Mapper.MapTo(new CityDTO(), rsCity);
			System.out.println(AppThread.CountDown + ": City All Done.");
			AppThread.CountDown--;
			if(AppThread.CountDown < 0)
				AppThread.CountDown = 0;
			this.interrupt();
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
