package vpn;

import java.util.LinkedList;

import vpn.model.DBConnection;
import vpn.model.HotelDTO;
import vpn.model.Mapper;
import vpn.model.RoomTypeDTO;

public class HotelThread extends Thread{
	public static LinkedList<HotelDTO> hotels;
	
	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		try { 
			var rs = DBConnection.CallProc("Sp_Hotel_Gets");
			hotels = Mapper.MapTo(new HotelDTO(), rs);
			System.out.println(AppThread.CountDown + ": Hotel All Done.");
			AppThread.CountDown--;
			if (AppThread.CountDown < 0)
				AppThread.CountDown = 0;
			this.interrupt(); 
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
