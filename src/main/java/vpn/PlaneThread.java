package vpn;

import java.sql.ResultSet;
import java.util.LinkedList;

import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.PlaneDTO;
import vpn.model.PositionDTO;

public class PlaneThread extends Thread{
	public static LinkedList<PlaneDTO> planes;


	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		try {
			var rsPlane = DBConnection.CallProc("Sp_Plane_Gets");
			planes = Mapper.MapTo(new PlaneDTO(), rsPlane);
			System.out.println(AppThread.CountDown + ": Plane All Done.");
			AppThread.CountDown--;
			if(AppThread.CountDown < 0)
				AppThread.CountDown = 0;
			this.interrupt();
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
