package vpn;

import java.util.LinkedList;

import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.TicketClassDTO;

public class TicketClassThread extends Thread{
	public static LinkedList<TicketClassDTO> ticketClasses;
	
	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		try {
			var rsTicketClass = DBConnection.CallProc("Sp_TicketClass_Gets");
			ticketClasses = Mapper.MapTo(new TicketClassDTO(), rsTicketClass);
			System.out.println(AppThread.CountDown + ": Ticket Class All Done.");
			AppThread.CountDown--;
			if(AppThread.CountDown < 0)
				AppThread.CountDown = 0;
			this.interrupt();
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
