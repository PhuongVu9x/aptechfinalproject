package vpn;

import java.sql.ResultSet;
import java.util.LinkedList;

import vpn.model.CarrierDTO;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.PermisionDTO;

public class CarrierThread extends Thread{
	public static LinkedList<CarrierDTO> carriers;
	public static LinkedList<CarrierDTO> carriersByPlane;
	
	
	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		try {

			var rsCarrier = DBConnection.CallProc("Sp_Carrier_Gets");
			carriers = Mapper.MapTo(new CarrierDTO(), rsCarrier);
			System.out.println(AppThread.CountDown + ": Carrier All Done.");
			AppThread.CountDown--;
			
			var rsCarrierByPlane = DBConnection.CallProc("Sp_Carrier_GetsAllByPlane");
			carriersByPlane = Mapper.MapTo(new CarrierDTO(), rsCarrierByPlane);
			System.out.println(AppThread.CountDown + ": Carrier By Plane All Done.");
			AppThread.CountDown--;
			if(AppThread.CountDown < 0)
				AppThread.CountDown = 0;
			this.interrupt();
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
