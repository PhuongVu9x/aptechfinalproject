package vpn.model;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;

import com.google.gson.Gson;

public class Mapper {
	public static <T> LinkedList<T> MapTo(T obj, ResultSet resultSet)
			throws IllegalArgumentException, IllegalAccessException, InstantiationException {
		var result = new LinkedList<T>();
		ResultSetMetaData rsmd = null;
		try {
			rsmd = resultSet.getMetaData();
			var colCount = rsmd.getColumnCount();
			while (resultSet.next()) {
				var newObj = (T) obj.getClass().newInstance();
				var props = newObj.getClass().getDeclaredFields();
				for (int i = 1; i <= colCount; i++) {
					String colName = rsmd.getColumnName(i);
					Object colVal = resultSet.getObject(i);
					String colType = rsmd.getColumnTypeName(i);
					
					for (var prop : props) {
						if(prop.getName().toString().contains("index")) {
							var index = result.size() + 1;
							prop.setInt(newObj, (int) index);
							continue;
						}
						
						if (prop.getType().toString().equals(GetType(colType)) && prop.getName().toString().equals(colName)) {
							
							switch (prop.getType().toString()) {
							case "int":
								prop.setInt(newObj, (int) colVal);
								break;
							case "double":
								prop.setDouble(newObj, (double) colVal);
								break;
							case "boolean":
								prop.setBoolean(newObj, (boolean) colVal);
								break;
							case "class java.lang.String":
								prop.set(newObj, (String) colVal);
								break;
							case "class java.sql.Timestamp":
								prop.set(newObj, (Timestamp) colVal);
								break;
							}

							break;
						}
					}
				}

				result.add(newObj);
			}

		} catch (SQLException e) {
			// TODO: handle exception
		}
		return result;
	}

	private static String GetType(String type) {
		var result = "";
		switch (type) {
		case "varchar":
		case "nvarchar":
		case "ntext":
		case "text":
			result = "class java.lang.String";
			break;
		case "bit":
			result = "boolean";
			break;
		case "float":
			result = "double";
			break;
		case "double":
			result = "float";
			break;
		case "decimal":
			result = "double";
			break;
		case "datetime":
			result = "class java.sql.Timestamp";
			break;
		default:
			result = type;
			break;
		}

		return result;
	}
	
	public static <T> String ToJson(T result) {
		var gson = new Gson();
		return gson.toJson(result);
	}
}
