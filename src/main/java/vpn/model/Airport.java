package vpn.model;

import java.sql.Timestamp;

public class Airport {
	public int _ap_id;

	public void set_ap_id(int ap_id) {
		_ap_id = ap_id;
	}

	public int get_ap_id() {
		return _ap_id;
	}
	
	public String _ap_name;

	public void set_ap_name(String ap_name) {
		_ap_name = ap_name;
	}

	public String get_ap_name() {
		return _ap_name;
	}
	
	public String _ap_backgound;

	public void set_background(String ap_background) {
		_ap_backgound = ap_background;
	}

	public String get_background() {
		return _ap_backgound;
	}

	public int _ap_city_id;

	public void set_ap_city_id(int ap_city_id) {
		_ap_city_id = ap_city_id;
	}

	public int get_ap_city_id() {
		return _ap_city_id;
	}

	public String _ap_icon;

	public void set_ap_icon(String ap_icon) {
		_ap_icon = ap_icon;
	}

	public String get_ap_icon() {
		return _ap_icon;
	}

	public Timestamp _ap_created_date;

	public void set_field(Timestamp ap_created_date) {
		_ap_created_date = ap_created_date;
	}

	public Timestamp get_field() {
		return _ap_created_date;
	}

	public Timestamp _ap_updated_date;

	public void set_ap_updated_date(Timestamp ap_updated_date) {
		_ap_updated_date = ap_updated_date;
	}

	public Timestamp get_ap_updated_date() {
		return _ap_updated_date;
	}

	public boolean _ap_status;

	public void set_ap_status(boolean ap_status) {
		_ap_status = ap_status;
	}

	public boolean get_ap_status() {
		return _ap_status;
	}
}
