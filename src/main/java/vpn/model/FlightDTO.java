package vpn.model;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.LinkedList;

import vpn.AppThread;

public class FlightDTO {
	public int _fl_id;

	public void set_fl_id(int fl_id) {
		_fl_id = fl_id;
	}

	public int get_fl_id() {
		return _fl_id;
	}

	public int _fl_index;

	public void set_fl_index(int fl_index) {
		_fl_index = fl_index;
	}

	public int get_fl_index() {
		return _fl_index;
	}

	public int _fl_return_id;

	public void set_fl_return_id(int fl_return_id) {
		_fl_return_id = fl_return_id;
	}

	public int get_fl_return_id() {
		return _fl_return_id;
	}

//	-------------- Quantity --------------
	public int _fl_adult_quan;

	public void set_fl_adult_quan(int fl_adult_quan) {
		_fl_adult_quan = fl_adult_quan;
	}

	public int get_fl_adult_quan() {
		return _fl_adult_quan;
	}

	public int _fl_child_quan;

	public void set_fl_child_quan(int fl_child_quan) {
		_fl_child_quan = fl_child_quan;
	}

	public int get_fl_child_quan() {
		return _fl_child_quan;
	}

	public int _fl_infant_quan;

	public void set_fl_infant_quan(int fl_infant_quan) {
		_fl_infant_quan = fl_infant_quan;
	}

	public int get_fl_infant_quan() {
		return _fl_infant_quan;
	}
	
	public int _fl_total_pasg;

	public void set_fl_total_pasg(int fl_total_pasg) {
		_fl_total_pasg = fl_total_pasg;
	}

	public int get_fl_total_pasg() {
		return _fl_total_pasg;
	}

//	-------------- Price --------------
	public double _fl_adult_price;

	public void set_fl_adult_price(double fl_adult_price) {
		_fl_adult_price = fl_adult_price;
	}

	public double get_fl_adult_price() {
		return _fl_adult_price;
	}

	public double _fl_child_price;

	public void set_fl_child_price(double fl_child_price) {
		_fl_child_price = fl_child_price;
	}

	public double get_fl_child_price() {
		return _fl_child_price;
	}

	public double _fl_infant_price;

	public void set_fl_infant_price(double fl_infant_price) {
		_fl_infant_price = fl_infant_price;
	}

	public double get_fl_infant_price() {
		return _fl_infant_price;
	}

	public double _fl_total_price;

	public void set_fl_total_price(double fl_total_price) {
		_fl_total_price = fl_total_price;
	}

	public double get_fl_total_price() {
		return _fl_total_price;
	}

	public double _fl_ec_price;

	public void set_fl_ec_price(double fl_ec_price) {
		_fl_ec_price = fl_ec_price;
	}

	public double get_fl_ec_price() {
		return _fl_ec_price;
	}

	public double _fl_sc_price;

	public void set_fl_sc_price(double fl_sc_price) {
		_fl_sc_price = fl_sc_price;
	}

	public double get_fl_sc_price() {
		return _fl_sc_price;
	}

	public double _fl_bs_price;

	public void set_fl_bs_price(double fl_bs_price) {
		_fl_bs_price = fl_bs_price;
	}

	public double get_fl_bs_price() {
		return _fl_bs_price;
	}

	public double _fl_fc_price;

	public void set_fl_fc_price(double fl_fc_price) {
		_fl_fc_price = fl_fc_price;
	}

	public double get_fl_fc_price() {
		return _fl_fc_price;
	}

	public double _fl_min_price;

	public void set_fl_min_price(double fl_min_price) {
		_fl_min_price = fl_min_price;
	}

	public double get_fl_min_price() {
		return _fl_min_price;
	}

	public double _fl_max_price;

	public void set_fl_max_price(double fl_max_price) {
		_fl_max_price = fl_max_price;
	}

	public double get_fl_max_price() {
		return _fl_max_price;
	}

	public int _fl_status;

	public void set_fl_status(int fl_status) {
		_fl_status = fl_status;
	}

	public int get_fl_status() {
		return _fl_status;
	}

	public int _fl_from_id;

	public void set_fl_from_id(int fl_from_id) {
		_fl_from_id = fl_from_id;
	}

	public int get_fl_from_id() {
		return _fl_from_id;
	}

	public int _fl_to_id;

	public void set_fl_to_id(int fl_to_id) {
		_fl_to_id = fl_to_id;
	}

	public int get_fl_to_id() {
		return _fl_to_id;
	}

	public int _fl_transit_id;

	public void set_fl_transit_id(int fl_transit_id) {
		_fl_transit_id = fl_transit_id;
	}

	public int get_fl_transit_id() {
		return _fl_transit_id;
	}

	public int _fl_plane_id;

	public void set_fl_plane_id(int fl_plane_id) {
		_fl_plane_id = fl_plane_id;
	}

	public int get_fl_plane_id() {
		return _fl_plane_id;
	}

	public Timestamp _fl_estimate_take_off;

	public void set_fl_estimate(Timestamp fl_estimate_take_off) {
		_fl_estimate_take_off = fl_estimate_take_off;
	}

	public Timestamp get_fl_estimate() {
		return _fl_estimate_take_off;
	}

	public Timestamp _fl_estimate_arrival;

	public void set_fl_estimate_arrival(Timestamp fl_estimate_arrival) {
		_fl_estimate_arrival = fl_estimate_arrival;
	}

	public Timestamp get_fl_estimate_arrival() {
		return _fl_estimate_arrival;
	}

	public Timestamp _fl_take_off;

	public void set_fl_take_off(Timestamp fl_take_off) {
		_fl_take_off = fl_take_off;
	}

	public Timestamp get_fl_take_off() {
		return _fl_take_off;
	}

	public Timestamp _fl_arrival;

	public void set_fl_arrival(Timestamp fl_arrival) {
		_fl_arrival = fl_arrival;
	}

	public Timestamp get_fl_arrival() {
		return _fl_arrival;
	}

	public Timestamp _fl_take_off_timestamp;

	public String get_fl_take_off_timestamp() {
		return AppThread.ToVnTime(_fl_take_off);
	}

	public Timestamp _fl_arrival_timestamp;

	public String get_fl_arrival_timestamp() {
		return AppThread.ToVnTime(_fl_arrival);
	}
	
	public Timestamp get_fl_take_off_mobile() {
		return AppThread.ToVnTimestamp(_fl_take_off);
	}
	
	public Timestamp get_fl_arrival_mobile() {
		return AppThread.ToVnTimestamp(_fl_arrival);
	}
	
	public Timestamp _fl_created_timestamp;

	public String get_fl_created_timestamp() {
		return AppThread.ToVnTime(_fl_created_date);
	}

	public Timestamp _fl_updated_timestamp;

	public String get_fl_updated_timestamp() {
		return AppThread.ToVnTime(_fl_updated_date);
	}


	public String _fl_fly_duration;

	public void set_fl_fly_duration(String fl_fly_duration) {
		_fl_fly_duration = fl_fly_duration;
	}

	public String get_fl_fly_duration() {
		return _fl_fly_duration;
	}

	public Timestamp _fl_created_date;

	public void set_fl_created_date(Timestamp fl_created_date) {
		_fl_created_date = fl_created_date;
	}

	public Timestamp get_fl_created_date() {
		return _fl_created_date;
	}

	public Timestamp _fl_updated_date;

	public void set_fl_updated_date(Timestamp fl_updated_date) {
		_fl_updated_date = fl_updated_date;
	}

	public Timestamp get_fl_updated_date() {
		return _fl_updated_date;
	}
	
	public int _ct_from_id;

	public void set_ct_from_id(int ct_from_id) {
		_ct_from_id = ct_from_id;
	}

	public int get_ct_from_id() {
		return _ct_from_id;
	}
	
	public int _ct_to_id;

	public void set_ct_to_id(int ct_to_id) {
		_ct_to_id = ct_to_id;
	}

	public int get_ct_to_id() {
		return _ct_to_id;
	}
	
	public String _ct_from_name;

	public void set_ct_from_name(String ct_from_name) {
		_ct_from_name = ct_from_name;
	}

	public String get_ct_from_name() {
		return _ct_from_name;
	}
	
	public String _ct_to_name;

	public void set_ct_to_name(String ct_to_name) {
		_ct_to_name = ct_to_name;
	}

	public String get_ct_to_name() {
		return _ct_from_name;
	}
	
	public Boolean _fl_domestic;

	public void set_fl_domestic(Boolean fl_domestic) {
		_fl_domestic = fl_domestic;
	}

	public Boolean get_fl_domestic() {
		return _fl_domestic;
	}
	
//	-------------- Airplane --------------
	public int _car_id;

	public void set_car_id(int car_id) {
		_car_id = car_id;
	}

	public int get_car_id() {
		return _car_id;
	}

	public String _car_name;

	public void set_car_name(String car_name) {
		_car_name = car_name;
	}

	public String get_car_name() {
		return _car_name;
	}

	public String _car_icon;

	public void set_car_icon(String car_icon) {
		_car_icon = car_icon;
	}

	public String get_car_icon() {
		return _car_icon;
	}

	public int _car_carbin;

	public void set_car_carbin(int car_carbin) {
		_car_carbin = car_carbin;
	}

	public int get_car_carbin() {
		return _car_carbin;
	}

	public int _car_checked;

	public void set_car_checked(int car_checked) {
		_car_checked = car_checked;
	}

	public int get_car_checked() {
		return _car_checked;
	}

	public String _pl_code;

	public void set_pl_code(String pl_code) {
		_pl_code = pl_code;
	}

	public String get_pl_code() {
		return _pl_code;
	}

	public String _fl_from_name;

	public void set_fl_from_name(String fl_from_name) {
		_fl_from_name = fl_from_name;
	}

	public String get_fl_from_name() {
		return _fl_from_name;
	}

	public String _fl_to_name;

	public void set_fl_to_name(String fl_to_name) {
		_fl_to_name = fl_to_name;
	}

	public String get_fl_to_name() {
		return _fl_to_name;
	}

	public String _fl_from_abbreviation;

	public void set_fl_from_abbreviation(String fl_from_abbreviation) {
		_fl_from_abbreviation = fl_from_abbreviation;
	}

	public String get_fl_from_abbreviation() {
		return _fl_from_abbreviation;
	}

	public String _fl_from_full_name;

	public void set_fl_from_full_name(String fl_from_full_name) {
		_fl_from_full_name = fl_from_full_name;
	}

	public String get_fl_from_full_name() {
		return _fl_from_name + " (" + _fl_from_abbreviation + ")";
	}

	public String _fl_to_abbreviation;

	public void set_fl_to_abbreviation(String fl_to_abbreviation) {
		_fl_to_abbreviation = fl_to_abbreviation;
	}

	public String get_fl_to_abbreviation() {
		return _fl_to_abbreviation;
	}

	public String _fl_to_full_name;

	public void set_fl_to_full_name(String fl_to_full_name) {
		_fl_to_full_name = fl_to_full_name;
	}

	public String get_fl_to_full_name() {
		return _fl_to_name + " (" + _fl_to_abbreviation + ")";
	}

	public Timestamp _fl_return_date;

	public void set_fl_return_date(Timestamp fl_return_date) {
		_fl_return_date = fl_return_date;
	}

	public Timestamp get_fl_return_date() {
		return _fl_return_date;
	}

	public String _fl_ap_from_name;

	public void set_fl_ap_from_name(String fl_ap_from_name) {
		_fl_ap_from_name = fl_ap_from_name;
	}

	public String get_fl_ap_from_name() {
		return _fl_ap_from_name;
	}
	
	public LocalDateTime _fl_take_off_local;

	public void set_fl_take_off_local(LocalDateTime fl_take_off_local) {
		_fl_take_off_local = fl_take_off_local;
	}

	public LocalDateTime get_fl_take_off_local() {
		return _fl_take_off_local;
	}

	public LocalDateTime _fl_arrival_local;

	public void set_fl_arrival_local(LocalDateTime fl_arrival_local) {
		_fl_arrival_local = fl_arrival_local;
	}

	public LocalDateTime get_fl_arrival_local() {
		return _fl_arrival_local;
	}

	public String _fl_ap_to_name;

	public void set_fl_ap_to_name(String fl_ap_to_name) {
		_fl_ap_to_name = fl_ap_to_name;
	}

	public String get_fl_ap_to_name() {
		return _fl_ap_to_name;
	}

	public String _fl_services;

	public void set_fl_services(String fl_services) {
		_fl_services = fl_services;
	}

	public String get_fl_services() {
		return _fl_services;
	}

	public LinkedList<ServiceDTO> _fl_services_convert;

	public void set_fl_services_convert(LinkedList<ServiceDTO> fl_services_convert) {
		_fl_services_convert = fl_services_convert;
	}

	public LinkedList<ServiceDTO> get_fl_services_convert() {
		return _fl_services_convert;
	}

	public String _pt_name;

	public void set_pt_name(String pt_name) {
		_pt_name = pt_name;
	}

	public String get_pt_name() {
		return _pt_name;
	}

	public int _pt_total_seats;

	public void set_pt_total_seats(int pt_total_seats) {
		_pt_total_seats = pt_total_seats;
	}

	public int get_pt_total_seats() {
		return _pt_total_seats;
	}

	public int _pt_ec_quantity;

	public void set_pt_ec_quantity(int pt_ec_quantity) {
		_pt_ec_quantity = pt_ec_quantity;
	}

	public int get_pt_ec_quantity() {
		return _pt_ec_quantity;
	}

	public int _pt_sc_quantity;

	public void set_pt_sc_quantity(int pt_sc_quantity) {
		_pt_sc_quantity = pt_sc_quantity;
	}

	public int get_pt_sc_quantity() {
		return _pt_sc_quantity;
	}

	public int _pt_bs_quantity;

	public void set_pt_bs_quantity(int pt_bs_quantity) {
		_pt_bs_quantity = pt_bs_quantity;
	}

	public int get_pt_bs_quantity() {
		return _pt_bs_quantity;
	}

	public int _pt_fc_quantity;

	public void set_pt_fc_quantity(int pt_fc_quantity) {
		_pt_fc_quantity = pt_fc_quantity;
	}

	public int get_pt_fc_quantity() {
		return _pt_fc_quantity;
	}
	
	public int _fl_ec_sold;

	public int get_fl_ec_sold() {
		return _fl_ec_sold;
	}
	
	public int _fl_sc_sold;

	public int get_fl_sc_sold() {
		return _fl_sc_sold;
	}
	
	public int _fl_bs_sold;

	public int get_fl_bs_sold() {
		return _fl_bs_sold;
	}
	
	public int _fl_fc_sold; 

	public int get_fl_fc_sold() {
		return _fl_fc_sold;
	}

	public LinkedList<SeatDTO> _fl_seats;

	public void set_fl_seats(LinkedList<SeatDTO> fl_seats) {
		_fl_seats = fl_seats;
	}

	public LinkedList<SeatDTO> get_fl_seats() {
		return _fl_seats;
	}

	public int _pl_id;

	public void set_pl_id(int pl_id) {
		_pl_id = pl_id;
	}

	public int get_pl_id() {
		return _pl_id;
	}

	public double _sell_price;

	public void set_sell_price(double sell_price) {
		_sell_price = sell_price;
	}

	public double get_sell_price() {
		return _sell_price;
	}

	public int _pas_quantity;

	public void set_pas_quantity(int pas_quantity) {
		_pas_quantity = pas_quantity;
	}

	public int get_pas_quantity() {
		return _pas_quantity;
	}

	public int _tc_id;

	public void set_tc_id(int tc_id) {
		_tc_id = tc_id;
	}

	public int get_tc_id() {
		return _tc_id;
	}

	public String _tc_name;

	public void set_tc_name(int tc_id) {
		if (_tc_id == 1) {
			_tc_name = "Economy";
		} else if (_tc_id == 2) {
			_tc_name = "Special Economy";
		} else if (_tc_id == 3) {
			_tc_name = "Business";
		} else {
			_tc_name = "First Class";
		}
	}

	public String get_tc_name() {
		if (_tc_id == 1) {
			_tc_name = "Economy";
		} else if (_tc_id == 2) {
			_tc_name = "Special Economy";
		} else if (_tc_id == 3) {
			_tc_name = "Business";
		} else {
			_tc_name = "First Class";
		}
		return _tc_name;
	}

}
