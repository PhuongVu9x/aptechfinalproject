package vpn.model;

import java.sql.*;
import java.util.LinkedList;

public class DBConnection {
	/*private static String url = "jdbc:sqlserver://;servername=TestJava.mssql.somee.com;encrypt=true;integratedSecurity=false;trustServerCertificate=true";
    private static String user = "phongvan1412_SQLLogin_1";
    private static String password = "hx37esdlfe";*/
    private static String url = "jdbc:sqlserver://115.79.25.225:9874;encrypt=true;integratedSecurity=false;trustServerCertificate=true";
    private static String user = "pv1412";
    private static String password = "_Phongvan1412";
    public static DBConnection instance = null;
    public static Connection getConnection() throws SQLException {
    	try {
			Connection conn;
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			conn = DriverManager.getConnection(url, user, password);
			return conn;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
    }
    
    public static final ResultSet CallProc(String storeName) throws ClassNotFoundException {
		try {
			CallableStatement state = getConnection().prepareCall(storeName);
			var result = state.executeQuery();
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static final ResultSet CallProc(String storeName, LinkedList<String> parameters) throws ClassNotFoundException {
		try {
			String str = "{ call " + storeName + "(";
			for (int i = 0; i < parameters.size(); i++) {
				if (i + 1 == parameters.size()) {
					str += "?";
				} else {
					str += "?,";
				}
			}

			str += ")}";
			PreparedStatement state = getConnection().prepareStatement(str);
			for (int i = 0; i < parameters.size(); i++) {
				state.setString(i + 1, parameters.get(i));
			}
			
			var result = state.executeQuery();
			return result;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

}
