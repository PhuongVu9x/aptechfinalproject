package vpn.model;

import java.sql.Timestamp;

public class TicketClassDTO {
	public int _tc_id;

	public void set_tc_id(int tc_id) {
		_tc_id = tc_id;
	}

	public int get_tc_id() {
		return _tc_id;
	}
	public int _tc_index;

	public void set_tc_index(int tc_index) {
		_tc_index = tc_index;
	}

	public int get_tc_index() {
		return _tc_index;
	}

	public String _tc_name;

	public void set_tc_name(String tc_name) {
		_tc_name = tc_name;
	}

	public String get_tc_name() {
		return _tc_name;
	}

	public String _tc_icon;

	public void set_tc_icon(String tc_icon) {
		_tc_icon = tc_icon;
	}

	public String get_tc_icon() {
		return _tc_icon;
	}

	public Timestamp _tc_created_date;

	public void set_field(Timestamp tc_created_date) {
		_tc_created_date = tc_created_date;
	}

	public Timestamp get_field() {
		return _tc_created_date;
	}

	public Timestamp _tc_updated_date;

	public void set_tc_updated_date(Timestamp tc_updated_date) {
		_tc_updated_date = tc_updated_date;
	}

	public Timestamp get_tc_updated_date() {
		return _tc_updated_date;
	}

	public boolean _tc_status;

	public void set_tc_status(boolean tc_status) {
		_tc_status = tc_status;
	}

	public boolean get_tc_status() {
		return _tc_status;
	}
}
