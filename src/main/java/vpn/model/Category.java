package vpn.model;

import java.sql.Timestamp;

public class Category {
	public int _cate_id;

	public void set_cate_id(int cate_id) {
		_cate_id = cate_id;
	}

	public int get_cate_id() {
		return _cate_id;
	}
	
	public String _cate_name;

	public void set_cate_name(String cate_name) {
		_cate_name = cate_name;
	}

	public String get_cate_name() {
		return _cate_name;
	}

	public String _cate_icon;

	public void set_cate_icon(String cate_icon) {
		_cate_icon = cate_icon;
	}

	public String get_cate_icon() {
		return _cate_icon;
	}

	public Timestamp _cate_created_date;

	public void set_field(Timestamp cate_created_date) {
		_cate_created_date = cate_created_date;
	}

	public Timestamp get_field() {
		return _cate_created_date;
	}

	public Timestamp _cate_updated_date;

	public void set_cate_updated_date(Timestamp cate_updated_date) {
		_cate_updated_date = cate_updated_date;
	}

	public Timestamp get_cate_updated_date() {
		return _cate_updated_date;
	}

	public boolean _cate_status;

	public void set_cate_status(boolean cate_status) {
		_cate_status = cate_status;
	}

	public boolean get_cate_status() {
		return _cate_status;
	}
}
