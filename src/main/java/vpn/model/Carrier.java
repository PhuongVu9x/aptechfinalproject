package vpn.model;

import java.sql.Timestamp;

public class Carrier {
	public int _car_id;

	public void set_car_id(int car_id) {
		_car_id = car_id;
	}

	public int get_car_id() {
		return _car_id;
	}
	
	public String _car_name;

	public void set_car_name(String car_name) {
		_car_name = car_name;
	}

	public String get_car_name() {
		return _car_name;
	}

	public String _car_icon;

	public void set_car_icon(String car_icon) {
		_car_icon = car_icon;
	}

	public String get_car_icon() {
		return _car_icon;
	}

	public Timestamp _car_created_date;

	public void set_field(Timestamp car_created_date) {
		_car_created_date = car_created_date;
	}

	public Timestamp get_field() {
		return _car_created_date;
	}

	public Timestamp _car_updated_date;

	public void set_car_updated_date(Timestamp car_updated_date) {
		_car_updated_date = car_updated_date;
	}

	public Timestamp get_car_updated_date() {
		return _car_updated_date;
	}

	public boolean _car_status;

	public void set_car_status(boolean car_status) {
		_car_status = car_status;
	}

	public boolean get_car_status() {
		return _car_status;
	}
}
