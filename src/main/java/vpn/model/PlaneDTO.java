package vpn.model;

import java.sql.Timestamp;

import vpn.AppThread;

public class PlaneDTO {
	public int _pl_id;

	public void set_pl_id(int pl_id) {
		_pl_id = pl_id;
	}

	public int get_pl_id() {
		return _pl_id;
	}
	public int _pl_index;

	public void set_pl_index(int pl_index) {
		_pl_index = pl_index;
	}

	public int get_pl_index() {
		return _pl_index;
	}

	public String _pl_code;

	public void set_pl_code(String pl_code) {
		_pl_code = pl_code;
	}

	public String get_pl_code() {
		return _pl_code;
	}

	public int _pl_carrier_id;

	public void set_field(int pl_carrier_id) {
		_pl_carrier_id = pl_carrier_id;
	}

	public int get_field() {
		return _pl_carrier_id;
	}

	public int _pl_pt_id;

	public void set_pl_pt_id(int pl_pt_id) {
		_pl_pt_id = pl_pt_id;
	}

	public int get_pl_pt_id() {
		return _pl_pt_id;
	}

	public int _pt_total_seats;

	public void set_pt_total_seats(int pt_total_seats) {
		_pt_total_seats = pt_total_seats;
	}

	public int get_pt_total_seats() {
		return _pt_total_seats;
	}

	public int _pt_ec_quantity;

	public void set_pt_ec_quantity(int pt_ec_quantity) {
		_pt_ec_quantity = pt_ec_quantity;
	}

	public int get_pt_ec_quantity() {
		return _pt_ec_quantity;
	}

	public int _pt_sc_quantity;

	public void set_pt_sc_quantity(int pt_sc_quantity) {
		_pt_sc_quantity = pt_sc_quantity;
	}

	public int get_pt_sc_quantity() {
		return _pt_sc_quantity;
	}

	public int _pt_bs_quantity;

	public void set_pt_bs_quantity(int pt_bs_quantity) {
		_pt_bs_quantity = pt_bs_quantity;
	}

	public int get_pt_bs_quantity() {
		return _pt_bs_quantity;
	}

	public int _pt_fc_quantity;

	public void set_pt_fc_quantity(int pt_fc_quantity) {
		_pt_fc_quantity = pt_fc_quantity;
	}

	public int get_pt_fc_quantity() {
		return _pt_fc_quantity;
	}

	public Timestamp _pl_created_date;

	public void set_pl_created_date(Timestamp pl_created_date) {
		_pl_created_date = pl_created_date;
	}

	public Timestamp get_pl_created_date() {
		return _pl_created_date;
	}

	public Timestamp _pl_updated_date;

	public void set_pl_updated_date(Timestamp pl_updated_date) {
		_pl_updated_date = pl_updated_date;
	}

	public Timestamp get_pl_updated_date() {
		return _pl_updated_date;
	}
	
	public Timestamp _pl_created_timestamp;

	public String get_pl_created_timestamp() {
		return AppThread.ToVnTime(_pl_created_date);
	}
	
	public Timestamp _pl_updated_timestamp;

	public String get_pl_updated_timestamp() {
		return AppThread.ToVnTime(_pl_updated_date);
	}


	public boolean _pl_status;

	public void set_pl_status(boolean pl_status) {
		_pl_status = pl_status;
	}

	public boolean get_pl_status() {
		return _pl_status;
	}
	
	public String _car_icon;

	public void set_car_icon(String car_icon) {
		_car_icon = car_icon;
	}

	public String get_car_icon() {
		return _car_icon;
	}
	
	public int _car_id;

	public void set_car_id(int car_id) {
		_car_id = car_id;
	}

	public int get_car_id() {
		return _car_id;
	}
	
	public String _car_name;

	public void set_car_name(String car_name) {
		_car_name = car_name;
	}

	public String get_car_name() {
		return _car_name;
	}

	public int _pt_id;

	public void set_pt_id(int pt_id) {
		_pt_id = pt_id;
	}

	public int get_pt_id() {
		return _pt_id;
	}

	public String _pt_name;

	public void set_pt_name(String pt_name) {
		_pt_name = pt_name;
	}

	public String get_pt_name() {
		return _pt_name;
	}
}
