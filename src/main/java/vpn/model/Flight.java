package vpn.model;

import java.sql.Timestamp;

public class Flight {
	public int _fl_id;

	public void set_fl_id(int fl_id) {
		_fl_id = fl_id;
	}

	public int get_fl_id() {
		return _fl_id;
	}

	public int _fl_from_id;

	public void set_fl_from_id(int fl_from_id) {
		_fl_from_id = fl_from_id;
	}

	public int get_fl_from_id() {
		return _fl_from_id;
	}

	public int _fl_to_id;

	public void set_fl_to_id(int fl_to_id) {
		_fl_to_id = fl_to_id;
	}

	public int get_fl_to_id() {
		return _fl_to_id;
	}

	public int _fl_transit_id;

	public void set_fl_transit_id(int fl_transit_id) {
		_fl_transit_id = fl_transit_id;
	}

	public int get_fl_transit_id() {
		return _fl_transit_id;
	}
	
	public int _fl_return_id;

	public void set_fl_return_id(int fl_return_id) {
		_fl_return_id = fl_return_id;
	}

	public int get_fl_return_id() {
		return _fl_return_id;
	}


	public int _fl_plane_id;

	public void set_fl_plane_id(int fl_plane_id) {
		_fl_plane_id = fl_plane_id;
	}

	public int get_fl_plane_id() {
		return _fl_plane_id;
	}

	public Timestamp _fl_estimate_take_of;

	public void set_fl_estimate(Timestamp fl_estimate_take_of) {
		_fl_estimate_take_of = fl_estimate_take_of;
	}

	public Timestamp get_fl_estimate() {
		return _fl_estimate_take_of;
	}

	public Timestamp _fl_estimate_arrival;

	public void set_fl_estimate_arrival(Timestamp fl_estimate_arrival) {
		_fl_estimate_arrival = fl_estimate_arrival;
	}

	public Timestamp get_fl_estimate_arrival() {
		return _fl_estimate_arrival;
	}

	public Timestamp _fl_take_off;

	public void set_fl_take_off(Timestamp fl_take_off) {
		_fl_take_off = fl_take_off;
	}

	public Timestamp get_fl_take_off() {
		return _fl_take_off;
	}

	public Timestamp _fl_arrival;

	public void set_fl_arrival(Timestamp fl_arrival) {
		_fl_arrival = fl_arrival;
	}

	public Timestamp get_fl_arrival() {
		return _fl_arrival;
	}

	public Timestamp _fl_created_date;

	public void set_fl_created_date(Timestamp fl_created_date) {
		_fl_created_date = fl_created_date;
	}

	public Timestamp get_fl_created_date() {
		return _fl_created_date;
	}

	public Timestamp _fl_updated_date;

	public void set_fl_updated_date(Timestamp fl_updated_date) {
		_fl_updated_date = fl_updated_date;
	}

	public Timestamp get_fl_updated_date() {
		return _fl_updated_date;
	}

	public int _fl_status;

	public void set_fl_status(int fl_status) {
		_fl_status = fl_status;
	}

	public int get_fl_status() {
		return _fl_status;
	}


}
