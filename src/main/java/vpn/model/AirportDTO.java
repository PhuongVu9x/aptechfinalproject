package vpn.model;

import java.sql.Timestamp;

public class AirportDTO {
	public int _ap_id;

	public void set_ap_id(int pos_id) {
		_ap_id = pos_id;
	}

	public int get_ap_id() {
		return _ap_id;
	}

	public int _ap_index;

	public void set_ap_index(int ap_index) {
		_ap_index = ap_index;
	}

	public int get_ap_index() {
		return _ap_index;
	}
 
	public String _ap_name;

	public void set_ap_name(String pos_name) {
		_ap_name = pos_name;
	}

	public String get_ap_name() {
		return _ap_name;
	}

	public String _ap_background;

	public void set_background(String ap_background) {
		_ap_background = ap_background;
	}

	public String get_background() {
		return _ap_background;
	}

	public int _ap_city_id;

	public void set_ap_city_id(int ap_city_id) {
		_ap_city_id = ap_city_id;
	}

	public int get_ap_city_id() {
		return _ap_city_id;
	}

	public String _ap_icon;

	public void set_ap_icon(String pos_icon) {
		_ap_icon = pos_icon;
	}

	public String get_ap_icon() {
		return _ap_icon;
	}

	public Timestamp _ap_created_date;

	public void set_field(Timestamp pos_created_date) {
		_ap_created_date = pos_created_date;
	}

	public Timestamp get_field() {
		return _ap_created_date;
	}

	public Timestamp _ap_updated_date;

	public void set_ap_updated_date(Timestamp pos_updated_date) {
		_ap_updated_date = pos_updated_date;
	}

	public Timestamp get_ap_updated_date() {
		return _ap_updated_date;
	}

	public String _ap_abbreviation;

	public void set_ap_abbreviation(String ap_abbreviation) {
		_ap_abbreviation = ap_abbreviation;
	}

	public String get_ap_abbreviation() {
		return _ap_abbreviation;
	}

	public boolean _ap_status;

	public void set_ap_status(boolean pos_status) {
		_ap_status = pos_status;
	}

	public boolean get_ap_status() {
		return _ap_status;
	}

	public int _city_id;

	public void set_city_id(int city_id) {
		_city_id = city_id;
	}

	public int get_city_id() {
		return _city_id;
	}

	
	public String _city_name;

	public void set_city_name(String city_name) {
		_city_name = city_name;
	}

	public String get_city_name() {
		return _city_name;
	}

	public boolean _city_status;

	public void set_city_status(boolean city_status) {
		_city_status = city_status;
	}

	public boolean get_city_status() {
		return _city_status;
	}

	public String _ap_full_name;

	public void set_ap_full_name(String ap_full_name) {
		_ap_full_name = ap_full_name;
	}

	public String get_ap_full_name() {
		return _city_name + " (" + _ap_abbreviation + ")";
	}

	public String _ct_name;

	public void set_ct_name(String ct_name) {
		_ct_name = ct_name;
	}

	public String get_ct_name() { 
		return _ct_name;
	}
}
