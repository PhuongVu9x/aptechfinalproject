package vpn.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.LinkedList;
import java.util.Locale;

import vpn.AppThread;

public class BillDTO {
	public int _bil_id;

	public void set_bil_id(int bil_id) {
		_bil_id = bil_id;
	}

	public int get_bil_id() {
		return _bil_id;
	}
	
	public String _bil_payment_code;

	public void set_bil_payment_code(String bil_payment_code) {
		_bil_payment_code = bil_payment_code;
	}

	public String get_bil_payment_code() {
		return _bil_payment_code;
	}
	
	public int _bil_fl_return_id;

	public void set_bil_fl_return_id(int bil_fl_return_id) {
		_bil_fl_return_id = bil_fl_return_id;
	}

	public int get_bil_fl_return_id() {
		return _bil_fl_return_id;
	}

	public int _bil_index;

	public void set_bil_index(int bil_index) {
		_bil_index = bil_index;
	}

	public int get_bil_index() {
		return _bil_index;
	}

	
	public int _bil_cus_id;

	public void set_bil_cus_id(int bil_cus_id) {
		_bil_cus_id = bil_cus_id;
	}

	public int get_bil_cus_id() {
		return _bil_cus_id;
	}

	public double _bil_payment;

	public void set_bil_payment(double bil_payment) {
		_bil_payment = bil_payment;
	}  

	public double get_bil_payment() {
		return _bil_payment;
	}

	public Timestamp _bil_created_date;

	public void set_bil_created_date(Timestamp bil_created_date) {
		_bil_created_date = bil_created_date;
	}

	public Timestamp get_bil_created_date() {
		return _bil_created_date;
	}

	public Timestamp _bil_updated_date;

	public void set_bil_updated_date(Timestamp bil_updated_date) {
		_bil_updated_date = bil_updated_date;
	}

	public Timestamp get_bil_updated_date() {
		return _bil_updated_date;
	}

	public String _bil_created_timestamp;

	public void set_bil_created_timestamp(String bil_created_timestamp) {
		_bil_created_timestamp = bil_created_timestamp;
	}

	public String get_bil_created_timestamp() {
		if(_bil_created_date == null)
			return "";
		return AppThread.ToVnTime(_bil_created_date);
	}

	public String _bil_updated_timestamp;

	public void set_bil_updated_timestamp(String bil_updated_timestamp) {
		
		_bil_updated_timestamp = bil_updated_timestamp;
	}
	
	public String get_bil_updated_timestamp() {
		if(_bil_updated_date == null)
			return "";
		return AppThread.ToVnTime(_bil_updated_date);
	}
	
	public String _bil_payment_string;
	
	public String get_bil_payment_string() {
		return AppThread.ToVND(_bil_payment);
	}

	public int _bil_status;

	public void set_bil_status(int bil_status) {
		_bil_status = bil_status;
	}

	public int get_bil_status() {
		return _bil_status;
	}
	
	public String _cus_full_name;

	public void set_cus_full_name(String cus_full_name) {
		_cus_full_name = cus_full_name;
	}

	public String get_cus_full_name() {
		return _cus_full_name;
	}

	public String _cus_phone;

	public void set_cus_phone(String cus_phone) {
		_cus_phone = cus_phone;
	}

	public String get_cus_phone() {
		return _cus_phone;
	}
	
	public int _bil_fl_id;

	public int get_bil_fl_id() {
		return _bil_fl_id;
	}
	
	public int _fl_id;

	public int get_fl_id() {
		return _fl_id;
	}

	public int _fl_ec_sold;

	public int get_fl_ec_sold() {
		return _fl_ec_sold;
	}
	
	public int _fl_sc_sold;

	public int get_fl_sc_sold() {
		return _fl_sc_sold;
	}
	
	public int _fl_bs_sold;

	public int get_fl_bs_sold() {
		return _fl_bs_sold;
	}
	
	public int _fl_fc_sold; 

	public int get_fl_fc_sold() {
		return _fl_fc_sold;
	}
	
	public int _pt_ec_quantity;

	public void set_pt_ec_quantity(int pt_ec_quantity) {
		_pt_ec_quantity = pt_ec_quantity;
	}

	public int get_pt_ec_quantity() {
		return _pt_ec_quantity;
	}

	public int _pt_sc_quantity;

	public void set_pt_sc_quantity(int pt_sc_quantity) {
		_pt_sc_quantity = pt_sc_quantity;
	}

	public int get_pt_sc_quantity() {
		return _pt_sc_quantity;
	}

	public int _pt_bs_quantity;

	public void set_pt_bs_quantity(int pt_bs_quantity) {
		_pt_bs_quantity = pt_bs_quantity;
	}

	public int get_pt_bs_quantity() {
		return _pt_bs_quantity;
	}

	public int _pt_fc_quantity;

	public void set_pt_fc_quantity(int pt_fc_quantity) {
		_pt_fc_quantity = pt_fc_quantity;
	}

	public int get_pt_fc_quantity() {
		return _pt_fc_quantity;
	}
	
	//Service
	public String _fl_services;

	public void set_fl_services(String fl_services) {
		_fl_services = fl_services;
	}

	public String get_fl_services() {
		return _fl_services;
	}
	
	public LinkedList<ServiceDTO> _fl_services_convert;

	public void set_fl_services_convert(LinkedList<ServiceDTO> fl_services_convert) {
		_fl_services_convert = fl_services_convert;
	}

	public LinkedList<ServiceDTO> get_fl_services_convert() {
		return _fl_services_convert;
	}
	
	public FlightDTO _bil_flight;

	public void set_bil_flight(FlightDTO bil_flight) {
		_bil_flight = bil_flight;
	}

	public FlightDTO get_bil_flight() {
		return _bil_flight;
	}
	
	public FlightDTO _bil_flight_return;

	public void set_bil_flight_return(FlightDTO bil_flight_return) {
		_bil_flight_return = bil_flight_return;
	}

	public FlightDTO get_bil_flight_return() {
		return _bil_flight_return;
	}
	
//	-------------- Airplane --------------
	public int _car_id;

	public void set_car_id(int car_id) {
		_car_id = car_id;
	}

	public int get_car_id() {
		return _car_id;
	}

	public String _car_name;

	public void set_car_name(String car_name) {
		_car_name = car_name;
	}

	public String get_car_name() {
		return _car_name;
	}

	public String _car_icon;

	public void set_car_icon(String car_icon) {
		_car_icon = car_icon;
	}

	public String get_car_icon() {
		return _car_icon;
	}

	public int _car_carbin;

	public void set_car_carbin(int car_carbin) {
		_car_carbin = car_carbin;
	}

	public int get_car_carbin() {
		return _car_carbin;
	}

	public int _car_checked;

	public void set_car_checked(int car_checked) {
		_car_checked = car_checked;
	}

	public int get_car_checked() {
		return _car_checked;
	}
	
	public LinkedList<TicketDTO> _tickets;
	
	public void set_tickets(LinkedList<TicketDTO> tickets) {
		_tickets = tickets;
	}
	
	public LinkedList<TicketDTO> get_tickets(){
		return _tickets;
	}
	
	public String _pl_code;
	
	public void set_pl_code(String pl_code) {
		_pl_code = pl_code;
	}	
	
	public String get_pl_code(){
		return _pl_code;
	}
}
