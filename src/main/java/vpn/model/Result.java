package vpn.model;

import java.util.LinkedList;

public class Result<T> {
	public T data;
	public boolean isSuccess;
	public String mes;
	public T success;
	public T fail;
	public Result() {
		
	}
	
	public Result(T data, boolean isSuccess,String mes) {
		this.data = data;
		this.isSuccess = isSuccess;
		this.mes = mes;
	}
}
