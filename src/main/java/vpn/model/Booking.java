package vpn.model;

import java.sql.Timestamp;

import vpn.AppThread;

public class Booking {
	public int _bo_id;

	public void set_bo_id(int bo_id) {
		_bo_id = bo_id;
	}

	public int get_bo_id() {
		return _bo_id;
	}

	public int _bo_cus_id;

	public void set_bo_cus_id(int bo_cus_id) {
		_bo_cus_id = bo_cus_id;
	}

	public int get_bo_cus_id() {
		return _bo_cus_id;
	}

	public Timestamp _bo_check_in;

	public void set_bo_check_in(Timestamp bo_check_in) {
		_bo_check_in = bo_check_in;
	}

	public Timestamp get_bo_check_in() {
		return _bo_check_in;
	}

	public Timestamp _bo_check_out;

	public void set_bo_check_out(Timestamp bo_check_out) {
		_bo_check_out = bo_check_out;
	}

	public Timestamp get_bo_check_out() {
		return _bo_check_out;
	}

	public double _bo_deposit;

	public void set_bo_deposit(double bo_deposit) {
		_bo_deposit = bo_deposit;
	}

	public double get_bo_deposit() {
		return _bo_deposit;
	}

	public double _bo_payment;

	public void set_bo_payment(double bo_payment) {
		_bo_payment = bo_payment;
	}

	public double get_bo_payment() {
		return _bo_payment;
	}

	public String _bo_payment_type;

	public void set_bo_payment_type(String bo_payment_type) {
		_bo_payment_type = bo_payment_type;
	}

	public String get_bo_payment_type() {
		return _bo_payment_type;
	}

	public Timestamp _bo_created_date;

	public void set_bo_created_date(Timestamp bo_created_date) {
		_bo_created_date = bo_created_date;
	}

	public Timestamp get_bo_created_date() {
		return _bo_created_date;
	}

	public Timestamp _bo_updated_date;

	public void set_bo_updated_date(Timestamp bo_updated_date) {
		_bo_updated_date = bo_updated_date;
	}

	public Timestamp get_bo_updated_date() {
		return _bo_updated_date;
	}

	public boolean _bo_status;

	public void set_bo_status(boolean bo_status) {
		_bo_status = bo_status;
	}

	public boolean get_bo_status() {
		return _bo_status;
	}

	public int _bo_total_customer;

	public void set_bo_total_customer(int bo_total_customer) {
		_bo_total_customer = bo_total_customer;
	}

	public int get_bo_total_customer() {
		return _bo_total_customer;
	}

	public String _cus_full_name;

	public void set_cus_full_name(String cus_full_name) {
		_cus_full_name = cus_full_name;
	}

	public String get_cus_full_name() {
		return _cus_full_name;
	}

	public String _cus_phone;

	public void set_cus_phone(String cus_phone) {
		_cus_phone = cus_phone;
	}

	public String get_cus_phone() {
		return _cus_phone;
	}

	public String _bo_payment_string;

	public String get_bo_payment_string() {
		return AppThread.ToVND(_bo_payment);
	}

	public String _bo_updated_timestamp;

	public String get_bo_updated_timestamp() {
		return AppThread.ToVnTime(_bo_updated_date);
	}

	public String _bo_created_timestamp;

	public String get_bo_created_timestamp() {
		return AppThread.ToVnTime(_bo_created_date);
	}

}
