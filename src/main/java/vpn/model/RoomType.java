package vpn.model;

import java.sql.Timestamp;

public class RoomType {
	public int _rt_id;

	public void set_rt_id(int rt_id) {
		_rt_id = rt_id;
	}

	public int get_rt_id() {
		return _rt_id;
	}
	
	public String _rt_name;

	public void set_rt_name(String rt_name) {
		_rt_name = rt_name;
	}

	public String get_rt_name() {
		return _rt_name;
	}

	public String _rt_icon;

	public void set_rt_icon(String rt_icon) {
		_rt_icon = rt_icon;
	}

	public String get_rt_icon() {
		return _rt_icon;
	}

	public Timestamp _rt_created_date;

	public void set_field(Timestamp rt_created_date) {
		_rt_created_date = rt_created_date;
	}

	public Timestamp get_field() {
		return _rt_created_date;
	}

	public Timestamp _rt_updated_date;

	public void set_rt_updated_date(Timestamp rt_updated_date) {
		_rt_updated_date = rt_updated_date;
	}

	public Timestamp get_rt_updated_date() {
		return _rt_updated_date;
	}

	public boolean _rt_status;

	public void set_rt_status(boolean rt_status) {
		_rt_status = rt_status;
	}

	public boolean get_rt_status() {
		return _rt_status;
	}
	
	public int _ret_percent;

	public void set_ret_percent(int ret_percent) {
		_ret_percent = ret_percent;
	}

	public int get_ret_percent() {
		return _ret_percent;
	}

}
