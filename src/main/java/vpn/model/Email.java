package vpn.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Email {
	public String _recipient;
	public String _msgBody;
	public String _subject;
    
    
    public void set_recipient(String recipient) {
		_recipient = recipient;
	}

	public String get_recipient() {
		return _recipient;
	}
	
	
	public void set_msgBody(String msgBody) {
		_msgBody = msgBody;
	}

	public String get_msgBody() {
		return _msgBody;
	}
	
	public void set_subject(String subject) {
		_subject = subject;
	}

	public String get_subject() {
		return _subject;
	}
}
