package vpn.model;

import java.sql.Timestamp;
import java.util.LinkedList;

import vpn.AppThread;

public class TicketDTO {
	public int _bil_id;

	public void set_bil_id(int bil_id) {
		_bil_id = bil_id;
	}

	public int get_bil_id() {
		return _bil_id;
	}
	
	public int _tk_id;

	public void set_tk_id(int tk_id) {
		_tk_id = tk_id;
	}

	public int get_tk_id() {
		return _tk_id;
	}
	
	public int _tk_index;

	public void set_tk_index(int tk_index) {
		_tk_index = tk_index;
	}

	public int get_tk_index() {
		return _tk_index;
	}

	public int _tk_bill_id;

	public void set_tk_bil_id(int tk_bill_id) {
		_tk_bill_id = tk_bill_id;
	}

	public int get_tk_bil_id() {
		return _tk_bill_id;
	}
	
	public int _tk_fl_id;

	public void set_tk_fl_id(int tk_fl_id) {
		_tk_fl_id = tk_fl_id;
	}

	public int get_tk_fl_id() {
		return _tk_fl_id;
	}

	public int _bil_fl_id;

	public void set_bil_fl_id(int bil_fl_id) {
		_bil_fl_id = bil_fl_id;
	}

	public int get_bil_fl_id() {
		return _bil_fl_id;
	}

	public int _tk_return_id;

	public void set_tk_return_id(int tk_return_id) {
		_tk_return_id = tk_return_id;
	}

	public int get_tk_return_id() {
		return _tk_return_id;
	}
	
	public FlightDTO _tk_flight;

	public void set_tk_flight(FlightDTO tk_flight) {
		_tk_flight = tk_flight;
	}

	public FlightDTO get_tk_flight() {
		return _tk_flight;
	}
	
	public LinkedList<ServiceDTO> _tk_fl_services_convert;

	public void set_tk_fl_services_convert(LinkedList<ServiceDTO> tk_fl_services_convert) {
		_tk_fl_services_convert = tk_fl_services_convert;
	}

	public LinkedList<ServiceDTO> get_tk_fl_services_convert() {
		return _tk_fl_services_convert;
	}
	
	public double _tk_payment;

	public void set_tk_payment(double tk_payment) {
		_tk_payment = tk_payment;
	}

	public double get_tk_payment() {
		return _tk_payment;
	}

	public String _tk_first_name;

	public void set_tk_first_name(String tk_first_name) {
		_tk_first_name = tk_first_name;
	}

	public String get_tk_first_name() {
		return _tk_first_name;
	}

	public String _tk_last_name;

	public void set_tk_last_name(String tk_last_name) {
		_tk_last_name = tk_last_name;
	}

	public String get_tk_last_name() {
		return _tk_last_name;
	}

	public String _tk_full_name;

	public void set_tk_full_name(String tk_full_name) {
		_tk_full_name = tk_full_name;
	}

	public String get_tk_full_name() {
		return _tk_full_name;
	}

	public String _tk_title;

	public void set_tk_title(String tk_title) {
		_tk_title = tk_title;
	}

	public String get_tk_title() {
		return _tk_title;
	}

	public String _tk_dob;

	public void set_tk_dob(String tk_dob) {
		_tk_dob = tk_dob;
	}

	public String get_tk_dob() {
		return _tk_dob;
	}

	public String _tk_nationality;

	public void set_tk_personality(String tk_nationality) {
		_tk_nationality = tk_nationality;
	}

	public String get_tk_personality() {
		return _tk_nationality;
	}

	public String _tk_passport;

	public void set_tk_passport(String tk_passport) {
		_tk_passport = tk_passport;
	}

	public String get_tk_passport() {
		return _tk_passport;
	}

	public String _tk_country;

	public void set_tk_country(String tk_country) {
		_tk_country = tk_country;
	}

	public String get_tk_country() {
		return _tk_country;
	}

	public Timestamp _tk_passport_expried;

	public void set_tk_passport_expried(Timestamp tk_passport_expried) {
		_tk_passport_expried = tk_passport_expried;
	}

	public Timestamp get_tk_passport_expried() {
		return _tk_passport_expried;
	}

	public Timestamp _tk_created_date;

	public void set_tk_created_date(Timestamp tk_created_date) {
		_tk_created_date = tk_created_date;
	}

	public Timestamp get_tk_created_date() {
		return _tk_created_date;
	}

	public Timestamp _tk_updated_date;

	public void set_tk_updated_date(Timestamp tk_updated_date) {
		_tk_updated_date = tk_updated_date;
	}

	public Timestamp get_tk_updated_date() {
		return _tk_updated_date;
	}

	public int _tk_status;

	public void set_tk_status(int tk_status) {
		_tk_status = tk_status;
	}

	public int get_tk_status() {
		return _tk_status;
	}

	public int _tk_gate;

	public void set_tk_gate(int tk_gate) {
		_tk_gate = tk_gate;
	}

	public int get_tk_gate() {
		return _tk_gate;
	}

	public int _tk_pas_id;

	public void set_tk_pas_id(int tk_pas_id) {
		_tk_pas_id = tk_pas_id;
	}

	public int get_tk_pas_id() {
		return _tk_pas_id;
	}

	public int _tk_guardian_id;

	public void set_tk_guardian_id(int tk_guardian_id) {
		_tk_guardian_id = tk_guardian_id;
	}

	public int get_tk_guardian_id() {
		return _tk_guardian_id;
	}

	public String _tk_seat;

	public void set_tk_seat(String tk_seat) {
		_tk_seat = tk_seat;
	}

	public String get_tk_seat() {
		return _tk_seat;
	}
	
	public int _tk_tc_id;

	public void set_tk_tc_id(int tk_tc_id) {
		_tk_tc_id = tk_tc_id;
	}

	public int get_tk_tc_id() {
		return _tk_tc_id;
	}

	public String _pas_title;

	public void set_pas_title(String pas_title) {
		_pas_title = pas_title;
	}

	public String get_pas_title() {
		return _pas_title;
	}

	public String _tk_payment_string;


	public String get_tk_payment_string() {
		return AppThread.ToVND(_tk_payment);
	}
	
	public String _tk_updated_timestamp;

	public String get_tk_updated_timestamp() {
		return AppThread.ToVnTime(_tk_updated_date);
	}
	
	public String _cus_full_name;

	public void set_cus_full_name(String cus_full_name) {
		_cus_full_name = cus_full_name;
	}

	public String get_cus_full_name() {
		return _cus_full_name;
	}

	public String _cus_email;

	public void set_cus_email(String cus_email) {
		_cus_email = cus_email;
	}

	public String get_cus_email() {
		return _cus_email;
	}

	public String _cus_phone;

	public void set_cus_phone(String cus_phone) {
		_cus_phone = cus_phone;
	}
	
	public String _tc_name;

	public void set_tc_name(String tc_name) {
		_tc_name = tc_name;
	}

	public String get_tc_name() {
		return _tc_name;
	}
	
	public int _tk_cabin;

	public void set_tk_cabin(int tk_cabin) {
		_tk_cabin = tk_cabin;
	}

	public int get_tk_cabin() {
		return _tk_cabin;
	}
	
	public int _tk_free_cabin;

	public void set_tk_free_cabin(int tk_free_cabin) {
		_tk_free_cabin = tk_free_cabin;
	}

	public int get_tk_free_cabin() {
		return _tk_free_cabin;
	}
	
	public int _tk_checked;

	public void set_tk_checkedn(int tk_checked) {
		_tk_checked = tk_checked;
	}

	public int get_tk_checked() {
		return _tk_checked;
	}
	
	public int _tk_free_checked;

	public void set_tk_free_checked(int tk_free_checked) {
		_tk_free_checked = tk_free_checked;
	}

	public int get_tk_free_checked() {
		return _tk_free_checked;
	}
}
