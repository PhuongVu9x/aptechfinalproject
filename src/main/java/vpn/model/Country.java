package vpn.model;

import java.sql.Timestamp;

public class Country {
	public int _ct_id;

	public void set_ct_id(int ct_id) {
		_ct_id = ct_id;
	}

	public int get_ct_id() {
		return _ct_id;
	}
	
	public String _ct_name;

	public void set_ct_name(String ct_name) {
		_ct_name = ct_name;
	}

	public String get_ct_name() {
		return _ct_name;
	}

	public String _ct_icon;

	public void set_ct_icon(String ct_icon) {
		_ct_icon = ct_icon;
	}

	public String get_ct_icon() {
		return _ct_icon;
	}

	public Timestamp _ct_created_date;

	public void set_field(Timestamp ct_created_date) {
		_ct_created_date = ct_created_date;
	}

	public Timestamp get_field() {
		return _ct_created_date;
	}

	public Timestamp _ct_updated_date;

	public void set_ct_updated_date(Timestamp ct_updated_date) {
		_ct_updated_date = ct_updated_date;
	}

	public Timestamp get_ct_updated_date() {
		return _ct_updated_date;
	}

	public boolean _ct_status;

	public void set_ct_status(boolean ct_status) {
		_ct_status = ct_status;
	}

	public boolean get_ct_status() {
		return _ct_status;
	}
}
