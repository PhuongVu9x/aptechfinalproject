package vpn.model;

public class UserDTO {
	public String _u_email;
	public String _u_password;
	public int _u_type;

	public void set_u_email(String u_email) {
		_u_email = u_email;
	}

	public String get_u_email() {
		return _u_email;
	}
	
	public void set_u_password(String u_password) {
		_u_password = u_password;
	}

	public String get_u_password() {
		return _u_password;
	}
	
	public void set_u_type(int u_type) {
		_u_type = u_type;
	}

	public int get_u_type() {
		return _u_type;
	}
}
