package vpn.model;

import java.sql.Timestamp;

public class Permision {
	public int _per_id;

	public void set_per_id(int per_id) {
		_per_id = per_id;
	}

	public int get_per_id() {
		return _per_id;
	}
	
	public String _per_name;

	public void set_per_name(String per_name) {
		_per_name = per_name;
	}

	public String get_per_name() {
		return _per_name;
	}

	public String _per_icon;

	public void set_per_icon(String per_icon) {
		_per_icon = per_icon;
	}

	public String get_per_icon() {
		return _per_icon;
	}

	public Timestamp _per_created_date;

	public void set_field(Timestamp per_created_date) {
		_per_created_date = per_created_date;
	}

	public Timestamp get_field() {
		return _per_created_date;
	}

	public Timestamp _per_updated_date;

	public void set_per_updated_date(Timestamp per_updated_date) {
		_per_updated_date = per_updated_date;
	}

	public Timestamp get_per_updated_date() {
		return _per_updated_date;
	}

	public boolean _per_status;

	public void set_per_status(boolean per_status) {
		_per_status = per_status;
	}

	public boolean get_per_status() {
		return _per_status;
	}
}
