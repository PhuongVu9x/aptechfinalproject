package vpn.model;

import java.sql.Timestamp;

public class Position {
	public int _pos_id;

	public void set_pos_id(int pos_id) {
		_pos_id = pos_id;
	}

	public int get_pos_id() {
		return _pos_id;
	}
	
	public String _pos_name;

	public void set_pos_name(String pos_name) {
		_pos_name = pos_name;
	}

	public String get_pos_name() {
		return _pos_name;
	}

	public String _pos_icon;

	public void set_pos_icon(String pos_icon) {
		_pos_icon = pos_icon;
	}

	public String get_pos_icon() {
		return _pos_icon;
	}

	public Timestamp _pos_created_date;

	public void set_field(Timestamp pos_created_date) {
		_pos_created_date = pos_created_date;
	}

	public Timestamp get_field() {
		return _pos_created_date;
	}

	public Timestamp _pos_updated_date;

	public void set_pos_updated_date(Timestamp pos_updated_date) {
		_pos_updated_date = pos_updated_date;
	}

	public Timestamp get_pos_updated_date() {
		return _pos_updated_date;
	}

	public boolean _pos_status;

	public void set_pos_status(boolean pos_status) {
		_pos_status = pos_status;
	}

	public boolean get_pos_status() {
		return _pos_status;
	}
}





