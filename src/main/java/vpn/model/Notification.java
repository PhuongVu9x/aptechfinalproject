package vpn.model;

import java.sql.Timestamp;

public class Notification {
	public int _noti_id;

	public void set_noti_id(int bo_id) {
		_noti_id = bo_id;
	}

	public int get_noti_id() {
		return _noti_id;
	}
	
	public int _noti_cus_id;

	public void set_noti_cus_id(int noti_cus_id) {
		_noti_cus_id = noti_cus_id;
	}

	public int get_noti_cus_id() {
		return _noti_cus_id;
	}
	
	public int _noti_fl_id;

	public void set_noti_fl_id(int noti_fl_id) {
		_noti_fl_id = noti_fl_id;
	}

	public int get_noti_fl_id() {
		return _noti_fl_id;
	}
	
	public String _noti_description;

	public void set_noti_description(String noti_description) {
		_noti_description = noti_description;
	}

	public String get_noti_description() {
		return _noti_description;
	}
	
	public int _noti_type;

	public void set_noti_type(int noti_type) {
		_noti_type = noti_type;
	}

	public int get_noti_type() {
		return _noti_type;
	}
	
	public int _noti_status;

	public void set_noti_status(int noti_status) {
		_noti_status = noti_status;
	}

	public int get_noti_status() {
		return _noti_status;
	}
	
	public int _noti_stt;

	public void set_noti_stt(int noti_stt) {
		_noti_stt = noti_stt;
	}

	public int get_noti_stt() {
		return _noti_stt;
	}
	
	public Timestamp _noti_created_date;

	public void set_field(Timestamp per_created_date) {
		_noti_created_date = per_created_date;
	}

	public Timestamp get_field() {
		return _noti_created_date;
	}

	public Timestamp _noti_updated_date;

	public void set_noti_updated_date(Timestamp per_updated_date) {
		_noti_updated_date = per_updated_date;
	}

	public Timestamp get_noti_updated_date() {
		return _noti_updated_date;
	}
}
