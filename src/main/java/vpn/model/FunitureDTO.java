package vpn.model;

import java.sql.Timestamp;

public class FunitureDTO {
	public int _fur_id;

	public void set_fur_id(int fur_id) {
		_fur_id = fur_id;
	}

	public int get_fur_id() {
		return _fur_id;
	}
	public int _fur_index;

	public void set_fur_index(int fur_index) {
		_fur_index = fur_index;
	}

	public int get_fur_index() {
		return _fur_index;
	}

	public String _fur_name;

	public void set_fur_name(String fur_name) {
		_fur_name = fur_name;
	}

	public String get_fur_name() {
		return _fur_name;
	}

	public String _fur_icon;

	public void set_fur_icon(String fur_icon) {
		_fur_icon = fur_icon;
	}

	public String get_fur_icon() {
		return _fur_icon;
	}

	public Timestamp _fur_created_date;

	public void set_field(Timestamp fur_created_date) {
		_fur_created_date = fur_created_date;
	}

	public Timestamp get_field() {
		return _fur_created_date;
	}

	public Timestamp _fur_updated_date;

	public void set_fur_updated_date(Timestamp fur_updated_date) {
		_fur_updated_date = fur_updated_date;
	}

	public Timestamp get_fur_updated_date() {
		return _fur_updated_date;
	}

	public boolean _fur_status;

	public void set_fur_status(boolean fur_status) {
		_fur_status = fur_status;
	}

	public boolean get_fur_status() {
		return _fur_status;
	}
}
