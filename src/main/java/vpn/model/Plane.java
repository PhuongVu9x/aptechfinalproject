package vpn.model;

import java.sql.Timestamp;

public class Plane {
	public int _pl_id;

	public void set_pl_id(int pl_id) {
		_pl_id = pl_id;
	}

	public int get_pl_id() {
		return _pl_id;
	}

	public String _pl_code;

	public void set_pl_code(String pl_code) {
		_pl_code = pl_code;
	}

	public String get_pl_code() {
		return _pl_code;
	}

	public int _pl_carrier_id;

	public void set_field(int pl_carrier_id) {
		_pl_carrier_id = pl_carrier_id;
	}

	public int get_field() {
		return _pl_carrier_id;
	}

	public int _pl_pt_id;

	public void set_pl_pt_id(int pl_pt_id) {
		_pl_pt_id = pl_pt_id;
	}

	public int get_pl_pt_id() {
		return _pl_pt_id;
	}

	public Timestamp _pl_created_date;

	public void set_pl_created_date(Timestamp pl_created_date) {
		_pl_created_date = pl_created_date;
	}

	public Timestamp get_pl_created_date() {
		return _pl_created_date;
	}

	public Timestamp _pl_updated_date;

	public void set_pl_updated_date(Timestamp pl_updated_date) {
		_pl_updated_date = pl_updated_date;
	}

	public Timestamp get_pl_updated_date() {
		return _pl_updated_date;
	}

	public boolean _pl_status;

	public void set_pl_status(boolean pl_status) {
		_pl_status = pl_status;
	}

	public boolean get_pl_status() {
		return _pl_status;
	}

}
