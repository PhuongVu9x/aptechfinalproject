package vpn.model;

import java.sql.Timestamp;

import vpn.AppThread;

public class BookingDetail {
	public int _db_id;

	public void set_db_id(int db_id) {
		_db_id = db_id;
	}

	public int get_db_id() {
		return _db_id;
	}
	public int _db_index;

	public void set_db_index(int db_index) {
		_db_index = db_index;
	}

	public int get_db_index() {
		return _db_index;
	}

	public int _db_ro_id;

	public void set_db_ro_id(int db_ro_id) {
		_db_ro_id = db_ro_id;
	}

	public int get_db_ro_id() {
		return _db_ro_id;
	}

	public String _db_cus_name;

	public void set_db_cus_name(String db_cus_name) {
		_db_cus_name = db_cus_name;
	}

	public String get_db_cus_name() {
		return _db_cus_name;
	}

	public String _bo_cus_passport;

	public void set_bo_cus_passport(String bo_cus_passport) {
		_bo_cus_passport = bo_cus_passport;
	}

	public String get_bo_cus_passport() {
		return _bo_cus_passport;
	}

	public Timestamp _db_created_date;

	public void set_db_created_date(Timestamp db_created_date) {
		_db_created_date = db_created_date;
	}

	public Timestamp get_db_created_date() {
		return _db_created_date;
	}

	public Timestamp _db_updated_date;

	public void set_db_updated_date(Timestamp db_updated_date) {
		_db_updated_date = db_updated_date;
	}

	public Timestamp get_db_updated_date() {
		return _db_updated_date;
	}

	public boolean _db_status;

	public void set_db_status(boolean db_status) {
		_db_status = db_status;
	}

	public boolean get_db_status() {
		return _db_status;
	}

	public int _rt_id;

	public void set_rt_id(int rt_id) {
		_rt_id = rt_id;
	}

	public int get_rt_id() {
		return _rt_id;
	}
	
	public String _rt_name;

	public void set_rt_name(String rt_name) {
		_rt_name = rt_name;
	}

	public String get_rt_name() {
		return _rt_name;
	}
	
	public double _db_payment; 

	public void set_db_payment(double db_payment) {
		_db_payment = db_payment;
	}

	public double get_db_payment() {
		return _db_payment;
	}
	public int _ro_ho_id; 

	public void set_ro_ho_id(int ro_ho_id) {
		_ro_ho_id = ro_ho_id;
	}

	public int get_ro_ho_id() {
		return _ro_ho_id;
	}
	
	public String _ro_furnitures;

	public void set_ro_furnitures(String ro_furnitures) {
		_ro_furnitures = ro_furnitures;
	}

	public String get_ro_furnitures() {
		return _ro_furnitures;
	}
	
	public String _ro_policies;

	public void set_ro_policies(String ro_policies) {
		_ro_policies = ro_policies;
	}
 
	public String get_ro_policies() {
		return _ro_policies;
	}
	
	public String _db_payment_string;

	public String get_db_payment_string() {
		return AppThread.ToVND(_db_payment);
	}

	public String _db_updated_timestamp;

	public String get_db_updated_timestamp() {
		return AppThread.ToVnTime(_db_updated_date);
	}

	public String _db_created_timestamp;

	public String get_db_created_timestamp() {
		return AppThread.ToVnTime(_db_created_date);
	}
}
