package vpn.model;

import java.util.LinkedList;

public class BillReq {
	public Bill _bil;

	public void set_bil(Bill bil) {
		_bil = bil;
	}

	public Bill get_bil() {
		return _bil;
	}
	
	public LinkedList<Ticket> _tickets;

	public void set_tickets(LinkedList<Ticket> tickets) {
		_tickets = tickets;
	}

	public LinkedList<Ticket> get_tickets() {
		return _tickets;
	}
	public LinkedList<TicketDTO> _mobile_tickets;

	public void set_mobile_tickets(LinkedList<TicketDTO> mobile_tickets) {
		_mobile_tickets = mobile_tickets;
	}

	public LinkedList<TicketDTO> get_mobile_tickets() {
		return _mobile_tickets;
	}
	
	public BillDTO _bil_mobile;
	
	public void set_bil_mobile(BillDTO bil) {
		_bil_mobile = bil;
	}

	public BillDTO get_bil_mobile() {
		return _bil_mobile;
	}
	
}