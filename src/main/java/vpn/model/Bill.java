package vpn.model;

import java.sql.Timestamp;

public class Bill {
	public int _bil_id;

	public void set_bil_id(int bil_id) {
		_bil_id = bil_id;
	}

	public int get_bil_id() {
		return _bil_id;
	}
	
	public int _bil_fl_return_id;

	public void set_bil_fl_return_id(int bil_fl_return_id) {
		_bil_fl_return_id = bil_fl_return_id;
	}

	public int get_bil_fl_return_id() {
		return _bil_fl_return_id;
	}

	public int _bil_cus_id;

	public void set_bil_cus_id(int bil_cus_id) {
		_bil_cus_id = bil_cus_id;
	}
	
	public int get_bil_cus_id() {
		return _bil_cus_id;
	}

	public int get_bil_fl_id() {
		return _bil_fl_id;
	}
	
	public int _bil_fl_id;

	public void set_bil_fl_id(int bil_fl_id) {
		_bil_fl_id = bil_fl_id;
	}

	public float _bil_payment;

	public void set_bil_payment(float bil_payment) {
		_bil_payment = bil_payment;
	}

	public float get_bil_payment() {
		return _bil_payment;
	}

	public Timestamp _bil_created_date;

	public void set_bil_created_date(Timestamp bil_created_date) {
		_bil_created_date = bil_created_date;
	}

	public Timestamp get_bil_created_date() {
		return _bil_created_date;
	}

	public Timestamp _bil_updated_date;

	public void set_bil_updated_date(Timestamp bil_updated_date) {
		_bil_updated_date = bil_updated_date;
	}

	public Timestamp get_bil_updated_date() {
		return _bil_updated_date;
	}

	public int _bil_status;

	public void set_bil_status(int bil_status) {
		_bil_status = bil_status;
	}

	public int get_bil_status() {
		return _bil_status;
	}
	
	public String _bil_payment_code;

	public void set_bil_payment_code(String bil_payment_code) {
		_bil_payment_code = bil_payment_code;
	}

	public String get_bil_payment_code() {
		return _bil_payment_code;
	}

	public String _bil_payment_type;

	public void set_bil_payment_type(String bil_payment_type) {
		_bil_payment_type = bil_payment_type;
	}

	public String get_bil_payment_type() {
		return _bil_payment_type;
	}
}
