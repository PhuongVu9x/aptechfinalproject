package vpn.model;

import java.sql.Timestamp;

public class AdminPermision {
	public int _ap_id;

	public void set_ap_id(int ap_id) {
		_ap_id = ap_id;
	}

	public int get_ap_id() {
		return _ap_id;
	}

	public int _ap_ad_id;

	public void set_ap_ad_id(int ap_ad_id) {
		_ap_ad_id = ap_ad_id;
	}

	public int get_ap_ad_id() {
		return _ap_ad_id;
	}

	public int _ap_per_id;

	public void set_ap_per_id(int ap_per_id) {
		_ap_per_id = ap_per_id;
	}

	public int get_ap_per_id() {
		return _ap_per_id;
	}

	public Timestamp _ap_created_date;

	public void set_ap_created_date(Timestamp ap_created_date) {
		_ap_created_date = ap_created_date;
	}

	public Timestamp get_ap_created_date() {
		return _ap_created_date;
	}

	public Timestamp _ap_updated_date;

	public void set_ap_updated_date(Timestamp ap_updated_date) {
		_ap_updated_date = ap_updated_date;
	}

	public Timestamp get_ap_updated_date() {
		return _ap_updated_date;
	}

	public boolean _ap_status;

	public void set_ap_status(boolean ap_status) {
		_ap_status = ap_status;
	}

	public boolean get_ap_status() {
		return _ap_status;
	}

}
