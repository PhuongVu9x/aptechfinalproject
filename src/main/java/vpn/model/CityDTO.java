package vpn.model;

import java.sql.Timestamp;

public class CityDTO {
	public int _city_id;

	public void set_city_id(int city_id) {
		_city_id = city_id;
	}

	public int get_city_id() {
		return _city_id;
	}
	public int _city_index;

	public void set_city_index(int city_index) {
		_city_index = city_index;
	}

	public int get_city_index() {
		return _city_index;
	}

	public String _city_name;

	public void set_city_name(String city_name) {
		_city_name = city_name;
	}

	public String get_city_name() {
		return _city_name;
	}

	public String _city_icon;

	public void set_city_icon(String city_icon) {
		_city_icon = city_icon;
	}

	public String get_city_icon() {
		return _city_icon;
	}

	public Timestamp _city_created_date;

	public void set_field(Timestamp city_created_date) {
		_city_created_date = city_created_date;
	}

	public Timestamp get_field() {
		return _city_created_date;
	}

	public Timestamp _city_updated_date;

	public void set_city_updated_date(Timestamp city_updated_date) {
		_city_updated_date = city_updated_date;
	}

	public Timestamp get_city_updated_date() {
		return _city_updated_date;
	}

	public boolean _city_status;

	public void set_city_status(boolean city_status) {
		_city_status = city_status;
	}

	public boolean get_city_status() {
		return _city_status;
	}
	
	public int _city_ct_id;

	public void set_city_ct_id(int city_ct_id) {
		_city_ct_id = city_ct_id;
	}

	public int get_city_ct_id() {
		return _city_ct_id;
	}

	public String _ct_name;

	public void set_ct_name(String ct_name) {
		_ct_name = ct_name;
	}

	public String get_ct_name() {
		return _ct_name;
	}

	public boolean _ct_status;

	public void set_ct_status(boolean ct_status) {
		_ct_status = ct_status;
	}

	public boolean get_ct_status() {
		return _ct_status;
	}

}
