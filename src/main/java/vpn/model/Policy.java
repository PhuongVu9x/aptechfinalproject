package vpn.model;

import java.sql.Timestamp;

public class Policy {
	public int _po_id;

	public void set_po_id(int po_id) {
		_po_id = po_id;
	}

	public int get_po_id() {
		return _po_id;
	}

	public String _po_icon;

	public void set_po_icon(String po_icon) {
		_po_icon = po_icon;
	}

	public String get_po_icon() {
		return _po_icon;
	}

	public Timestamp _po_created_date;

	public void set_field(Timestamp po_created_date) {
		_po_created_date = po_created_date;
	}

	public Timestamp get_field() {
		return _po_created_date;
	}

	public Timestamp _po_updated_date;

	public void set_po_updated_date(Timestamp po_updated_date) {
		_po_updated_date = po_updated_date;
	}

	public Timestamp get_po_updated_date() {
		return _po_updated_date;
	}

	public boolean _po_status;

	public void set_po_status(boolean po_status) {
		_po_status = po_status;
	}

	public boolean get_po_status() {
		return _po_status;
	}
	
	public String _po_description;

	public void set_po_description(String po_description) {
		_po_description = po_description;
	}

	public String get_po_description() {
		return _po_description;
	}
}
