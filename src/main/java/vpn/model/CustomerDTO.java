package vpn.model;

import java.sql.Timestamp;

public class CustomerDTO {
	public int _cus_id;

	public void set_cus_id(int cus_id) {
		_cus_id = cus_id;
	}

	public int get_cus_id() {
		return _cus_id;
	}
	public int _cus_index;

	public void set_cus_index(int cus_index) {
		_cus_index = cus_index;
	}

	public int get_cus_index() {
		return _cus_index;
	}
	
	public String _cus_token;

	public void set_cus_token(String cus_token) {
		_cus_token = cus_token;
	}

	public String get_cus_token() {
		return _cus_token;
	}
	
	public String _cart_detail;

	public void set_cart_detail(String cart_detail) {
		_cart_detail = cart_detail;
	}

	public String get_cart_detail() {
		return _cart_detail;
	}
	
	public String _cus_active_token;

	public void set_cus_active_token(String cus_active_token) {
		_cus_active_token = cus_active_token;
	}

	public String get_cus_active_token() {
		return _cus_active_token;
	}
	
	public String _cus_first_name;

	public void set_cus_first_name(String cus_first_name) {
		_cus_first_name = cus_first_name;
	}

	public String get_cus_first_name() {
		return _cus_first_name;
	}

	public String _cus_last_name;

	public void set_cus_last_name(String cus_last_name) {
		_cus_last_name = cus_last_name;
	}

	public String get_cus_last_name() {
		return _cus_last_name;
	}

	public String _cus_full_name;

	public void set_cus_full_name(String cus_full_name) {
		_cus_full_name = cus_full_name;
	}

	public String get_cus_full_name() {
		return _cus_full_name;
	}

	public String _cus_email;

	public void set_cus_email(String cus_email) {
		_cus_email = cus_email;
	}

	public String get_cus_email() {
		return _cus_email;
	}

	public String _cus_dob;

	public void set_cus_dob(String cus_dob) {
		_cus_dob = cus_dob;
	}

	public String get_cus_dob() {
		return _cus_dob;
	}

	public String _cus_password;

	public void set_cus_(String cus_password) {
		_cus_password = cus_password;
	}

	public String get_cus_() {
		return _cus_password;
	}

	public String _cus_phone;

	public void set_cus_phone(String cus_phone) {
		_cus_phone = cus_phone;
	}

	public String get_cus_phone() {
		return _cus_phone;
	}
	
	public String _cus_address;

	public void set_cus_address(String cus_address) {
		_cus_address = cus_address;
	}

	public String get_cus_address() {
		return _cus_address;
	}
	
	public String _cus_avatar;

	public void set_cus_avatar(String cus_avatar) {
		_cus_avatar = cus_avatar;
	}

	public String get_cus_avatar() {
		return _cus_avatar;
	}

	public Timestamp _cus_created_date;

	public void set_cus_created_date(Timestamp cus_created_date) {
		_cus_created_date = cus_created_date;
	}

	public Timestamp get_cus_created_date() {
		return _cus_created_date;
	}

	public Timestamp _cus_updated_date;

	public void set_cus_updated_date(Timestamp cus_updated_date) {
		_cus_updated_date = cus_updated_date;
	}

	public Timestamp get_cus_updated_date() {
		return _cus_updated_date;
	}
	
	public boolean _cus_active_status;

	public void set_cus_active_status(boolean cus_active_status) {
		_cus_active_status = cus_active_status;
	}

	public boolean get_cus_active_status() {
		return _cus_active_status;
	}
	
	public boolean _cus_status;

	public void set_cus_status(boolean cus_status) {
		_cus_status = cus_status;
	}

	public boolean get_cus_status() {
		return _cus_status;
	}
}
