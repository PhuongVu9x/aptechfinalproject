package vpn.model;

import java.sql.Timestamp;

public class PlaneTypeDTO {
	public int _pt_id;

	public void set_pt_id(int pt_id) {
		_pt_id = pt_id;
	}

	public int get_pt_id() {
		return _pt_id;
	}
	public int _pt_index;

	public void set_pt_index(int pt_index) {
		_pt_index = pt_index;
	}

	public int get_pt_index() {
		return _pt_index;
	}

	public String _pt_name;

	public void set_pt_name(String pt_name) {
		_pt_name = pt_name;
	}

	public String get_pt_name() {
		return _pt_name;
	}
	
	public int _pt_total_seats;

	public void set_pt_total_seats(int pt_total_seats) {
		_pt_total_seats = pt_total_seats;
	}

	public int get_pt_total_seats() {
		return _pt_total_seats;
	}

	public int _pt_ec_quantity;

	public void set_pt_ec_quantity(int pt_ec_quantity) {
		_pt_ec_quantity = pt_ec_quantity;
	}

	public int get_pt_ec_quantity() {
		return _pt_ec_quantity;
	}

	public int _pt_sc_quantity;

	public void set_pt_sc_quantity(int pt_sc_quantity) {
		_pt_sc_quantity = pt_sc_quantity;
	}

	public int get_pt_sc_quantity() {
		return _pt_sc_quantity;
	}

	public int _pt_bs_quantity;

	public void set_pt_bs_quantity(int pt_bs_quantity) {
		_pt_bs_quantity = pt_bs_quantity;
	}

	public int get_pt_bs_quantity() {
		return _pt_bs_quantity;
	}

	public int _pt_fc_quantity;

	public void set_pt_fc_quantity(int pt_fc_quantity) {
		_pt_fc_quantity = pt_fc_quantity;
	}

	public int get_pt_fc_quantity() {
		return _pt_fc_quantity;
	}

	public Timestamp _pt_created_date;

	public void set_pt_created_date(Timestamp pt_created_date) {
		_pt_created_date = pt_created_date;
	}

	public Timestamp get_pt_created_date() {
		return _pt_created_date;
	}

	public Timestamp _pt_updated_date;

	public void set_pt_updated_date(Timestamp pt_updated_date) {
		_pt_updated_date = pt_updated_date;
	}

	public Timestamp get_pt_updated_date() {
		return _pt_updated_date;
	}

	public boolean _pt_status;

	public void set_pt_status(boolean pt_status) {
		_pt_status = pt_status;
	}

	public boolean get_pt_status() {
		return _pt_status;
	}
}
