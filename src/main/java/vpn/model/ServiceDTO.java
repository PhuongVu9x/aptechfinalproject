package vpn.model;

import java.sql.Timestamp;

public class ServiceDTO {
	public int _ser_id;

	public void set_ser_id(int ser_id) {
		_ser_id = ser_id;
	}

	public int get_ser_id() {
		return _ser_id;
	}
	public int _ser_index;

	public void set_ser_index(int ser_index) {
		_ser_index = ser_index;
	}

	public int get_ser_index() {
		return _ser_index;
	}

	public String _ser_name;

	public void set_ser_name(String ser_name) {
		_ser_name = ser_name;
	}

	public String get_ser_name() {
		return _ser_name;
	}

	public String _ser_icon;

	public void set_ser_icon(String ser_icon) {
		_ser_icon = ser_icon;
	}

	public String get_ser_icon() {
		return _ser_icon;
	}

	public Timestamp _ser_created_date;

	public void set_field(Timestamp ser_created_date) {
		_ser_created_date = ser_created_date;
	}

	public Timestamp get_field() {
		return _ser_created_date;
	}

	public Timestamp _ser_updated_date;

	public void set_ser_updated_date(Timestamp ser_updated_date) {
		_ser_updated_date = ser_updated_date;
	}

	public Timestamp get_ser_updated_date() {
		return _ser_updated_date;
	}

	public boolean _ser_status;

	public void set_ser_status(boolean ser_status) {
		_ser_status = ser_status;
	}

	public boolean get_ser_status() {
		return _ser_status;
	}
}
