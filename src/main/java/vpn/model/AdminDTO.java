package vpn.model;

import java.sql.Timestamp;

public class AdminDTO {
	public int _ad_id;

	public void set_ad_id(int ad_id) {
		_ad_id = ad_id;
	}

	public int get_ad_id() {
		return _ad_id;
	}
	public int _ad_index;

	public void set_ad_index(int ad_index) {
		_ad_index = ad_index;
	}

	public int get_ad_index() {
		return _ad_index;
	}

	public String _ad_title;

	public void set_ad_title(String ad_title) {
		_ad_title = ad_title;
	}

	public String get_ad_title() {
		return _ad_title;
	}

	public String _ad_first_name;

	public void set_ad_first_name(String ad_first_name) {
		_ad_first_name = ad_first_name;
	}

	public String get_ad_first_name() {
		return _ad_first_name;
	}

	public String _ad_last_name;

	public void set_ad_last_name(String ad_last_name) {
		_ad_last_name = ad_last_name;
	}

	public String get_ad_last_name() {
		return _ad_last_name;
	}

	public String _ad_full_name;

	public void set_ad_full_name(String ad_full_name) {
		_ad_full_name = ad_full_name;
	}

	public String get_ad_full_name() {
		return _ad_full_name;
	}

	public String _ad_email;

	public void set_ad_email(String ad_email) {
		_ad_email = ad_email;
	}

	public String get_ad_email() {
		return _ad_email;
	}

	public String _ad_token;

	public void set_ad_token(String ad_token) {
		_ad_token = ad_token;
	}

	public String get_ad_token() {
		return _ad_token;
	}

	public Timestamp _ad_dob;

	public void set_ad_dob(Timestamp ad_dob) {
		_ad_dob = ad_dob;
	}

	public Timestamp get_ad_dob() {
		return _ad_dob;
	}

	public String _ad_password;

	public void set_ad_password(String ad_password) {
		_ad_password = ad_password;
	}

	public String get_ad_password() {
		return _ad_password;
	}

	public String _ad_phone;

	public void set_ad_phone(String ad_phone) {
		_ad_phone = ad_phone;
	}

	public String get_ad_phone() {
		return _ad_phone;
	}

	public String _ad_avatar;

	public void set_ad_avatar(String ad_avatar) {
		_ad_avatar = ad_avatar;
	}

	public String get_ad_avatar() {
		return _ad_avatar;
	}

	public Timestamp _ad_created_date;

	public void set_ad_created_date(Timestamp ad_craeted_date) {
		_ad_created_date = ad_craeted_date;
	}

	public Timestamp get_ad_created_date() {
		return _ad_created_date;
	}

	public Timestamp _ad_updated_date;

	public void set_ad_updated_date(Timestamp ad_updated_date) {
		_ad_updated_date = ad_updated_date;
	}

	public Timestamp get_ad_updated_date() {
		return _ad_updated_date;
	}

	public boolean _ad_status;

	public void set_ad_status(boolean ad_status) {
		_ad_status = ad_status;
	}

	public boolean get_ad_status() {
		return _ad_status;
	}

	public int _ad_pos_id;

	public void set_ad_pos_id(int ad_pos_id) {
		_ad_pos_id = ad_pos_id;
	}

	public int get_ad_pos_id() {
		return _ad_pos_id;
	}
	
	public int _pos_id;

	public void set_pos_id(int pos_id) {
		_pos_id = pos_id;
	}

	public int get_pos_id() {
		return _pos_id;
	}
	
	public String _pos_name;

	public void set_pos_name(String pos_name) {
		_pos_name = pos_name;
	}

	public String get_pos_name() {
		return _pos_name;
	}
	
	public String _pos_icon;

	public void set_pos_icon(String pos_icon) {
		_pos_icon = pos_icon;
	}

	public String get_pos_icon() {
		return _pos_icon;
	}

	public String _ad_address;

	public void set_ad_address(String ad_address) {
		_ad_address = ad_address;
	}

	public String get_ad_address() {
		return _ad_address;
	}

	public int _per_id;

	public void set_per_id(int per_id) {
		_per_id = per_id;
	}

	public int get_per_id() {
		return _per_id;
	}
	
	public String _per_name;

	public void set_per_name(String per_name) {
		_per_name = per_name;
	}

	public String get_per_name() {
		return _per_name;
	}
}
