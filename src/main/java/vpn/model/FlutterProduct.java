package vpn.model;

public class FlutterProduct {
	public int _id;

	public void set_id(int fl_id) {
		_id = fl_id;
	}

	public int get_id() {
		return _id;
	}
	
	public String _name;

	public void set_name(String name) {
		_name = name;
	}

	public String get_name() {
		return _name;
	}
	
	public String _image;

	public void set_image(String image) {
		_image = image;
	}

	public String get_image() {
		return _image;
	}

	public double _price;

	public void set_price(double fl_adult_price) {
		_price = fl_adult_price;
	}

	public double get_price() {
		return _price;
	}

}
