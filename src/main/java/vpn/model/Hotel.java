package vpn.model;

import java.sql.Timestamp;

public class Hotel {
	public int _ho_id;

	public void set_ho_id(int ho_id) {
		_ho_id = ho_id;
	}

	public int get_ho_id() {
		return _ho_id;
	}

	public String _ho_name;

	public void set_ho_name(String ho_name) {
		_ho_name = ho_name;
	}

	public String get_ho_name() {
		return _ho_name;
	}

	public int _ho_city_id;

	public void set_ho_city_id(int ho_city_id) {
		_ho_city_id = ho_city_id;
	}

	public int get_ho_city_id() {
		return _ho_city_id;
	}

	public int _ho_rooms;

	public void set_ho_rooms(int ho_rooms) {
		_ho_rooms = ho_rooms;
	}

	public int get_ho_rooms() {
		return _ho_rooms;
	}

	public int _ho_signle_rooms;

	public void set_ho_signle_rooms(int ho_single_rooms) {
		_ho_signle_rooms = ho_single_rooms;
	}

	public int get_ho_signle_rooms() {
		return _ho_signle_rooms;
	}

	public int _ho_double_rooms;

	public void set_ho_double_rooms(int ho_double_rooms) {
		_ho_double_rooms = ho_double_rooms;
	}

	public int get_ho_double_rooms() {
		return _ho_double_rooms;
	}

	public int _ho_triple_rooms;

	public void set_ho_triple_rooms(int ho_tripble_rooms) {
		_ho_triple_rooms = ho_tripble_rooms;
	}

	public int get_ho_triple_rooms() {
		return _ho_triple_rooms;
	}

	public int _ho_stars;

	public void set_ho_stars(int ho_stars) {
		_ho_stars = ho_stars;
	}

	public int get_ho_stars() {
		return _ho_stars;
	}

	public String _ho_address;

	public void set_ho_address(String ho_address) {
		_ho_address = ho_address;
	}

	public String get_ho_address() {
		return _ho_address;
	}

	public double _ho_from_price;

	public void set_ho_from_price(double ho_from_price) {
		_ho_from_price = ho_from_price;
	}

	public double get_ho_from_price() {
		return _ho_from_price;
	}

	public double _ho_to_price;

	public void set_ho_to_price(double ho_to_price) {
		_ho_to_price = ho_to_price;
	}

	public double get_ho_to_price() {
		return _ho_to_price;
	}

	public String _ho_icon;

	public void set_ho_icon(String ho_icon) {
		_ho_icon = ho_icon;
	}

	public String get_ho_icon() {
		return _ho_icon;
	}
	
	public String _ho_background;

	public void set_ho_background(String ho_background) {
		_ho_background = ho_background;
	}

	public String get_ho_background() {
		return _ho_background;
	}

	public Timestamp _ho_created_date;

	public void set_ho_created_date(Timestamp ho_created_date) {
		_ho_created_date = ho_created_date;
	}

	public Timestamp get_ho_created_date() {
		return _ho_created_date;
	}

	public Timestamp _ho_updated_date;

	public void set_ho_updated_date(Timestamp ho_updated_date) {
		_ho_updated_date = ho_updated_date;
	}

	public Timestamp get_ho_updated_date() {
		return _ho_updated_date;
	}

	public int _ho_status;

	public void set_ho_status(int ho_status) {
		_ho_status = ho_status;
	}

	public int get_ho_status() {
		return _ho_status;
	}

}
