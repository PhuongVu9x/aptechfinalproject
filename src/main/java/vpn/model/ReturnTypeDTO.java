package vpn.model;

import java.sql.Timestamp;

public class ReturnTypeDTO {
	public int _ret_id;

	public void set_ret_id(int ret_id) {
		_ret_id = ret_id;
	}

	public int get_ret_id() {
		return _ret_id;
	}
	public int _ret_index;

	public void set_ret_index(int ret_index) {
		_ret_index = ret_index;
	}

	public int get_ret_index() {
		return _ret_index;
	}

	public String _ret_name;

	public void set_ret_name(String ret_name) {
		_ret_name = ret_name;
	}

	public String get_ret_name() {
		return _ret_name;
	}

	public String _ret_icon;

	public void set_ret_icon(String ret_icon) {
		_ret_icon = ret_icon;
	}

	public String get_ret_icon() {
		return _ret_icon;
	}
	public int _ret_percent;

	public void set_ret_percent(int ret_percent) {
		_ret_percent = ret_percent;
	}

	public int get_ret_percent() {
		return _ret_percent;
	}
	public Timestamp _ret_created_date;

	public void set_field(Timestamp ret_created_date) {
		_ret_created_date = ret_created_date;
	}

	public Timestamp get_field() {
		return _ret_created_date;
	}

	public Timestamp _ret_updated_date;

	public void set_ret_updated_date(Timestamp ret_updated_date) {
		_ret_updated_date = ret_updated_date;
	}

	public Timestamp get_ret_updated_date() {
		return _ret_updated_date;
	}

	public boolean _ret_status;

	public void set_ret_status(boolean ret_status) {
		_ret_status = ret_status;
	}

	public boolean get_ret_status() {
		return _ret_status;
	}
}
