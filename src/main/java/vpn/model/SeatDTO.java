package vpn.model;

public class SeatDTO {
	public int _id;

	public void set_idx(int id) {
		_id = id;
	}

	public int get_id() {
		return _id;
	}
	
	public String _code;

	public void set_code(String code) {
		_code = code;
	}

	public String get_code() {
		return _code;
	}
	public int _index;

	public void set_index(int index) {
		_index = index;
	}

	public int get_index() {
		return _index;
	}

	public String _class;

	public void set_class(String _class) {
		this._class = _class;
	}

	public String get_class() {
		return _class;
	}

	public boolean _status;

	public void set_status(boolean status) {
		_status = status;
	}

	public boolean get_status() {
		return _status;
	}
	
	public int _class_id;

	public void set_class_id(int class_id) {
		_class_id = class_id;
	}

	public int get_class_id() {
		return _class_id;
	}


	public SeatDTO(String code,int _class_id,String _class,boolean status) {
		this._code = code;
		this._class_id = _class_id;
		this._class = _class;
		this._status = status;
	}
}
