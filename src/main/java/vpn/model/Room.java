package vpn.model;

import java.sql.Timestamp;

public class Room {
	public int _ro_id;

	public void set_ro_id(int ro_id) {
		_ro_id = ro_id;
	}

	public int get_ro_id() {
		return _ro_id;
	}

	public String _ro_number;

	public void set_ro_number(String ro_number) {
		_ro_number = ro_number;
	}

	public int _ro_ho_id;

	public void set_ro_ho_id(int ro_ho_id) {
		_ro_ho_id = ro_ho_id;
	}

	public int get_ro_ho_id() {
		return _ro_ho_id;
	}

	public int _ro_room_type_id;

	public void set_ro_room_type_id(int ro_room_type_id) {
		_ro_room_type_id = ro_room_type_id;
	}

	public int get_ro_room_type_id() {
		return _ro_room_type_id;
	}

	public double _ro_price;

	public void set_ro_price(double ro_price) {
		_ro_price = ro_price;
	}

	public double get_ro_price() {
		return _ro_price;
	}

	public String _ro_funitures;

	public void set_ro_funitures(String ro_funitures) {
		_ro_funitures = ro_funitures;
	}

	public String get_ro_funitures() {
		return _ro_funitures;
	}

	public String _ro_policies;

	public void set_ro_policies(String ro_policies) {
		_ro_policies = ro_policies;
	}

	public String get_ro_policies() {
		return _ro_policies;
	}

	public String _ro_icon;

	public void set_ro_icon(String ro_icon) {
		_ro_icon = ro_icon;
	}

	public String get_ro_icon() {
		return _ro_icon;
	}

	public String _ro_images;

	public void set_ro_images(String ro_images) {
		_ro_images = ro_images;
	}

	public String get_ro_images() {
		return _ro_images;
	}

	public int _ro_max_customer;

	public void set_ro_max_customer(int ro_max_customer) {
		_ro_max_customer = ro_max_customer;
	}

	public int get_ro_max_customer() {
		return _ro_max_customer;
	}

	public Timestamp _ro_created_date;

	public void set_ro_created_date(Timestamp ro_created_date) {
		_ro_created_date = ro_created_date;
	}

	public Timestamp get_ro_created_date() {
		return _ro_created_date;
	}

	public Timestamp _ro_updated_date;

	public void set_ro_updated_date(Timestamp ro_updated_date) {
		_ro_updated_date = ro_updated_date;
	}

	public Timestamp get_ro_updated_date() {
		return _ro_updated_date;
	}

	public int _ro_status;

	public void set_ro_status(int ro_status) {
		_ro_status = ro_status;
	}

	public int get_ro_status() {
		return _ro_status;
	}
	
	public String _ro_code;

	public void set_ro_code(String ro_code) {
		_ro_code = ro_code;
	}

	public String get_ro_code() {
		return _ro_code;
	}

}
