package vpn;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import vpn.model.AdminDTO;
import vpn.model.DBConnection;
import vpn.model.Mapper;

public class NotificationThread extends Thread{
public static int _cus_id = 0;
public static int _fl_id = 0;
public static int _noti_type = 0;
	public void run() {
		SetupDefault();
	}
	
	public void SetupDefault() {
		try {
			var _noti_description = "";
			for(var flight : FlightThread.flights) {
				if(flight._fl_id == NotificationThread._fl_id) {
					var milisecs = flight._fl_take_off.getTime() - System.currentTimeMillis() ;
					var days = milisecs / 1000 / 60 /60 /24;
					if(_noti_type == 1)
						_noti_description = "Congratulations! Your ticket has been successfully booked! Only " +days+ " days left until your flight!";
					else 
						_noti_description = "Congratulations! Your ticket has been successfully booked! Only " +days+ " days left until your flight!";
					break;
				}
			}
			LocalDateTime now = LocalDateTime.now();
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			
			var params = new LinkedList<String>();
			params.add(_cus_id+"");
			params.add(_fl_id+"");
			params.add(_noti_description+"");
			params.add(_noti_type+"");
			params.add(dtf.format(now));
			DBConnection.CallProc("Sp_Noti_Insert", params);
			
			this.stop();
		} catch (ClassNotFoundException | IllegalArgumentException  e) {			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
