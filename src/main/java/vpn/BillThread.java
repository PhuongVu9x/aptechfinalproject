package vpn;

import java.util.HashMap;
import java.util.LinkedList;

import vpn.model.AirportDTO;
import vpn.model.BillDTO;
import vpn.model.DBConnection;
import vpn.model.Mapper;

public class BillThread extends Thread{
	public static LinkedList<BillDTO> bills;
	public static LinkedList<BillDTO> customerBillsHistory;
	public static HashMap<String,BillDTO> billsByUser;
	
	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		try {
			var rsAirport = DBConnection.CallProc("Sp_Bill_Gets");
			var rsCusBil= DBConnection.CallProc("Sp_Customer_BillHistory");
			bills = Mapper.MapTo(new BillDTO(), rsAirport);
			customerBillsHistory = Mapper.MapTo(new BillDTO(), rsCusBil);
			System.out.println(AppThread.CountDown + ": Bill All Done.");
			AppThread.CountDown--;
			if (AppThread.CountDown < 0)
				AppThread.CountDown = 0;
			this.interrupt();
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
