package vpn.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import vpn.model.Admin;
import vpn.model.AdminDTO;
import vpn.model.DBConnection;
import vpn.model.Decrypt;
import vpn.model.Mapper;
import vpn.model.Result;
import vpn.vnpay.Config;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
@Controller
public class BaseController {
	@RequestMapping(value = "/admin/login", method = RequestMethod.GET)
	public String admin_login(Model model) {
		model.addAttribute("admin", new Admin());

		return "admin/login";
	}
	
	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public String error() {

		return "shared/error";
	}
	 
	@RequestMapping(value = "/admin/login", method = RequestMethod.POST)
	public String login(String _ad_email, String _ad_password, RedirectAttributes redirectAttributes,HttpServletResponse response)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {

		String token = UUID.randomUUID().toString();
		var params = new LinkedList<String>();
		params.add(_ad_email);
		params.add(Decrypt.ToMd5(_ad_password));
		params.add(token);
		var rs = DBConnection.CallProc("Sp_Admin_Login", params);
		var data = Mapper.MapTo(new AdminDTO(), rs);
		var isSuccess = data.isEmpty();
		var mes = isSuccess ? "Wrong Email or Password.Please check again" : "";
		var result = new Result<AdminDTO>(null, isSuccess, mes);
		if (isSuccess) {
			//redirectAttributes.addFlashAttribute("admin", result);
			return "redirect:/admin/login";
		}

		var admin = data.getFirst();
		
		int cookieExpiry = 86400; 										
		Cookie createCookie = new Cookie("userCookie","Admin");					
		createCookie.setMaxAge(cookieExpiry);
		createCookie.setPath("/");
		response.addCookie(createCookie);
		redirectAttributes.addFlashAttribute("admin", admin);
		return "redirect:/admin/home";
	}		
	
	@RequestMapping(value = "/vnpay_return", method = RequestMethod.GET)
	public String vnpay_return(Model model) {

		return "admin/login";
	}
	
	@RequestMapping(value = "/vnpay", method = RequestMethod.POST)
	public String vnpay(HttpServletRequest req) throws UnsupportedEncodingException {
		String vnp_Version = "2.1.0";
        String vnp_Command = "pay";
        String orderType = "other";
        long amount = Integer.parseInt(req.getParameter("amount"))*100;
        String bankCode = req.getParameter("bankCode");
        
        String vnp_TxnRef = Config.getRandomNumber(8);
        String vnp_IpAddr = Config.getIpAddress(req);

        String vnp_TmnCode = Config.vnp_TmnCode;
        
        Map<String, String> vnp_Params = new HashMap<>();
        vnp_Params.put("vnp_Version", vnp_Version);
        vnp_Params.put("vnp_Command", vnp_Command);
        vnp_Params.put("vnp_TmnCode", vnp_TmnCode);
        vnp_Params.put("vnp_Amount", String.valueOf(amount));
        vnp_Params.put("vnp_CurrCode", "VND");
        
        if (bankCode != null && !bankCode.isEmpty()) {
            vnp_Params.put("vnp_BankCode", bankCode);
        }
        vnp_Params.put("vnp_TxnRef", vnp_TxnRef);
        vnp_Params.put("vnp_OrderInfo", "Thanh toan don hang:" + vnp_TxnRef);
        vnp_Params.put("vnp_OrderType", orderType);
        String locate = req.getParameter("language");
        if (locate != null && !locate.isEmpty()) {
            vnp_Params.put("vnp_Locale", locate);
        } else {
            vnp_Params.put("vnp_Locale", "vn");
        }
        vnp_Params.put("vnp_ReturnUrl", Config.vnp_ReturnUrl);
        vnp_Params.put("vnp_IpAddr", vnp_IpAddr);

        Calendar cld = Calendar.getInstance(TimeZone.getTimeZone("Etc/GMT+7"));
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        String vnp_CreateDate = formatter.format(cld.getTime());
        vnp_Params.put("vnp_CreateDate", vnp_CreateDate);
        cld.add(Calendar.MINUTE, 15);
        String vnp_ExpireDate = formatter.format(cld.getTime());
        vnp_Params.put("vnp_ExpireDate", vnp_ExpireDate);
        List fieldNames = new ArrayList(vnp_Params.keySet());
        Collections.sort(fieldNames);
        StringBuilder hashData = new StringBuilder();
        StringBuilder query = new StringBuilder();
        Iterator itr = fieldNames.iterator();
        while (itr.hasNext()) {
            String fieldName = (String) itr.next();
            String fieldValue = (String) vnp_Params.get(fieldName);
            if ((fieldValue != null) && (fieldValue.length() > 0)) {
                //Build hash data
                hashData.append(fieldName);
                hashData.append('=');
                hashData.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
                //Build query
                query.append(URLEncoder.encode(fieldName, StandardCharsets.US_ASCII.toString()));
                query.append('=');
                query.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
                if (itr.hasNext()) {
                    query.append('&');
                    hashData.append('&');
                }
            }
        }
        String queryUrl = query.toString();
        String vnp_SecureHash = Config.hmacSHA512(Config.secretKey, hashData.toString());
        queryUrl += "&vnp_SecureHash=" + vnp_SecureHash;
        String paymentUrl = Config.vnp_PayUrl + "?" + queryUrl;
        com.google.gson.JsonObject job = new JsonObject();
        job.addProperty("code", "00");
        job.addProperty("message", "success");
        job.addProperty("data", paymentUrl);
        Gson gson = new Gson();
		return "123";
	}
	
	@RequestMapping(value = "/vnpay", method = RequestMethod.GET)
	public String vnpayview(Model model) {

		return "shared/vpcpay";
	}
}
