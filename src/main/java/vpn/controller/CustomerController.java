package vpn.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.LinkedList;
import org.apache.commons.lang3.RandomStringUtils;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import vpn.AdminThread;
import vpn.AirportThread;
import vpn.CarrierThread;
import vpn.FlightThread;
import vpn.ServiceThread;
import vpn.model.*;

@Controller
public class CustomerController {
	/* -----------------------------------------------------------------------------------
	   ----------------------------------- HOME ------------------------------------------
	   -----------------------------------------------------------------------------------*/
	@RequestMapping("/home")
    public String Home(){
        return "client/home";
    }
	
	@RequestMapping(value = "/profile/{_cus_id}")
    public String Profile(@PathVariable String _cus_id, Model model) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException{
		//String cusId = _cus_id.substring(_cus_id.length() - 2, _cus_id.length());
		String cusId = _cus_id.substring(_cus_id.length() - 1, _cus_id.length());
		
		System.out.println(cusId);
		var params = new LinkedList<String>();
		params.add(Integer.parseInt(cusId) + "");
		var rsUser = DBConnection.CallProc("Sp_Customer_GetById_Web", params);
		var dataUser = Mapper.MapTo(new CustomerDTO(), rsUser);
		var isSuccessUser = !dataUser.isEmpty();
		var mesUser = isSuccessUser ? "" : "No data found(s)!";
		var resultUser = new Result<LinkedList<CustomerDTO>>(dataUser,isSuccessUser,mesUser);
		model.addAttribute("resultUser", resultUser);
		model.addAttribute("randomStr", _cus_id);
		model.addAttribute("updateNoti", "Update successful");
		
        return "client/profile";
    }
	
	@RequestMapping(value = "/profile/update-information", method = RequestMethod.POST)
	String CustomerUpdate(Model model, int _cus_id, String randomString, String _cus_first_name, String _cus_last_name, String _cus_dob, String _cus_email, String _cus_password,
					   String _cus_avatar, String _cus_phone, String _cus_address, @RequestParam("cusFile") MultipartFile cusFile, HttpServletRequest request, RedirectAttributes redirectAttributes) 
					   throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		String secretStr = RandomStringUtils.randomAlphanumeric(15) + String.valueOf(_cus_id);
		
		String cus_avatar = _cus_avatar;
		if (!cusFile.getOriginalFilename().equals("")) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(cusFile, serverPath);
			cus_avatar = "http://localhost:8080/images/" + cusFile.getOriginalFilename();
		}else {
			cus_avatar = "http://localhost:8080/images/" + _cus_avatar;
		}
		
		var params = new LinkedList<String>();
		params.add(_cus_id + "");
		params.add(_cus_first_name);
		params.add(_cus_last_name);
		params.add(_cus_first_name + " " + _cus_last_name);
		params.add(_cus_email);
		params.add(_cus_phone);
		params.add(_cus_address);
		params.add(cus_avatar);
		params.add(_cus_dob);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));
		
		var rsUpdate = DBConnection.CallProc("Sp_Customer_UpdateWeb", params);
		var dataUpdate = Mapper.MapTo(new CustomerDTO(), rsUpdate);
		var isSuccessUpdate = !dataUpdate.isEmpty();
		var mes = isSuccessUpdate ? "Update Customer Successfull." : "Update Customer Fail!";
		
		redirectAttributes.addFlashAttribute("mes", mes);
		redirectAttributes.addAttribute("secretStr", secretStr);
		model.addAttribute("randomStr", secretStr);
		
		return "redirect:/profile/{secretStr}";
	}
	
	@RequestMapping(value = "/profile/bill_history/{_cus_id}", method = RequestMethod.GET)
    public String BillHistory(@PathVariable String _cus_id, Model model) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException{
		//String cusId = _cus_id.substring(_cus_id.length() - 2, _cus_id.length());
		String cusId = _cus_id.substring(_cus_id.length() - 1, _cus_id.length());
		
		
		//Get Customer
		var params = new LinkedList<String>();
		params.add(Integer.parseInt(cusId) + "");
		var rsUser = DBConnection.CallProc("Sp_Customer_GetById_Web", params);
		var dataUser = Mapper.MapTo(new CustomerDTO(), rsUser);
		var isSuccessUser = !dataUser.isEmpty();
		var mesUser = isSuccessUser ? "" : "No data found(s)!";
		var resultUser = new Result<LinkedList<CustomerDTO>>(dataUser,isSuccessUser,mesUser);
		model.addAttribute("resultUser", resultUser);
		
		//Get Bill By Customer
		var paramsBill = new LinkedList<String>();
		paramsBill.add(Integer.parseInt(cusId) + "");
		var rsBill = DBConnection.CallProc("Sp_Bill_GetByCustomer", paramsBill);
		var dataBill = Mapper.MapTo(new BillDTO(), rsBill); 
		var isSuccessBill = !dataBill.isEmpty();
		var mesBill = isSuccessBill ? "" : "No data found(s)!";
		var resultBills = new Result<LinkedList<BillDTO>>(dataBill,isSuccessBill,mesBill);
		
		
		for (BillDTO billDTO : resultBills.data) {
			//Get Ticket Information By Customer
			var paramsTicket = new LinkedList<String>();
			paramsTicket.add(billDTO._bil_id + "");	
			var rsTicket = DBConnection.CallProc("Sp_Ticket_GetByBill", paramsTicket);
			var dataTicket = Mapper.MapTo(new TicketDTO(), rsTicket);
			var isSuccessTicket = !dataTicket.isEmpty();
			var mesTicket = isSuccessTicket ? "" : "No Data Found!";
			var resultTickets = new Result<LinkedList<TicketDTO>>(dataTicket, isSuccessTicket, mesTicket);
			model.addAttribute("resultTickets", resultTickets);
			
			billDTO._tickets = dataTicket;
			if(isSuccessTicket) {
				for(var ticketDTO : billDTO._tickets) {
					var rsFlights = DBConnection.CallProc("Sp_Flight_Gets");
					var dataFlights = Mapper.MapTo(new FlightDTO(), rsFlights);
					var isSuccessFlights = !dataFlights.isEmpty();
					var mesFlights = isSuccessBill ? "" : "No data found(s)!";
					var resultFlights = new Result<LinkedList<FlightDTO>>(dataFlights,isSuccessFlights,mesFlights);
					
					if(isSuccessFlights) {
						for(var flight : resultFlights.data) {
							if(flight._fl_id == ticketDTO._tk_fl_id) {
								ticketDTO._tk_fl_services_convert = SubstringFlightServices(flight._fl_services);
								ticketDTO._tk_flight = flight;
								break;
							}
						}
					}
				}
			}
		}
		
		model.addAttribute("resultBills", resultBills);
		model.addAttribute("randomStr", _cus_id);
		
        return "client/bill_history";
    }
	
	@RequestMapping(value = "/register-notification", method = RequestMethod.GET)
	public String RegisterSuccess(){
		return "client/success";
	}
	
	@RequestMapping(value = "/user/logout/{_cus_id}", method = RequestMethod.GET)
	public String CustomerLogout(@PathVariable String _cus_id, RedirectAttributes redirectAttributes,HttpServletResponse response)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		//String cusId = _cus_id.substring(_cus_id.length() - 2, _cus_id.length());
		//String cusId = _cus_id.substring(_cus_id.length() - 1, _cus_id.length());
				
		var params = new LinkedList<String>();
		params.add(Integer.parseInt(_cus_id) + "");
		var rs = DBConnection.CallProc("Sp_Customer_Logout_Web", params);
		var data = Mapper.MapTo(new CustomerDTO(), rs);
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "Logout Fail. Something wrong in Server!";
		var result = new Result<CustomerDTO>(isSuccess ? data.getFirst() : null, isSuccess, mes);
		return "redirect:/";
	}
	
	@RequestMapping(value = "/eticket", method = RequestMethod.GET)
    public String Eticket(Model model) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException{
		return "client/eticket";
    }
	/* -----------------------------------------------------------------------------------
	   ----------------------------------- BOOKING FORM ----------------------------------
	   -----------------------------------------------------------------------------------*/
	@RequestMapping(value = "/", method = RequestMethod.GET)
    public String BookingForm(Model model) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException{
		var rsAirports = DBConnection.CallProc("Sp_Airport_Gets");
		var dataAirports = Mapper.MapTo(new AirportDTO(), rsAirports);
		var isSuccessAirports = !dataAirports.isEmpty();
		var mesAirports = isSuccessAirports ? "" : "No Data Found.";
		var resultAirports = new Result<LinkedList<AirportDTO>>(dataAirports, isSuccessAirports, mesAirports);

		model.addAttribute("resultAirports", resultAirports);
		
		
		  var rsTicketClasses = DBConnection.CallProc("Sp_TicketClass_Gets"); 
		  var dataTicketClasses = Mapper.MapTo(new TicketClassDTO(), rsTicketClasses); 
		  var isSuccessTicketClasses = !dataAirports.isEmpty(); 
		  var mesTicketClasses = isSuccessAirports ? "" : "No Data Found."; 
		  var resultTicketClasses = new Result<LinkedList<TicketClassDTO>>(dataTicketClasses, isSuccessTicketClasses, mesTicketClasses);
		  
		  model.addAttribute("resultTicketClasses", resultTicketClasses);
		 
		
		//var rsHotels = DBConnection.CallProc("Sp_Hotel_Gets");
		//var dataHotels = Mapper.MapTo(new HotelDTO(), rsHotels);
		//var isSuccessHotels = !dataHotels.isEmpty();
		//var mesHotels = isSuccessHotels ? "" : "No Data Found.";
		//var resultHotels = new Result<LinkedList<HotelDTO>>(dataHotels, isSuccessHotels, mesHotels);
		//model.addAttribute("resultHotels", resultHotels);
        return "shared/booking_form";
    }
	
	/* -----------------------------------------------------------------------------------
	   ------------------------------- FILTER FLIGHT -------------------------------------
	   -----------------------------------------------------------------------------------*/
	@PostMapping("/filterflight")
	public String FilterFlight(HttpServletRequest request, Model model) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		//Prepare parameters to get Flight 
		String ticketClass = request.getParameter("ticketClass-selection");
		String airline = "0";
		String adultQuan = request.getParameter("flightAdultQuan");
		if(adultQuan == null || adultQuan == "") {
			adultQuan = "1";
		}
		String childQuan = request.getParameter("flightChildQuan");
		if(childQuan == null || childQuan == "") {
			childQuan = "0";
		}
		String infantQuan = request.getParameter("flightInfantQuan");
		if(infantQuan == null || infantQuan == "") {
			infantQuan = "0";
		}
		
		String departureDestination = request.getParameter("departure-selection");
		String arrivalDestination = request.getParameter("arrival-selection");
		String departureDate = request.getParameter("departure-calendar");
		
		int quanTotal = Integer.parseInt(adultQuan) + Integer.parseInt(childQuan) + Integer.parseInt(infantQuan);

		DateTimeFormatter DATEFORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate ldDeparture = LocalDate.parse(departureDate, DATEFORMATTER);
		
		//Filter: Convert Airline  
		LinkedList<String> airlines = new LinkedList<String>();
		if(request.getParameter("airlines") != null && request.getParameter("airlines").length() > 0) {
			var airlineConvert = Arrays.asList(request.getParameter("airlines").split(","));
			for(String air : airlineConvert) {
				airlines.add(air);
			}
		}
		
		//Filter: Convert Facilities 
		LinkedList<String> facilities = new LinkedList<String>();
		if(request.getParameter("facilities") != null && request.getParameter("facilities").length() > 0) {
			var facilitiesConvert = Arrays.asList(request.getParameter("facilities").split(","));
			for(String facility : facilitiesConvert) {
				facilities.add(facility);
			}
		}
		
		//Get Plane Information
		var isSuccessAirlines = !CarrierThread.carriers.isEmpty();
		var mesAirlines = isSuccessAirlines ? "" : "No Data Found.";
		var resultAirlines = new Result<LinkedList<CarrierDTO>>(CarrierThread.carriers, isSuccessAirlines, mesAirlines);
		model.addAttribute("resultAirlines", resultAirlines);
		
		//Get Airport Information
		var isSuccessAirports = !AirportThread.airports.isEmpty();
		var mesAirports = isSuccessAirports ? "" : "No Data Found.";
		var resultAirports = new Result<LinkedList<AirportDTO>>(AirportThread.airports, isSuccessAirports, mesAirports);
		model.addAttribute("resultAirports", resultAirports);
		
		//Get Flight List
		var rsService = DBConnection.CallProc("Sp_Service_Gets"); 
		var dataServices = Mapper.MapTo(new ServiceDTO(), rsService);
		var dataServicestReturn = new LinkedList<ServiceDTO>();
		for (ServiceDTO serviceDTO : dataServices) {
			dataServicestReturn.add(serviceDTO);
		}
		var isSuccessServices = !dataServices.isEmpty();
		var mesServices = isSuccessServices ? "" : "No Data Found.";
		var resultServices = new Result<LinkedList<ServiceDTO>>(dataServicestReturn, isSuccessServices, mesServices);
		model.addAttribute("resultServices", resultServices);
		
		var params = new LinkedList<String>();
		params.add(departureDestination);
		params.add(arrivalDestination);
		params.add(airline);
		params.add(DATEFORMATTER.format(ldDeparture));
		params.add(ticketClass);
		params.add(String.valueOf(quanTotal));
		var rs = DBConnection.CallProc("Sp_Flight_GetsForCustomer", params); 
		var dataFlights = Mapper.MapTo(new FlightDTO(), rs);
		var isSuccessFlights = !dataFlights.isEmpty();
		var mesFlights = isSuccessFlights ? "" : "Update Flight Fail!";
		var resultFlights = new Result<LinkedList<FlightDTO>>(dataFlights, isSuccessFlights, mesFlights);
		
		for (FlightDTO flightDTO : resultFlights.data) {
			flightDTO._fl_domestic = false;
			flightDTO._fl_services_convert = SubstringFlightServices(flightDTO._fl_services);
			flightDTO._fl_fly_duration = ConvertTimestamp(flightDTO._fl_take_off, flightDTO._fl_arrival);
			flightDTO._fl_adult_quan = Integer.parseInt(adultQuan);
			flightDTO._fl_child_quan = Integer.parseInt(childQuan);
			flightDTO._fl_infant_quan = Integer.parseInt(infantQuan);
			flightDTO._tc_id = Integer.parseInt(ticketClass);
			flightDTO._fl_adult_price = PriceAdultDetail(flightDTO._fl_adult_quan, flightDTO);
			flightDTO._fl_child_price = PriceChildDetail(flightDTO._fl_child_quan, flightDTO);
			flightDTO._fl_infant_price = PriceInfantDetail(flightDTO._fl_infant_quan, flightDTO);
			flightDTO._fl_total_price = flightDTO._fl_adult_price + flightDTO._fl_child_price + flightDTO._fl_infant_price;
			flightDTO._fl_total_pasg = flightDTO._fl_adult_quan + flightDTO._fl_child_quan + flightDTO._fl_infant_quan;
			
			if(!FilterAirline(flightDTO._car_id, request.getParameter("airlines"))){
				continue;
			}
			
			if(!FilterFacilities(flightDTO._fl_services_convert, request.getParameter("facilities"))){
				continue;
			}
			
			if(flightDTO._ct_from_id == flightDTO._ct_to_id) {
				flightDTO._fl_domestic = true;
			}
		}
		
		model.addAttribute("resultFlights", resultFlights);
        return "client/flight/FilterFlight";
	}
	
	/* -----------------------------------------------------------------------------------
	   ------------------------------- 4-STEP FORM (FLIGHT) ------------------------------
	   -----------------------------------------------------------------------------------*/
	@RequestMapping(value = "/passenger-detail", method = RequestMethod.POST)
	public String PassengerDetail(HttpServletRequest request, Model model) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var flightID = request.getParameter("_fl_id");
		var ticketClass = request.getParameter("_tc_id");
		var adultQuan = request.getParameter("flightAdultQuan");
		var childQuan = request.getParameter("flightChildQuan");
		var infantQuan = request.getParameter("flightInfantQuan");

		var params = new LinkedList<String>();
		params.add(flightID);
		
		System.out.println(flightID);
		
		var rsService = DBConnection.CallProc("Sp_Service_Gets"); 
		var dataServices = Mapper.MapTo(new ServiceDTO(), rsService);
		var dataServicestReturn = new LinkedList<ServiceDTO>();
		for (ServiceDTO serviceDTO : dataServices) {
			dataServicestReturn.add(serviceDTO);
		}
		var isSuccessServices = !dataServices.isEmpty();
		var mesServices = isSuccessServices ? "" : "No Data Found.";
		var resultServices = new Result<LinkedList<ServiceDTO>>(dataServicestReturn, isSuccessServices, mesServices);
		model.addAttribute("resultServices", resultServices);
		
		var rs = DBConnection.CallProc("Sp_Flight_Info", params); 
		var dataFlights = Mapper.MapTo(new FlightDTO(), rs);
		var dataFlightReturn = new LinkedList<FlightDTO>();
		for (FlightDTO flightDTO : dataFlights) {
			flightDTO._tc_id = Integer.parseInt(ticketClass);
			flightDTO._fl_domestic = false;
			flightDTO._fl_services_convert = SubstringFlightServices(flightDTO._fl_services);
			flightDTO._fl_fly_duration = ConvertTimestamp(flightDTO._fl_take_off, flightDTO._fl_arrival);
			flightDTO._fl_adult_quan = Integer.parseInt(adultQuan);
			flightDTO._fl_child_quan = Integer.parseInt(childQuan);
			flightDTO._fl_infant_quan = Integer.parseInt(infantQuan);
			flightDTO._fl_adult_price = PriceAdultDetail(flightDTO._fl_adult_quan, flightDTO);
			flightDTO._fl_child_price = PriceChildDetail(flightDTO._fl_child_quan, flightDTO);
			flightDTO._fl_infant_price = PriceInfantDetail(flightDTO._fl_infant_quan, flightDTO);
			flightDTO._fl_total_price = flightDTO._fl_adult_price + flightDTO._fl_child_price + flightDTO._fl_infant_price;
			flightDTO._fl_total_pasg = flightDTO._fl_adult_quan + flightDTO._fl_child_quan + flightDTO._fl_infant_quan;
			
			if(flightDTO._ct_from_id == flightDTO._ct_to_id) {
				flightDTO._fl_domestic = true;
			}
			
			dataFlightReturn.add(flightDTO);
		}
		var isSuccessFlights = !dataFlights.isEmpty();
		var mesFlights = isSuccessFlights ? "" : "No data found(s)!";
		var rsData = isSuccessFlights ? dataFlights.getFirst() : null;
		
		//GET SEAT
		if (isSuccessFlights) {
			params.add(ticketClass);
			var rsTicket = DBConnection.CallProc("Sp_Ticket_GetsByFlight", params);
			var dataTicket = Mapper.MapTo(new TicketDTO(), rsTicket);

			var index = 1;
			var seatID = 1;
			var rows = 0;
			var lastCol = 'A';
			var firstCol = 'A';
			rsData._fl_seats = new LinkedList<SeatDTO>();
			switch(Integer.parseInt(ticketClass)) {
			case 1:
				// economy seat
				rows = rsData._pt_ec_quantity / 6  + 1;
				if(rows > 0) {
					for (int i = 1; i <= rows; i++) {
						for (char j = 'A'; j <= 'F'; j++) {
							var code = j + "" + index + "";
							var _class = "Economic";
							var status = CheckSeat(dataTicket, code);
							var seat = new SeatDTO(code, Integer.parseInt(ticketClass), _class, status);
							rsData._fl_seats.add(seat);
						}
						index++;
					}
				}else {
					lastCol =  (char)(firstCol + rsData._pt_ec_quantity);
					for (char j = firstCol; j < lastCol; j++) {
						var code = j + "" + index;
						var _class = "Economic";
						var status = CheckSeat(dataTicket, code);
						var seat = new SeatDTO(code, Integer.parseInt(ticketClass), _class, status);
						
						rsData._fl_seats.add(seat);
					}
				}
				
				for (SeatDTO seat : rsData._fl_seats) {
					seat._id = seatID;
					seatID++;
				}
				
				break;
			case 2:
				// special economy seat
				rows = rsData._pt_sc_quantity / 6  + 1;
				if(rows > 0) {
					for (int i = 1; i <= rows; i++) {
						for (char j = 'A'; j <= 'F'; j++) {
							var code = j + "" + index + "";
							var _class = "Special Economic";
							var status = CheckSeat(dataTicket, code);
							var seat = new SeatDTO(code, Integer.parseInt(ticketClass), _class, status);
							rsData._fl_seats.add(seat);
						}
						index++;
					}
				}else {
					lastCol =  (char)(firstCol + rsData._pt_sc_quantity);
					for (char j = firstCol; j < lastCol; j++) {
						var code = j + "" + index;
						var _class = "Special Economic";
						var status = CheckSeat(dataTicket, code);
						var seat = new SeatDTO(code, Integer.parseInt(ticketClass), _class, status);
						rsData._fl_seats.add(seat);
					}
				}
				
				for (SeatDTO seat : rsData._fl_seats) {
					seat._id = seatID;
					seatID++;
				}
				
				break;
			case 3:
				// business seat
				rows = rsData._pt_bs_quantity / 6;
				if(rows > 0) {
					for (int i = 1; i <= rows; i++) {
						for (char j = 'A'; j <= 'F'; j++) {
							var code = j + "" + index + "";
							var _class = "Business";
							var status = CheckSeat(dataTicket, code);
							var seat = new SeatDTO(code, Integer.parseInt(ticketClass), _class, status);
							rsData._fl_seats.add(seat);
						}
						index++;
					}
				}else {
					lastCol =  (char)(firstCol + rsData._pt_bs_quantity);
					for (char j = firstCol; j < lastCol; j++) {
						var code = j + "" + index;
						var _class = "Business";
						var status = CheckSeat(dataTicket, code);
						var seat = new SeatDTO(code, Integer.parseInt(ticketClass), _class, status);
						rsData._fl_seats.add(seat);
					}
				}
				
				for (SeatDTO seat : rsData._fl_seats) {
					seat._id = seatID;
					seatID++;
				}
				
				break;
			case 4:
				// business seat
				rows = rsData._pt_fc_quantity / 6;
				if(rows > 0) {
					
				}else {
					lastCol =  (char)(firstCol + rsData._pt_fc_quantity);
					for (char j = firstCol; j < lastCol; j++) {
						var code = j + "" + index;
						var _class = "First Class";
						var status = CheckSeat(dataTicket, code);
						var seat = new SeatDTO(code, Integer.parseInt(ticketClass), _class, status);
						rsData._fl_seats.add(seat);
					}
				}
				
				for (SeatDTO seat : rsData._fl_seats) {
					seat._id = seatID;
					seatID++;
				}
				
				break;
			}
		}
		var resultFlights = new Result<LinkedList<FlightDTO>>(dataFlightReturn, isSuccessFlights, mesFlights);
		model.addAttribute("resultFlights", resultFlights);
		
		return "client/flight/4-step Form/PassengerDetail";
	}

	boolean CheckSeat(LinkedList<TicketDTO> tickets, String seat) {
		var check = false;
		for (TicketDTO ticket : tickets) {
			if (ticket._tk_seat.equals(seat)) {
				check = true;
				break;
			}
		}

		return check;
	}
	
	@RequestMapping(value = "/payment-flight", method = RequestMethod.GET)
	public String PaymentFlight(){
		return "client/flight/4-step Form/PaymentFlight";
	}
	
	@RequestMapping(value = "/notification", method = RequestMethod.GET)
	public String Notification(HttpServletRequest request, Model model){
		String transactionStatus = request.getParameter("vnp_TransactionStatus");
		String paymentCode = request.getParameter("vnp_TmnCode");
		
		model.addAttribute("status", transactionStatus);
		model.addAttribute("_bill_payment_code", paymentCode);
		return "client/notification";
	}
	/* ------------------------------- FUNCTION ------------------------------------- */
	private String ConvertTimestamp(Timestamp departure, Timestamp arrival) {
		var duration = arrival.getTime() - departure.getTime();
		var milliseconds = Math.floor((duration % 1000) / 100);
	    var seconds = Math.floor((duration / 1000) % 60);
	    var minutes = Math.floor((duration / (1000 * 60)) % 60);
	    var hours = Math.floor((duration / (1000 * 60 * 60)) % 24);
	    
	    String minutesDisplay;
	    String hoursDisplay;
	    
	    if(minutes < 10) {
	    	minutesDisplay =  "0" + minutes;
	    }else {
	    	minutesDisplay = String.valueOf(minutes);
	    }
	    
	    hoursDisplay = String.valueOf(hours).substring(0, String.valueOf(hours).length() - 2) + "h" + " " + String.valueOf(minutesDisplay).substring(0, String.valueOf(minutesDisplay).length() - 2) + "m";
	    return hoursDisplay;
	}
	
	private LinkedList<ServiceDTO> SubstringFlightServices(String services) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var arrSerID = new LinkedList<String>(Arrays.asList(services.split(",")));
		var arrResult = new LinkedList<ServiceDTO>();
		
		for (String service : arrSerID) {
			for(ServiceDTO serviceDTO: ServiceThread.services) {
				if(service.equals(String.valueOf(serviceDTO._ser_id))) {
					arrResult.add(serviceDTO);
				}
			}
		}
		
		return arrResult;
	}
	
	private boolean FilterAirline(int airlineID, String airlinesParam) {
		if(airlinesParam == null || airlinesParam.length() <= 0) {
			return true;
		}
		return airlinesParam.contains(String.valueOf(airlineID));
	}
	
	private boolean FilterFacilities(LinkedList<ServiceDTO> servicesArr, String servicesParam) {
		/*
		 * int check = 0;
		 * 
		 * if(servicesParam == null || servicesParam.length() <= 0) { return true; }
		 * 
		 * var facilityConvert = Arrays.asList(servicesParam.split(",")); for(String
		 * facility : facilityConvert) { for(ServiceDTO services : servicesArr) {
		 * if(String.valueOf(services._ser_id).equals(facility)) { check++; } } } return
		 * check >= facilityConvert.size();
		 */
		boolean check = false;
		
		if(servicesParam == null || servicesParam.length() <= 0) {
			return true;
		}
		
		var facilityConvert = Arrays.asList(servicesParam.split(","));
		System.out.println(facilityConvert);
		System.out.println(servicesParam);
		for(String facility : facilityConvert) {
			if(check) {
				break;
			}
			for(ServiceDTO services : servicesArr) {
				if(String.valueOf(services._ser_id).equals(facility)) {
					check = true;
					break;
				}
			}
		}
		System.out.println(check);
		return check;
	}
	
	private double PriceAdultDetail(int adultQuan, FlightDTO flightDTO) {
		double adultPrice = 0;
		
		switch(flightDTO._tc_id) {
			case 1:
				adultPrice = flightDTO._fl_ec_price * adultQuan;
				break;
			case 2:
				adultPrice = flightDTO._fl_sc_price * adultQuan;
				break;
			case 3:
				adultPrice = flightDTO._fl_bs_price * adultQuan;
				break;
			case 4:
				adultPrice = flightDTO._fl_fc_price * adultQuan;
				break;
		}
		return adultPrice;
	}
	
	private double PriceChildDetail(int childQuan, FlightDTO flightDTO) {
		double childPrice = 0;
		
		switch(flightDTO._tc_id) {
			case 1:
				childPrice = (flightDTO._fl_ec_price * 20 /100) * childQuan;
				break;
			case 2:
				childPrice = (flightDTO._fl_sc_price * 20 /100) * childQuan;
				break;
			case 3:
				childPrice = (flightDTO._fl_bs_price * 20 /100) * childQuan;
				break;
			case 4:
				childPrice = (flightDTO._fl_fc_price * 20 /100) * childQuan;
				break;
		}
		return childPrice;
	}
	
	private double PriceInfantDetail(int infantQuan, FlightDTO flightDTO) {
		double infantPrice = 0;
		
		switch(flightDTO._tc_id) {
			case 1:
				infantPrice = (flightDTO._fl_ec_price * 50 /100) * infantQuan;
				break;
			case 2:
				infantPrice = (flightDTO._fl_sc_price * 50 /100) * infantQuan;
				break;
			case 3:
				infantPrice = (flightDTO._fl_bs_price * 50 /100) * infantQuan;
				break;
			case 4:
				infantPrice = (flightDTO._fl_fc_price * 50 /100) * infantQuan;
				break;
		}
		return infantPrice;
	}
	
	void UpdaloadFile(MultipartFile multipartFile, String serverPath) {
		try {
			byte[] bytes = multipartFile.getBytes();

			File serverFile = new File(serverPath + File.separator + multipartFile.getOriginalFilename());
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
			stream.write(bytes);
			stream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/* -----------------------------------------------------------------------------------
	   ------------------------------- FILTER HOTEL-------------------------------------
	   -----------------------------------------------------------------------------------*/
	@RequestMapping("/filterhotel")
	 public String FilterHotel(){
	     return "client/hotel/FilterHotel";
	 }
}
