package vpn.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.UUID;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import vpn.AdminThread;
import vpn.AirportThread;
import vpn.BillThread;
import vpn.BookingThread;
import vpn.CarrierThread;
import vpn.CategoryThread;
import vpn.CityThread;
import vpn.CountryThread;
import vpn.CustomerThread;
import vpn.FlightThread;
import vpn.FurnitureThread;
import vpn.HotelThread;
import vpn.PermisionThread;
import vpn.PlaneThread;
import vpn.PlaneTypeThread;
import vpn.PolicyThread;
import vpn.PositionThread;
import vpn.ReturnTypeThread;
import vpn.RoomThread;
import vpn.RoomTypeThread;
import vpn.ServiceThread;
import vpn.TicketClassThread;
import vpn.model.Admin;
import vpn.model.AdminDTO;
import vpn.model.AirportDTO;
import vpn.model.BillDTO;
import vpn.model.BookingDTO;
import vpn.model.CarrierDTO;
import vpn.model.CategoryDTO;
import vpn.model.CityDTO;
import vpn.model.CountryDTO;
import vpn.model.ReturnTypeDTO;
import vpn.model.RoomDTO;
import vpn.model.RoomTypeDTO;
import vpn.model.ServiceDTO;
import vpn.model.CustomerDTO;
import vpn.model.DBConnection;
import vpn.model.Decrypt;
import vpn.model.FlightDTO;
import vpn.model.FunitureDTO;
import vpn.model.HotelDTO;
import vpn.model.Mapper;
import vpn.model.PermisionDTO;
import vpn.model.PlaneDTO;
import vpn.model.PlaneTypeDTO;
import vpn.model.PolicyDTO;
import vpn.model.Position;
import vpn.model.PositionDTO;
import vpn.model.Result;
import vpn.model.TicketClassDTO;
import jakarta.servlet.http.Cookie;
@Controller
public class AdminController {
	@RequestMapping(value = "/admin/dashboard", method = RequestMethod.GET)
	public String dashboard(Model model) {
		var isSuccessBill = !BillThread.bills.isEmpty();
		var mesBill = isSuccessBill ? "" : "No Data Found.";
		var resultBill = new Result<LinkedList<BillDTO>>(BillThread.bills, isSuccessBill, mesBill);
		model.addAttribute("resultBill", resultBill);
		                
		var isSuccessBooking = !BookingThread.bookings.isEmpty();
		var mesBooking = isSuccessBooking ? "" : "No Data Found.";
		var resultBooking = new Result<LinkedList<BookingDTO>>(BookingThread.bookings, isSuccessBooking, mesBooking);
		model.addAttribute("resultBooking", resultBooking);
		
		var isSuccess = !BillThread.customerBillsHistory.isEmpty();
		var mes = isSuccess ? "" : "No Data Found.";
		var result = new Result<LinkedList<BillDTO>>(BillThread.customerBillsHistory, isSuccess, mes);
		model.addAttribute("resultCustomerBillsHistory", result);
		
		var isSuccessCustomer = !CustomerThread.customers.isEmpty();
		var mesCustomer = isSuccessCustomer ? "" : "No Data Found.";
		var resultCustomer = new Result<LinkedList<CustomerDTO>>(CustomerThread.customers, isSuccessCustomer, mesCustomer);
		model.addAttribute("resultCustomer", resultCustomer);
		
		return "admin/super/dashboard";
	}

	@RequestMapping(value = "/admin/home", method = RequestMethod.GET)
	public String adminHome() {

		return "admin/home";
	}

	@RequestMapping(value = "/admin/logout", method = RequestMethod.POST)
	public String logout(int _ad_id, RedirectAttributes redirectAttributes,HttpServletResponse response)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(_ad_id + "");
		var rs = DBConnection.CallProc("Sp_Admin_Logout", params);
		var data = Mapper.MapTo(new AdminDTO(), rs);
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "Logout Fail. Something wrong in Server!";
		var result = new Result<AdminDTO>(isSuccess ? data.getFirst() : null, isSuccess, mes);
		return "admin/login"; 
	}

	// =================================== Super Settings
	@RequestMapping(value = "/admin/settings", method = RequestMethod.GET)
	public String settings(Model model)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {

		var isSuccessPosition = !PositionThread.positions.isEmpty();
		var mesPosition = isSuccessPosition ? "" : "No Data Found.";
		var resultPosition = new Result<LinkedList<PositionDTO>>(PositionThread.positions, isSuccessPosition,
				mesPosition);
		model.addAttribute("resultPosition", resultPosition);

		var isSuccessPermision = !PermisionThread.permisions.isEmpty();
		var mesPermision = isSuccessPermision ? "" : "No Data Found.";
		var resultPermision = new Result<LinkedList<PermisionDTO>>(PermisionThread.permisions, isSuccessPermision,
				mesPermision);
		model.addAttribute("resultPermision", resultPermision); 

		var isSuccessCategory = !CategoryThread.categories.isEmpty();
		var mesCategory = isSuccessCategory ? "" : "No Data Found.";
		var resultCategory = new Result<LinkedList<CategoryDTO>>(CategoryThread.categories, isSuccessCategory,
				mesCategory);
		model.addAttribute("resultCategory", resultCategory);

		var isSuccessCountry = !CountryThread.countries.isEmpty();
		var mesCountry = isSuccessCountry ? "" : "No Data Found.";
		var resultCountry = new Result<LinkedList<CountryDTO>>(CountryThread.countries, isSuccessCountry, mesCountry);
		model.addAttribute("resultCountry", resultCountry);

		var isSuccessCity = !CityThread.cities.isEmpty();
		var mesCity = isSuccessCity ? "" : "No Data Found.";
		var resultCity = new Result<LinkedList<CityDTO>>(CityThread.cities, isSuccessCity, mesCity);
		model.addAttribute("resultCity", resultCity);

		return "admin/super/settings";
	}
	// =================================== ADMIN

	@RequestMapping(value = "/admin/insert", method = RequestMethod.POST)
	String admininsert(String _ad_first_name, String _ad_last_name, String _ad_email, String _ad_phone,
			String _ad_title, int _ad_position_id, String _ad_address, int _ad_per_id,
			@RequestParam("adminFile") MultipartFile adminFile, HttpServletRequest request,
			RedirectAttributes redirectAttributes) throws ClassNotFoundException, IllegalArgumentException,
			IllegalAccessException, InstantiationException, InterruptedException {
		String ad_avatar = "http://localhost:8080/images/default_avatar.jpg";
		if (!adminFile.getOriginalFilename().equals("")) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(adminFile, serverPath);
			ad_avatar = "http://localhost:8080/images/" + adminFile.getOriginalFilename();
		}
		var _ad_password = Decrypt.ToMd5("123456");
		var params = new LinkedList<String>();
		params.add(_ad_first_name);
		params.add(_ad_last_name);
		params.add(_ad_first_name + " " + _ad_last_name);
		params.add(_ad_email);
		params.add(_ad_password);
		params.add(_ad_phone);
		params.add(ad_avatar);
		params.add(_ad_title);
		params.add(_ad_position_id + "");
		params.add(_ad_address.isEmpty() ? "" : _ad_address);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Admin_Insert", params);
		var data = Mapper.MapTo(new AdminDTO(), rs);
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "Create Admin Successfull." : "Create Admin Fail!";

		var adminThread = new AdminThread();
		adminThread.start();
		adminThread.join();
		if (isSuccess) {
			var adminPerParams = new LinkedList<String>();
			adminPerParams.add(data.getFirst()._ad_id + "");
			adminPerParams.add(_ad_per_id + "");
			adminPerParams.add(dtf.format(now));
			DBConnection.CallProc("Sp_AdminPermision_Insert", adminPerParams);
		}

		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/admin/administrators";
	}

	@RequestMapping(value = "/admin/administrators", method = RequestMethod.GET)
	public String administrators(Model model)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {

		var isSuccess = !AdminThread.admins.isEmpty();
		var mes = isSuccess ? "" : "No Data Found.";
		var result = new Result<LinkedList<AdminDTO>>(AdminThread.admins, isSuccess, mes);
		model.addAttribute("resultAdmin", result);

		var isSuccessPosition = !PositionThread.positions.isEmpty();
		var mesPosition = isSuccessPosition ? "" : "No Data Found.";
		var resultPosition = new Result<LinkedList<PositionDTO>>(PositionThread.positions, isSuccessPosition,
				mesPosition);
		model.addAttribute("resultPosition", resultPosition);

		var isSuccessPermision = !PermisionThread.permisions.isEmpty();
		var mesPermision = isSuccessPermision ? "" : "No Data Found.";
		var resultPermision = new Result<LinkedList<PermisionDTO>>(PermisionThread.permisions, isSuccessPermision,
				mesPermision);
		model.addAttribute("resultPermision", resultPermision);
		return "admin/super/administrators";
	}

	@RequestMapping(value = "/admin/update", method = RequestMethod.POST)
	String adminupdate(int _ad_id, String _ad_first_name, String _ad_last_name, String _ad_email, String _ad_phone,
			String _ad_avatar, String _ad_title, int _ad_position_id, String _ad_address, int _ad_per_id,
			@RequestParam("adminFile") MultipartFile adminFile, HttpServletRequest request,
			RedirectAttributes redirectAttributes) throws ClassNotFoundException, IllegalArgumentException,
			IllegalAccessException, InstantiationException, InterruptedException {

		String ad_avatar = _ad_avatar;
		if (!adminFile.getOriginalFilename().equals("")) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(adminFile, serverPath);
			ad_avatar = "http://localhost:8080/images/" + adminFile.getOriginalFilename();
		}

		var params = new LinkedList<String>();
		params.add(_ad_id + "");
		params.add(_ad_first_name);
		params.add(_ad_last_name);
		params.add(_ad_first_name + " " + _ad_last_name);
		params.add(_ad_email);
		params.add(_ad_phone);
		params.add(ad_avatar);
		params.add(_ad_title);
		params.add(_ad_position_id + "");
		params.add(_ad_address);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Admin_Update", params);
		var data = Mapper.MapTo(new AdminDTO(), rs);
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "Update Admin Successfull." : "Update Admin Fail!";
		var adminThread = new AdminThread();
		adminThread.start();
		adminThread.join();
		if (isSuccess) {
			var adminPerParams = new LinkedList<String>();
			adminPerParams.add(data.getFirst()._ad_id + "");
			adminPerParams.add(_ad_per_id + "");
			adminPerParams.add(dtf.format(now));
			DBConnection.CallProc("Sp_AdminPermision_Update", adminPerParams);
		}

		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/admin/administrators";
	}

	@RequestMapping(value = "/admin/updateavatar", method = RequestMethod.POST)
	String updateavatar(int adminId, @RequestParam("adminAvatar") MultipartFile adminAvatar, HttpServletRequest request,
			RedirectAttributes redirectAttributes) throws ClassNotFoundException, IllegalArgumentException,
			IllegalAccessException, InstantiationException, InterruptedException {
		String ad_avatar = "";
		if (!adminAvatar.getOriginalFilename().equals("")) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(adminAvatar, serverPath);
			ad_avatar = "http://localhost:8080/images/" + adminAvatar.getOriginalFilename();
		}
		var params = new LinkedList<String>();
		params.add(adminId + "");
		params.add(ad_avatar);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_Admin_UpdateAvatar", params);

		var adminThread = new AdminThread();
		adminThread.start();
		adminThread.join();

		var data = Mapper.MapTo(new AdminDTO(), rs);
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Avatar Fail";
		var result = new Result<AdminDTO>(isSuccess ? data.getFirst() : null, isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/admin/administrators";
	}

	// =================================== CUSTOMER

	@RequestMapping(value = "/admin/customers", method = RequestMethod.GET)
	public String customers(Model model)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var isSuccess = !CustomerThread.customers.isEmpty();
		var mes = isSuccess ? "" : "No Data Found.";
		var result = new Result<LinkedList<CustomerDTO>>(CustomerThread.customers, isSuccess, mes);

		model.addAttribute("resultCustomer", result);
		return "admin/super/customers";
	}
	// =================================== POSITION

	@RequestMapping(value = "/position/insert", method = RequestMethod.POST)
	String positioninsert(String _pos_name, String _pos_icon, @RequestParam("file") MultipartFile file,
			HttpServletRequest request, RedirectAttributes redirectAttributes) throws ClassNotFoundException,
			IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {

		String pos_icon = "http://localhost:8080/images/No_Image_Available.jpg";
		if (!file.getOriginalFilename().equals("")) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(file, serverPath);
			pos_icon = "http://localhost:8080/images/" + file.getOriginalFilename();
		}
		var params = new LinkedList<String>();
		params.add(_pos_name);
		params.add(pos_icon);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Position_Insert", params);
		var data = Mapper.MapTo(new PositionDTO(), rs);
		var isSuccess = !data.isEmpty() ? true : false;

		var positionThread = new PositionThread();
		positionThread.start();
		positionThread.join();
		var mes = isSuccess ? "Create Position Successfull." : "Create Position Fail!";
		// var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,
		// isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/admin/settings";
	}

	@RequestMapping(value = "/position/update", method = RequestMethod.POST)
	String positionupdate(int _pos_id, String _pos_name, String _pos_icon, @RequestParam("file") MultipartFile file,
			HttpServletRequest request, RedirectAttributes redirectAttributes) throws ClassNotFoundException,
			IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {

		String pos_icon = _pos_icon;
		if (!file.getOriginalFilename().equals("")) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(file, serverPath);
			pos_icon = "http://localhost:8080/images/" + file.getOriginalFilename();
		}

		var params = new LinkedList<String>();
		params.add(_pos_id + "");
		params.add(_pos_name);
		params.add(pos_icon);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Position_Update", params);
		var data = Mapper.MapTo(new PositionDTO(), rs);
		var isSuccess = !data.isEmpty();
		var positionThread = new PositionThread();
		positionThread.start();
		positionThread.join();
		var mes = isSuccess ? "Update Position Successfull." : "Update Position Fail!";
		// var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,
		// isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/admin/settings";
	}

	// =================================== PERMISION

	@RequestMapping(value = "/permision/insert", method = RequestMethod.POST)
	String permisioninsert(String _per_name, String _per_icon,
			@RequestParam("permisionFile") MultipartFile permisionFile, HttpServletRequest request,
			RedirectAttributes redirectAttributes) throws ClassNotFoundException, IllegalArgumentException,
			IllegalAccessException, InstantiationException, InterruptedException {

		String per_icon = "http://localhost:8080/images/No_Image_Available.jpg";
		if (!permisionFile.getOriginalFilename().equals("")) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(permisionFile, serverPath);
			per_icon = "http://localhost:8080/images/" + permisionFile.getOriginalFilename();
		}
		var params = new LinkedList<String>();
		params.add(_per_name);
		params.add(per_icon);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Permision_Insert", params);
		var data = Mapper.MapTo(new PermisionDTO(), rs);
		var isSuccess = !data.isEmpty();
		var permisionThread = new PermisionThread();
		permisionThread.start();
		permisionThread.join();
		var mes = isSuccess ? "Create Permision Successfull." : "Create Permision Fail!";
		// var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,
		// isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/admin/settings";
	}

	@RequestMapping(value = "/permision/update", method = RequestMethod.POST)
	String permisionupdate(int _per_id, String _per_name, String _per_icon,
			@RequestParam("permisionFile") MultipartFile permisionFile, HttpServletRequest request,
			RedirectAttributes redirectAttributes) throws ClassNotFoundException, IllegalArgumentException,
			IllegalAccessException, InstantiationException, InterruptedException {
		String per_icon = _per_icon;
		if (!permisionFile.getOriginalFilename().equals("")) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(permisionFile, serverPath);
			per_icon = "http://localhost:8080/images/" + permisionFile.getOriginalFilename();
		}

		var params = new LinkedList<String>();
		params.add(_per_id + "");
		params.add(_per_name);
		params.add(per_icon);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Permision_Update", params);
		var data = Mapper.MapTo(new PermisionDTO(), rs);
		var isSuccess = !data.isEmpty();
		var permisionThread = new PermisionThread();
		permisionThread.start();
		permisionThread.join();
		var mes = isSuccess ? "Update Permision Successfull." : "Update Permision Fail!";
		// var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,
		// isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/admin/settings";
	}

	// =================================== CATEGORY

	@RequestMapping(value = "/category/insert", method = RequestMethod.POST)
	String categoryinsert(String _cate_name, String _cate_icon,
			@RequestParam("categoryFile") MultipartFile categoryFile, HttpServletRequest request,
			RedirectAttributes redirectAttributes) throws ClassNotFoundException, IllegalArgumentException,
			IllegalAccessException, InstantiationException, InterruptedException {

		String cate_icon = _cate_icon;
		if (categoryFile != null) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(categoryFile, serverPath);
			cate_icon = "http://localhost:8080/images/" + categoryFile.getOriginalFilename();
		}
		var params = new LinkedList<String>();
		params.add(_cate_name);
		params.add(cate_icon);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Category_Insert", params);
		var data = Mapper.MapTo(new CategoryDTO(), rs);
		var isSuccess = !data.isEmpty();
		var categoryThread = new CategoryThread();
		categoryThread.start();
		categoryThread.join();
		var mes = isSuccess ? "Create Category Successfull." : "Create Category Fail!";
		// var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,
		// isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/admin/settings";
	}

	@RequestMapping(value = "/category/update", method = RequestMethod.POST)
	String categoryupdate(int _cate_id, String _cate_name, String _cate_icon,
			@RequestParam("categoryFile") MultipartFile categoryFile, HttpServletRequest request,
			RedirectAttributes redirectAttributes) throws ClassNotFoundException, IllegalArgumentException,
			IllegalAccessException, InstantiationException, InterruptedException {

		String cate_icon = _cate_icon;
		if (categoryFile != null) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(categoryFile, serverPath);
			cate_icon = "http://localhost:8080/images/" + categoryFile.getOriginalFilename();
		}

		var params = new LinkedList<String>();
		params.add(_cate_id + "");
		params.add(_cate_name);
		params.add(cate_icon);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Category_Update", params);
		var data = Mapper.MapTo(new CategoryDTO(), rs);
		var isSuccess = !data.isEmpty();
		var categoryThread = new CategoryThread();
		categoryThread.start();
		categoryThread.join();
		var mes = isSuccess ? "Update Category Successfull." : "Update Category Fail!";
		// var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,
		// isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/admin/settings";
	}
	// =================================== ReturnType

	@RequestMapping(value = "/country/insert", method = RequestMethod.POST)
	String countryinsert(String _ct_name, RedirectAttributes redirectAttributes) throws ClassNotFoundException,
			IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {

		var params = new LinkedList<String>();
		params.add(_ct_name);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Country_Insert", params);
		var data = Mapper.MapTo(new CountryDTO(), rs);
		var isSuccess = !data.isEmpty();
		var countryThread = new CountryThread();
		countryThread.start();
		countryThread.join();
		;
		var mes = isSuccess ? "Create Country Successfull." : "Create Country Fail!";
		// var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,
		// isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/admin/settings";
	}

	@RequestMapping(value = "/country/update", method = RequestMethod.POST)
	String Countryupdate(int _ct_id, String _ct_name, RedirectAttributes redirectAttributes)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException,
			InterruptedException {

		var params = new LinkedList<String>();
		params.add(_ct_id + "");
		params.add(_ct_name);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Country_Update", params);
		var data = Mapper.MapTo(new CategoryDTO(), rs);
		var isSuccess = !data.isEmpty();
		var countryThread = new CountryThread();
		countryThread.start();
		countryThread.join();
		var mes = isSuccess ? "Update Country Successfull." : "Update Country Fail!";
		// var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,
		// isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/admin/settings";
	}

	// =================================== CITY

	@RequestMapping(value = "/city/insert", method = RequestMethod.POST)
	String cityinsert(int _city_ct_id, String _city_name, RedirectAttributes redirectAttributes)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException,
			InterruptedException {

		var params = new LinkedList<String>();
		params.add(_city_name);
		params.add(_city_ct_id + "");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_City_Insert", params);
		var data = Mapper.MapTo(new CityDTO(), rs);
		var isSuccess = !data.isEmpty();
		var cityThread = new CityThread();
		cityThread.start();
		cityThread.join();
		var mes = isSuccess ? "Create City Successfull." : "Create City Fail!";
		// var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,
		// isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/admin/settings";
	}

	@RequestMapping(value = "/city/update", method = RequestMethod.POST)
	String cityupdate(int _city_id, int _city_ct_id, String _city_name, RedirectAttributes redirectAttributes)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException,
			InterruptedException {

		var params = new LinkedList<String>();
		params.add(_city_id + "");
		params.add(_city_ct_id + "");
		params.add(_city_name);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_City_Update", params);
		var data = Mapper.MapTo(new CityDTO(), rs);
		var isSuccess = !data.isEmpty();
		var cityThread = new CityThread();
		cityThread.start();
		cityThread.join();
		var mes = isSuccess ? "Update City Successfull." : "Update City Fail!";
		// var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,
		// isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/admin/settings";
	}

	// =================================== Flight Settings
	@RequestMapping(value = "/flight/settings", method = RequestMethod.GET)
	public String flightsettings(Model model)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {

		var isSuccessAirport = !AirportThread.airports.isEmpty();
		var mesAirport = isSuccessAirport ? "" : "No Data Found.";
		var resultAirport = new Result<LinkedList<AirportDTO>>(AirportThread.airports, isSuccessAirport, mesAirport);
		model.addAttribute("resultAirport", resultAirport);

		var isSuccessCarrier = !CarrierThread.carriers.isEmpty();
		var mesCarrier = isSuccessCarrier ? "" : "No Data Found.";
		var resultCarrier = new Result<LinkedList<CarrierDTO>>(CarrierThread.carriers, isSuccessCarrier, mesCarrier);
		model.addAttribute("resultCarrier", resultCarrier);

		var isSuccessTicketClass = !TicketClassThread.ticketClasses.isEmpty();
		var mesTicketClass = isSuccessTicketClass ? "" : "No Data Found.";
		var resultTicketClass = new Result<LinkedList<TicketClassDTO>>(TicketClassThread.ticketClasses,
				isSuccessTicketClass, mesTicketClass);
		model.addAttribute("resultTicketClass", resultTicketClass);

		var isSuccessReturnType = !ReturnTypeThread.returnTypes.isEmpty();
		var mesReturnType = isSuccessReturnType ? "" : "No Data Found.";
		var resultReturnType = new Result<LinkedList<ReturnTypeDTO>>(ReturnTypeThread.returnTypes, isSuccessReturnType,
				mesReturnType);
		model.addAttribute("resultReturnType", resultReturnType);

		var isSuccessCity = !CityThread.cities.isEmpty();
		var mesCity = isSuccessCity ? "" : "No Data Found.";
		var resultCity = new Result<LinkedList<CityDTO>>(CityThread.cities, isSuccessCity, mesCity);
		model.addAttribute("resultCity", resultCity);
		
		var isSuccessService = !ServiceThread.services.isEmpty();
		var mesService = isSuccessService ? "" : "No Data Found.";
		var resultService = new Result<LinkedList<ServiceDTO>>(ServiceThread.services, isSuccessService, mesService);
		model.addAttribute("resultService", resultService);

		return "admin/flight/settings";
	}

	// =================================== Flight
		@RequestMapping(value = "/flight", method = RequestMethod.GET)
		public String flight(Model model)
				throws IllegalArgumentException, IllegalAccessException, InstantiationException, ClassNotFoundException {

			var isSuccessPlane = !PlaneThread.planes.isEmpty();
			var mesPlane = isSuccessPlane ? "" : "No Data Found.";
			var resultPlane = new Result<LinkedList<PlaneDTO>>(PlaneThread.planes, isSuccessPlane, mesPlane);
			model.addAttribute("resultPlane", resultPlane);

			var isSuccessCarrier = !CarrierThread.carriersByPlane.isEmpty();
			var mesCarrier = isSuccessCarrier ? "" : "No Data Found.";
			var resultCarrier = new Result<LinkedList<CarrierDTO>>(CarrierThread.carriersByPlane, isSuccessCarrier,
					mesCarrier);
			model.addAttribute("resultCarrierAllByPlane", resultCarrier);

			var isSuccessAirport = !AirportThread.airports.isEmpty();
			var mesAirport = isSuccessAirport ? "" : "No Data Found.";
			var resultAirport = new Result<LinkedList<AirportDTO>>(AirportThread.airports, isSuccessAirport, mesAirport);
			model.addAttribute("resultAirport", resultAirport);

			var rsFlight = DBConnection.CallProc("Sp_Flight_Gets");
			var flights = Mapper.MapTo(new FlightDTO(), rsFlight);
			var isSuccessFlight = flights.isEmpty();
			var mesFlight = isSuccessFlight ? "" : "No Data Found.";
			var resultFlight = new Result<LinkedList<FlightDTO>>(flights, isSuccessFlight, mesFlight);
			model.addAttribute("resultFlight", resultFlight);
			
			var isSuccessService = !ServiceThread.services.isEmpty();
			var mesService = isSuccessService ? "" : "No Data Found.";
			var resultService = new Result<LinkedList<ServiceDTO>>(ServiceThread.services, isSuccessService, mesService);
			model.addAttribute("resultService", resultService);
			
			return "admin/flight/flight";
		}
	@RequestMapping(value = "/flight/insert", method = RequestMethod.POST)
	String flightinsert(int _fl_plane_id, int _fl_from_id, int _fl_to_id, LocalDateTime _fl_take_off,
			LocalDateTime _fl_arrival, double _fl_ec_price, double _fl_sc_price, double _fl_bs_price,
			double _fl_fc_price, String _fl_services,RedirectAttributes redirectAttributes) throws ClassNotFoundException,
			IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		var params = new LinkedList<String>();
		params.add(_fl_plane_id + "");
		params.add(_fl_from_id + "");
		params.add(_fl_to_id + "");
		params.add(_fl_ec_price + "");
		params.add(_fl_sc_price + "");
		params.add(_fl_bs_price + "");
		params.add(_fl_fc_price + "");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(_fl_take_off));
		params.add(dtf.format(_fl_arrival));
		params.add(_fl_services);
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_Flight_Insert", params);
		var data = Mapper.MapTo(new FlightDTO(), rs);
		var isSuccess = !data.isEmpty();
		var flightThread = new FlightThread();
		flightThread.start();
		flightThread.join();
		var mes = isSuccess ? "Create Flight Successfull." : "Create Flight Fail!";
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/flight";
	}

	@RequestMapping(value = "/flight/update", method = RequestMethod.POST)
	String flightupdate(int _fl_id, int _fl_plane_id, int _fl_from_id, int _fl_to_id, LocalDateTime _fl_take_off,
			LocalDateTime _fl_arrival, double _fl_ec_price, double _fl_sc_price, double _fl_bs_price,
			double _fl_fc_price, String _fl_services, RedirectAttributes redirectAttributes) throws ClassNotFoundException,
			IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		var params = new LinkedList<String>();
		params.add(_fl_id + "");
		params.add(_fl_plane_id + "");
		params.add(_fl_from_id + "");
		params.add(_fl_to_id + "");
		params.add(_fl_ec_price + "");
		params.add(_fl_sc_price + "");
		params.add(_fl_bs_price + "");
		params.add(_fl_fc_price + "");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(_fl_take_off));
		params.add(dtf.format(_fl_arrival));
		params.add(_fl_services);
		params.add(dtf.format(now)); 
		var rs = DBConnection.CallProc("Sp_Flight_Update", params);
		var data = Mapper.MapTo(new FlightDTO(), rs);
		var isSuccess = !data.isEmpty();
		var flightThread = new FlightThread();
		flightThread.start();
		flightThread.join();
		var mes = isSuccess ? "Update Flight Successfull." : "Update Flight Fail!";
		// var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,
		// isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/flight";
	}

	// =================================== Bill

	@RequestMapping(value = "/flight/bill", method = RequestMethod.GET)
	public String flightbill(Model model) {
		var isSuccessBill = !BillThread.bills.isEmpty();
		var mesBill = isSuccessBill ? "" : "No Data Found.";
		var resultBill = new Result<LinkedList<BillDTO>>(BillThread.bills, isSuccessBill, mesBill);
		model.addAttribute("resultBill", resultBill);

		return "admin/flight/bill";
	}

	// =================================== Airport
	@RequestMapping(value = "/airport/insert", method = RequestMethod.POST)
	String airportinsert(int _ap_city_id, String _ap_name, RedirectAttributes redirectAttributes)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException,
			InterruptedException {

		var params = new LinkedList<String>();
		params.add(_ap_name);
		params.add("");
		params.add("");
		params.add(_ap_city_id + "");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Airport_Insert", params);
		var data = Mapper.MapTo(new AirportDTO(), rs);
		var isSuccess = !data.isEmpty();
		var airportThread = new AirportThread();
		airportThread.start();
		airportThread.join();
		var mes = isSuccess ? "Create Airport Successfull." : "Create Airport Fail!";
		// var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,
		// isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/flight/settings";
	}

	@RequestMapping(value = "/airport/update", method = RequestMethod.POST)
	String airportupdate(int _ap_id, int _ap_city_id, String _ap_name, RedirectAttributes redirectAttributes)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException,
			InterruptedException {

		var params = new LinkedList<String>();
		params.add(_ap_id + "");
		params.add(_ap_name);
		params.add(_ap_city_id + "");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Airport_Update", params);
		var data = Mapper.MapTo(new AirportDTO(), rs);
		var isSuccess = !data.isEmpty();
		var airportThread = new AirportThread();
		airportThread.start();
		airportThread.join();
		var mes = isSuccess ? "Update Airport Successfull." : "Update Airport Fail!";
		// var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,
		// isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/flight/settings";
	}

	// =================================== Carrier
	@RequestMapping(value = "/carrier/insert", method = RequestMethod.POST)
	String carrierinsert(int _car_city_id, String _car_name, @RequestParam("carrierFile") MultipartFile carrierFile,
			HttpServletRequest request, RedirectAttributes redirectAttributes) throws ClassNotFoundException,
			IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {

		String _car_icon = "http://localhost:8080/images/No_Image_Available.jpg";
		if (!carrierFile.getOriginalFilename().equals("")) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(carrierFile, serverPath);
			_car_icon = "http://localhost:8080/images/" + carrierFile.getOriginalFilename();
		}
		var params = new LinkedList<String>();
		params.add(_car_name);
		params.add(_car_icon);
		params.add(_car_city_id + "");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Carrier_Insert", params);
		var data = Mapper.MapTo(new CarrierDTO(), rs);
		var isSuccess = !data.isEmpty();
		var carrierThread = new CarrierThread();
		carrierThread.start();
		carrierThread.join();
		var mes = isSuccess ? "Create Carrier Successfull." : "Create Carrier Fail!";
		
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/flight/settings";
	}

	@RequestMapping(value = "/carrier/update", method = RequestMethod.POST)
	String carrierupdate(int _car_id, int _car_city_id, String _car_name, String _car_icon,
			@RequestParam("carrierFile") MultipartFile carrierFile, HttpServletRequest request,
			RedirectAttributes redirectAttributes) throws ClassNotFoundException, IllegalArgumentException,
			IllegalAccessException, InstantiationException, InterruptedException {

		String car_icon = _car_icon;
		if (!carrierFile.getOriginalFilename().equals("")) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(carrierFile, serverPath);
			car_icon = "http://localhost:8080/images/" + carrierFile.getOriginalFilename();
		}
		var params = new LinkedList<String>();
		params.add(_car_id + "");
		params.add(_car_name);
		params.add(car_icon);
		params.add(_car_city_id + "");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Carrier_Update", params);
		var data = Mapper.MapTo(new CarrierDTO(), rs);
		var isSuccess = !data.isEmpty();
		var carrierThread = new CarrierThread();
		carrierThread.start();
		carrierThread.join();
		var mes = isSuccess ? "Update Carrier Successfull." : "Update Carrier Fail!";
		// var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,
		// isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/flight/settings";
	}

	// =================================== Ticket Class
	@RequestMapping(value = "/ticketclass/insert", method = RequestMethod.POST)
	String ticketclassinsert(String _tc_name, RedirectAttributes redirectAttributes) throws ClassNotFoundException,
			IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {

		var params = new LinkedList<String>();
		params.add(_tc_name);
		params.add("");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_TicketClass_Insert", params);
		var data = Mapper.MapTo(new TicketClassDTO(), rs);
		var isSuccess = !data.isEmpty();
		var ticketClassThread = new TicketClassThread();
		ticketClassThread.start();
		ticketClassThread.join();
		var mes = isSuccess ? "Create Ticket Class Successfull." : "Create Ticket Class Fail!";
		// var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,
		// isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/flight/settings";
	}

	@RequestMapping(value = "/ticketclass/update", method = RequestMethod.POST)
	String ticketclassupdate(int _tc_id, String _tc_name, RedirectAttributes redirectAttributes)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException,
			InterruptedException {

		var params = new LinkedList<String>();
		params.add(_tc_id + "");
		params.add(_tc_name);
		params.add("");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_TicketClass_Update", params);
		var data = Mapper.MapTo(new TicketClassDTO(), rs);
		var isSuccess = !data.isEmpty();
		var ticketClassThread = new TicketClassThread();
		ticketClassThread.start();
		ticketClassThread.join();
		var mes = isSuccess ? "Update Ticket Class Successfull." : "Update Ticket Class Fail!";
		// var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,
		// isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/flight/settings";
	}
	// =================================== Service
		@RequestMapping(value = "/service/insert", method = RequestMethod.POST)
		String serviceinsert(String _ser_name, String _ser_icon,
				@RequestParam("serviceFile") MultipartFile serviceFile, HttpServletRequest request,
				RedirectAttributes redirectAttributes) throws ClassNotFoundException, IllegalArgumentException,
				IllegalAccessException, InstantiationException, InterruptedException {

			String ser_icon = "http://localhost:8080/images/No_Image_Available.jpg";
			if (!serviceFile.getOriginalFilename().equals("")) {
				var serverPath = request.getSession().getServletContext().getRealPath("images");
				UpdaloadFile(serviceFile, serverPath);
				ser_icon = "http://localhost:8080/images/" + serviceFile.getOriginalFilename();
			}
			var params = new LinkedList<String>();
			params.add(_ser_name);
			params.add(ser_icon);
			LocalDateTime now = LocalDateTime.now();
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			params.add(dtf.format(now));

			var rs = DBConnection.CallProc("Sp_Service_Insert", params);
			var data = Mapper.MapTo(new ServiceDTO(), rs);
			var isSuccess = !data.isEmpty();
			var serviceThread = new ServiceThread();
			serviceThread.start();
			serviceThread.join();
			var mes = isSuccess ? "Create Service Successfull." : "Create Service Fail!";
			redirectAttributes.addFlashAttribute("mes", mes);
			return "redirect:/flight/settings";
		}

		@RequestMapping(value = "/service/update", method = RequestMethod.POST)
		String serviceupdate(int _ser_id,String _ser_name, String _ser_icon,
				@RequestParam("serviceFile") MultipartFile serviceFile, HttpServletRequest request,
				RedirectAttributes redirectAttributes) throws ClassNotFoundException, IllegalArgumentException,
				IllegalAccessException, InstantiationException, InterruptedException {

			String ser_icon = _ser_icon;

			if (!serviceFile.getOriginalFilename().equals("")) {
				var serverPath = request.getSession().getServletContext().getRealPath("images");
				UpdaloadFile(serviceFile, serverPath);
				ser_icon = "http://localhost:8080/images/" + serviceFile.getOriginalFilename();
			}

			var params = new LinkedList<String>();
			params.add(_ser_id + "");
			params.add(_ser_name);
			params.add(ser_icon);
			LocalDateTime now = LocalDateTime.now();
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			params.add(dtf.format(now));

			var rs = DBConnection.CallProc("Sp_Service_Update", params);
			var data = Mapper.MapTo(new ServiceDTO(), rs);
			var isSuccess = !data.isEmpty();
			var serviceThread = new ServiceThread();
			serviceThread.start();
			serviceThread.join();
			var mes = isSuccess ? "Update Service Successfull." : "Update Service Fail!";
			redirectAttributes.addFlashAttribute("mes", mes);
			return "redirect:/flight/settings";
		}
	// =================================== Return Type
	@RequestMapping(value = "/returntype/insert", method = RequestMethod.POST)
	String returntypeinsert(String _ret_name, int _ret_percent, RedirectAttributes redirectAttributes)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException,
			InterruptedException {

		var params = new LinkedList<String>();
		params.add(_ret_name);
		params.add(_ret_percent + "");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_ReturnType_Insert", params);
		var data = Mapper.MapTo(new ReturnTypeDTO(), rs);
		var isSuccess = !data.isEmpty();
		var returnTypeThread = new ReturnTypeThread();
		returnTypeThread.start();
		returnTypeThread.join();
		var mes = isSuccess ? "Create Return Type Successfull." : "Create Return Type Fail!";
		// var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,
		// isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/flight/settings";
	}

	@RequestMapping(value = "/returntype/update", method = RequestMethod.POST)
	String returntypeupdate(int _ret_id, String _ret_name, int _ret_percent, RedirectAttributes redirectAttributes)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException,
			InterruptedException {

		var params = new LinkedList<String>();
		params.add(_ret_id + "");
		params.add(_ret_name);
		params.add(_ret_percent + "");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_ReturnType_Update", params);
		var data = Mapper.MapTo(new ReturnTypeDTO(), rs);
		var isSuccess = !data.isEmpty();
		var returnTypeThread = new ReturnTypeThread();
		returnTypeThread.start();
		returnTypeThread.join();
		var mes = isSuccess ? "Update Return Type Successfull." : "Update Return Type Fail!";
		// var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,
		// isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/flight/settings";
	}

	// =================================== Plane
	@RequestMapping(value = "/flight/plane", method = RequestMethod.GET)
	public String flightplane(Model model)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {

		var isSuccessPlane = !PlaneThread.planes.isEmpty();
		var mesPlane = isSuccessPlane ? "" : "No Data Found.";
		var resultPlane = new Result<LinkedList<PlaneDTO>>(PlaneThread.planes, isSuccessPlane, mesPlane);
		model.addAttribute("resultPlane", resultPlane);

		var isSuccessPlaneType = !PlaneTypeThread.planeTypes.isEmpty();
		var mesPlaneType = isSuccessPlaneType ? "" : "No Data Found.";
		var resultPlaneType = new Result<LinkedList<PlaneTypeDTO>>(PlaneTypeThread.planeTypes, isSuccessPlaneType,
				mesPlaneType);
		model.addAttribute("resultPlaneType", resultPlaneType);
 
		var isSuccessCarrier = !CarrierThread.carriers.isEmpty();
		var mesCarrier = isSuccessCarrier ? "" : "No Data Found.";
		var resultCarrier = new Result<LinkedList<CarrierDTO>>(CarrierThread.carriers, isSuccessCarrier, mesCarrier);
		model.addAttribute("resultCarrier", resultCarrier);
		
		var isSuccessAirport = !AirportThread.airports.isEmpty();
		var mesAirport = isSuccessAirport ? "" : "No Data Found.";
		var resultAirport = new Result<LinkedList<AirportDTO>>(AirportThread.airports, isSuccessAirport, mesAirport);
		model.addAttribute("resultAirport", resultAirport);
		
		var isSuccessService = !ServiceThread.services.isEmpty();
		var mesService = isSuccessService ? "" : "No Data Found.";
		var resultService = new Result<LinkedList<ServiceDTO>>(ServiceThread.services, isSuccessService, mesService);
		model.addAttribute("resultService", resultService);
		
		return "admin/flight/plane";
	}

	@RequestMapping(value = "/plane/insert", method = RequestMethod.POST)
	String planeinsert(String _pl_code, int _pl_carrier_id, int _pl_pt_id, RedirectAttributes redirectAttributes)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException,
			InterruptedException {

		var params = new LinkedList<String>();
		params.add(_pl_code);
		params.add(_pl_carrier_id + "");
		params.add(_pl_pt_id + "");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Plane_Insert", params);
		var data = Mapper.MapTo(new PlaneDTO(), rs);
		var isSuccess = !data.isEmpty();
		var planeThread = new PlaneThread();
		planeThread.start();
		planeThread.join();
		var mes = isSuccess ? "Create Plane Successfull." : "Create Plane Fail!";
		// var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,
		// isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/flight/plane";
	}

	@RequestMapping(value = "/plane/update", method = RequestMethod.POST)
	String planeupdate(int _pl_id, String _pl_code, int _pl_carrier_id, int _pl_pt_id,
			RedirectAttributes redirectAttributes) throws ClassNotFoundException, IllegalArgumentException,
			IllegalAccessException, InstantiationException, InterruptedException {
		var params = new LinkedList<String>();
		params.add(_pl_id + "");
		params.add(_pl_code);
		params.add(_pl_carrier_id + "");
		params.add(_pl_pt_id + "");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Plane_Update", params);
		var data = Mapper.MapTo(new PlaneDTO(), rs);
		var isSuccess = !data.isEmpty();
		var planeThread = new PlaneThread();
		planeThread.start();
		planeThread.join();
		var mes = isSuccess ? "Update Plane Successfull." : "Update Plane Fail!";
		// var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,
		// isSuccess, mes);
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/flight/plane";
	}

	// =================================== Hotel
	@RequestMapping(value = "/hotel", method = RequestMethod.GET)
	public String hotel(Model model)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {

		var isSuccessHotel = !HotelThread.hotels.isEmpty();
		var mesHotel = isSuccessHotel ? "" : "No Data Found.";
		var resultHotel = new Result<LinkedList<HotelDTO>>(HotelThread.hotels, isSuccessHotel, mesHotel);
		model.addAttribute("resultHotel", resultHotel);
		
		var isSuccessCity = !CityThread.cities.isEmpty();
		var mesCity = isSuccessCity ? "" : "No Data Found.";
		var resultCity = new Result<LinkedList<CityDTO>>(CityThread.cities, isSuccessCity, mesCity);
		model.addAttribute("resultCity", resultCity);
		
		var isSuccessFurniture = !FurnitureThread.furnitures.isEmpty();
		var mesFurniture = isSuccessFurniture ? "" : "No Data Found.";
		var resultFurniture = new Result<LinkedList<FunitureDTO>>(FurnitureThread.furnitures, isSuccessFurniture,
				mesFurniture);
		model.addAttribute("resultFurniture", resultFurniture);

		var isSuccessPolicy = !PolicyThread.policies.isEmpty();
		var mesPolicy = isSuccessPolicy ? "" : "No Data Found.";
		var resultPolicy = new Result<LinkedList<PolicyDTO>>(PolicyThread.policies, isSuccessPolicy, mesPolicy);
		model.addAttribute("resultPolicy", resultPolicy);

		var isSuccessRoomType = !RoomTypeThread.roomTypes.isEmpty();
		var mesRoomType = isSuccessRoomType ? "" : "No Data Found.";
		var resultRoomType = new Result<LinkedList<RoomTypeDTO>>(RoomTypeThread.roomTypes, isSuccessRoomType,
				mesRoomType);
		model.addAttribute("resultRoomType", resultRoomType);
		return "admin/hotel/hotel";
	}

	@RequestMapping(value = "/hotel/insert", method = RequestMethod.POST)
	String hotelinsert(int _ho_city_id, String _ho_name, int _ho_single_rooms ,int _ho_double_rooms, 
						int _ho_triple_rooms,int _ho_stars, String _ho_address,
			@RequestParam("hotelFile") MultipartFile hotelFile,
			HttpServletRequest request, RedirectAttributes redirectAttributes) throws ClassNotFoundException,
			IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {

		String ho_background = "http://localhost:8080/images/No_Image_Available.jpg";
		if (!hotelFile.getOriginalFilename().equals("")) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(hotelFile, serverPath);
			ho_background = "http://localhost:8080/images/" + hotelFile.getOriginalFilename();
		}
		var _ho_rooms = _ho_single_rooms + _ho_double_rooms + _ho_triple_rooms;
		var params = new LinkedList<String>();
		params.add(_ho_name);
		params.add(_ho_city_id+"");
		params.add(_ho_rooms + "");
		params.add(_ho_single_rooms + "");
		params.add(_ho_double_rooms + "");
		params.add(_ho_triple_rooms + "");
		params.add(_ho_stars + "");
		params.add(_ho_address);
		params.add(ho_background);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Hotel_Insert", params);
		var data = Mapper.MapTo(new HotelDTO(), rs);
		var isSuccess = !data.isEmpty();
		var hotelThread = new HotelThread();
		hotelThread.start();
		hotelThread.join();
		var mes = isSuccess ? "Create Hotel Successfull." : "Create Hotel Fail!";
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/hotel";
	}

	@RequestMapping(value = "/hotel/update", method = RequestMethod.POST)
	String hotelupdate(int _ho_id,String _ho_background, int _ho_city_id, String _ho_name, int _ho_single_rooms ,
						int _ho_double_rooms, int _ho_triple_rooms,int _ho_stars, String _ho_address,
			@RequestParam("hotelFile") MultipartFile hotelFile, HttpServletRequest request,
			RedirectAttributes redirectAttributes) throws ClassNotFoundException, IllegalArgumentException,
			IllegalAccessException, InstantiationException, InterruptedException {

		String ho_background = _ho_background;
		if (!hotelFile.getOriginalFilename().equals("")) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(hotelFile, serverPath);
			ho_background = "http://localhost:8080/images/" + hotelFile.getOriginalFilename();
		}
		var _ho_rooms = _ho_single_rooms + _ho_double_rooms + _ho_triple_rooms;
		var params = new LinkedList<String>();
		
		params.add(_ho_id + "");
		params.add(_ho_name);
		params.add(_ho_city_id+"");
		params.add(_ho_rooms + "");
		params.add(_ho_single_rooms + "");
		params.add(_ho_double_rooms + "");
		params.add(_ho_triple_rooms + "");
		params.add(_ho_stars + "");
		params.add(_ho_address);
		params.add(ho_background);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Hotel_Update", params);
		var data = Mapper.MapTo(new HotelDTO(), rs);
		var isSuccess = !data.isEmpty();
		var hotelThread = new HotelThread();
		hotelThread.start();
		hotelThread.join();
		var mes = isSuccess ? "Update Hotel Successfull." : "Update Hotel Fail!";
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/hotel";
	}

	// =================================== Booking
	@RequestMapping(value = "/hotel/bill", method = RequestMethod.GET)
	public String hotelbill(Model model) {
		var isSuccessBooking = !BookingThread.bookings.isEmpty();
		var mesBooking = isSuccessBooking ? "" : "No Data Found.";
		var resultBooking = new Result<LinkedList<BookingDTO>>(BookingThread.bookings, isSuccessBooking, mesBooking);
		model.addAttribute("resultBooking", resultBooking);
		
		var isSuccessHotel = !HotelThread.hotels.isEmpty();
		var mesHotel = isSuccessHotel ? "" : "No Data Found.";
		var resultHotel = new Result<LinkedList<HotelDTO>>(HotelThread.hotels, isSuccessHotel, mesHotel);
		model.addAttribute("resultHotel", resultHotel);
		
		var isSuccessFurniture = !FurnitureThread.furnitures.isEmpty();
		var mesFurniture = isSuccessFurniture ? "" : "No Data Found.";
		var resultFurniture = new Result<LinkedList<FunitureDTO>>(FurnitureThread.furnitures, isSuccessFurniture,
				mesFurniture);
		model.addAttribute("resultFurniture", resultFurniture);

		var isSuccessPolicy = !PolicyThread.policies.isEmpty();
		var mesPolicy = isSuccessPolicy ? "" : "No Data Found.";
		var resultPolicy = new Result<LinkedList<PolicyDTO>>(PolicyThread.policies, isSuccessPolicy, mesPolicy);
		model.addAttribute("resultPolicy", resultPolicy);
		
		return "admin/hotel/bill";
	}

	// =================================== Room
	@RequestMapping(value = "/hotel/room", method = RequestMethod.GET)
	public String hotelroom(Model model)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var isSuccessHotel = !HotelThread.hotels.isEmpty();
		var mesHotel = isSuccessHotel ? "" : "No Data Found.";
		var resultHotel = new Result<LinkedList<HotelDTO>>(HotelThread.hotels, isSuccessHotel, mesHotel);
		model.addAttribute("resultHotel", resultHotel);
		
		var isSuccessRoom = !RoomThread.rooms.isEmpty();
		var mesRoom = isSuccessRoom ? "" : "No Data Found.";
		var resultRoom = new Result<LinkedList<RoomDTO>>(RoomThread.rooms, isSuccessRoom, mesRoom);
		model.addAttribute("resultRoom", resultRoom);
		
		var isSuccessFurniture = !FurnitureThread.furnitures.isEmpty();
		var mesFurniture = isSuccessFurniture ? "" : "No Data Found.";
		var resultFurniture = new Result<LinkedList<FunitureDTO>>(FurnitureThread.furnitures, isSuccessFurniture,
				mesFurniture);
		model.addAttribute("resultFurniture", resultFurniture);

		var isSuccessPolicy = !PolicyThread.policies.isEmpty();
		var mesPolicy = isSuccessPolicy ? "" : "No Data Found.";
		var resultPolicy = new Result<LinkedList<PolicyDTO>>(PolicyThread.policies, isSuccessPolicy, mesPolicy);
		model.addAttribute("resultPolicy", resultPolicy);

		var isSuccessRoomType = !RoomTypeThread.roomTypes.isEmpty();
		var mesRoomType = isSuccessRoomType ? "" : "No Data Found.";
		var resultRoomType = new Result<LinkedList<RoomTypeDTO>>(RoomTypeThread.roomTypes, isSuccessRoomType,
				mesRoomType);
		model.addAttribute("resultRoomType", resultRoomType);
		
		return "admin/hotel/room";
	}
	
	@RequestMapping(value = "/room/insert", method = RequestMethod.POST)
	String roominsert(int _ro_hotel_id, String _ro_code, int _ro_room_type_id ,double _ro_price, 
						int _ro_furnitures,int _ro_policies,
			@RequestParam("roomFile") MultipartFile roomFile,
			HttpServletRequest request, RedirectAttributes redirectAttributes) throws ClassNotFoundException,
			IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {

		String ro_file = "http://localhost:8080/images/No_Image_Available.jpg";
		if (!roomFile.getOriginalFilename().equals("")) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(roomFile, serverPath);
			ro_file = "http://localhost:8080/images/" + roomFile.getOriginalFilename();
		}
		
		var params = new LinkedList<String>();
		params.add(_ro_code);
		params.add(_ro_hotel_id+"");
		params.add(_ro_room_type_id + "");
		params.add(_ro_price + "");
		params.add(_ro_furnitures + "");
		params.add(_ro_policies + "");
		params.add(ro_file);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Room_Insert", params);
		var data = Mapper.MapTo(new RoomDTO(), rs);
		var isSuccess = !data.isEmpty();
		var roomThread = new RoomThread();
		roomThread.start();
		roomThread.join();
		var mes = isSuccess ? "Create Room Successfull." : "Create Room Fail!";
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/hotel/room";
	}

	@RequestMapping(value = "/room/update", method = RequestMethod.POST)
	String roomupdate(int _ro_id,int _ro_hotel_id, String _ro_code, int _ro_room_type_id ,double _ro_price, 
			int _ro_furnitures,int _ro_policies,
			@RequestParam("roomFile") MultipartFile roomFile,HttpServletRequest request,
			RedirectAttributes redirectAttributes) throws ClassNotFoundException, IllegalArgumentException,
			IllegalAccessException, InstantiationException, InterruptedException {

		
		String ro_file = "http://localhost:8080/images/No_Image_Available.jpg";
		if (!roomFile.getOriginalFilename().equals("")) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(roomFile, serverPath);
			ro_file = "http://localhost:8080/images/" + roomFile.getOriginalFilename();
		}
		
		var params = new LinkedList<String>();
		params.add(_ro_id+"");
		params.add(_ro_code);
		params.add(_ro_hotel_id+"");
		params.add(_ro_room_type_id + "");
		params.add(_ro_price + "");
		params.add(_ro_furnitures + "");
		params.add(_ro_policies + "");
		params.add(ro_file);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Room_Update", params);
		var data = Mapper.MapTo(new RoomDTO(), rs);
		var isSuccess = !data.isEmpty();
		var roomThread = new RoomThread();
		roomThread.start();
		roomThread.join();
		var mes = isSuccess ? "Update Room Successfull." : "Update Room Fail!";
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/hotel/room";
	}

	// =================================== Hotel Settings
	@RequestMapping(value = "/hotel/settings", method = RequestMethod.GET)
	public String hotelsettings(Model model) {
		var isSuccessFurniture = !FurnitureThread.furnitures.isEmpty();
		var mesFurniture = isSuccessFurniture ? "" : "No Data Found.";
		var resultFurniture = new Result<LinkedList<FunitureDTO>>(FurnitureThread.furnitures, isSuccessFurniture,
				mesFurniture);
		model.addAttribute("resultFurniture", resultFurniture);

		var isSuccessPolicy = !PolicyThread.policies.isEmpty();
		var mesPolicy = isSuccessPolicy ? "" : "No Data Found.";
		var resultPolicy = new Result<LinkedList<PolicyDTO>>(PolicyThread.policies, isSuccessPolicy, mesPolicy);
		model.addAttribute("resultPolicy", resultPolicy);

		var isSuccessRoomType = !RoomTypeThread.roomTypes.isEmpty();
		var mesRoomType = isSuccessRoomType ? "" : "No Data Found.";
		var resultRoomType = new Result<LinkedList<RoomTypeDTO>>(RoomTypeThread.roomTypes, isSuccessRoomType,
				mesRoomType);
		model.addAttribute("resultRoomType", resultRoomType);
		return "admin/hotel/settings";
	}

	// =================================== Furniture
	@RequestMapping(value = "/furniture/insert", method = RequestMethod.POST)
	String furnitureinsert(String _fur_name, String _fur_icon,
			@RequestParam("furnitureFile") MultipartFile furnitureFile, HttpServletRequest request,
			RedirectAttributes redirectAttributes) throws ClassNotFoundException, IllegalArgumentException,
			IllegalAccessException, InstantiationException, InterruptedException {

		String fur_icon = "http://localhost:8080/images/No_Image_Available.jpg";
		if (!furnitureFile.getOriginalFilename().equals("")) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(furnitureFile, serverPath);
			fur_icon = "http://localhost:8080/images/" + furnitureFile.getOriginalFilename();
		}
		var params = new LinkedList<String>();
		params.add(_fur_name);
		params.add(fur_icon);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Furniture_Insert", params);
		var data = Mapper.MapTo(new FunitureDTO(), rs);
		var isSuccess = !data.isEmpty();
		var furnitureThread = new FurnitureThread();
		furnitureThread.start();
		furnitureThread.join();
		var mes = isSuccess ? "Create Furniture Successfull." : "Create Furniture Fail!";
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/hotel/settings";
	}

	@RequestMapping(value = "/furniture/update", method = RequestMethod.POST)
	String furnitureupdate(int _fur_id, String _fur_name, String _fur_icon,
			@RequestParam("furnitureFile") MultipartFile furnitureFile, HttpServletRequest request,
			RedirectAttributes redirectAttributes) throws ClassNotFoundException, IllegalArgumentException,
			IllegalAccessException, InstantiationException, InterruptedException {

		String fur_icon = _fur_icon;

		if (!furnitureFile.getOriginalFilename().equals("")) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(furnitureFile, serverPath);
			fur_icon = "http://localhost:8080/images/" + furnitureFile.getOriginalFilename();
		}

		var params = new LinkedList<String>();
		params.add(_fur_id + "");
		params.add(_fur_name);
		params.add(fur_icon);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Furniture_Update", params);
		var data = Mapper.MapTo(new FunitureDTO(), rs);
		var isSuccess = !data.isEmpty();
		var furnitureThread = new FurnitureThread();
		furnitureThread.start();
		furnitureThread.join();
		var mes = isSuccess ? "Update Furniture Successfull." : "Update Furniture Fail!";
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/hotel/settings";
	}

	// =================================== Policy
	@RequestMapping(value = "/policy/insert", method = RequestMethod.POST)
	String policyinsert(String _po_description, String _po_icon, @RequestParam("policyFile") MultipartFile policyFile,
			HttpServletRequest request, RedirectAttributes redirectAttributes) throws ClassNotFoundException,
			IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {

		String po_icon = "http://localhost:8080/images/No_Image_Available.jpg";
		if (!policyFile.getOriginalFilename().equals("")) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(policyFile, serverPath);
			po_icon = "http://localhost:8080/images/" + policyFile.getOriginalFilename();
		}
		var params = new LinkedList<String>();
		params.add(_po_description);
		params.add(po_icon);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Policy_Insert", params);
		var data = Mapper.MapTo(new PolicyDTO(), rs);
		var isSuccess = !data.isEmpty();
		var policyThread = new PolicyThread();
		policyThread.start();
		policyThread.join();
		var mes = isSuccess ? "Create Policy Successfull." : "Create Policy Fail!";
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/hotel/settings";
	}

	@RequestMapping(value = "/policy/update", method = RequestMethod.POST)
	String policyupdate(int _po_id, String _po_description, String _po_icon,
			@RequestParam("policyFile") MultipartFile policyFile, HttpServletRequest request,
			RedirectAttributes redirectAttributes) throws ClassNotFoundException, IllegalArgumentException,
			IllegalAccessException, InstantiationException, InterruptedException {

		String po_icon = _po_icon;
		if (!policyFile.getOriginalFilename().equals("")) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(policyFile, serverPath);
			po_icon = "http://localhost:8080/images/" + policyFile.getOriginalFilename();
		}
		var params = new LinkedList<String>();
		params.add(_po_id + "");
		params.add(_po_description);
		params.add(po_icon);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Policy_Update", params);
		var data = Mapper.MapTo(new PolicyDTO(), rs);
		var isSuccess = !data.isEmpty();
		var policyThread = new PolicyThread();
		policyThread.start();
		policyThread.join();
		var mes = isSuccess ? "Update Policy Successfull." : "Update Policy Fail!";
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/hotel/settings";
	}

	// =================================== Return Type
	@RequestMapping(value = "/roomtype/insert", method = RequestMethod.POST)
	String roomtypeinsert(String _rt_name, int _rt_max_customer, RedirectAttributes redirectAttributes)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException,
			InterruptedException {

		var params = new LinkedList<String>();
		params.add(_rt_name);
		params.add(_rt_max_customer + "");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_RoomType_Insert", params);
		var data = Mapper.MapTo(new RoomTypeDTO(), rs);
		var isSuccess = !data.isEmpty();
		var returnTypeThread = new RoomTypeThread();
		returnTypeThread.start();
		returnTypeThread.join();
		var mes = isSuccess ? "Create Room Type Successfull." : "Create Room Type Fail!";
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/hotel/settings";
	}

	@RequestMapping(value = "/roomtype/update", method = RequestMethod.POST)
	String roomtypeupdate(int _rt_id, String _rt_name, int _rt_max_customer, RedirectAttributes redirectAttributes)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException,
			InterruptedException {

		var params = new LinkedList<String>();
		params.add(_rt_id + "");
		params.add(_rt_name);
		params.add(_rt_max_customer + "");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_RoomType_Update", params);
		var data = Mapper.MapTo(new RoomTypeDTO(), rs);
		var isSuccess = !data.isEmpty();
		var roomTypeThread = new RoomTypeThread();
		roomTypeThread.start();
		roomTypeThread.join();
		var mes = isSuccess ? "Update Room Type Successfull." : "Update Room Type Fail!";
		redirectAttributes.addFlashAttribute("mes", mes);
		return "redirect:/hotel/settings";
	}

	void UpdaloadFile(MultipartFile multipartFile, String serverPath) {
		try {
			byte[] bytes = multipartFile.getBytes();

			File serverFile = new File(serverPath + File.separator + multipartFile.getOriginalFilename());
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
			stream.write(bytes);
			stream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void Debug(String text) {
		System.out.println(text);
	}
}
