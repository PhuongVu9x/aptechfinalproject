package vpn;

import java.util.LinkedList;

import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.RoomDTO;
import vpn.model.RoomTypeDTO;

public class RoomThread extends Thread{
	public static LinkedList<RoomDTO> rooms;
	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		try {
			var rs = DBConnection.CallProc("Sp_Room_Gets");
			rooms = Mapper.MapTo(new RoomDTO(), rs);
			System.out.println(AppThread.CountDown + ": Room All Done.");
			AppThread.CountDown--;
			if (AppThread.CountDown < 0)
				AppThread.CountDown = 0;
			this.interrupt(); 
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
