package vpn;

import java.util.LinkedList;

import vpn.model.CustomerDTO;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.PermisionDTO;

public class CustomerThread extends Thread{
	public static LinkedList<CustomerDTO> customers;
	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		if (customers == null) {
			try {
				var rs = DBConnection.CallProc("Sp_Customer_Gets");
				customers = Mapper.MapTo(new CustomerDTO(), rs);
				System.out.println(AppThread.CountDown + ": Customer All Done.");
				AppThread.CountDown--;
				if(AppThread.CountDown < 0)
					AppThread.CountDown = 0;
				this.interrupt();
			} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
					| InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
