package vpn.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vpn.model.Policy;
import vpn.model.PolicyDTO;
import vpn.PolicyThread;
import vpn.RoomTypeThread;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.Result;
@RestController
public class PolicyController {
	Result<PolicyDTO> insert(@Validated @RequestBody Policy Policy) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Policy._po_description);
		params.add(Policy._po_icon);
		params.add(Policy._po_created_date+"");
		
		var rs = DBConnection.CallProc("Sp_Policy_Insert", params);
		var data = Mapper.MapTo(new PolicyDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Create Policy Fail!";
		var result = new Result<PolicyDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<PolicyDTO> gets(@Validated @RequestBody PolicyDTO Policy) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Policy._po_description);
		var rs = DBConnection.CallProc("Sp_Policy_Gets", params);
		var data = Mapper.MapTo(new PolicyDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<PolicyDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	/*Result<PolicyDTO> info(@Validated @RequestBody Policy Policy) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Policy._po_id+"");
		var rs = DBConnection.CallProc("Sp_Policy_Info", params);
		var data = Mapper.MapTo(new PolicyDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found!";
		var result = new Result<PolicyDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}*/
	Result<PolicyDTO> updateIcon(@Validated @RequestBody Policy Policy) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Policy._po_id+"");
		params.add(Policy._po_icon);
		var rs = DBConnection.CallProc("Sp_Policy_UpdateIcon", params);
		var data = Mapper.MapTo(new PolicyDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Policy Icon Fail";
		var result = new Result<PolicyDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<PolicyDTO> update(@Validated @RequestBody Policy Policy) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Policy._po_id+"");
		params.add(Policy._po_description);
		params.add(Policy._po_updated_date+"");
		var rs = DBConnection.CallProc("Sp_Policy_Update", params);
		var data = Mapper.MapTo(new PolicyDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Policy Fail!";
		var result = new Result<PolicyDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	} 
	@RequestMapping(value = "api/policy/changestatus", method = RequestMethod.POST)
	Result<PolicyDTO> changeStatus(@Validated @RequestBody Policy Policy) 
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		var params = new LinkedList<String>();
		params.add(Policy._po_id+"");
		params.add(Policy._po_status+"");
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_Policy_ChangeStatus", params);
		var data = Mapper.MapTo(new PolicyDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "Policy Change Status Fail!";
		var thread = new PolicyThread();
		thread.start();
		thread.join();
		var result = new Result<PolicyDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
}
