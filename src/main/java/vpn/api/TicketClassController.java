package vpn.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vpn.model.TicketClass;
import vpn.model.TicketClassDTO;
import vpn.TicketClassThread;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.Result;
@RestController
public class TicketClassController {
	Result<TicketClassDTO> insert(@Validated @RequestBody TicketClass TicketClass) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(TicketClass._tc_name);
		params.add(TicketClass._tc_icon);
		params.add(TicketClass._tc_created_date+"");
		
		var rs = DBConnection.CallProc("Sp_TicketClass_Insert", params);
		var data = Mapper.MapTo(new TicketClassDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "Create TicketClass Fail!";
		var result = new Result<TicketClassDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	@RequestMapping(value="api/ticketclass/gets",method = RequestMethod.GET)
	Result<LinkedList<TicketClassDTO>> gets() throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add("");
		params.add("1");
		var rs = DBConnection.CallProc("Sp_TicketClass_Gets", params); 
		var data = Mapper.MapTo(new TicketClassDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<LinkedList<TicketClassDTO>>(data,isSuccess,mes);
		return result;
	}
	/*Result<TicketClassDTO> info(@Validated @RequestBody TicketClass TicketClass) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(TicketClass._tc_id+"");
		var rs = DBConnection.CallProc("Sp_TicketClass_Info", params);
		var data = Mapper.MapTo(new TicketClassDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found!";
		var result = new Result<TicketClassDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}*/
	Result<TicketClassDTO> updateIcon(@Validated @RequestBody TicketClass TicketClass) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(TicketClass._tc_id+"");
		params.add(TicketClass._tc_icon);
		var rs = DBConnection.CallProc("Sp_TicketClass_UpdateIcon", params);
		var data = Mapper.MapTo(new TicketClassDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update TicketClass Icon Fail";
		var result = new Result<TicketClassDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<TicketClassDTO> update(@Validated @RequestBody TicketClass TicketClass) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(TicketClass._tc_id+"");
		params.add(TicketClass._tc_name);
		params.add(TicketClass._tc_updated_date+"");
		var rs = DBConnection.CallProc("Sp_TicketClass_Update", params);
		var data = Mapper.MapTo(new TicketClassDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update TicketClass Fail!";
		var result = new Result<TicketClassDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	@RequestMapping(value="api/ticketclass/changestatus",method = RequestMethod.POST)
	Result<TicketClassDTO> changeStatus(@Validated @RequestBody TicketClass TicketClass) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		var params = new LinkedList<String>();
		params.add(TicketClass._tc_id+"");
		params.add(TicketClass._tc_status+"");
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_TicketClass_ChangeStatus", params);
		var data = Mapper.MapTo(new TicketClassDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "TicketClass Change Status Fail!";
		var ticketClassThread = new TicketClassThread();
		ticketClassThread.start();
		ticketClassThread.join();
		var result = new Result<TicketClassDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
}
