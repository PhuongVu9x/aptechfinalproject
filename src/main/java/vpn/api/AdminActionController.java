package vpn.api;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.UUID;

import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import jakarta.servlet.http.HttpServletRequest;
import vpn.AdminThread;
import vpn.model.Admin;
import vpn.model.AdminDTO;
import vpn.model.DBConnection;
import vpn.model.Decrypt;
import vpn.model.FlutterProduct;
import vpn.model.Mapper;
import vpn.model.Result;

@RestController
public class AdminActionController {
	@RequestMapping(value = "/adminLogin", method = RequestMethod.POST)
	Result<AdminDTO> login(@Validated @RequestBody Admin admin)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		String token = UUID.randomUUID().toString();
		var params = new LinkedList<String>();
		params.add(admin._ad_email);
		params.add(Decrypt.ToMd5(admin._ad_password));
		params.add(token);
		var rs = DBConnection.CallProc("Sp_Admin_Login", params);
		var data = Mapper.MapTo(new AdminDTO(), rs);
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "Wrong Email or Password.Please try again";
		var result = new Result<AdminDTO>(isSuccess ? data.getFirst() : null, isSuccess, mes);
		return result;
	}
	@RequestMapping(value = "api/admin/logout", method = RequestMethod.POST)
	Result<AdminDTO> logout(@Validated  @RequestBody Admin admin)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		
		var params = new LinkedList<String>();
		params.add(admin._ad_id + "");
		var rs = DBConnection.CallProc("Sp_Admin_Logout", params);
		var data = Mapper.MapTo(new AdminDTO(), rs);
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "Logout Fail. Something wrong in Server!";
		var result = new Result<AdminDTO>(isSuccess ? data.getFirst() : null, isSuccess, mes);
		return result;
	}

	Result<AdminDTO> gets(@Validated @RequestBody AdminDTO admin)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(admin._ad_first_name);
		params.add(admin._ad_last_name);
		params.add(admin._ad_full_name);
		params.add(admin._ad_email);
		params.add(admin._ad_dob + "");
		params.add(admin._ad_phone);
		params.add(admin._pos_name);

		var rs = DBConnection.CallProc("Sp_Admin_Gets", params);
		var data = Mapper.MapTo(new AdminDTO(), rs);
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<AdminDTO>(isSuccess ? data.getFirst() : null, isSuccess, mes);
		return result;
	}

	Result<AdminDTO> info(@Validated @RequestBody Admin admin)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(admin._ad_id + "");
		var rs = DBConnection.CallProc("Sp_Admin_Info", params);
		var data = Mapper.MapTo(new AdminDTO(), rs);
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found!";
		var result = new Result<AdminDTO>(isSuccess ? data.getFirst() : null, isSuccess, mes);
		return result;
	}

	@RequestMapping(value = "/api/admin/updateavatar", method = RequestMethod.POST)
	Result<AdminDTO> updateAvatar(@Validated @RequestBody FormData formData, HttpServletRequest request)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		Debug(formData.id + "");
		Debug(formData.file.getOriginalFilename());
		String ad_avatar = "";
		if (formData.file != null) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			UpdaloadFile(formData.file, serverPath);
			ad_avatar = "http://localhost:8080/images/" + formData.file.getOriginalFilename();
		}

		var params = new LinkedList<String>();
		params.add(formData.id + "");
		params.add(ad_avatar);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_Admin_UpdateAvatar", params);
		var data = Mapper.MapTo(new AdminDTO(), rs);
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Avatar Fail";
		var result = new Result<AdminDTO>(isSuccess ? data.getFirst() : null, isSuccess, mes);
		return result;
	}

	Result<AdminDTO> update(@Validated @RequestBody Admin admin)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(admin._ad_id + "");
		params.add(admin._ad_first_name);
		params.add(admin._ad_last_name);
		params.add(admin._ad_full_name);
		params.add(admin._ad_email);
		params.add(admin._ad_dob + "");
		params.add(admin._ad_phone);
		params.add(admin._ad_position_id + "");
		params.add(admin._ad_updated_date + "");
		var rs = DBConnection.CallProc("Sp_Admin_Update", params);
		var data = Mapper.MapTo(new AdminDTO(), rs);
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Admin Update Fail!";
		var result = new Result<AdminDTO>(isSuccess ? data.getFirst() : null, isSuccess, mes);
		return result;
	}

	@RequestMapping(value = "/api/admin/changestatus", method = RequestMethod.POST)
	Result<AdminDTO> changeStatus(@Validated @RequestBody Admin admin)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		
		var params = new LinkedList<String>();
		params.add(admin._ad_id + "");
		params.add(admin._ad_status + "");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_Admin_ChangeStatus", params);
		var data = Mapper.MapTo(new AdminDTO(), rs);
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "Admin Change Status Success" : "Admin Change Status Fail!";
		var result = new Result<AdminDTO>(isSuccess ? data.getFirst() : null, isSuccess, mes);
		
		var adminThread = new AdminThread();
		adminThread.start();

		return result;
	}

	@RequestMapping(value = "/api/admin/changepassword", method = RequestMethod.POST)
	Result<AdminDTO> changePassword(@Validated @RequestBody Admin admin)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(admin._ad_id + "");
		params.add(Decrypt.ToMd5(admin._ad_password));
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_Admin_ChangePassword", params);
		var data = Mapper.MapTo(new AdminDTO(), rs);
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Admin Change Password Fail!";
		var result = new Result<AdminDTO>(isSuccess ? data.getFirst() : null, isSuccess, mes);
		return result;
	}
	
	@RequestMapping(value = "/api/flutter/gets", method = RequestMethod.GET)
	LinkedList<FlutterProduct> getflutterproduct()
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		
		var rs = DBConnection.CallProc("select * from Flutter_Product");
		var data = Mapper.MapTo(new FlutterProduct(), rs);
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "No data found.";
		var result = new Result<LinkedList<FlutterProduct>>(isSuccess ? data : null, isSuccess, mes);
		return data;
	}
	
	@RequestMapping(value = "/api/flutter/delete", method = RequestMethod.POST)
	FlutterProduct deleteflutterproduct(@Validated @RequestBody int id)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var query = "Delete from Flutter_Product where _id = " + id + ";select top 1 * from Flutter_Product where _id = " + id;
		var rs = DBConnection.CallProc(query);
		var data = Mapper.MapTo(new FlutterProduct(), rs);
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "Delete Success" : "Delete Fail!";
		var result = new Result<FlutterProduct>(isSuccess ? data.getFirst() : null, isSuccess, mes);
		return isSuccess ? data.getFirst() : null;
	}
	
	
	public class FormData{
		public int id;
		MultipartFile file;
	}
	
	void UpdaloadFile(MultipartFile multipartFile, String serverPath) {
		try {
			byte[] bytes = multipartFile.getBytes();

			File serverFile = new File(serverPath + File.separator + multipartFile.getOriginalFilename());
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
			stream.write(bytes);
			stream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void Debug(String text) {
		System.out.println(text);
	}
}
