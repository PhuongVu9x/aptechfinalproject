package vpn.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.UUID;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vpn.BillThread;
import vpn.FlightThread;
import vpn.NotificationThread;
import vpn.model.Admin;
import vpn.model.AdminDTO;
import vpn.model.BillDTO;
import vpn.model.DBConnection;
import vpn.model.Decrypt;
import vpn.model.FlightDTO;
import vpn.model.Mapper;
import vpn.model.Notification;
import vpn.model.Result;

@RestController
public class NotificationController {
	@RequestMapping(value = "api/noti/gets", method = RequestMethod.GET)
	Result<LinkedList<Notification>> login(int _noti_cus_id,int _noti_type)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(_noti_cus_id+"");
		params.add(_noti_type+"");
		var rs = DBConnection.CallProc("Sp_Noti_Gets", params);
		var data = Mapper.MapTo(new Notification(), rs);
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "Empty";
		var result = new Result<LinkedList<Notification>>(data, isSuccess, mes);
		return result;  
	}
	
	@RequestMapping(value = "api/noti/changestatus", method = RequestMethod.POST)
	Result<Notification> changestatus(@Validated @RequestBody Notification noti)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		System.out.println(noti._noti_id);
		System.out.println(123);
		System.out.println(noti._noti_status);
		var params = new LinkedList<String>();
		
		params.add(noti._noti_id+"");
		params.add(noti._noti_status+"");   
		 
		var rs = DBConnection.CallProc("Sp_Noti_ChangeStatus", params);
		var data = Mapper.MapTo(new Notification(), rs);
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "Update Notification Success" : "Update Notification Fail";
		var result = new Result<Notification>(isSuccess ? data.getFirst() : null, isSuccess, mes);
		return result;
	}
	
	@RequestMapping(value = "api/noti/2nd", method = RequestMethod.POST)
	Result<String> insert()
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add("2");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));
		params.add("4");
		var rs = DBConnection.CallProc("Sp_Flight_GetByNotiStt",params);
		var data = Mapper.MapTo(new BillDTO(), rs);
		var isSuccess = !data.isEmpty();
		if(isSuccess) {
			var notiParam = new LinkedList<String>();
			var notiParamMobile = new LinkedList<String>();
			var _noti_description = "";
			for(var bill : data) {
				_noti_description = "Get ready to embark on a wonderful journey. Only 4 days left until your flight with flight number "+ bill._pl_code +
						" We wish you a safe and enjoyable flight!";
				notiParamMobile.add(bill._bil_cus_id+"");
				notiParamMobile.add(bill._bil_fl_id+"");
				notiParamMobile.add(_noti_description+"");
				notiParamMobile.add("1");
				notiParamMobile.add(dtf.format(now));
				DBConnection.CallProc("Sp_Noti_Insert",notiParamMobile);
			
				_noti_description = "Get ready to embark on a wonderful journey. Only 4 days left until your flight with flight number "+ bill._pl_code +
						" We wish you a safe and enjoyable flight!";
				
				notiParam.add(bill._bil_cus_id+"");
				notiParam.add(bill._bil_fl_id+"");
				notiParam.add(_noti_description+"");
				notiParam.add("2");
				notiParam.add(dtf.format(now));
				DBConnection.CallProc("Sp_Noti_Insert",notiParam);
			}
			
		}
		var result = new Result<String>("qwe",true,"Notification Service Done");
		return result;
	}
}
