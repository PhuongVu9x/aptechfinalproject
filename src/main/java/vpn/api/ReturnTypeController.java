package vpn.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vpn.model.ReturnType;
import vpn.model.ReturnTypeDTO;
import vpn.ReturnTypeThread;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.Result;
@RestController
public class ReturnTypeController
{
	Result<ReturnTypeDTO> insert(@Validated @RequestBody ReturnType ReturnType) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(ReturnType._ret_name);
		params.add(ReturnType._ret_icon);
		params.add(ReturnType._ret_created_date+"");
		
		var rs = DBConnection.CallProc("Sp_ReturnType_Insert", params);
		var data = Mapper.MapTo(new ReturnTypeDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Create ReturnType Fail!";
		var result = new Result<ReturnTypeDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<ReturnTypeDTO> gets(@Validated @RequestBody ReturnTypeDTO ReturnType) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(ReturnType._ret_name);
		var rs = DBConnection.CallProc("Sp_ReturnType_Gets", params);
		var data = Mapper.MapTo(new ReturnTypeDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<ReturnTypeDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	/*Result<ReturnTypeDTO> info(@Validated @RequestBody ReturnType ReturnType) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(ReturnType._ret_id+"");
		var rs = DBConnection.CallProc("Sp_ReturnType_Info", params);
		var data = Mapper.MapTo(new ReturnTypeDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found!";
		var result = new Result<ReturnTypeDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}*/
	
	Result<ReturnTypeDTO> update(@Validated @RequestBody ReturnType ReturnType) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(ReturnType._ret_id+"");
		params.add(ReturnType._ret_name);
		params.add(ReturnType._ret_updated_date+"");
		var rs = DBConnection.CallProc("Sp_ReturnType_Update", params);
		var data = Mapper.MapTo(new ReturnTypeDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update ReturnType Fail!";
		var result = new Result<ReturnTypeDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	@RequestMapping(value="api/returntype/changestatus",method = RequestMethod.POST)
	Result<ReturnTypeDTO> changeStatus(@Validated @RequestBody ReturnType ReturnType) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		var params = new LinkedList<String>();
		params.add(ReturnType._ret_id+"");
		params.add(ReturnType._ret_status+"");
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_ReturnType_ChangeStatus", params);
		var data = Mapper.MapTo(new ReturnTypeDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "ReturnType Change Status Fail!";
		var returnTypeThread = new ReturnTypeThread();
		returnTypeThread.start();
		returnTypeThread.join();
		var result = new Result<ReturnTypeDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
}
