package vpn.api;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import jakarta.servlet.http.HttpServletRequest;
import vpn.model.DBConnection;
import vpn.model.FlightDTO;
import vpn.model.Mapper;
import vpn.model.Result;
import vpn.model.SeatDTO;
import vpn.model.Ticket;
import vpn.model.TicketDTO;
import vpn.vnpay.Config;
import vpn.BillThread;
import vpn.FlightThread;
import vpn.NotificationThread;
import vpn.TicketThread;
import vpn.model.Bill;
import vpn.model.BillDTO;
import vpn.model.BillReq;
import vpn.model.CustomerDTO;
@RestController
public class BillController {
	@RequestMapping(value="api/bill/insert",method = RequestMethod.POST)
	Result<BillDTO> insert(@Validated @RequestBody BillReq data) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		
		var params = new LinkedList<String>();
		params.add(data._bil._bil_cus_id+"");
		params.add(data._bil._bil_payment_type+"");
		params.add(data._bil._bil_payment_code+"");
		params.add(data._bil._bil_payment+"");
		
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		params.add(dtf.format(now));
		
		var rs = DBConnection.CallProc("Sp_Bill_Insert", params);
		var returnData = Mapper.MapTo(new BillDTO(), rs); 
		var isSuccess = !returnData.isEmpty();
		var mes = isSuccess ? "" : "Create Bill Fail!";
		var inserted = isSuccess ? returnData.getFirst() : null;
		var result = new Result<BillDTO>(inserted,isSuccess,mes);
	
		if(isSuccess) {
			
			var fl_ids = new LinkedList<Integer>();
			for (Ticket ticket : data._tickets) {
				if(!fl_ids.contains(ticket._tk_fl_id))
					fl_ids.add(ticket._tk_fl_id);
				var nameSpaceIndex = ticket._tk_full_name.indexOf(" ");
				
				var ticketParams = new LinkedList<String>();
				ticketParams.add(ticket._tk_fl_id+"");
				ticketParams.add(inserted._bil_id+"");
				ticketParams.add(ticket._tk_payment+"");
				ticketParams.add(ticket._tk_full_name.substring(0,nameSpaceIndex));
				ticketParams.add(ticket._tk_full_name.substring(nameSpaceIndex));
				ticketParams.add(ticket._tk_full_name+""); 
				ticketParams.add(ticket._tk_dob);
				ticketParams.add(ticket._tk_nationality+"");
				ticketParams.add(ticket._tk_passport+"");
				ticketParams.add(ticket._tk_country+"");
				ticketParams.add(ticket._tk_passport_expired+"");
				ticketParams.add(ticket._tk_title);
				ticketParams.add(ticket._tk_return_type+"");
				ticketParams.add("1");
				ticketParams.add(ticket._tk_pas_id+"");
				ticketParams.add(ticket._tk_guardian_id+"");
				ticketParams.add(ticket._tk_seat);
				ticketParams.add(ticket._tk_tc_id+"");
				ticketParams.add(ticket._tk_cabin+"");
				ticketParams.add(ticket._tk_free_cabin+"");
				ticketParams.add(ticket._tk_checked+"");
				ticketParams.add(ticket._tk_free_checked+"");
				ticketParams.add("");
				ticketParams.add(dtf.format(now));
				
				DBConnection.CallProc("Sp_Ticket_Insert", ticketParams);
			}
			for(var fl_id : fl_ids) {
				NotificationThread._cus_id = data._bil._bil_cus_id;
				NotificationThread._fl_id = fl_id;
				NotificationThread._noti_type = 2;

				var notiThread = new NotificationThread();
				notiThread.start();
			}
		}
		var ticketThread = new TicketThread();
		ticketThread.start();
		ticketThread.join();
		
		var billThread = new BillThread();
		billThread.start();
		billThread.join();
		return result;
	}
	
	@RequestMapping(value="api/bill/insertbill",method = RequestMethod.POST)
	Result<BillDTO> insertBill(@Validated @RequestBody BillReq data) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		
		var params = new LinkedList<String>();
		params.add(data._bil._bil_cus_id+"");
		params.add(data._bil._bil_payment_type+"");
		params.add(data._bil._bil_payment_code+"");
		params.add(data._bil._bil_payment+"");
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		params.add(dtf.format(now));
		
		var rs = DBConnection.CallProc("Sp_Bill_Insert", params);
		var returnData = Mapper.MapTo(new BillDTO(), rs); 
		var isSuccess = !returnData.isEmpty();
		var mes = isSuccess ? "" : "Create Bill Fail!";
		var inserted = isSuccess ? returnData.getFirst() : null;
		var result = new Result<BillDTO>(inserted,isSuccess,mes);
	
		if(isSuccess) {
			var fl_ids = new LinkedList<Integer>();
			for (Ticket ticket : data._tickets) {
				var nameSpaceIndex = ticket._tk_full_name.indexOf(" ");
				if(!fl_ids.contains(ticket._tk_fl_id))
					fl_ids.add(ticket._tk_fl_id);
				var ticketParams = new LinkedList<String>();
				ticketParams.add(ticket._tk_fl_id+"");
				
				ticketParams.add(inserted._bil_id+"");
				ticketParams.add(ticket._tk_payment+"");
				ticketParams.add(ticket._tk_full_name.substring(0,nameSpaceIndex));
				ticketParams.add(ticket._tk_full_name.substring(nameSpaceIndex));
				ticketParams.add(ticket._tk_full_name+"");
				ticketParams.add(ticket._tk_dob+"");
				ticketParams.add(ticket._tk_nationality+"");
				ticketParams.add(ticket._tk_passport+"");
				ticketParams.add(ticket._tk_country+"");
				ticketParams.add(ticket._tk_passport_expired+"");
				ticketParams.add(ticket._tk_title);
				ticketParams.add(ticket._tk_return_type+"");
				ticketParams.add("1");
				ticketParams.add(ticket._tk_pas_id+"");
				ticketParams.add(ticket._tk_guardian_id+"");
				ticketParams.add(ticket._tk_seat);
				ticketParams.add(ticket._tk_tc_id+"");
				ticketParams.add(ticket._tk_cabin+"");
				ticketParams.add(ticket._tk_free_cabin+"");
				ticketParams.add(ticket._tk_checked+"");
				ticketParams.add(ticket._tk_free_checked+"");
				ticketParams.add("");
				ticketParams.add(dtf.format(now));
				
				DBConnection.CallProc("Sp_Ticket_Insert", ticketParams);
			}
			
			for(var fl_id : fl_ids) {
				NotificationThread._cus_id = data._bil._bil_cus_id;
				NotificationThread._fl_id = fl_id;
				NotificationThread._noti_type = 1;

				var notiThread = new NotificationThread();
				notiThread.start();
			}
		}
		var registration = new CustomerDTO();
		registration._cus_id = data._bil._bil_cus_id;
		EmailController.SendEticket(registration);
		
		var ticketThread = new TicketThread();
		ticketThread.start();
		ticketThread.join();
		
		var billThread = new BillThread();
		billThread.start();
		billThread.join();
		return result;
	}
	
	@RequestMapping(value="api/bill/updatepayment",method = RequestMethod.POST)
	Result<BillDTO> updatepayment(@Validated @RequestBody Bill bill) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		
		var params = new LinkedList<String>();
		params.add(bill._bil_id+"");
		params.add(bill._bil_payment_code+"");
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		params.add(dtf.format(now));
		
		var rs = DBConnection.CallProc("Sp_Bill_UpdatePayment", params);
		var returnData = Mapper.MapTo(new BillDTO(), rs); 
		var isSuccess = !returnData.isEmpty();
		var mes = isSuccess ? "" : "Create Bill Fail!";
		var inserted = isSuccess ? returnData.getFirst() : null;
		var result = new Result<BillDTO>(inserted,isSuccess,mes);
		
		var billThread = new BillThread();
		billThread.start();
		billThread.join();
		return result;
	}
	
	@RequestMapping(value="api/bill/getbycustomer",method = RequestMethod.GET)
	Result<LinkedList<BillDTO>> getbycustomer(int _cus_id) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(_cus_id+"");
		var rs = DBConnection.CallProc("Sp_Bill_GetByCustomer", params);
		var data = Mapper.MapTo(new BillDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<LinkedList<BillDTO>>(data,isSuccess,mes);
		return result;
	}
	
	@RequestMapping(value="api/bill/getbycustomermobile",method = RequestMethod.GET)
	Result<LinkedList<BillDTO>> getbycustomermobile(int _cus_id) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		
		var params = new LinkedList<String>();
		params.add(_cus_id+"");
		var rs = DBConnection.CallProc("Sp_Bill_GetByCustomer", params);
		var data = Mapper.MapTo(new BillDTO(), rs); 
		
		
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "No data found(s)!";
		if(isSuccess) {
			var rsTickets = DBConnection.CallProc("Sp_Ticket_Gets");
			var tickets = Mapper.MapTo(new TicketDTO(), rsTickets);
			var rsFlight = DBConnection.CallProc("Sp_Flight_Gets");
			var flights = Mapper.MapTo(new FlightDTO(), rsFlight);
			for (var bill : data) {
				bill._tickets = new LinkedList<TicketDTO>();
				for (var ticket : tickets) {
					if(bill._bil_id == ticket._tk_bill_id) {
						for (var flight : flights) {
							if(ticket._tk_fl_id == flight._fl_id) {
								ticket._tk_flight = flight;
							}
						}
						bill._tickets.add(ticket);
					}
				}
			}
		}
		var result = new Result<LinkedList<BillDTO>>(data,isSuccess,mes);
		return result;
	}
	
	@RequestMapping(value="api/cart/save",method = RequestMethod.POST)
	Result<CustomerDTO> save(@Validated @RequestBody CustomerDTO req) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(req._cus_id+"");
		params.add(req._cart_detail+"");
		var rs = DBConnection.CallProc("Sp_Cart_Save", params);
		var data = Mapper.MapTo(new CustomerDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<CustomerDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	
	@RequestMapping(value="api/cart/get",method = RequestMethod.GET)
	Result<CustomerDTO> get(@Validated @RequestBody CustomerDTO req) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(req._cus_id+"");
		var rs = DBConnection.CallProc("Sp_Cart_Get", params);
		var data = Mapper.MapTo(new CustomerDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<CustomerDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	
	@RequestMapping(value="api/bill/getbyplane",method = RequestMethod.GET)
	Result<LinkedList<BillDTO>> getbyplane(int _pl_id) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(_pl_id+"");
		var rs = DBConnection.CallProc("Sp_Bill_GetByPlane", params);
		var data = Mapper.MapTo(new BillDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<LinkedList<BillDTO>>(data,isSuccess,mes);
		return result;
	}
	
	@RequestMapping(value="api/bill/getbyflight",method = RequestMethod.GET)
	Result<LinkedList<BillDTO>> getbyflight(int _fl_id) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(_fl_id+"");
		var rs = DBConnection.CallProc("Sp_Bill_GetByFlight", params); 
		var data = Mapper.MapTo(new BillDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<LinkedList<BillDTO>>(data,isSuccess,mes);
		return result;
	}
	
	Result<BillDTO> gets(@Validated @RequestBody BillDTO Bill) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Bill._cus_full_name+"");
		params.add(Bill._bil_created_date+"");
		var rs = DBConnection.CallProc("Sp_Bill_Gets", params);
		var data = Mapper.MapTo(new BillDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<BillDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<BillDTO> info(@Validated @RequestBody Bill Bill) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Bill._bil_id+"");
		var rs = DBConnection.CallProc("Sp_Bill_Info", params);
		var data = Mapper.MapTo(new BillDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found!";
		var result = new Result<BillDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	
	/*Result<BillDTO> update(@Validated @RequestBody Bill Bill) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Bill._bil_id+"");
		params.add(Bill._bil_payment+"");
		params.add(Bill._bil_updated_date+"");
		var rs = DBConnection.CallProc("Sp_Bill_Update", params);
		var data = Mapper.MapTo(new BillDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Bill Fail!";
		var result = new Result<BillDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}*/
	Result<BillDTO> changeStatus(@Validated @RequestBody Bill Bill) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Bill._bil_id+"");
		params.add(Bill._bil_status+"");
		var rs = DBConnection.CallProc("Sp_Bill_ChangeStatus", params);
		var data = Mapper.MapTo(new BillDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Bill Change Status Fail!";
		var result = new Result<BillDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
		
	@RequestMapping(value = "api/vnpay", method = RequestMethod.POST)
	public String vnpay(HttpServletRequest req, float payment) throws UnsupportedEncodingException {
		System.out.println(payment);
		String vnp_Version = "2.1.0";
        String vnp_Command = "pay";
        String orderType = "other";
        //long amount = Integer.parseInt(req.getParameter("amount"))*100;
        long amount = (long) (payment * 100);
        String bankCode = req.getParameter("bankCode");
        
        String vnp_TxnRef = Config.getRandomNumber(8);
        String vnp_IpAddr = Config.getIpAddress(req);

        String vnp_TmnCode = Config.vnp_TmnCode;
        
        Map<String, String> vnp_Params = new HashMap<>();
        vnp_Params.put("vnp_Version", vnp_Version);
        vnp_Params.put("vnp_Command", vnp_Command);
        vnp_Params.put("vnp_TmnCode", vnp_TmnCode);
        vnp_Params.put("vnp_Amount", String.valueOf(amount));
        vnp_Params.put("vnp_CurrCode", "VND");
        
        if (bankCode != null && !bankCode.isEmpty()) {
            vnp_Params.put("vnp_BankCode", bankCode);
        }
        vnp_Params.put("vnp_TxnRef", vnp_TxnRef);
        vnp_Params.put("vnp_OrderInfo", "Thanh toan don hang:" + vnp_TxnRef);
        vnp_Params.put("vnp_OrderType", orderType);
        String locate = req.getParameter("language");
        if (locate != null && !locate.isEmpty()) {
            vnp_Params.put("vnp_Locale", locate);
        } else {
            vnp_Params.put("vnp_Locale", "vn");
        }
        
        String transactionStatus = vnp_Params.get("vnp_TransactionStatus");
        System.out.println(transactionStatus);
        vnp_Params.put("vnp_ReturnUrl", Config.vnp_ReturnUrl);
        vnp_Params.put("vnp_IpAddr", vnp_IpAddr);

        Calendar cld = Calendar.getInstance(TimeZone.getTimeZone("Etc/GMT+7"));
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        String vnp_CreateDate = formatter.format(cld.getTime());
        vnp_Params.put("vnp_CreateDate", vnp_CreateDate);
        cld.add(Calendar.MINUTE, 15);
        String vnp_ExpireDate = formatter.format(cld.getTime());
        vnp_Params.put("vnp_ExpireDate", vnp_ExpireDate);
        List fieldNames = new ArrayList(vnp_Params.keySet());
        Collections.sort(fieldNames);
        StringBuilder hashData = new StringBuilder();
        StringBuilder query = new StringBuilder();
        Iterator itr = fieldNames.iterator();
        while (itr.hasNext()) {
            String fieldName = (String) itr.next();
            String fieldValue = (String) vnp_Params.get(fieldName);
            if ((fieldValue != null) && (fieldValue.length() > 0)) {
                //Build hash data
                hashData.append(fieldName);
                hashData.append('=');
                hashData.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
                //Build query
                query.append(URLEncoder.encode(fieldName, StandardCharsets.US_ASCII.toString()));
                query.append('=');
                query.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
                if (itr.hasNext()) {
                    query.append('&');
                    hashData.append('&');
                }
            }
        }
        String queryUrl = query.toString();
        String vnp_SecureHash = Config.hmacSHA512(Config.secretKey, hashData.toString());
        queryUrl += "&vnp_SecureHash=" + vnp_SecureHash;
        String paymentUrl = Config.vnp_PayUrl + "?" + queryUrl;
        com.google.gson.JsonObject job = new JsonObject();
        job.addProperty("code", "00");
        job.addProperty("message", "success");
        job.addProperty("data", paymentUrl);
        Gson gson = new Gson();
        
		return gson.toJson(job);
	}

}



