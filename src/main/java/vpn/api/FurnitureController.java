package vpn.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vpn.FurnitureThread;
import vpn.PolicyThread;
import vpn.model.DBConnection;
import vpn.model.Funiture;
import vpn.model.FunitureDTO;
import vpn.model.Mapper;
import vpn.model.Result;

@RestController
public class FurnitureController {
	@RequestMapping(value = "api/furniture/changestatus", method = RequestMethod.POST)
	Result<FunitureDTO> changeStatus(@Validated @RequestBody FunitureDTO furniture)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		var params = new LinkedList<String>();
		params.add(furniture._fur_id+"");
		params.add(furniture._fur_status+"");
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_Furniture_ChangeStatus", params);
		var data = Mapper.MapTo(new FunitureDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "Furniture Change Status Fail!";
		var thread = new FurnitureThread();
		thread.start();
		thread.join();
		var result = new Result<FunitureDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
}
