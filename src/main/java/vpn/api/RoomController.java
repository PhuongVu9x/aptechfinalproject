package vpn.api;

import java.io.File;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServletRequest;
import vpn.AppThread;
import vpn.HotelThread;
import vpn.RoomThread;
import vpn.model.DBConnection;
import vpn.model.Room;
import vpn.model.RoomDTO;
import vpn.model.Mapper;
import vpn.model.Result;
@RestController
public class RoomController {
	@RequestMapping(value="api/room/insert",method = RequestMethod.POST)
	Result<RoomDTO> insert(int _ro_ho_id,int _ro_id,String _ro_code, int _ro_room_type_id,
			float _ro_price,String _ro_furnitures, String _ro_policies,
			HttpServletRequest request,MultipartFile roomFile) 
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		String ro_file = "http://localhost:8080/images/No_Image_Available.jpg";
		if (roomFile != null) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			var fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath();
			AppThread.UpdaloadFile(roomFile, serverPath);
			ro_file = fileDownloadUri.path("images").toUriString() + "/" + roomFile.getOriginalFilename();
		}
		var params = new LinkedList<String>();
		params.add(_ro_code);
		params.add(_ro_ho_id+"");
		params.add(_ro_room_type_id + "");
		params.add(_ro_price + "");
		params.add(_ro_furnitures + "");
		params.add(_ro_policies + "");
		params.add(ro_file);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));
		
		var rs = DBConnection.CallProc("Sp_Room_Insert", params);
		var data = Mapper.MapTo(new RoomDTO(), rs);
		var isSuccess = !data.isEmpty();
		var roomThread = new RoomThread();
		roomThread.start();
		roomThread.join(); 
		var hotelThread = new HotelThread();
		hotelThread.start();
		hotelThread.join();
		var mes = isSuccess ? "Create Room Successfull." : "Create Room Fail!";
		var result = new Result<RoomDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	
	@RequestMapping(value="api/room/update",method = RequestMethod.POST)
	Result<RoomDTO> update(int _ro_ho_id,int _ro_id,String _ro_code, int _ro_room_type_id,
			float _ro_price,String _ro_furnitures, String _ro_policies,
			HttpServletRequest request,MultipartFile roomFile) 
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		String ro_file = "http://localhost:8080/images/No_Image_Available.jpg";
		if (roomFile != null) {
			var serverPath = request.getSession().getServletContext().getRealPath("images");
			var fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath();
			AppThread.UpdaloadFile(roomFile, serverPath);
			ro_file = fileDownloadUri.path("images").toUriString() + "/" + roomFile.getOriginalFilename();
		}
		var params = new LinkedList<String>();
		params.add(_ro_id+"");
		params.add(_ro_code);
		params.add(_ro_ho_id+"");
		params.add(_ro_room_type_id + "");
		params.add(_ro_price + "");
		params.add(_ro_furnitures + "");
		params.add(_ro_policies + "");
		params.add(ro_file);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Room_Update", params);
		var data = Mapper.MapTo(new RoomDTO(), rs);
		var isSuccess = !data.isEmpty();
		var roomThread = new RoomThread();
		roomThread.start();
		roomThread.join(); 
		var hotelThread = new HotelThread();
		hotelThread.start();
		hotelThread.join();
		var mes = isSuccess ? "Create Room Successfull." : "Create Room Fail!";
		var result = new Result<RoomDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	
	Result<RoomDTO> gets(@Validated @RequestBody RoomDTO Room) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Room._ro_code);
		params.add(Room._ro_ho_id+"");
		params.add(Room._ro_room_type_id+"");
		params.add(Room._ro_price+"");
		params.add(Room._ro_furnitures);
		params.add(Room._ro_policies);
		params.add(Room._ro_max_customer+"");
		var rs = DBConnection.CallProc("Sp_Room_Gets", params);
		var data = Mapper.MapTo(new RoomDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<RoomDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<RoomDTO> info(@Validated @RequestBody Room Room) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Room._ro_id+"");
		var rs = DBConnection.CallProc("Sp_Room_Info", params);
		var data = Mapper.MapTo(new RoomDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found!";
		var result = new Result<RoomDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<RoomDTO> updateIcon(@Validated @RequestBody Room Room) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Room._ro_id+"");
		params.add(Room._ro_icon);
		var rs = DBConnection.CallProc("Sp_Room_UpdateIcon", params);
		var data = Mapper.MapTo(new RoomDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Room Icon Fail";
		var result = new Result<RoomDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<RoomDTO> updateBackground(@Validated @RequestBody Room Room) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Room._ro_id+"");
		params.add(Room._ro_images);
		var rs = DBConnection.CallProc("Sp_Room_UpdateBackground", params);
		var data = Mapper.MapTo(new RoomDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Room Background Fail";
		var result = new Result<RoomDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<RoomDTO> update(@Validated @RequestBody Room Room) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Room._ro_id+"");
		params.add(Room._ro_code);
		params.add(Room._ro_ho_id+"");
		params.add(Room._ro_room_type_id+"");
		params.add(Room._ro_price+"");
		params.add(Room._ro_funitures);
		params.add(Room._ro_policies);
		params.add(Room._ro_max_customer+"");
		params.add(Room._ro_updated_date+"");
		var rs = DBConnection.CallProc("Sp_Room_Update", params);
		var data = Mapper.MapTo(new RoomDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Room Fail!";
		var result = new Result<RoomDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	@RequestMapping(value="api/room/getbyhotel",method = RequestMethod.GET)
	Result<LinkedList<RoomDTO>> getbyhotel(int _ho_id) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add("");
		params.add(_ho_id+"");
		var rs = DBConnection.CallProc("Sp_Room_Gets", params);
		var data = Mapper.MapTo(new RoomDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<LinkedList<RoomDTO>>(data,isSuccess,mes);
		return result;
	}
	
	@RequestMapping(value="api/room/changestatus",method = RequestMethod.POST)
	Result<RoomDTO> changeStatus(@Validated @RequestBody Room room) 
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		var params = new LinkedList<String>();
		params.add(room._ro_id+"");
		params.add(room._ro_status+"");
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_Room_ChangeStatus", params);
		var data = Mapper.MapTo(new RoomDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var roomThread = new RoomThread();
		roomThread.start();
		roomThread.join();
		var hotelThread = new HotelThread();
		hotelThread.start();
		hotelThread.join();
		var mes = isSuccess ? "" : "Room Change Status Fail!";
		var result = new Result<RoomDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	
	void Debug(String text) {
		System.out.println(text);
	}
}
