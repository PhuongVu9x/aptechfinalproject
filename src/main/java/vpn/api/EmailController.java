package vpn.api;

import vpn.model.AdminDTO;
import vpn.model.CustomerDTO;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.Result;
import vpn.model.TicketDTO;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.Properties;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailController {
	
	public static String sender = "bichvanphamnguyen1412@gmail.com";
	public static String username = "bichvanphamnguyen1412@gmail.com";
	public static String password = "rava fszx qidh slob";
	public static String smtp = "smtp.gmail.com";
	public static String port = "587";
	
    @RequestMapping(value = "api/mail/register", method = RequestMethod.POST)
    public boolean RegisterMail(@Validated @RequestBody CustomerDTO registration) {
         boolean result = true;
         
         Properties prop = new Properties();
 		 prop.put("mail.smtp.host", smtp);
         prop.put("mail.smtp.port", port);
         prop.put("mail.smtp.auth", "true");
         prop.put("mail.smtp.starttls.enable", "true"); //TLS
         
         Session session = Session.getInstance(prop,
                 new javax.mail.Authenticator() {
                     protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                         return new javax.mail.PasswordAuthentication(username, password);
                     }
                 });

         try {
        	 String token = UUID.randomUUID().toString();
        	 String activationLink = "http://localhost:8080/api/account/activate?token=" + registration._cus_active_token + registration._cus_id;
        	 String emailContent = "<html><body>" +
			                       "<p>Congratulations! Your registration is successful.</p>" +
			                       "<p>Please click on the following link to activate your account:</p>" +
			                       "<a href='" + activationLink + "'>Click here to activate</a>" +
			                       "</body></html>";
        	 
             Message message = new MimeMessage(session);
             message.setFrom(new InternetAddress(sender));
             message.setRecipients(
                     Message.RecipientType.TO,
                     InternetAddress.parse(sender)
             );
             message.setSubject("Welcome to VPNTravel");
             message.setContent(emailContent, "text/html");

             Transport.send(message);

             System.out.println("Done");
         } catch (MessagingException e) {
             e.printStackTrace();
             result = false;
         }
         
         return result;
    }
    
    @RequestMapping(value = "api/ticket/returnticket", method = RequestMethod.POST)
    boolean returnticket(@Validated @RequestBody String ids)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {

		Properties prop = new Properties();
		prop.put("mail.smtp.host", smtp);
		prop.put("mail.smtp.port", port);
		prop.put("mail.smtp.auth", "true");
		prop.put("mail.smtp.starttls.enable", "true"); // TLS

		Session session = Session.getInstance(prop, new javax.mail.Authenticator() {
			protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
				return new javax.mail.PasswordAuthentication(username, password);
			}
		});
		var params = new LinkedList<String>();
		params.add(ids);
		var rs = DBConnection.CallProc("Sp_Ticket_Return",params);
		var data = Mapper.MapTo(new TicketDTO(), rs);
		var isSuccess = !data.isEmpty();
		if (isSuccess) {
			LocalDateTime now = LocalDateTime.now();
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			params.add(dtf.format(now));
			var cus = data.getFirst();
			var body = "";
			for (var ticket : data) {
				var notiParam = new LinkedList<String>();
				var notiParamMobile = new LinkedList<String>();
				var _noti_description = "";
				_noti_description = "Congratulation! You will get your refund in next 2 weeks";
				notiParamMobile.add(ticket._bil_id+"");
				notiParamMobile.add(ticket._bil_fl_id+"");
				notiParamMobile.add(_noti_description+"");
				notiParamMobile.add("1");
				notiParamMobile.add(dtf.format(now));
				DBConnection.CallProc("Sp_Noti_Insert",notiParamMobile);
			
				_noti_description = "Congratulation! You will get your refund in next 2 weeks";
				
				notiParamMobile.add(ticket._bil_id+"");
				notiParamMobile.add(ticket._bil_fl_id+"");
				notiParamMobile.add(_noti_description+"");
				notiParam.add("2");
				notiParam.add(dtf.format(now));
				DBConnection.CallProc("Sp_Noti_Insert",notiParam);
				body += "<tr>"
						+ "    <td> " + ticket._tk_title + " " + ticket._tk_full_name+ "</td>"
						+ "    <td> " + ticket._tk_seat+ "</td>"
						+ "    <td> " + ticket._tk_payment+ "</td>"
						+ "</tr>";
			}
			try {
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(sender));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(cus._cus_email));
				message.setSubject("Return ticket");

				message.setContent("<div>"
							  + "	<div>Hi " + cus._cus_full_name + " </div>"
							  + "	<div>Your return ticket(s)</div>"
							  + "	<table>"
							  + "		<thead>"
							  + "			<tr>"
							  + "				<th>Full Name</th>"
							  + "				<th>Seat</th>"
							  + "				<th>Payment</th>"
							  + "			</tr>"
							  + "		</thead>"
							  + "		<tbody>"
							  + 			body
							  + "		</tbody>"
							  + "	</table>"
							  + "</div>", "text/html");

				Transport.send(message);
				
			} catch (MessagingException e) {
				e.printStackTrace();
			}
		}
		return isSuccess;
	}
    
    @RequestMapping(value = "api/mail/eticket", method = RequestMethod.POST)
    public boolean Eticket(@Validated @RequestBody CustomerDTO registration) {
         boolean result = true;
         
         Properties prop = new Properties();
 		 prop.put("mail.smtp.host", smtp);
         prop.put("mail.smtp.port", port);
         prop.put("mail.smtp.auth", "true");
         prop.put("mail.smtp.starttls.enable", "true"); 
         
         Session session = Session.getInstance(prop,
                 new javax.mail.Authenticator() {
                     protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                         return new javax.mail.PasswordAuthentication(username, password);
                     }
                 });

         try {
        	 String token = UUID.randomUUID().toString();
        	 String eTicketLink = "http://localhost:8080/api/eticket?token=" + registration._cus_active_token + registration._cus_id;
        	 String emailContent = "<html><body>" +
			                       "<p>Congratulations! Your booking is successful.</p>" +
			                       "<p>Please click on the following link to view your E-ticket:</p>" +
			                       "<a href='" + eTicketLink + "'>Click here to view</a>" +
			                       "</body></html>";
        	 
             Message message = new MimeMessage(session);
             message.setFrom(new InternetAddress(sender));
             message.setRecipients(
                     Message.RecipientType.TO,
                     InternetAddress.parse(sender)
             );
             message.setSubject("Thank you for your booking");
             message.setContent(emailContent, "text/html");

             Transport.send(message);

             System.out.println("Done");
         } catch (MessagingException e) {
             e.printStackTrace();
             result = false;
         }
         
         return result;
    }
    
    public static boolean SendEticket(CustomerDTO registration) {
        boolean result = true;
        
        Properties prop = new Properties();
		 prop.put("mail.smtp.host", smtp);
        prop.put("mail.smtp.port", port);
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true"); 
        
        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                        return new javax.mail.PasswordAuthentication(username, password);
                    }
                });

        try {
       	 String token = UUID.randomUUID().toString();
       	 String eTicketLink = "http://localhost:8080/api/eticket?token=" + registration._cus_active_token + registration._cus_id;
       	 String emailContent = "<html><body>" +
			                       "<p>Congratulations! Your booking is successful.</p>" +
			                       "<p>Please click on the following link to view your E-ticket:</p>" +
			                       "<a href='" + eTicketLink + "'>Click here to view</a>" +
			                       "</body></html>";
       	 
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(sender));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(sender)
            );
            message.setSubject("Thank you for your booking");
            message.setContent(emailContent, "text/html");

            Transport.send(message);

            System.out.println("Done");
        } catch (MessagingException e) {
            e.printStackTrace();
            result = false;
        }
        
        return result;
   }
}
