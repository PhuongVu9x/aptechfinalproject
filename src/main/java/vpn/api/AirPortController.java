package vpn.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vpn.AirportThread;
import vpn.model.Airport;
import vpn.model.AirportDTO;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.Result;

@RestController
public class AirPortController {
	Result<AirportDTO> insert(@Validated @RequestBody Airport airport) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(airport._ap_name);
		params.add(airport._ap_icon);
		params.add(airport._ap_backgound);
		params.add(airport._ap_city_id+"");
		params.add(airport._ap_created_date+"");
		
		var rs = DBConnection.CallProc("Sp_Airport_Insert", params);
		var data = Mapper.MapTo(new AirportDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Create Airport Fail!";
		var result = new Result<AirportDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	@RequestMapping(value="api/airport/gets",method = RequestMethod.GET)
	Result<LinkedList<AirportDTO>> gets() throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add("");
		params.add("");
		params.add("1");
		var rsAirport = DBConnection.CallProc("Sp_Airport_Gets",params);
		var dataAirport = Mapper.MapTo(new AirportDTO(), rsAirport);
		var isSuccessAirport = !dataAirport.isEmpty();
		var mesAirport = isSuccessAirport ? "" : "No Data Found.";
		var result = new Result<LinkedList<AirportDTO>>(dataAirport, isSuccessAirport, mesAirport);
		return result;
	}
	Result<AirportDTO> info(@Validated @RequestBody Airport airport) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(airport._ap_id+"");
		var rs = DBConnection.CallProc("Sp_Airport_Info", params);
		var data = Mapper.MapTo(new AirportDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found!";
		var result = new Result<AirportDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<AirportDTO> updateIcon(@Validated @RequestBody Airport airport) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(airport._ap_id+"");
		params.add(airport._ap_icon);
		var rs = DBConnection.CallProc("Sp_Airport_UpdateIcon", params);
		var data = Mapper.MapTo(new AirportDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Airport Icon Fail";
		var result = new Result<AirportDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<AirportDTO> updateBackground(@Validated @RequestBody Airport airport) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(airport._ap_id+"");
		params.add(airport._ap_backgound);
		var rs = DBConnection.CallProc("Sp_Airport_UpdateBackground", params);
		var data = Mapper.MapTo(new AirportDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Airport Background Fail";
		var result = new Result<AirportDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<AirportDTO> update(@Validated @RequestBody Airport airport) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(airport._ap_id+"");
		params.add(airport._ap_name);
		params.add(airport._ap_city_id+"");
		params.add(airport._ap_updated_date+"");
		var rs = DBConnection.CallProc("Sp_Airport_Update", params);
		var data = Mapper.MapTo(new AirportDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Airport Fail!";
		var result = new Result<AirportDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	@RequestMapping(value="api/airport/changestatus",method = RequestMethod.POST)
	Result<AirportDTO> changeStatus(@Validated @RequestBody Airport airport) 
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {

		var params = new LinkedList<String>();
		params.add(airport._ap_id+"");
		params.add(airport._ap_status+"");
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_Airport_ChangeStatus", params);
		var data = Mapper.MapTo(new AirportDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "Airport Change Status Fail!";
		var airportThread = new AirportThread();
		airportThread.start();
		airportThread.join();
		var result = new Result<AirportDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
}
