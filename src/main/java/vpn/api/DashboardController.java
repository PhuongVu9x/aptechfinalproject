package vpn.api;

import java.util.LinkedList;
import java.util.UUID;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vpn.BillThread;
import vpn.BookingThread;
import vpn.model.BillDTO;
import vpn.model.BookingDTO;
import vpn.model.BookingDetail;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.Result;

@RestController
public class DashboardController {
	@RequestMapping(value = "api/dashboard/bills", method = RequestMethod.GET)
	Result<LinkedList<BillDTO>> bills()
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var isSuccess = !BillThread.bills.isEmpty();
		var mes = isSuccess ? "" : "No Data Found.";
		var result = new Result<LinkedList<BillDTO>>(BillThread.bills, isSuccess, mes);
		return result;
	}
	
	@RequestMapping(value = "api/dashboard/bookings", method = RequestMethod.GET)
	Result<LinkedList<BookingDTO>> bookings()
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var isSuccess = !BookingThread.bookings.isEmpty();
		var mes = isSuccess ? "" : "No Data Found.";
		var result = new Result<LinkedList<BookingDTO>>(BookingThread.bookings, isSuccess, mes);
		return result;
	}
}
