package vpn.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vpn.CarrierThread;
import vpn.model.Carrier;
import vpn.model.CarrierDTO;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.Result;
@RestController
public class CarrierController {
	Result<CarrierDTO> insert(@Validated @RequestBody Carrier Carrier) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Carrier._car_name);
		params.add(Carrier._car_icon);
		params.add(Carrier._car_created_date+"");
		
		var rs = DBConnection.CallProc("Sp_Carrier_Insert", params);
		var data = Mapper.MapTo(new CarrierDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Create Carrier Fail!";
		var result = new Result<CarrierDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<CarrierDTO> gets(@Validated @RequestBody CarrierDTO Carrier) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Carrier._car_name);
		var rs = DBConnection.CallProc("Sp_Carrier_Gets", params);
		var data = Mapper.MapTo(new CarrierDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<CarrierDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	/*Result<CarrierDTO> info(@Validated @RequestBody Carrier Carrier) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Carrier._car_id+"");
		var rs = DBConnection.CallProc("Sp_Carrier_Info", params);
		var data = Mapper.MapTo(new CarrierDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found!";
		var result = new Result<CarrierDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}*/
	Result<CarrierDTO> updateIcon(@Validated @RequestBody Carrier Carrier) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Carrier._car_id+"");
		params.add(Carrier._car_icon);
		var rs = DBConnection.CallProc("Sp_Carrier_UpdateIcon", params);
		var data = Mapper.MapTo(new CarrierDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Carrier Icon Fail";
		var result = new Result<CarrierDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<CarrierDTO> update(@Validated @RequestBody Carrier Carrier) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Carrier._car_id+"");
		params.add(Carrier._car_name);
		params.add(Carrier._car_updated_date+"");
		var rs = DBConnection.CallProc("Sp_Carrier_Update", params);
		var data = Mapper.MapTo(new CarrierDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Carrier Fail!";
		var result = new Result<CarrierDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	@RequestMapping(value="api/carrier/changestatus",method = RequestMethod.POST)
	Result<CarrierDTO> changeStatus(@Validated @RequestBody Carrier Carrier) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		var params = new LinkedList<String>();
		params.add(Carrier._car_id+"");
		params.add(Carrier._car_status+"");
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_Carrier_ChangeStatus", params);
		var data = Mapper.MapTo(new CarrierDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "Carrier Change Status Fail!";
		var carrierThread = new CarrierThread();
		carrierThread.start();
		carrierThread.join();
		var result = new Result<CarrierDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
}
