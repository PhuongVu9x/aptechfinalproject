package vpn.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vpn.CountryThread;
import vpn.model.Country;
import vpn.model.CountryDTO;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.Result;
@RestController
public class CountryController {
	Result<CountryDTO> insert(@Validated @RequestBody Country Country) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Country._ct_name);
		params.add(Country._ct_icon);
		params.add(Country._ct_created_date+"");
		
		var rs = DBConnection.CallProc("Sp_Country_Insert", params);
		var data = Mapper.MapTo(new CountryDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Create Country Fail!";
		var result = new Result<CountryDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<CountryDTO> gets(@Validated @RequestBody CountryDTO Country) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Country._ct_name);
		var rs = DBConnection.CallProc("Sp_Country_Gets", params);
		var data = Mapper.MapTo(new CountryDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<CountryDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	/*Result<CountryDTO> info(@Validated @RequestBody Country Country) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Country._ct_id+"");
		var rs = DBConnection.CallProc("Sp_ct_Info", params);
		var data = Mapper.MapTo(new CountryDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found!";
		var result = new Result<CountryDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}*/

	Result<CountryDTO> update(@Validated @RequestBody Country Country) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Country._ct_id+"");
		params.add(Country._ct_name);
		params.add(Country._ct_updated_date+"");
		var rs = DBConnection.CallProc("Sp_Country_Update", params);
		var data = Mapper.MapTo(new CountryDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Country Fail!";
		var result = new Result<CountryDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	@RequestMapping(value="api/country/changestatus",method = RequestMethod.POST)
	Result<CountryDTO> changeStatus(@Validated @RequestBody Country Country) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		var params = new LinkedList<String>();
		params.add(Country._ct_id+"");
		params.add(Country._ct_status+"");
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_Country_ChangeStatus", params);
		var data = Mapper.MapTo(new CountryDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "Country Change Status Fail!";
		var countryThread = new CountryThread();
		countryThread.start();
		countryThread.join();
		var result = new Result<CountryDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
}
