package vpn.api;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vpn.model.Flight;
import vpn.model.FlightDTO;
import vpn.AppThread;
import vpn.ArrivalFlightThread;
import vpn.FlightThread;
import vpn.TakeOffFlight;
import vpn.model.Bill;
import vpn.model.BillDTO;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.Result;
import vpn.model.SeatDTO;
import vpn.model.TicketDTO;

@RestController
public class FlightController {
	@RequestMapping(value="api/flight/insert",method = RequestMethod.POST)
	Result<FlightDTO> insert(@Validated @RequestBody FlightDTO flight)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		var params = new LinkedList<String>();
		params.add(flight._fl_plane_id + "");
		params.add(flight._fl_from_id + "");
		params.add(flight._fl_to_id + "");
		params.add(flight._fl_ec_price + "");
		params.add(flight._fl_sc_price + "");
		params.add(flight._fl_bs_price + "");
		params.add(flight._fl_fc_price + "");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(flight._fl_take_off_local));
		params.add(dtf.format(flight._fl_arrival_local));
		params.add(flight._fl_services);
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_Flight_Insert", params);
		var data = Mapper.MapTo(new FlightDTO(), rs);
		var isSuccess = !data.isEmpty();
		var flightThread = new FlightThread();
		flightThread.start();
		flightThread.join();
		var mes = isSuccess ? "Create Flight Successfull." : "Create Flight Fail!";
		var result = new Result<FlightDTO>(isSuccess ? data.getFirst() : null, isSuccess, mes);
		return result;
	}

	//auto services
	@RequestMapping(value="api/flight/updatestatus",method = RequestMethod.GET)
	Result<String> updateStatus() throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		try {
			var rsFlights = DBConnection.CallProc("Sp_Flight_TodayGets");
			var flights = Mapper.MapTo(new FlightDTO(), rsFlights);
			var takeOffFlight = new LinkedList<FlightDTO>();
			var arrivalFlight = new LinkedList<FlightDTO>();
			var leftFlight = new LinkedList<FlightDTO>();
			LocalDateTime now = LocalDateTime.now();  
			Timestamp timestamp = Timestamp.valueOf(now);
			ArrivalFlightThread.flights.clear();
			TakeOffFlight.flights.clear();
			
			for (var flight : flights) {
				if(flight._fl_arrival.compareTo(timestamp) <= 0) {
					ArrivalFlightThread.flights.add(flight);
				}
				else if(flight._fl_take_off.compareTo(timestamp) < 0) {
					TakeOffFlight.flights.add(flight);
				}else {
					leftFlight.add(flight);
				}
			}
			FlightThread.todayFlights.removeAll(takeOffFlight);
			
			var takeOffFlightThread = new TakeOffFlight();
			takeOffFlightThread.start();
			
			var arrivalFlightThread = new ArrivalFlightThread();
			arrivalFlightThread.start();
			
			var flightThread = new FlightThread();
			flightThread.start();
			flightThread.join();
			var result = new Result<String>("qwe",true,"Flight Service Done");
			return result;
		}catch(Exception e){
			var result = new Result<String>("qwe",false,"Flight Service Fail -- " + e);
			return result;

		}
	}
	
	@RequestMapping(value="api/flight/getbyplane",method = RequestMethod.GET)
	Result<LinkedList<FlightDTO>> getbyplane(int _pl_id) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(_pl_id+"");
		var rs = DBConnection.CallProc("Sp_Flight_GetByPlane", params);
		var data = Mapper.MapTo(new FlightDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<LinkedList<FlightDTO>>(data,isSuccess,mes);
		return result;
	}
 
	@RequestMapping(value = "api/flight/gets", method = RequestMethod.GET)
	Result<LinkedList<FlightDTO>> gets(FlightDTO Flight,int _fl_from_id,int _fl_to_id,int _tc_id,int _pas_quantity,
			String _fl_take_off, String _fl_return_date)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(_fl_from_id + "");
		params.add(_fl_to_id + "");
		params.add(Flight._car_id + "");
		params.add(_fl_take_off == null ? " " : _fl_take_off);
		params.add(_tc_id + "");
		params.add(_pas_quantity + "");
		var rs = DBConnection.CallProc("Sp_Flight_GetsForCustomer", params);
		var data = Mapper.MapTo(new FlightDTO(), rs);
		var resData = FillJson(data,_tc_id);
		var isSuccess = !resData.isEmpty();
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<LinkedList<FlightDTO>>(resData, isSuccess, mes);
		if (_fl_return_date != null) {
			var paramsReturn = new LinkedList<String>();
			paramsReturn.add(Flight._fl_to_id + "");
			paramsReturn.add(Flight._fl_from_id + "");
			paramsReturn.add(Flight._car_id + "");
			paramsReturn.add(_fl_return_date == null ? " " : _fl_return_date);
			paramsReturn.add(Flight._tc_id + "");
			paramsReturn.add(Flight._pas_quantity + "");
			var rsReturn = DBConnection.CallProc("Sp_Flight_Gets", params);
			var dataReturn = Mapper.MapTo(new FlightDTO(), rsReturn);
			var resDataReturn = FillJson(dataReturn,_tc_id);
			
			var isSuccessReturn = !resDataReturn.isEmpty();
			if (isSuccessReturn) {
				result.success = new LinkedList<FlightDTO>();
				result.success.addAll(resDataReturn);
			}
		}
 
		return result;
	}
	
	@RequestMapping(value = "api/flight/getsall", method = RequestMethod.GET)
	Result<LinkedList<FlightDTO>> getsall()
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		
		var rsFlight = DBConnection.CallProc("Sp_Flight_Gets");
		var flights = Mapper.MapTo(new FlightDTO(), rsFlight);
		var isSuccessFlight = !flights.isEmpty();
		var mesFlight = isSuccessFlight ? "" : "No Data Found.";
		var resultFlight = new Result<LinkedList<FlightDTO>>(flights, isSuccessFlight, mesFlight);
		return resultFlight;
	}
	
	private LinkedList<FlightDTO> FillJson(LinkedList<FlightDTO> data,int _tc_id){
		var dataReturn = new LinkedList<FlightDTO>();
		for (FlightDTO flightDTO : data) {
			flightDTO._fl_estimate_take_off = AppThread.AddHours(flightDTO._fl_estimate_take_off,7);
			flightDTO._fl_estimate_arrival = AppThread.AddHours(flightDTO._fl_estimate_arrival,7);
			flightDTO._fl_take_off = AppThread.AddHours(flightDTO._fl_take_off,7);
			flightDTO._fl_arrival = AppThread.AddHours(flightDTO._fl_arrival,7);
			flightDTO._fl_created_date = AppThread.AddHours(flightDTO._fl_created_date,7);
			flightDTO._fl_updated_date = AppThread.AddHours(flightDTO._fl_updated_date,7);
			switch(_tc_id) {
			case 1:
				flightDTO._sell_price = flightDTO._fl_ec_price;
				break;
			case 2:
				flightDTO._sell_price = flightDTO._fl_sc_price;
				break;
			case 3:
				flightDTO._sell_price = flightDTO._fl_bs_price;
				break;
			case 4:
				flightDTO._sell_price = flightDTO._fl_fc_price;
				break;
			}
			
			dataReturn.add(flightDTO);
		}
		
		return dataReturn;
	}

	@RequestMapping(value = "api/flight/info", method = RequestMethod.GET)
	Result<FlightDTO> info(int _fl_id,int _tc_id)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();

		params.add(_fl_id + "");
		var rs = DBConnection.CallProc("Sp_Flight_Info", params);
		var data = Mapper.MapTo(new FlightDTO(), rs);
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "No data found(s)!";
		var rsData = isSuccess ? data.getFirst() : null;
		if (isSuccess) {
			System.out.println(_tc_id);
			params.add(_tc_id + "");
			var rsTicket = DBConnection.CallProc("Sp_Ticket_GetsByFlight", params);
			var dataTicket = Mapper.MapTo(new TicketDTO(), rsTicket);
			var index = 1;
			var rows = 0;
			var lastCol = 'A';
			var firstCol = 'A';
			rsData._fl_seats = new LinkedList<SeatDTO>();
			switch(_tc_id) {
			case 1:
				// economy seat
				rows = rsData._pt_ec_quantity / 6  + 1;
				if(rows > 0) {
					for (int i = 1; i <= rows; i++) {
						for (char j = 'A'; j <= 'F'; j++) {
							var code = j + "" + index + "";
							var _class = "Economic";
							var status = CheckSeat(dataTicket, code);
							var seat = new SeatDTO(code, _tc_id, _class, status);
							rsData._fl_seats.add(seat);
						}
						index++;
					}
				}else {
					lastCol =  (char)(firstCol + rsData._pt_ec_quantity);
					for (char j = firstCol; j < lastCol; j++) {
						var code = j + "" + index;
						var _class = "Economic";
						var status = CheckSeat(dataTicket, code);
						var seat = new SeatDTO(code, _tc_id, _class, status);
						rsData._fl_seats.add(seat);
					}
				}
				break;
			case 2:
				// special economy seat
				rows = rsData._pt_sc_quantity / 6  + 1;
				if(rows > 0) {
					for (int i = 1; i <= rows; i++) {
						for (char j = 'A'; j <= 'F'; j++) {
							var code = j + "" + index + "";
							var _class = "Special Economic";
							var status = CheckSeat(dataTicket, code);
							var seat = new SeatDTO(code, _tc_id, _class, status);
							rsData._fl_seats.add(seat);
						}
						index++;
					}
				}else {
					lastCol =  (char)(firstCol + rsData._pt_sc_quantity);
					for (char j = firstCol; j < lastCol; j++) {
						var code = j + "" + index;
						var _class = "Special Economic";
						var status = CheckSeat(dataTicket, code);
						var seat = new SeatDTO(code, _tc_id, _class, status);
						rsData._fl_seats.add(seat);
					}
				}
				break;
			case 3:
				// business seat
				rows = rsData._pt_bs_quantity / 6;
				if(rows > 0) {
					for (int i = 1; i <= rows; i++) {
						for (char j = 'A'; j <= 'F'; j++) {
							var code = j + "" + index + "";
							var _class = "Business";
							var status = CheckSeat(dataTicket, code);
							var seat = new SeatDTO(code, _tc_id, _class, status);
							rsData._fl_seats.add(seat);
						}
						index++;
					}
				}else {
					lastCol =  (char)(firstCol + rsData._pt_bs_quantity);
					for (char j = firstCol; j < lastCol; j++) {
						var code = j + "" + index;
						var _class = "Business";
						var status = CheckSeat(dataTicket, code);
						var seat = new SeatDTO(code, _tc_id, _class, status);
						rsData._fl_seats.add(seat);
					}
				}
				
				
				break;
			case 4:
				// business seat
				rows = rsData._pt_fc_quantity / 6;
				if(rows > 0) {
					
				}else {
					lastCol =  (char)(firstCol + rsData._pt_fc_quantity);
					for (char j = firstCol; j < lastCol; j++) {
						var code = j + "" + index;
						var _class = "First Class";
						var status = CheckSeat(dataTicket, code);
						var seat = new SeatDTO(code, _tc_id, _class, status);
						rsData._fl_seats.add(seat);
					}
				}
				
				break;
			}
		}
		var result = new Result<FlightDTO>(rsData, isSuccess, mes);
		return result;
	}

	boolean CheckSeat(LinkedList<TicketDTO> tickets, String seat) {
		var check = false;
		for (TicketDTO ticket : tickets) {
			if (ticket._tk_seat.equals(seat)) {
				check = true;
				break;
			}
		}

		return check;
	}
	@RequestMapping(value = "api/flight/infor", method = RequestMethod.GET)
	Result<FlightDTO> infor(int _fl_id)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(_fl_id + "");
		var rs = DBConnection.CallProc("Sp_Flight_Info", params);
		var data = Mapper.MapTo(new FlightDTO(), rs);
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "No data found!";
		var result = new Result<FlightDTO>(isSuccess ? data.getFirst() : null, isSuccess, mes);
		return result;
	}

	@RequestMapping(value = "api/flight/update", method = RequestMethod.POST)
	Result<FlightDTO> update(@Validated @RequestBody FlightDTO flight)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		var params = new LinkedList<String>();
		params.add(flight._fl_id + "");
		params.add(flight._fl_plane_id + "");
		params.add(flight._fl_from_id + "");
		params.add(flight._fl_to_id + "");
		params.add(flight._fl_ec_price + "");
		params.add(flight._fl_sc_price + "");
		params.add(flight._fl_bs_price + "");
		params.add(flight._fl_fc_price + "");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(flight._fl_take_off_local));
		params.add(dtf.format(flight._fl_arrival_local));
		params.add(flight._fl_services);
		params.add(dtf.format(now)); 
		var rs = DBConnection.CallProc("Sp_Flight_Update", params);
		var data = Mapper.MapTo(new FlightDTO(), rs);
		var isSuccess = !data.isEmpty();
		var flightThread = new FlightThread();
		flightThread.start();
		flightThread.join();
		var mes = isSuccess ? "Update Flight Successfull." : "Update Flight Fail!";
		var result = new Result<FlightDTO>(isSuccess ? data.getFirst() : null, isSuccess, mes);
		return result;
	}
	@RequestMapping(value = "api/flight/changestatus", method = RequestMethod.POST)
	Result<FlightDTO> changeStatus(@Validated @RequestBody Flight flight)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		var params = new LinkedList<String>();
		params.add(flight._fl_id + "");
		params.add(flight._fl_status + "");
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_Flight_ChangeStatus", params);
		var data = Mapper.MapTo(new FlightDTO(), rs);
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "Flight Change Status Success!" : "Flight Change Status Fail!";
		var flightThread = new FlightThread();
		flightThread.start();
		flightThread.join();
		var result = new Result<FlightDTO>(isSuccess ? data.getFirst() : null, isSuccess, mes);
		return result;
	}
}
