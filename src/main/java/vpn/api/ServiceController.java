package vpn.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vpn.ServiceThread;
import vpn.model.DBConnection;
import vpn.model.ServiceDTO;
import vpn.model.Mapper;
import vpn.model.Result;

@RestController
public class ServiceController {
	@RequestMapping(value = "api/service/changestatus", method = RequestMethod.POST)
	Result<ServiceDTO> changeStatus(@Validated @RequestBody ServiceDTO service)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		System.out.println(service._ser_id);
		System.out.println(service._ser_status);
		var params = new LinkedList<String>();
		params.add(service._ser_id+"");
		params.add(service._ser_status+"");
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_Service_ChangeStatus", params);
		var data = Mapper.MapTo(new ServiceDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "Service Change Status Fail!";
		var thread = new ServiceThread();
		thread.start();
		thread.join();
		var result = new Result<ServiceDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
}
