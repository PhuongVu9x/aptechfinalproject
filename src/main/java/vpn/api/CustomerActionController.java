package vpn.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.UUID;

import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import vpn.CustomerThread;
import vpn.model.AdminDTO;
import vpn.model.Customer;
import vpn.model.CustomerDTO;
import vpn.model.DBConnection;
import vpn.model.Decrypt;
import vpn.model.Mapper;
import vpn.model.Result;
import vpn.model.UserDTO;

@RestController
public class CustomerActionController {
	@RequestMapping(value="/api/customer/register",method = RequestMethod.POST)
	Result<CustomerDTO> insert(@Validated @RequestBody Customer customer) 
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var paramsCheck = new LinkedList<String>();
		paramsCheck.add(customer._cus_email);
		var rs = DBConnection.CallProc("Sp_Customer_Check", paramsCheck);
		var data = Mapper.MapTo(new CustomerDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "Email already Exsisted" : "";
		var result = new Result<CustomerDTO>(null,isSuccess,mes);
		if(isSuccess) {
			return result;
		}
		
		var params = new LinkedList<String>();
		params.add(customer._cus_email);
		params.add(Decrypt.ToMd5(customer._cus_password));
		params.add(customer._cus_first_name);
		params.add(customer._cus_last_name);
		params.add(customer._cus_phone);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));
		
		rs = DBConnection.CallProc("Sp_Customer_Insert", params);
		data = Mapper.MapTo(new CustomerDTO(), rs); 
		isSuccess = !data.isEmpty();
		mes = isSuccess ? "" : "Create Customer Fail!";
		result = new Result<CustomerDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	@RequestMapping(value="api/customer/login",method = RequestMethod.POST)
	Result<CustomerDTO> login(@Validated @RequestBody Customer customer) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		String token = UUID.randomUUID().toString();
		var params = new LinkedList<String>();
		params.add(customer._cus_email);
		params.add(Decrypt.ToMd5(customer._cus_password));
		/*params.add(token); 
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));*/
		var rs = DBConnection.CallProc("Sp_Customer_Login", params);
		var data = Mapper.MapTo(new CustomerDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "Wrong Email or Password.Please check again";
		var dataRs = isSuccess ? data.getFirst() : null;
		if(isSuccess)
			dataRs._cus_password = Decrypt.FromMd5(dataRs._cus_password);
		var result = new Result<CustomerDTO>(dataRs,isSuccess,mes);
		return result;
	}
	@RequestMapping(value="api/customer/logout",method = RequestMethod.POST)
	Result<CustomerDTO> logout(@Validated @RequestBody Customer customer) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(customer._cus_id+"");
		params.add(customer._cus_token);
		var rs = DBConnection.CallProc("Sp_Customer_Logout", params);
		var data = Mapper.MapTo(new CustomerDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "Logout Fail. Something wrong in Server!";
		var result = new Result<CustomerDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<CustomerDTO> gets(@Validated @RequestBody Customer Customer) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Customer._cus_first_name);
		params.add(Customer._cus_last_name);
		params.add(Customer._cus_full_name);
		params.add(Customer._cus_email);
		params.add(Customer._cus_dob+"");
		params.add(Customer._cus_phone);
		
		var rs = DBConnection.CallProc("Sp_Customer_Gets", params);
		var data = Mapper.MapTo(new CustomerDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<CustomerDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<CustomerDTO> info(@Validated @RequestBody Customer Customer) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Customer._cus_id+"");
		var rs = DBConnection.CallProc("Sp_Customer_Info", params);
		var data = Mapper.MapTo(new CustomerDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found!";
		var result = new Result<CustomerDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<CustomerDTO> updateAvatar(@Validated @RequestBody Customer Customer) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Customer._cus_id+"");
		params.add(Customer._cus_avatar);
		var rs = DBConnection.CallProc("Sp_Customer_UpdateAvatar", params);
		var data = Mapper.MapTo(new CustomerDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Avatar Fail";
		var result = new Result<CustomerDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	@RequestMapping(value="api/customer/update",method = RequestMethod.POST)
	Result<CustomerDTO> update(@Validated @RequestBody Customer customer) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(customer._cus_id+"");
		params.add(customer._cus_first_name);
		params.add(customer._cus_last_name);
		params.add(customer._cus_first_name + " " + customer._cus_last_name);
		params.add(customer._cus_email);
		params.add(customer._cus_dob);
		params.add(customer._cus_phone);
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_Customer_Update", params);
		var data = Mapper.MapTo(new CustomerDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "Customer Update Fail!";
		var result = new Result<CustomerDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<CustomerDTO> changeStatus(@Validated @RequestBody Customer Customer) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Customer._cus_id+"");
		params.add(Customer._cus_status+"");
		var rs = DBConnection.CallProc("Sp_Customer_ChangeStatus", params);
		var data = Mapper.MapTo(new CustomerDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Customer Change Status Fail!";
		var result = new Result<CustomerDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<CustomerDTO> changePassword(@Validated @RequestBody Customer Customer,String _cus_new_password) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Customer._cus_email);
		params.add(Customer._cus_password);
		params.add(_cus_new_password);
		var rs = DBConnection.CallProc("Sp_Customer_ChangePassword", params);
		var data = Mapper.MapTo(new CustomerDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Customer Change Password Fail!";
		var customerThread = new CustomerThread();
		customerThread.start();
		var result = new Result<CustomerDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	
	@RequestMapping(value = "api/customer/getbyid", method = RequestMethod.GET)
	Result<CustomerDTO> getbyid(@Validated int _cus_id)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		CustomerDTO customer = null;
		for(var item : CustomerThread.customers) {
			if(item._cus_id == _cus_id) {
				customer = item;
			}
		}
		var isSuccess = customer != null;
		var mes = isSuccess ? "" : "Data not found.";
		var result = new Result<CustomerDTO>(customer, isSuccess, mes);
		return result;
	}
	
	/* -----------------------------------------------------------------------------------
	   ----------------------------- USER REGISTRATION -----------------------------------
	   -----------------------------------------------------------------------------------*/
	@RequestMapping(value="api/user/login",method = RequestMethod.POST)
	public Result<CustomerDTO> Login(@Validated @RequestBody CustomerDTO registration) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var result =  new Result<CustomerDTO>();
		var params = new LinkedList<String>();
		
		params.add(registration._cus_email);
		params.add(Decrypt.ToMd5(registration._cus_password));
		
		var rs = DBConnection.CallProc("Sp_Customer_Login", params);
		var data = Mapper.MapTo(new CustomerDTO(), rs);
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Wrong Email or Password.Please check again";
		result = new Result<CustomerDTO>(isSuccess ? data.getFirst() : null, isSuccess, mes);
		
		return result;
	}
	
	@RequestMapping(value="api/user/registration",method = RequestMethod.POST)
	public Result<CustomerDTO> Register(@Validated @RequestBody CustomerDTO registration) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var result =  new Result<CustomerDTO>();
		var params = new LinkedList<String>();
		
		//Check email existed
		params.add(registration._cus_email);
		
		var rs = DBConnection.CallProc("Sp_Check_Email", params);
		var data = Mapper.MapTo(new CustomerDTO(), rs);
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "Email existed" : "";
		
		//Insert email
		if(!isSuccess) {
			String token = UUID.randomUUID().toString();
			LocalDateTime now = LocalDateTime.now();
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			
			var params1 = new LinkedList<String>();
			params1.add(registration._cus_email);
			params1.add(Decrypt.ToMd5(registration._cus_password));
			params1.add(token);
			params1.add(dtf.format(now));
			
			var rs1 = DBConnection.CallProc("Sp_Customer_Insert_Web", params1);
			var data1 = Mapper.MapTo(new CustomerDTO(), rs1);
			var isSuccess1 = !data1.isEmpty() ? true : false;
			var mes1 = isSuccess1 ? "Insert successfully" : "Insert failed";
			result = new Result<CustomerDTO>(isSuccess1 ? data1.getFirst() : null, isSuccess1, mes1);
		}
		return result;
	}
	
	@RequestMapping(value="api/account/activate",method = RequestMethod.GET)
	public ModelAndView ActivateAccount(@Validated @RequestParam String token) throws IllegalArgumentException, IllegalAccessException, InstantiationException, ClassNotFoundException {
		System.out.println(token);
		//String _cus_id = token.substring(token.length() - 2, token.length());
		String _cus_id = token.substring(token.length() - 1, token.length());
		
		var result =  new Result<CustomerDTO>();
		var params = new LinkedList<String>();
		
		params.add(_cus_id);
		params.add(token.substring(token.length() - 2));
		
		var rs = DBConnection.CallProc("Sp_Change_Active_Stt", params);
		var data = Mapper.MapTo(new CustomerDTO(), rs);
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "Update successfully" : "Update Failed";
		result = new Result<CustomerDTO>(isSuccess ? data.getFirst() : null, isSuccess, mes);
		
		if (!isSuccess) {
	        // Redirect to the success URL
	        return new ModelAndView(new RedirectView("http://localhost:8080/register-notification"));
	    } else {
	        // Redirect to the error page
	        return new ModelAndView(new RedirectView("http://localhost:8080/error"));
	    }
	}
	
	@RequestMapping(value="api/profile/changepassword",method = RequestMethod.POST)
	Result<CustomerDTO> ChangeCusPassword(String _cus_email, String _cus_password, String _cus_new_password) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(_cus_email);
		params.add(_cus_password);
		params.add(Decrypt.ToMd5(_cus_new_password));
		
		var rs = DBConnection.CallProc("Sp_Customer_ChangePassword", params);
		var data = Mapper.MapTo(new CustomerDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Customer Change Password Fail!";
		
		var customerThread = new CustomerThread();
		customerThread.start();
		
		var result = new Result<CustomerDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	
	@RequestMapping(value="api/eticket",method = RequestMethod.GET)
	public ModelAndView Eticket(@Validated @RequestParam String token) throws IllegalArgumentException, IllegalAccessException, InstantiationException, ClassNotFoundException {
		// Redirect to the eticket URL
        return new ModelAndView(new RedirectView("http://localhost:8080/eticket"));
	}
}
