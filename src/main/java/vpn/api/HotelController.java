package vpn.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vpn.model.Hotel;
import vpn.model.HotelDTO;
import vpn.HotelThread;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.Result;
@RestController
public class HotelController {
	Result<HotelDTO> insert(@Validated @RequestBody Hotel Hotel) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Hotel._ho_name);
		params.add(Hotel._ho_city_id+"");
		params.add(Hotel._ho_rooms+"");
		params.add(Hotel._ho_signle_rooms+"");
		params.add(Hotel._ho_double_rooms+"");
		params.add(Hotel._ho_triple_rooms+"");
		params.add(Hotel._ho_stars+"");
		params.add(Hotel._ho_address);
		params.add(Hotel._ho_from_price+"");
		params.add(Hotel._ho_to_price+"");
		params.add(Hotel._ho_icon);
		params.add(Hotel._ho_background);
		params.add(Hotel._ho_created_date+"");
		
		var rs = DBConnection.CallProc("Sp_Hotel_Insert", params);
		var data = Mapper.MapTo(new HotelDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Create Hotel Fail!";
		var result = new Result<HotelDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	@RequestMapping(value="api/hotel/gets",method = RequestMethod.GET)
	Result<LinkedList<HotelDTO>> gets() throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var isSuccess = !HotelThread.hotels.isEmpty();
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<LinkedList<HotelDTO>>(HotelThread.hotels,isSuccess,mes);
		return result;
	}
	Result<HotelDTO> info(@Validated @RequestBody Hotel Hotel) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Hotel._ho_id+"");
		var rs = DBConnection.CallProc("Sp_Hotel_Info", params);
		var data = Mapper.MapTo(new HotelDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found!";
		var result = new Result<HotelDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<HotelDTO> updateIcon(@Validated @RequestBody Hotel Hotel) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Hotel._ho_id+"");
		params.add(Hotel._ho_icon);
		var rs = DBConnection.CallProc("Sp_Hotel_UpdateIcon", params);
		var data = Mapper.MapTo(new HotelDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Hotel Icon Fail";
		var result = new Result<HotelDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<HotelDTO> updateBackground(@Validated @RequestBody Hotel Hotel) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Hotel._ho_id+"");
		params.add(Hotel._ho_background);
		var rs = DBConnection.CallProc("Sp_Hotel_UpdateBackground", params);
		var data = Mapper.MapTo(new HotelDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Hotel Background Fail";
		var result = new Result<HotelDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<HotelDTO> update(@Validated @RequestBody Hotel Hotel) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Hotel._ho_id+"");
		params.add(Hotel._ho_name);
		params.add(Hotel._ho_updated_date+"");
		var rs = DBConnection.CallProc("Sp_Hotel_Update", params);
		var data = Mapper.MapTo(new HotelDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Hotel Fail!";
		var result = new Result<HotelDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	@RequestMapping(value="api/hotel/changestatus",method = RequestMethod.POST)
	Result<HotelDTO> changeStatus(@Validated @RequestBody Hotel hotel) 
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		var params = new LinkedList<String>();
		params.add(hotel._ho_id+"");
		params.add(hotel._ho_status+"");
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_Hotel_ChangeStatus", params);
		var data = Mapper.MapTo(new HotelDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "Hotel Change Status Fail!";
		var hotelThread = new HotelThread();
		hotelThread.start();
		hotelThread.join();
		var result = new Result<HotelDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
}
