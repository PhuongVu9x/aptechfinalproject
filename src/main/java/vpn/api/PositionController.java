package vpn.api;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import vpn.model.Position;
import vpn.model.PositionDTO;
import vpn.PositionThread;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.Result;
@RestController
public class PositionController {
	
	@RequestMapping(value="api/position/insert",method = RequestMethod.POST)
	Result<PositionDTO> insert(@RequestBody Position Position,int _pos_id,String _pos_name,String _pos_icon,@RequestBody MultipartFile file) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		
		System.out.println(Position._pos_name);
		System.out.println(Position._pos_icon);
		System.out.println(LocalDate.now().toString());
		System.out.println(_pos_id);
		System.out.println(_pos_name);
		System.out.println(_pos_icon);
		/*var params = new LinkedList<String>();
		params.add(Position._pos_name);
		params.add(Position._pos_icon);
		params.add(LocalDate.now().toString());
		
		var rs = DBConnection.CallProc("Sp_Position_Insert", params);
		var data = Mapper.MapTo(new PositionDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Create Position Fail!";
		var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);*/
		var result = new Result<PositionDTO>();
		return result;
	}
	@RequestMapping(value="api/position/gets",method = RequestMethod.GET)
	Result<LinkedList<PositionDTO>> gets(@Validated @RequestBody PositionDTO Position) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		Debug("123");
		var params = new LinkedList<String>();
		params.add(Position._pos_name);
		var rs = DBConnection.CallProc("Sp_Position_Gets", params);
		var data = Mapper.MapTo(new PositionDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<LinkedList<PositionDTO>>(isSuccess ? data: null,isSuccess,mes);
		return result;
	}
	/*Result<PositionDTO> info(@Validated @RequestBody Position Position) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Position._pos_id+"");
		var rs = DBConnection.CallProc("Sp_Position_Info", params);
		var data = Mapper.MapTo(new PositionDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found!";
		var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}*/
	Result<PositionDTO> updateIcon(@Validated @RequestBody Position Position) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Position._pos_id+"");
		params.add(Position._pos_icon);
		var rs = DBConnection.CallProc("Sp_Position_UpdateIcon", params);
		var data = Mapper.MapTo(new PositionDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Position Icon Fail";
		var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<PositionDTO> update(@Validated @RequestBody Position Position) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Position._pos_id+"");
		params.add(Position._pos_name);
		params.add(Position._pos_updated_date+"");
		var rs = DBConnection.CallProc("Sp_Position_Update", params);
		var data = Mapper.MapTo(new PositionDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Position Fail!";
		var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	@RequestMapping(value="api/position/changestatus",method = RequestMethod.POST)
	Result<PositionDTO> changeStatus(@RequestBody Position position) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		
		var params = new LinkedList<String>();
		params.add(position._pos_id+"");
		params.add(position._pos_status+"");
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_Position_ChangeStatus", params);
		var data = Mapper.MapTo(new PositionDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "Position Change Status Successfull." : "Position Change Status Fail!";
		var positionThread = new PositionThread();
		positionThread.start();
		positionThread.join();
		var result = new Result<PositionDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	void Debug(String text) {
		System.out.println(text);
	}
}
