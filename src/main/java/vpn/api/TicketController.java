package vpn.api;

import java.util.LinkedList;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import vpn.model.Bill;
import vpn.model.BillDTO;
import vpn.model.BookingDTO;
import vpn.model.BookingDetail;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.Result;
import vpn.model.SeatDTO;
import vpn.model.TicketDTO;

@RestController
public class TicketController {
	@RequestMapping(value="api/ticket/updatestatus",method = RequestMethod.GET)
	Result<TicketDTO> updateStatus(TicketDTO ticket) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		System.out.println(ticket._tk_id);
		System.out.println(ticket._tk_status);
		var params = new LinkedList<String>();
		params.add(ticket._tk_id+"");
		params.add(ticket._tk_status+"");
		var rs = DBConnection.CallProc("Sp_Ticket_ChangeStatus", params);
		var data = Mapper.MapTo(new TicketDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "Update Ticket Status Fail!";
		var result = new Result<TicketDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	
	@RequestMapping(value="api/ticket/updatestatus-web",method = RequestMethod.POST)
	Result<TicketDTO> UpdateStatus(int _tk_id, int _tk_status) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		System.out.println(_tk_id);
		System.out.println(_tk_status);
		var params = new LinkedList<String>();
		params.add(_tk_id+"");
		params.add(_tk_status+"");
		var rs = DBConnection.CallProc("Sp_Ticket_ChangeStatus_Web", params);
		var data = Mapper.MapTo(new TicketDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "Update Ticket Status Fail!";
		var result = new Result<TicketDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	
	@RequestMapping(value = "/api/ticket/gets", method = RequestMethod.GET)
	Result<LinkedList<TicketDTO>> gets(@Validated int _bil_id)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(_bil_id + "");
		var rs = DBConnection.CallProc("Sp_Ticket_Gets", params);
		var data = Mapper.MapTo(new TicketDTO(), rs);
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "No Data Found!";
		var result = new Result<LinkedList<TicketDTO>>(data, isSuccess, mes);
		return result;
	}

	@RequestMapping(value = "api/ticket/check", method = RequestMethod.GET)
	Result<LinkedList<SeatDTO>> info(Integer _fl_id, String[] _tc_code,int _tc_id)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		
		var params = new LinkedList<String>();
		params.add(_fl_id + ""); 
		params.add(_tc_id + ""); 
		var rs = DBConnection.CallProc("Sp_Ticket_GetsByFlight", params);
		var data = Mapper.MapTo(new TicketDTO(), rs);
		var result = new Result<LinkedList<SeatDTO>>();
		result.fail = new LinkedList<SeatDTO>();
		if (!data.isEmpty()) {
			for (TicketDTO ticket : data) {
				for (String code : _tc_code) {
					System.out.println(_tc_code);
					if (ticket._tk_seat.equals(code) && ticket._tk_tc_id == _tc_id) {
						var seat = new SeatDTO(code, 0, "", false);
						result.fail.add(seat);
					}
				}
			}
		}
		result.isSuccess = result.fail.isEmpty(); 
		return result;
	}
	
	@RequestMapping(value = "api/ticket/check-web", method = RequestMethod.GET)
	Result<LinkedList<SeatDTO>> CheckSeat(Integer _fl_id, String _tc_code, int _tc_id)
	        throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
	    System.out.println(_fl_id);
	    System.out.println(_tc_id);

	    // Parse JSON array into a String[]
	    ObjectMapper objectMapper = new ObjectMapper();
	    String[] tcCodeArray;
	    try {
	        tcCodeArray = objectMapper.readValue(_tc_code, String[].class);
	    } catch (Exception e) {
	        // Handle parsing exception
	        e.printStackTrace();
	        return new Result<>(); // Return appropriate result in case of failure
	    }

	    var params = new LinkedList<String>();
	    params.add(_fl_id + "");
	    params.add(_tc_id + "");
	    var rs = DBConnection.CallProc("Sp_Ticket_GetsByFlight", params);
	    var data = Mapper.MapTo(new TicketDTO(), rs);
	    var result = new Result<LinkedList<SeatDTO>>();
	    result.fail = new LinkedList<SeatDTO>();
	    if (!data.isEmpty()) {
	        for (TicketDTO ticket : data) {
	            for (String code : tcCodeArray) {
	                System.out.println(code);
	                if (ticket._tk_seat.equals(code) && ticket._tk_tc_id == _tc_id) {
	                    var seat = new SeatDTO(code, 0, "", false);
	                    result.fail.add(seat);
	                }
	            }
	        }
	    }
	    result.isSuccess = result.fail.isEmpty();
	    return result;
	}
	@RequestMapping(value = "api/ticket/getbybill", method = RequestMethod.GET)
	Result<LinkedList<TicketDTO>> getbybill(int _bil_id)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(_bil_id + "");	
		var rs = DBConnection.CallProc("Sp_Ticket_GetByBill", params);
		var data = Mapper.MapTo(new TicketDTO(), rs);
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "No Data Found!";
		var result = new Result<LinkedList<TicketDTO>>(data, isSuccess, mes);
		return result;
	}
	
	
	@RequestMapping(value = "/api/ticket/getbybooking", method = RequestMethod.GET)
	Result<LinkedList<BookingDetail>> getbybooking(@Validated @RequestBody BookingDTO param)
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(param._bo_id + "");
		var rs = DBConnection.CallProc("Sp_BookingDetail_GetByBooking", params);
		var data = Mapper.MapTo(new BookingDetail(), rs);
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "No Data Found!";
		var result = new Result<LinkedList<BookingDetail>>(data, isSuccess, mes);
		return result;
	}
}
