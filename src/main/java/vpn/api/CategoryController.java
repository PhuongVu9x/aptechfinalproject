package vpn.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vpn.CategoryThread;
import vpn.model.Category;
import vpn.model.CategoryDTO;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.Permision;
import vpn.model.PermisionDTO;
import vpn.model.Result;
@RestController
public class CategoryController {
	Result<CategoryDTO> insert(@Validated @RequestBody Category Category) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Category._cate_name);
		params.add(Category._cate_icon);
		params.add(Category._cate_created_date+"");
		
		var rs = DBConnection.CallProc("Sp_Category_Insert", params);
		var data = Mapper.MapTo(new CategoryDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Create Category Fail!";
		var result = new Result<CategoryDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<CategoryDTO> gets(@Validated @RequestBody CategoryDTO Category) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Category._cate_name);
		var rs = DBConnection.CallProc("Sp_Category_Gets", params);
		var data = Mapper.MapTo(new CategoryDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<CategoryDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	/*Result<CategoryDTO> info(@Validated @RequestBody Category Category) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Category._cate_id+"");
		var rs = DBConnection.CallProc("Sp_Category_Info", params);
		var data = Mapper.MapTo(new CategoryDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found!";
		var result = new Result<CategoryDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}*/
	Result<CategoryDTO> updateIcon(@Validated @RequestBody Category Category) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Category._cate_id+"");
		params.add(Category._cate_icon);
		var rs = DBConnection.CallProc("Sp_Category_UpdateIcon", params);
		var data = Mapper.MapTo(new CategoryDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Category Icon Fail";
		var result = new Result<CategoryDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<CategoryDTO> update(@Validated @RequestBody Category Category) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Category._cate_name);
		params.add(Category._cate_updated_date+"");
		var rs = DBConnection.CallProc("Sp_Category_Update", params);
		var data = Mapper.MapTo(new CategoryDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Category Fail!";
		var result = new Result<CategoryDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	@RequestMapping(value="api/category/changestatus",method = RequestMethod.POST)
	Result<CategoryDTO> changeStatus(@Validated @RequestBody Category Category) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		var params = new LinkedList<String>();
		params.add(Category._cate_id+"");
		params.add(Category._cate_status+"");
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_Category_ChangeStatus", params);
		var data = Mapper.MapTo(new CategoryDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Category Change Status Fail!";
		var categoryThread = new CategoryThread();
		categoryThread.start();
		categoryThread.join();
		var result = new Result<CategoryDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
}
