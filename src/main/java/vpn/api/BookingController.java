package vpn.api;

import java.util.LinkedList;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vpn.BookingThread;
import vpn.CustomerThread;
import vpn.model.BookingDTO;
import vpn.model.BookingDetail;
import vpn.model.CustomerDTO;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.Result;

@RestController
public class BookingController {
	@RequestMapping(value="/api/booking/detail",method = RequestMethod.GET)
	Result<LinkedList<BookingDetail>> gets(@Validated int _bo_id) 
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(_bo_id+"");
		var rs = DBConnection.CallProc("Sp_BookingDetail_Gets", params);
		var data = Mapper.MapTo(new BookingDetail(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "No Data Found!";
		var result = new Result<LinkedList<BookingDetail>>(data,isSuccess,mes);
		return result;
	}
	
	@RequestMapping(value="/api/booking/getCustomerByBooking",method = RequestMethod.GET)
	Result<CustomerDTO> getCustomerByBooking(@Validated int _bo_id) 
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var customerId = 0;
		for(var item : BookingThread.bookings) {
			if(item._bo_id == _bo_id) {
				customerId = item._bo_cus_id;
			}
		}
		CustomerDTO customer = null;
		for(var item : CustomerThread.customers) {
			if(item._cus_id == customerId) {
				customer = item;
			}
		}
		var isSuccess = customer != null;
		var mes = isSuccess ? "" : "No Data Found!";
		var result = new Result<CustomerDTO>(customer,isSuccess,mes);
		return result;
	}
	
	@RequestMapping(value="api/booking/getbycustomer",method = RequestMethod.GET)
	Result<LinkedList<BookingDTO>> getbycustomer(int _cus_id) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(_cus_id+"");
		var rs = DBConnection.CallProc("Sp_Booking_GetByCustomer", params);
		var data = Mapper.MapTo(new BookingDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<LinkedList<BookingDTO>>(data,isSuccess,mes);
		return result;
	}
	
	@RequestMapping(value="api/booking/getbyhotel",method = RequestMethod.GET)
	Result<LinkedList<BookingDetail>> getbyhotel(int _ho_id) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(_ho_id+"");
		var rs = DBConnection.CallProc("Sp_Booking_GetByHotel", params);
		var data = Mapper.MapTo(new BookingDetail(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<LinkedList<BookingDetail>>(data,isSuccess,mes);
		return result;
	}
}
