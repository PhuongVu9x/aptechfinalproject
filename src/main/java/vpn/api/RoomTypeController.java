package vpn.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vpn.model.RoomType;
import vpn.model.RoomTypeDTO;
import vpn.AirportThread;
import vpn.RoomTypeThread;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.Result;
@RestController
public class RoomTypeController {
	Result<RoomTypeDTO> insert(@Validated @RequestBody RoomType RoomType) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(RoomType._rt_name);
		params.add(RoomType._rt_icon);
		params.add(RoomType._rt_created_date+"");
		
		var rs = DBConnection.CallProc("Sp_RoomType_Insert", params);
		var data = Mapper.MapTo(new RoomTypeDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Create RoomType Fail!";
		var result = new Result<RoomTypeDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<RoomTypeDTO> gets(@Validated @RequestBody RoomTypeDTO RoomType) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(RoomType._rt_name);
		var rs = DBConnection.CallProc("Sp_RoomType_Gets", params);
		var data = Mapper.MapTo(new RoomTypeDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<RoomTypeDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	/*Result<RoomTypeDTO> info(@Validated @RequestBody RoomType RoomType) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(RoomType._rt_id+"");
		var rs = DBConnection.CallProc("Sp_RoomType_Info", params);
		var data = Mapper.MapTo(new RoomTypeDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found!";
		var result = new Result<RoomTypeDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}*/
	
	Result<RoomTypeDTO> update(@Validated @RequestBody RoomType RoomType) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(RoomType._rt_id+"");
		params.add(RoomType._rt_name);
		params.add(RoomType._rt_updated_date+"");
		var rs = DBConnection.CallProc("Sp_RoomType_Update", params);
		var data = Mapper.MapTo(new RoomTypeDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update RoomType Fail!";
		var result = new Result<RoomTypeDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	@RequestMapping(value = "/api/roomtype/changeStatus", method = RequestMethod.POST)
	Result<RoomTypeDTO> changeStatus(@Validated @RequestBody RoomType RoomType) 
			throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		var params = new LinkedList<String>();
		params.add(RoomType._rt_id+"");
		params.add(RoomType._rt_status+"");
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_RoomType_ChangeStatus", params);
		var data = Mapper.MapTo(new RoomTypeDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "RoomType Change Status Fail!";
		var thread = new RoomTypeThread();
		thread.start();
		thread.join();
		var result = new Result<RoomTypeDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
}
