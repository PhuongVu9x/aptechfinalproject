package vpn.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vpn.CityThread;
import vpn.model.City;
import vpn.model.CityDTO;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.Result;
@RestController
public class CityController {
	Result<CityDTO> insert(@Validated @RequestBody City City) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(City._city_name);
		params.add(City._city_icon);
		params.add(City._city_created_date+"");
		
		var rs = DBConnection.CallProc("Sp_City_Insert", params);
		var data = Mapper.MapTo(new CityDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Create City Fail!";
		var result = new Result<CityDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<CityDTO> gets(@Validated @RequestBody CityDTO City) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(City._city_name);
		var rs = DBConnection.CallProc("Sp_City_Gets", params);
		var data = Mapper.MapTo(new CityDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<CityDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	  
	Result<CityDTO> updateIcon(@Validated @RequestBody City City) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(City._city_id+"");
		params.add(City._city_icon);
		var rs = DBConnection.CallProc("Sp_City_UpdateIcon", params);
		var data = Mapper.MapTo(new CityDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update City Icon Fail";
		var result = new Result<CityDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<CityDTO> update(@Validated @RequestBody City City) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(City._city_id+"");
		params.add(City._city_name);
		params.add(City._city_updated_date+"");
		var rs = DBConnection.CallProc("Sp_City_Update", params);
		var data = Mapper.MapTo(new CityDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update City Fail!";
		var result = new Result<CityDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	@RequestMapping(value="api/city/changestatus",method = RequestMethod.POST)
	Result<CityDTO> changeStatus(@Validated @RequestBody City City) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		var params = new LinkedList<String>();
		params.add(City._city_id+"");
		params.add(City._city_status+"");
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_City_ChangeStatus", params);
		var data = Mapper.MapTo(new CityDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "City Change Status Fail!";
		var cityThread = new CityThread();
		cityThread.start();
		cityThread.join();
		var result = new Result<CityDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
}
