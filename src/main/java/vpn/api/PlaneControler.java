package vpn.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vpn.PlaneThread;
import vpn.model.DBConnection;
import vpn.model.Plane;
import vpn.model.PlaneDTO;
import vpn.model.Mapper;
import vpn.model.Result;
@RestController
public class PlaneControler {
	@RequestMapping(value="api/plane/insert",method = RequestMethod.POST)
	Result<PlaneDTO> insert(@Validated @RequestBody Plane plane) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		var params = new LinkedList<String>();
		params.add(plane._pl_code);
		params.add(plane._pl_carrier_id + "");
		params.add(plane._pl_pt_id + "");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Plane_Insert", params);
		var data = Mapper.MapTo(new PlaneDTO(), rs);
		var isSuccess = !data.isEmpty();
		var planeThread = new PlaneThread();
		planeThread.start();
		planeThread.join();
		var mes = isSuccess ? "Create Plane Successfull." : "Create Plane Fail!";
		var result = new Result<PlaneDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	@RequestMapping(value="api/plane/gets",method = RequestMethod.GET)
	Result<LinkedList<PlaneDTO>> gets() throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var isSuccess = !PlaneThread.planes.isEmpty();
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<LinkedList<PlaneDTO>>(PlaneThread.planes,isSuccess,mes);
		return result;
	}
	Result<PlaneDTO> info(@Validated @RequestBody Plane Plane) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Plane._pl_id+"");
		var rs = DBConnection.CallProc("Sp_Plane_Info", params);
		var data = Mapper.MapTo(new PlaneDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "" : "No data found!";
		var result = new Result<PlaneDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	@RequestMapping(value="api/plane/update",method = RequestMethod.POST)
	Result<PlaneDTO> update(@Validated @RequestBody Plane plane) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		var params = new LinkedList<String>();
		params.add(plane._pl_id + "");
		params.add(plane._pl_code);
		params.add(plane._pl_carrier_id + "");
		params.add(plane._pl_pt_id + "");
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		params.add(dtf.format(now));

		var rs = DBConnection.CallProc("Sp_Plane_Update", params);
		var data = Mapper.MapTo(new PlaneDTO(), rs);
		var isSuccess = !data.isEmpty();
		var planeThread = new PlaneThread();
		planeThread.start();
		planeThread.join();
		var mes = isSuccess ? "Update Plane Successfull!" : "Update Plane Fail!";
		 var result = new Result<PlaneDTO>(isSuccess ? data.getFirst() : null,
				 isSuccess, mes);
		return result;
	}
	@RequestMapping(value="api/plane/changestatus",method = RequestMethod.POST)
	Result<PlaneDTO> changeStatus(@Validated @RequestBody Plane Plane) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException, InterruptedException {
		var params = new LinkedList<String>();
		params.add(Plane._pl_id+"");
		params.add(Plane._pl_status+"");
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_Plane_ChangeStatus", params);
		var data = Mapper.MapTo(new PlaneDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "Plane Change Status Success!" : "Plane Change Status Fail!";
		var planeThread = new PlaneThread();
		planeThread.start();
		planeThread.join();
		var result = new Result<PlaneDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
}
