package vpn.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vpn.model.Permision;
import vpn.model.PermisionDTO;
import vpn.model.Position;
import vpn.model.PositionDTO;
import vpn.PermisionThread;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.Result;
@RestController
public class PermisionController {
	Result<PermisionDTO> insert(@Validated @RequestBody Permision Permision) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Permision._per_name);
		params.add(Permision._per_icon);
		params.add(Permision._per_created_date+"");
		
		var rs = DBConnection.CallProc("Sp_Permision_Insert", params);
		var data = Mapper.MapTo(new PermisionDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Create Permision Fail!";
		var result = new Result<PermisionDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<PermisionDTO> gets(@Validated @RequestBody PermisionDTO Permision) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Permision._per_name);
		var rs = DBConnection.CallProc("Sp_Permision_Gets", params);
		var data = Mapper.MapTo(new PermisionDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found(s)!";
		var result = new Result<PermisionDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	/*Result<PermisionDTO> info(@Validated @RequestBody Permision Permision) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Permision._per_id+"");
		var rs = DBConnection.CallProc("Sp_Permision_Info", params);
		var data = Mapper.MapTo(new PermisionDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "No data found!";
		var result = new Result<PermisionDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}*/
	Result<PermisionDTO> updateIcon(@Validated @RequestBody Permision Permision) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Permision._per_id+"");
		params.add(Permision._per_icon);
		var rs = DBConnection.CallProc("Sp_Permision_UpdateIcon", params);
		var data = Mapper.MapTo(new PermisionDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Permision Icon Fail";
		var result = new Result<PermisionDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	Result<PermisionDTO> update(@Validated @RequestBody Permision Permision) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		var params = new LinkedList<String>();
		params.add(Permision._per_id+"");
		params.add(Permision._per_name);
		params.add(Permision._per_updated_date+"");
		var rs = DBConnection.CallProc("Sp_Permision_Update", params);
		var data = Mapper.MapTo(new PermisionDTO(), rs); 
		var isSuccess = !data.isEmpty() ? true : false;
		var mes = isSuccess ? "" : "Update Permision Fail!";
		var result = new Result<PermisionDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	@RequestMapping(value="api/permision/changestatus",method = RequestMethod.POST)
	Result<PermisionDTO> changeStatus(@RequestBody Permision permision) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		
		var params = new LinkedList<String>();
		params.add(permision._per_id+"");
		params.add(permision._per_status+"");
		LocalDateTime now = LocalDateTime.now();  
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		params.add(dtf.format(now));
		var rs = DBConnection.CallProc("Sp_Permision_ChangeStatus", params);
		var data = Mapper.MapTo(new PermisionDTO(), rs); 
		var isSuccess = !data.isEmpty();
		var mes = isSuccess ? "Permision Change Status Successfull." : "Permision Change Status Fail!";
		var permisionThread = new PermisionThread();
		permisionThread.start();
		var result = new Result<PermisionDTO>(isSuccess ? data.getFirst() : null,isSuccess,mes);
		return result;
	}
	void Debug(String text) {
		System.out.println(text);
	}
}
