package vpn;

import java.sql.ResultSet;
import java.util.LinkedList;
import vpn.model.CategoryDTO;
import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.PositionDTO;

public class CategoryThread extends Thread {
	public static LinkedList<CategoryDTO> categories;
	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		try {
			var rsCategory = DBConnection.CallProc("Sp_Category_Gets");
			categories = Mapper.MapTo(new CategoryDTO(), rsCategory);
			System.out.println(AppThread.CountDown + ": Category All Done.");
			AppThread.CountDown--;
			if(AppThread.CountDown < 0)
				AppThread.CountDown = 0;
			this.interrupt();
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
