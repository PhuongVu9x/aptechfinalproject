package vpn;

import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;


import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import vpn.controller.AdminController;

@Component
public class RequestProcessingTimeInterceptor implements HandlerInterceptor {

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		if (handler instanceof HandlerMethod && ((HandlerMethod) handler).getBean() instanceof AdminController) {
			//
			for (Cookie cookie : request.getCookies()) {
				if (cookie.getName().equals("userCookie")) {
					if (cookie.getValue().equals("Admin")) {
						return true;
					} 
				} 
			}
			
			response.sendRedirect("/admin/login");
		}
		return true;
	}
}