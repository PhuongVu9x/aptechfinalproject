package vpn;

import java.sql.ResultSet;
import java.util.LinkedList;

import vpn.model.DBConnection;
import vpn.model.Mapper;
import vpn.model.ReturnTypeDTO;

public class ReturnTypeThread extends Thread{
	public static LinkedList<ReturnTypeDTO> returnTypes;

	
	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		try {
			var rsReturnType = DBConnection.CallProc("Sp_ReturnType_Gets");
			returnTypes = Mapper.MapTo(new ReturnTypeDTO(), rsReturnType);
			System.out.println(AppThread.CountDown + ": Return Type All Done.");
			AppThread.CountDown--;
			if(AppThread.CountDown < 0)
				AppThread.CountDown = 0;
			this.interrupt();
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
