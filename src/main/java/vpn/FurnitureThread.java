package vpn;
import java.util.LinkedList;

import vpn.model.AirportDTO;
import vpn.model.DBConnection;
import vpn.model.FunitureDTO;
import vpn.model.Mapper;
public class FurnitureThread extends Thread{
	public static LinkedList<FunitureDTO> furnitures;
	
	public void run() {
		SetupDefault();
	}

	public void SetupDefault() {
		try {
			var rs = DBConnection.CallProc("Sp_Furniture_Gets");
			furnitures = Mapper.MapTo(new FunitureDTO(), rs);
			System.out.println(AppThread.CountDown + ": Furniture All Done.");
			AppThread.CountDown--;
			if (AppThread.CountDown < 0)
				AppThread.CountDown = 0;
			this.interrupt();
		} catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
